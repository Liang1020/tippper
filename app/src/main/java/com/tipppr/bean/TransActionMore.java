package com.tipppr.bean;

import com.common.util.StringUtils;
import com.tipppr.android.App;
import com.tipppr.tipppr.bean.User;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by wangzy on 16/3/28.
 */
public class TransActionMore {


    private String amount;
    private String amount2me;


    private String status;
    private String mtTime;
    private String createAt;


    private User  sendder;
    private User receiverUser1;
    private User receiverUser2;
    private SimpleDateFormat sdf;


    public TransActionMore() {

        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    }

    public User getSendder() {
        return sendder;
    }

    public void setSendder(User sendder) {
        this.sendder = sendder;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public String getStatusText() {

//        0.success
//        1.Refund
//        2.Refunded

        if ("0".equals(status)) {
            return "Success";
        }
        if ("1".equals(status)) {
            return "Refund";
        }
        if ("2".equals(status)) {
            return "Refunded";
        }

        return "";
    }


    public void setStatus(String status) {
        this.status = status;
    }

    public String getMtTime() {
        return mtTime;
    }

    public String getTimeLabel() {
        //2016-03-31T20:07:54.847-07:00

        if (!StringUtils.isEmpty(mtTime)) {

            String tempStr = mtTime.replace("T", " ");
            //2016-03-31 20:07:54.847-07:00

            return tempStr.substring(0, 16);

        }

        return "";
    }

    private SimpleDateFormat sdfLocal = null;

    public String getLocaledTime() {

        long timeMills = Long.parseLong(mtTime) * 1000;

        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(timeMills);

        Date d = calendar.getTime();
        if (null == sdfLocal) {
            sdfLocal = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }

        String timeLabel = sdfLocal.format(d);

        String resultTex = timeLabel.substring(0, 16);
//
        return resultTex;

//        try {
//            if (null == sdfLocal) {
//                sdfLocal = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            }
//
//            if (!StringUtils.isEmpty(mtTime)) {
//                String tempStr = mtTime.replace("T", " ");
//                //2016-03-31 20:07:54.847-07:00
//                String sfttime = tempStr.substring(0, 19);
//
//                Date sdfDte = sdfLocal.parse(sfttime);
//
//                // yyyy-MM-dd HH:mm:ss
//                String localDate = DateTool.timeTrans2LocalTimeFromMST(sfttime);
//
//                String resultTex=localDate.substring(0,16);
//
//                return resultTex;
//
//            }
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//

//        return "";
    }


    public void setMtTime(String mtTime) {
        this.mtTime = mtTime;
    }

    public User getReceiverUser1() {
        return receiverUser1;
    }

    public void setReceiverUser1(User receiverUser1) {
        this.receiverUser1 = receiverUser1;
    }

    public User getReceiverUser2() {
        return receiverUser2;
    }

    public void setReceiverUser2(User receiverUser2) {
        this.receiverUser2 = receiverUser2;
    }

    public boolean isSent(User currentAppUser) {

        if (sendder.getUserId().equals(currentAppUser.getUserId())) {
            return true;
        } else {
            return false;
        }
    }

    public String getHeaderImageUrl() {

        String imageHeader = "";
        if (null != receiverUser1) {
            imageHeader = receiverUser1.getHeaderImage();
        }
        if (null != receiverUser2 && receiverUser2.isBuiness()) {
            imageHeader = receiverUser2.getHeaderImage();
        }
        return imageHeader;
    }

    public String getSenderHeaderImageUrl() {

        String imageHeader = "";

        imageHeader = sendder.getHeaderImage();

        return imageHeader;
    }


    public String getAmount2me() {
        return amount2me;
    }

    public void setAmount2me(String amount2me) {
        this.amount2me = amount2me;
    }


    public String getSenderName() {

        String senderName = sendder.isBuiness() ? sendder.getBussness() : sendder.getName().replace(App.getApp().getSplit(), " ");
        return senderName;
    }


    public String getNameLabel() {

        String names = "";

        if (null != receiverUser1) {
            if (receiverUser1.isBuiness()) {
                names = receiverUser1.getBussness().replace(App.getApp().getSplit(), " ");
            } else {
                names = receiverUser1.getName().replace(App.getApp().getSplit(), " ");
            }
        }
        if (null != receiverUser2) {
            if (receiverUser2.isBuiness()) {
                names = receiverUser2.getBussness() + "; " + names;
            } else {
                names = names + "; " + receiverUser2.getName();
            }
        }

        return names;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getBirllTransDate() {


        return createAt;
    }
}
