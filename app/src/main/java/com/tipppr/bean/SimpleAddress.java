package com.tipppr.bean;

import android.location.Address;

import java.util.Locale;

/**
 * Created by wangzy on 16/3/29.
 */
public class SimpleAddress extends Address {


    private String simplePlaceId;
    private String simpleDesccrption;
    private String simpleFormattedAddress;
    private String simpleLat, simpleLng;


    public SimpleAddress() {
        super(Locale.getDefault());

    }

    /**
     * Constructs a new Address object set to the given Locale and with all
     * other fields initialized to null or false.
     *
     * @param locale
     */
    public SimpleAddress(Locale locale) {
        super(locale);
    }

    public String getSimplePlaceId() {
        return simplePlaceId;
    }

    public void setSimplePlaceId(String simplePlaceId) {
        this.simplePlaceId = simplePlaceId;
    }

    public String getSimpleDesccrption() {
        return simpleDesccrption;
    }

    public void setSimpleDesccrption(String simpleDesccrption) {
        this.simpleDesccrption = simpleDesccrption;
    }

    public String getSimpleFormattedAddress() {
        return simpleFormattedAddress;
    }

    public void setSimpleFormattedAddress(String simpleFormattedAddress) {
        this.simpleFormattedAddress = simpleFormattedAddress;
    }

    public String getSimpleLat() {
        return simpleLat;
    }

    public void setSimpleLat(String simpleLat) {
        this.simpleLat = simpleLat;
    }

    public String getSimpleLng() {
        return simpleLng;
    }

    public void setSimpleLng(String simpleLng) {
        this.simpleLng = simpleLng;
    }
}
