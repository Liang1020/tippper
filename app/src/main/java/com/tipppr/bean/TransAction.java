package com.tipppr.bean;

import com.tipppr.tipppr.bean.User;

import java.util.ArrayList;

/**
 * Created by wangzy on 16/3/28.
 */
public class TransAction {

    private String amount;
    private String email;
    private String sendUserId;
    private String created_at;

    private ArrayList<User> receivers;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSendUserId() {
        return sendUserId;
    }

    public void setSendUserId(String sendUserId) {
        this.sendUserId = sendUserId;
    }

    public TransAction() {
        receivers = new ArrayList<>();
    }


    public void addReceivers(User user) {
        receivers.add(user);
    }

    public ArrayList<User> getReceivers() {
        return receivers;
    }

    public void setReceivers(ArrayList<User> receivers) {
        this.receivers = receivers;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
