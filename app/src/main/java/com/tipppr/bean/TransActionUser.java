package com.tipppr.bean;

import com.tipppr.tipppr.bean.User;

/**
 * Created by wangzy on 16/4/1.
 */
public class TransActionUser extends User{

    private String historyId;
    private String transAmount;


    public String getHistoryId() {
        return historyId;
    }

    public void setHistoryId(String historyId) {
        this.historyId = historyId;
    }

    public String getTransAmount() {
        return transAmount;
    }

    public void setTransAmount(String transAmount) {
        this.transAmount = transAmount;
    }
}
