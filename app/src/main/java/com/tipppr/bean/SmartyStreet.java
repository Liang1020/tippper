package com.tipppr.bean;

import com.common.util.StringUtils;

/**
 * Created by wangzy on 16/4/3.
 */
public class SmartyStreet {


    private String address;
    private String latitude;
    private String longtitude;

    private boolean isOk;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public boolean isValidate() {
        return !StringUtils.isEmpty(latitude) && !StringUtils.isEmpty(longtitude);
    }

    public boolean isOk() {

        return isOk;
    }

    public void setOk(boolean ok) {
        isOk = ok;
    }
}
