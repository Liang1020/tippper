package com.tipppr.tipppr;

import android.Manifest;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;

import com.baidu.location.BDLocation;
import com.callback.OnRequestEndCallBack;
import com.callback.OnUploadCallBack;
import com.common.BaseActivity;
import com.common.callback.NetCallBackMini;
import com.common.net.FormFile;
import com.common.net.NetException;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.BitmapTool;
import com.common.util.DateTool;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.push.SendTagsFragment;
import com.pushwoosh.PushManager;
import com.pushwoosh.SendPushTagsCallBack;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.tipppr.android.App;
import com.tipppr.bean.SmartyStreet;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.json.JSONHelper;
import com.tipppr.tipppr.net.NetInterface;
import com.tipppr.tipppr.photo.lib.SharedpreferencesUtil;
import com.xlist.pull.refresh.XListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by wangzy on 15/10/29.
 */
public class BaseTippprActivity extends BaseActivity {


    private View pageViewLocationSelection;
    private EditText editTextLocationInput;
    private ListView listViewLocation;
    //    protected Animation animationShow, animationHide;
    protected View photoView;
    public HashMap<Integer, AsyncTask> taskMap;

    public int pageSize = 20;
    public int totallCount = 0;
    public int currentPage = 0;

    public final short SHORT_REQUEST_ADDCATCH = 1000;
    public final short SHORT_REQUEST_SELECTIMAGE = 1001;
    public final short SHORT_ACCESS_LOCATION = 1003;
    public final short SHORT_REQUEST_READ_EXTEAL = 1004;
    public final short SHORT_REQUEST_READ_EXTEAL_CATCH = 1005;
    public final short SHORT_REQUEST_MEDIAS = 1006;
    public final short SHORT_REQUEST_PHONE_STATE = 1007;
    public final short SHORT_REQUEST_CANCEL_VERIFY = 1008;
    public final short SHORT_REQUESET_VERIFY = 1009;
    public final short SHORT_REQUEST_FILL_CARD = 1010;

    public SharedpreferencesUtil sharedpreferencesUtil;

    public final String KEY_FBHEADER = "fbheader";
    public int maxMoney = 200;
    public final String keyemailPwd = "asdafdsasd";


    private static final String SEND_TAGS_STATUS_FRAGMENT_TAG = "send_tags_status_fragment_tag";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        taskMap = new HashMap<Integer, AsyncTask>();

//        animationHide = AnimationUtils.loadAnimation(this, R.anim.slide_bottom_out);
//        animationShow = AnimationUtils.loadAnimation(this, R.anim.slide_bottom_in);
        sharedpreferencesUtil = new SharedpreferencesUtil(this);

    }


    public void setUpPushWoosh(User user) {
        Map<String, Object> tags = new HashMap();

        if (null != user) {
            tags.put("userid", String.valueOf(user.getUserId()));

            LogUtil.i(App.tag, "tag user id:" + String.valueOf(user.getUserId()));
        } else {
            tags.put("userid", String.valueOf(-1));
        }


        PushManager.getInstance(BaseTippprActivity.this).sendTags(BaseTippprActivity.this, tags, new SendPushTagsCallBack() {
            @Override
            public void taskStarted() {
                LogUtil.e(App.tag, "send tag start");
            }

            @Override
            public void onSentTagsSuccess(Map<String, String> map) {
                LogUtil.e(App.tag, "send tag success");
            }

            @Override
            public void onSentTagsError(Exception e) {
                LogUtil.e(App.tag, "send tag error");

            }
        });


    }


    public void checkAndSendTagsIfWeCan(String tagtag, String stringTag) {
        SendTagsFragment sendTagsFragment = getSendTagsFragment();

        if (sendTagsFragment.canSendTags()) {
            sendTagsFragment.submitTags(this, tagtag, stringTag);
        }
    }


    private SendTagsFragment getSendTagsFragment() {
        FragmentManager fragmentManager = getFragmentManager();

        SendTagsFragment sendTagsFragment = (SendTagsFragment) fragmentManager.findFragmentByTag(SEND_TAGS_STATUS_FRAGMENT_TAG);


        if (null == sendTagsFragment) {
            sendTagsFragment = new SendTagsFragment();
            sendTagsFragment.setRetainInstance(true);

            fragmentManager.beginTransaction().add(sendTagsFragment, SEND_TAGS_STATUS_FRAGMENT_TAG).commit();

            fragmentManager.executePendingTransactions();
        }

        return sendTagsFragment;
    }


//    final String auth_id = "06f35c33-ca42-08ac-8f6a-922ec6f2ba0c";
//    final String auto_token = "ZOqsOBQfb71awom0iwWO";

    final String auth_id = "f0446a88-98b8-b14d-d336-e6bb6a6be992";
    final String auto_token = "GwZW0KDmMHipmbXdYpag";


    public SmartyStreet getAddressListFromSmartyAPI(String input, String country, boolean isUsa) {

        SmartyStreet smartyStreet = new SmartyStreet();
        smartyStreet.setAddress(input);

        try {
            String urlText = isUsa ? getSmartAmericanUrl(input) : getSmartStreetInternalUrl(country, input);
            URL url = new URL(urlText);

            String jsonStr = JSONHelper.getJsonFromInputStream(url.openStream());

            LogUtil.i(App.tag, "smart url:" + urlText);
            LogUtil.i(App.tag, jsonStr);
            JSONArray jsonArray = new JSONArray(jsonStr);

            if (null != jsonArray && jsonArray.length() > 0) {
                smartyStreet.setOk(true);

                JSONObject metadata = jsonArray.getJSONObject(0).getJSONObject("metadata");

                String lat = JSONHelper.getStringFroJso("latitude", metadata);
                String lng = JSONHelper.getStringFroJso("longitude", metadata);

                smartyStreet.setLatitude(lat);
                smartyStreet.setLongtitude(lng);
            }


        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.e(App.tag, "get address list error:" + e.getLocalizedMessage());
        }
        return smartyStreet;
    }

    private String getSmartStreetInternalUrl(String country, String freefrom) throws UnsupportedEncodingException {
        String url = "https://international-street.api.smartystreets.com/verify?auth-id=" + auth_id +
                "&auth-token=" + auto_token + "&country=" + country + "&freeform=" + URLEncoder.encode(freefrom, "utf-8") + "&geocode=true";

        return url;
    }

    private String getSmartAmericanUrl(String street) throws UnsupportedEncodingException {

        String url = "https://api.smartystreets.com/street-address?auth-id=" + auth_id + "&auth-token=" + auto_token
                + "&street=" + URLEncoder.encode(street, "utf-8") + "&city=&state=&candidates=1";
        return url;
    }


    public void stopXlist(XListView xListViewMyTravel) {
        xListViewMyTravel.stopRefresh();
        xListViewMyTravel.stopLoadMore();
    }

    public void dealFooter(List arrayList, int totalNumber, XListView listViewNannies) {
        try {

            if (arrayList.size() >= totalNumber || ListUtiles.getListSize(arrayList) == 0) {
                listViewNannies.getmFooterView().hide();
            } else {
                listViewNannies.getmFooterView().show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        LogUtil.i(App.tag, "data size:" + arrayList.size());
    }

    public void clearTaskMap() {

        Set<Map.Entry<Integer, AsyncTask>> set = taskMap.entrySet();

        for (Map.Entry<Integer, AsyncTask> taskEntry : set) {
            AsyncTask task = taskEntry.getValue();
            if (null != task && task.getStatus() == AsyncTask.Status.RUNNING) {
                task.cancel(true);
                LogUtil.i(App.tag, "cancell task:" + task.hashCode());
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    BaseTask getPreAprrovalTask = null;

    public void getPreApproval(final NetCallBackMini netCallBack) {
        if (null != getPreAprrovalTask && getPreAprrovalTask.getStatus() == AsyncTask.Status.RUNNING) {
            getPreAprrovalTask.cancel(true);
        }

        getPreAprrovalTask = new BaseTask(this, new NetCallBack() {


            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {

                    JSONObject jso = new JSONObject();
                    netResult = NetInterface.getPreApproval(BaseTippprActivity.this, jso);
                } catch (Exception e) {

                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != netCallBack) {
                    netCallBack.onFinish(result);
                }
            }
        });


        getPreAprrovalTask.execute(new HashMap<String, String>());

    }


    @Override
    public void startActivity(Intent intent) {

        try {
            super.startActivity(intent);
        } catch (Exception e) {
            onStartActivityException(e);
        }

    }

    /**
     * when call startActivity exception
     *
     * @param e
     */
    public void onStartActivityException(Exception e) {

        LogUtil.e(App.tag, "start activity exception!!!");

    }

    public void onLocationReceive(BDLocation bdLocation, Location googleLocation) {


    }


    @Override
    protected void onDestroy() {
        clearTaskMap();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



    }


    //=====new photo lib ====
    public short SELECT_PIC_KITKAT = 801;
    public short SELECT_PIC = 802;
    public short TAKE_BIG_PICTURE = 803;
    public short CROP_BIG_PICTURE = 804;
    public int outputX = 150;
    public int outputY = 150;

//    public SharedpreferencesUtil sharedpreferencesUtil;


    public byte TYPE_REQEST_PICTURE_TAKE = 0;
    public byte TYPE_REQEST_PICTURE_SELECT = 1;

    public byte type_request_picture = TYPE_REQEST_PICTURE_TAKE;

    //
    public void startTakePicture() {
        type_request_picture = TYPE_REQEST_PICTURE_TAKE;
        sharedpreferencesUtil.delectImageTemp();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, sharedpreferencesUtil.getImageTempNameUri());
        startActivityForResult(intent, TAKE_BIG_PICTURE);
    }

    public void startSelectPicture() {
        type_request_picture = TYPE_REQEST_PICTURE_SELECT;
        sharedpreferencesUtil.delectImageTemp();
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/jpeg");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            startActivityForResult(intent, SELECT_PIC_KITKAT);
        } else {
            startActivityForResult(intent, SELECT_PIC);
        }
    }


    public void onTakePhtoClick(int maskWidth, int maskHeight) {

        type_request_picture = TYPE_REQEST_PICTURE_TAKE;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

//            ContextCompat.checkSelfPermission(this,Manifest.permission_group.CAMERA)

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                    ) {
                takePictureFromCamera(maskWidth, maskHeight);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, SHORT_REQUEST_READ_EXTEAL);
            }
        } else {
            takePictureFromCamera(maskWidth, maskHeight);
        }
    }

    public void onChosePhtoClick(int maskWidth, int maskHeight) {
        type_request_picture = TYPE_REQEST_PICTURE_SELECT;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                selectPictureFromAblum(maskWidth, maskHeight);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission_group.CAMERA}, SHORT_REQUEST_READ_EXTEAL);
            }
        } else {
            selectPictureFromAblum(maskWidth, maskHeight);
        }
    }

    public void startTakePicture(int w, int h) {
        this.outputX = w;
        this.outputY = h;
        startTakePicture();
    }

    public void startSelectPicture(int w, int h) {
        this.outputX = w;
        this.outputY = h;
        startSelectPicture();
    }


    public void cropImageUri(Uri uriIn, Uri uriOut, int outputX, int outputY, int requestCode) {


//        Intent intent = new Intent("com.android.camera.action.CROP");
//        intent.setDataAndType(uriIn, "image/*");
//        intent.putExtra("crop", "true");
//        intent.putExtra("aspectX", 1);
//        intent.putExtra("aspectY", 1);
//        intent.putExtra("outputX", outputX);
//        intent.putExtra("outputY", outputY);
//        intent.putExtra("scale", true);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriOut);
//        intent.putExtra("return-data", false);
//        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
//        intent.putExtra("noFaceDetection", true); // no face detection
//        startActivityForResult(intent, requestCode);

        startCropImageActivity(uriIn, 256, 256);

    }


    private void startCropImageActivity(Uri imageUri, int width, int height) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this, width, height);
    }


    public void takePictureFromCamera(int w, int h) {
        startTakePicture(w, h);
    }

    public void selectPictureFromAblum(int w, int h) {
        startSelectPicture(w, h);
    }

    public String getNumberTextFromParseNumber(Number number) {

        if (null == number) {
            return "";
        }

        String text = String.valueOf(number.floatValue());
        if (StringUtils.isEmpty(text)) {
            return "";
        } else if (text.endsWith(".0")) {
            return String.valueOf(number.intValue());
        } else {
            return String.valueOf(number.floatValue());
        }

    }


    public void closeSoftWareKeyBoradInBase() {

        /**隐藏软键盘**/
        View view = getWindow().peekDecorView();
        if (view != null) {
            InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

//        // Check if no view has focus:
//        View view = this.getCurrentFocus();
//        if (view != null) {
//            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//        }
    }


    public void onReceiveVideoSelect(Context context, Intent intent) {
//        Toast.makeText(BaseScreamingReelActivity.this, "yippiee Video ", Toast.LENGTH_SHORT).show();
//        Toast.makeText(BaseScreamingReelActivity.this, "Video SIZE :" + intent.getStringArrayListExtra("list").size(), Toast.LENGTH_SHORT).show();
    }

    public void onReceivePictureSelect(Context context, Intent intent) {

//        Toast.makeText(BaseScreamingReelActivity.this, "yippiee Image ", Toast.LENGTH_SHORT).show();
//        Toast.makeText(BaseScreamingReelActivity.this, "Image SIZE :" + intent.getStringArrayListExtra("list").size(), Toast.LENGTH_SHORT).show();

    }


    BaseTask getProfileTask = null;

    public void getProfile(final Context context, final OnRequestEndCallBack onRequestEndCallBack) {
        if (null != getProfileTask && getProfileTask.getStatus() == AsyncTask.Status.RUNNING) {
            getProfileTask.cancel(true);
        }

        getProfileTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.getProfile(context, new JSONObject());
                } catch (Exception e) {
                    LogUtil.e(App.tag, "get profile error:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != onRequestEndCallBack) {
                    onRequestEndCallBack.onRequestEnd(result);
                }

            }
        }, context);

        getProfileTask.execute(new HashMap<String, String>());

    }


    public void getProfile(final Context context, final boolean showdialog,final boolean cancellAbale, final OnRequestEndCallBack onRequestEndCallBack) {
        if (null != getProfileTask && getProfileTask.getStatus() == AsyncTask.Status.RUNNING) {
            getProfileTask.cancel(true);
        }

        getProfileTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showdialog) {
                    baseTask.showDialogForSelf(cancellAbale);
                }

            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.getProfile(context, new JSONObject());
                } catch (Exception e) {
                    LogUtil.e(App.tag, "get profile error:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (showdialog) {
                    baseTask.hideDialogSelf();
                }

                if (null != onRequestEndCallBack) {
                    onRequestEndCallBack.onRequestEnd(result);
                }

            }
        }, context);

        getProfileTask.execute(new HashMap<String, String>());

    }


    BaseTask allTask = null;

    public void getAllProfile(final Context context, final String userId, final OnRequestEndCallBack onRequestEndCallBack) {

        if (null != allTask && allTask.getStatus() == AsyncTask.Status.RUNNING) {
            allTask.cancel(true);
        }

        allTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.getUserAllProfile(context, userId, new JSONObject());
                } catch (Exception e) {
                    LogUtil.e(App.tag, "get profile error:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != onRequestEndCallBack) {
                    onRequestEndCallBack.onRequestEnd(result);
                }

            }
        }, context);

        allTask.execute(new HashMap<String, String>());

    }


    private BaseTask updateTask;

    public void updateProfile(final Context context, final JSONObject jsonObject, final OnRequestEndCallBack onRequestEndCallBack) {

        if (null != updateTask && updateTask.getStatus() == AsyncTask.Status.RUNNING) {
            updateTask.cancel(true);
        }

        updateTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.updateProfile(context, jsonObject);
                } catch (Exception e) {
                    LogUtil.e(App.tag, "update profile error:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != onRequestEndCallBack) {
                    onRequestEndCallBack.onRequestEnd(result);
                }

            }
        }, context);

        updateTask.execute(new HashMap<String, String>());

    }


    protected void upLoadHeaderImg(final Context context, final String fileUrl, final OnUploadCallBack onUploadCallBack) {
        reSetTask();
        baseTask = new BaseTask(new NetCallBack() {
            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    byte[] b = BitmapTool.readStream(new FileInputStream(fileUrl));

                    LogUtil.i(App.tag, "upload file len:" + b.length);

                    FormFile form = new FormFile("avatar", b, "crop_cache_file.png", "application/octet-stream");
                    FormFile[] temp = {form};
                    HashMap<String, String> map = new HashMap<String, String>();

                    LogUtil.i(App.tag, "token:" + App.getApp().getToken());
                    map.put("api_token", App.getApp().getToken());
                    netResult = NetInterface.uploadWithFromFile(temp, map);
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.e(App.tag, "upload error:" + e.getLocalizedMessage());
                    if (e instanceof NetException) {
                        LogUtil.e(App.tag, "upload error:" + ((NetException) e).getCode());
                    }
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null != onUploadCallBack) {
                    onUploadCallBack.onUploadEnd(result, baseTask.weakReferenceContext.get());
                }


                if (null != result && StringUtils.isEmpty(result.getMessage())) {
                    try {
                        File file = new File(fileUrl);
                        if (null != file && file.exists() && file.isFile()) {
                            file.delete();
                        }
                        LogUtil.e(App.tag, "deleted file:" + fileUrl);
                    } catch (Exception e) {
                        LogUtil.e(App.tag, "delete file fail");
                    }

                }


            }
        }, context);
        baseTask.execute(new HashMap<String, String>());
    }


    public boolean checkCard(String cardNumber, String expiredMonthDate, String cvv) {


        if (StringUtils.isEmpty(cardNumber) || StringUtils.computStrlen(cardNumber.replace("-", "")) != 16) {
            Tool.showMessageDialog("Oops!", "Your credit card number isn't quite right.  Please check before continuing.", this);
            return false;
        }

        if (StringUtils.isEmpty(expiredMonthDate)) {
            Tool.showMessageDialog("Oops!", "Your credit card expiry date isn't quite right.  Please follow the mm/yyyy format to continue.", this);
            return false;
        }


        try {
            String[] input = expiredMonthDate.split("/");

            SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");

            Date d = sdf.parse(expiredMonthDate);

            Date now = DateTool.getNow();


            if (now.getTime() > d.getTime()) {
                Tool.showMessageDialog("Oops!", "Your credit card expiry date isn't quite right.  Please follow the mm/yyyy format to continue.", this);
                return false;

            }

            if (StringUtils.computStrlen(cvv) != 3 && StringUtils.computStrlen(cvv) != 4) {
                Tool.showMessageDialog("Oops!", "Your credit CCV isn't quite right.  Please check before continuing.", this);
                return false;

            }

//            int month=Integer.parseInt(input[0]);
//            if(month<0 || month >12){
//                Tool.showMessageDialog("Month is invalidate", this);
//                return false;
//            }
//
//            int year=Integer.parseInt(input[1]);
//            if(year< DateTool.getThisYear()){
//                Tool.showMessageDialog("Year is invalidate", this);
//                return false;
//            }

        } catch (Exception e) {
            Tool.showMessageDialog("Your credit card expiry date isn't quite right.  Please follow the mm/yyyy format to continue.", this);
            return false;
        }


        return true;
    }
}

