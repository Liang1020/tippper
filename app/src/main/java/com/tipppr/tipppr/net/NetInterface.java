package com.tipppr.tipppr.net;

import android.content.Context;

import com.common.net.FormFile;
import com.common.net.HttpRequester;
import com.common.net.NetException;
import com.common.net.NetResult;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.tipppr.android.App;
import com.tipppr.tipppr.json.JSONHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wangzy on 15/7/16.
 */
public class NetInterface {


//    private static String base_path = "http://192.168.1.121:8000/api/";
//    private static String base_path = "http://106.185.55.185:8767/api/";

    //      private static String base_path = "http://54.183.9.48/api/";//product server
//    private static String base_path = "http://52.53.151.246/api/";//test  server from ivan
    private static String base_path = "https://portal.tipppr.com/api/";//test  server from ivan
    //52.52.113.24
//    private static String base_path = "http://52.52.113.24/api/";//test  server from ivan
//    private static String base_path = "http://192.168.1.108:8000/api/";

    public static NetResult regist(Context context, JSONObject customizedObject) throws Exception {
        String path = "register";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseRegist(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult login(Context context, JSONObject customizedObject, final boolean needToken) throws Exception {
        String path = "login";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseLogin(jsonInputStream, needToken);
            return netResult;
        }
        return null;
    }

    public static NetResult facebookUserLogin(Context context, JSONObject customizedObject) throws Exception {
        String path = "facebook";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseLogin(jsonInputStream, false);
            return netResult;
        }
        return null;
    }


    public static NetResult getProfile(Context context, JSONObject customizedObject) throws Exception {
        String path = "me";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);

        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseMe(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult updateProfile(Context context, JSONObject customizedObject) throws Exception {
        String path = "updateprofile";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseUpdate(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult uploadWithFromFile(FormFile[] formFiles, HashMap<String, String> map) throws IOException, NetException,
            Exception {
        String loacalPath = "uploadimage";
        String resultpath = base_path + loacalPath;
        LogUtil.i(App.tag, "upload to:" + (resultpath));
        String json = HttpRequester.postUpload(resultpath, map, formFiles, App.getApp().getToken());
        if (null != json) {
            NetResult netResult = JSONHelper.parseUpload(json);
            return netResult;
        }
        return null;
    }


    public static NetResult search(Context context, JSONObject customizedObject) throws Exception {
        String path = "usersearch";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseSearch(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult beFriend(Context context, JSONObject customizedObject) throws Exception {
        String path = "beFriend";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseBefriend(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult getFriendList(Context context, JSONObject customizedObject) throws Exception {
        String path = "getFriends";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseFriendList(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult getpendingFriendList(Context context, JSONObject customizedObject) throws Exception {
        String path = "getPendingFriendships";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parsePendingFriends(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult acceptFriendRequest(Context context, JSONObject customizedObject) throws Exception {

        String path = "acceptFriendRequest";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseRequest(jsonInputStream);
            return netResult;
        }
        return null;

    }


    public static NetResult denyFriendRequest(Context context, JSONObject customizedObject) throws Exception {

        String path = "denyFriendRequest";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseRequest(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult getUserAllProfile(Context context, String userId, JSONObject customizedObject) throws Exception {
        String path = "showuser/" + userId;
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseUserAllProfile(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult changePassword(Context context, JSONObject customizedObject) throws Exception {
        String path = "changepassword";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.changePwd(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult resetpassword(Context context, JSONObject customizedObject) throws Exception {
        String path = "resetpassword";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.changePwd(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult reportUser(Context context, JSONObject customizedObject) throws Exception {
        String path = "report";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.changePwd(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult deleteFriend(Context context, JSONObject customizedObject) throws Exception {
        String path = "unFriend";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.changePwd(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult review(Context context, JSONObject customizedObject) throws Exception {
        String path = "review";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.changePwd(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult getPayPalInfo(Context context, String uid, JSONObject customizedObject) throws Exception {
        String path = uid + File.separator + "paypal";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parsePaypalInfo(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult getPreApproval(Context context, JSONObject customizedObject) throws Exception {
        String path = "Preapproval";
        InputStream jsonInputStream = createGetStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parsePreArovalKey(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult vrify(Context context, JSONObject customizedObject) throws Exception {
        String path = "verifytoken";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseVerifyToken(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult getTokens(Context context, JSONObject customizedObject) throws Exception {
        String path = "gettokens";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.changePwd(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult savePayKey(Context context, JSONObject customizedObject) throws Exception {
        String path = "paykey";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.changePwd(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult getPayHistory(Context context, JSONObject customizedObject) throws Exception {
        String path = "history";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseHistory3(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult getPayBillHistory(Context context, JSONObject customizedObject) throws Exception {
        String path = "billhistory";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseHistory3(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult payBillPro(Context context, JSONObject customizedObject) throws Exception {
        String path = "billpro";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseBefriend(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult getBillpro(Context context, JSONObject customizedObject) throws Exception {
        String path = "getBillpro";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseBefriend(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult deleteBillInfo(Context context, JSONObject customizedObject) throws Exception {
        String path = "deleteCardInfo";
        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseBefriend(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult saveReceiveMethod(Context context, JSONObject customizedObject) throws Exception {
        String path = "paymethod";

        InputStream jsonInputStream = createPostStream(context, customizedObject, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseBefriend(jsonInputStream);
            return netResult;
        }
        return null;

//        StringBuffer stringBuffer = new StringBuffer();
//
//        stringBuffer.append("method=" + method);
//
//        InputStream inputStream = HttpRequester.doHttpRequestPost(context, base_path + path, stringBuffer.toString().getBytes());
//        String resonse = JSONHelper.getTextFromInputStream(inputStream);
//        LogUtil.i(App.tag, "resonse:" + resonse);


    }

    //=====================common method======================

    private static InputStream createGetStream(Context context, JSONObject customizedObject, String path) throws Exception {
        String jsonRequest = customizedObject.toString();
        LogUtil.i(App.tag, "path:" + path);
        LogUtil.i(App.tag, "" + jsonRequest);
        InputStream jsonInputStream = HttpRequester.doHttpRequestText(context, path, jsonRequest, null, HttpRequester.REQUEST_GET);
        return jsonInputStream;
    }

    private static InputStream createPostStream(Context context, JSONObject customizedObject, String path) throws Exception {

        if (null != App.getApp().getUser() && !StringUtils.isEmpty(App.getApp().getToken())) {
            customizedObject.put("api_token", App.getApp().getToken());
        }

        String jsonRequest = customizedObject.toString();
        LogUtil.e(App.tag, "path:" + path);
        LogUtil.e(App.tag, "requestjson:" + jsonRequest);
        InputStream jsonInputStream = HttpRequester.doHttpRequestJson(context, path, jsonRequest, App.getApp().getToken(), HttpRequester.REQUEST_POST);
        return jsonInputStream;
    }

    private static InputStream createPostStream(Context context, String text, String path) throws Exception {

        LogUtil.e(App.tag, "path:" + path);
        LogUtil.e(App.tag, "requestText:" + text);
        InputStream jsonInputStream = HttpRequester.doHttpRequestJson(context, path, text, App.getApp().getToken(), HttpRequester.REQUEST_POST);
        return jsonInputStream;
    }


    /**
     * create request Object from map to jsonobject
     *
     * @param paramMap
     * @return
     * @throws JSONException
     */
    public static JSONObject buildCustomizedObject(HashMap<String, String> paramMap) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        for (Map.Entry<String, String> entry : paramMap.entrySet()) {
            jsonObject.put(entry.getKey(), entry.getValue());
        }
        return jsonObject;
    }

    /**
     * build final object
     *
     * @param customedObject
     * @param commonObject
     * @return
     * @throws JSONException
     */
    public static JSONObject createRequestJsonObject(JSONObject customedObject, JSONObject commonObject) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("customized", customedObject);
        jsonObject.put("common", commonObject);
        return jsonObject;

    }

}
