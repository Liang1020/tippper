package com.tipppr.tipppr.adapter;

import android.content.Context;
import android.location.Address;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.common.util.StringUtils;
import com.tipppr.android.R;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 16/1/12.
 */
public class AddressAdapter extends BaseAdapter {

    private ArrayList<Address> addresses;
    private Context context;
    private LayoutInflater layoutInflater;


    public AddressAdapter(Context context, ArrayList<Address> addresses) {

        this.context = context;
        this.addresses = addresses;
        this.layoutInflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return addresses.size();
    }

    @Override
    public Object getItem(int position) {
        return addresses.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (null == convertView) {

            convertView = layoutInflater.inflate(R.layout.item_address, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ViewHolder) convertView.getTag();

        }

        Address address = addresses.get(position);


        String featureName = address.getFeatureName();

        String area = address.getAdminArea();

        String subArea = address.getSubAdminArea();

        String country = address.getCountryName();


        viewHolder.textView.setText(
                 featureName+ (StringUtils.isEmpty(area)?"":","+area)+
                         (StringUtils.isEmpty(subArea)?"":subArea)+
                 (StringUtils.isEmpty(country)?"":","+country));

        viewHolder.textView.setText(
                featureName+ (StringUtils.isEmpty(area)?"":","+area)+
                        (StringUtils.isEmpty(subArea)?"":subArea)+
                        (StringUtils.isEmpty(country)?"":","+country));

//        viewHolder.textView.setText(address.getFeatureName());

        return convertView;
    }


    class ViewHolder {

        @InjectView(R.id.textViewAddress)
        TextView textView;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }


}
