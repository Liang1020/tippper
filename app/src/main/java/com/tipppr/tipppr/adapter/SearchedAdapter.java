package com.tipppr.tipppr.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.common.util.NumberHelper;
import com.common.util.StringUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.tipppr.android.R;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.view.HexImageView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 16/2/1.
 */
public class SearchedAdapter extends BaseAdapter {


    private Context context;
    private LayoutInflater layoutInflater;
    private Bitmap bitmapMaskHeader;
    private ArrayList<Target> targets;
    private ArrayList<User> arrayListUsers;


    public SearchedAdapter(Context context, ArrayList<User> users) {

        this.context = context;

        this.layoutInflater = LayoutInflater.from(context);

        this.arrayListUsers = users;

        this.bitmapMaskHeader = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_mask_header);

        this.targets = new ArrayList<>();
    }


    @Override
    public int getCount() {
        return arrayListUsers.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayListUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_searched_user, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        final HexImageView hexImageView = viewHolder.hexImageView;

        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                hexImageView.setImageBitmap(ImageUtils.clipPatchImageNorecyle(bitmap, bitmapMaskHeader));
                hexImageView.setImageBitmap(bitmap);
//                LogUtil.i(App.tag, "load url sucess");
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        targets.add(target);

        User user = arrayListUsers.get(position);

        String headerUrl = user.getHeaderImage();
        if (StringUtils.isEmpty(headerUrl)) {
            Picasso.with(context).load(R.drawable.hary).
                    placeholder(R.drawable.hary).
                    resize(bitmapMaskHeader.getWidth(), bitmapMaskHeader.getHeight()).
                    into(target);
        } else {
            Picasso.with(context).load(headerUrl).
                    placeholder(R.drawable.hary).
                    resize(bitmapMaskHeader.getWidth(), bitmapMaskHeader.getHeight()).
                    into(target);
        }

//        String name = arrayListUsers.get(position).getProfileName();
        if (arrayListUsers.get(position).isBuiness()) {
            viewHolder.textViewName.setText(arrayListUsers.get(position).getBussness());
        } else {
            viewHolder.textViewName.setText(arrayListUsers.get(position).getProfileName());
        }

        String distance = user.getDistance();

        if (!StringUtils.isEmpty(distance)) {

//            LatLng latlngCurrent = App.getApp().getCurrentLatlng();
//            double tlat = Double.parseDouble(user.getLat());
//            double tlng = Double.parseDouble(user.getLng());
//            distance = "" + Tool.getDistance(tlng, tlat, latlngCurrent.longitude, latlngCurrent.latitude);

//            distance = "" + Tool.getDistance(latlngCurrent, new LatLng(tlat, tlng));

//             String ret = NumberHelper.keepDecimal1(String.valueOf(Double.parseDouble(distance)/1000.0f));
            String ret = NumberHelper.keepDecimal1(String.valueOf(Double.parseDouble(distance)));
            if (ret.endsWith(".0")) {
                ret = ret.replace(".0", "");
            }

            viewHolder.textViewDistance.setText(ret + " kilometers away");
            viewHolder.textViewDistance.setVisibility(View.VISIBLE);
        } else {
            viewHolder.textViewDistance.setText("");
            viewHolder.textViewDistance.setVisibility(View.GONE);
        }


        return convertView;
    }

    public void desctory() {

        targets.clear();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();

        desctory();
    }

    class ViewHolder {

        @InjectView(R.id.hexImageView)
        HexImageView hexImageView;

        @InjectView(R.id.textViewName)
        TextView textViewName;

        @InjectView(R.id.imaveViewAdd)
        ImageView imaveViewAdd;

        @InjectView(R.id.textViewDistance)
        TextView textViewDistance;

        public ViewHolder(View v) {

            ButterKnife.inject(this, v);
        }

    }


}

