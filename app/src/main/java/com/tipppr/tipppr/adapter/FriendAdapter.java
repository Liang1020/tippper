package com.tipppr.tipppr.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.common.util.StringUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.tipppr.android.R;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.view.HexImageView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 16/2/1.
 */
public class FriendAdapter extends BaseAdapter {


    private Context context;
    private LayoutInflater layoutInflater;
    private Bitmap bitmapMaskHeader;
    private ArrayList<Target> targets;
    private ArrayList<User> arrayListUsers;
    private boolean canDeelete;

    private User userInProfilePage;
    private OnDeleteClickListener onDeleteClickListener;
    private int index = -1;
    private boolean select = false;

    public FriendAdapter(Context context, ArrayList<User> users) {

        this.context = context;

        this.layoutInflater = LayoutInflater.from(context);

        this.arrayListUsers = users;

        this.bitmapMaskHeader = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_mask_header);

        this.targets = new ArrayList<>();
    }

    public FriendAdapter(Context context, boolean select, ArrayList<User> users) {

        this.context = context;

        this.layoutInflater = LayoutInflater.from(context);

        this.arrayListUsers = users;

        this.bitmapMaskHeader = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_mask_header);

        this.targets = new ArrayList<>();
        this.select = select;
    }

    public FriendAdapter(Context context, ArrayList<User> users, boolean canDeelete) {

        this.context = context;

        this.layoutInflater = LayoutInflater.from(context);

        this.arrayListUsers = users;

        this.bitmapMaskHeader = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_mask_header);

        this.targets = new ArrayList<>();
        this.canDeelete = canDeelete;
    }


    public FriendAdapter(Context context, ArrayList<User> users, User userInProfilePage) {

        this.context = context;

        this.layoutInflater = LayoutInflater.from(context);

        this.arrayListUsers = users;

        this.bitmapMaskHeader = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_mask_header);

        this.targets = new ArrayList<>();

        this.userInProfilePage = userInProfilePage;
    }


    @Override
    public int getCount() {
        return arrayListUsers.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayListUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolderInUserAdapter viewHolder = null;

        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_user_profile, null);
            viewHolder = new ViewHolderInUserAdapter(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderInUserAdapter) convertView.getTag();
        }


        final HexImageView hexImageView = viewHolder.hexImageView;

        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                hexImageView.setImageBitmap(ImageUtils.clipPatchImageNorecyle(bitmap, bitmapMaskHeader));
                hexImageView.setImageBitmap(bitmap);
//                LogUtil.i(App.tag, "load url sucess");
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        targets.add(target);

        String headerUrl = arrayListUsers.get(position).getHeaderImage();
        if (StringUtils.isEmpty(headerUrl)) {
            Picasso.with(context).load(R.drawable.hary).
                    placeholder(R.drawable.hary).
                    resize(bitmapMaskHeader.getWidth(), bitmapMaskHeader.getHeight()).
                    into(target);
        } else {
            Picasso.with(context).load(headerUrl).
                    placeholder(R.drawable.hary).
                    resize(bitmapMaskHeader.getWidth(), bitmapMaskHeader.getHeight()).
                    into(target);
        }

//        String name = arrayListUsers.get(position).getName();
//        viewHolder.textViewName.setText(name);

        if (arrayListUsers.get(position).isBuiness()) {
            viewHolder.textViewName.setText(arrayListUsers.get(position).getBussness());
        } else {
            viewHolder.textViewName.setText(arrayListUsers.get(position).getName());
        }

        viewHolder.linearLayoutDeleteFirend.setVisibility(canDeelete ? View.VISIBLE : View.INVISIBLE);

        viewHolder.linearLayoutDeleteFirend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onDeleteClickListener) {
                    onDeleteClickListener.onDelClick(arrayListUsers.get(position));
                }
            }
        });

        if (select) {
            viewHolder.imageViewIconOperate.setBackgroundResource(R.drawable.icon_agree);
            if (index == position) {
                viewHolder.linearLayoutDeleteFirend.setVisibility(View.VISIBLE);
            } else {
                viewHolder.linearLayoutDeleteFirend.setVisibility(View.GONE);
            }


        }


        return convertView;
    }

    public void desctory() {
        targets.clear();
    }

    public void selectIndex(int index) {

        this.index = index;
        notifyDataSetChanged();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        desctory();
    }

    class ViewHolderInUserAdapter {

        @InjectView(R.id.hexImageProfileList)
        HexImageView hexImageView;

        @InjectView(R.id.textViewName)
        TextView textViewName;

        @InjectView(R.id.linearLayoutDeleteFirend)
        View linearLayoutDeleteFirend;


        @InjectView(R.id.imageViewIconOperate)
        ImageView imageViewIconOperate;

        public ViewHolderInUserAdapter(View v) {
            ButterKnife.inject(this, v);
        }

    }


    public static interface OnDeleteClickListener {
        public void onDelClick(User user);
    }

    public OnDeleteClickListener getOnDeleteClickListener() {
        return onDeleteClickListener;
    }

    public void setOnDeleteClickListener(OnDeleteClickListener onDeleteClickListener) {
        this.onDeleteClickListener = onDeleteClickListener;
    }
}

