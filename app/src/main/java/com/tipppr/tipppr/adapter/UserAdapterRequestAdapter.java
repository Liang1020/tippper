package com.tipppr.tipppr.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.common.util.StringUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.tipppr.android.R;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.view.HexImageView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 16/2/1.
 */
public class UserAdapterRequestAdapter extends BaseAdapter {


    private Context context;
    private LayoutInflater layoutInflater;
    private Bitmap bitmapMaskHeader;
    private ArrayList<Target> targets;
    private ArrayList<User> arrayListUsers;
    private OnOperateClickListener onOperateClickListener;
    private boolean isMyRequest;

    public UserAdapterRequestAdapter(Context context, ArrayList<User> users, boolean isMyRequest) {

        this.context = context;

        this.layoutInflater = LayoutInflater.from(context);

        this.arrayListUsers = users;

        this.bitmapMaskHeader = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_mask_header);

        this.targets = new ArrayList<>();
        this.isMyRequest = isMyRequest;

    }


    @Override
    public int getCount() {
        return arrayListUsers.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayListUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_user_request, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        final HexImageView hexImageView = viewHolder.hexImageView;

        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                hexImageView.setImageBitmap(ImageUtils.clipPatchImageNorecyle(bitmap, bitmapMaskHeader));
                hexImageView.setImageBitmap(bitmap);
//                LogUtil.i(App.tag, "load url sucess");
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        targets.add(target);
//
//        String uid = arrayListUsers.get(position).getUserId();
//        if (App.getApp().getUser().getUserId().equals(uid)) {
//            viewHolder.viewOperration.setVisibility(View.INVISIBLE);
//        } else {
//            viewHolder.viewOperration.setVisibility(View.VISIBLE);
//        }

        String headerUrl = arrayListUsers.get(position).getHeaderImage();
        if (StringUtils.isEmpty(headerUrl)) {
            Picasso.with(context).load(R.drawable.hary).
                    placeholder(R.drawable.hary).
                    resize(bitmapMaskHeader.getWidth(), bitmapMaskHeader.getHeight()).
                    into(target);
        } else {
            Picasso.with(context).load(headerUrl).
                    placeholder(R.drawable.hary).
                    resize(bitmapMaskHeader.getWidth(), bitmapMaskHeader.getHeight()).
                    into(target);
        }


        User user = arrayListUsers.get(position);
        if (user.isBuiness()) {
            String name = arrayListUsers.get(position).getBussness();
            viewHolder.textViewName.setText(name);
        } else {
            String name = arrayListUsers.get(position).getName();
            viewHolder.textViewName.setText(name);
        }

        viewHolder.imaveViewAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (null != onOperateClickListener) {
                    onOperateClickListener.onClickAgree(arrayListUsers.get(position), position, UserAdapterRequestAdapter.this);
                }
            }
        });

        viewHolder.imaveViewDelay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onOperateClickListener) {
                    onOperateClickListener.onClickDelay(arrayListUsers.get(position), position, UserAdapterRequestAdapter.this, isMyRequest);
                }

            }
        });

        viewHolder.textViewName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (null != onOperateClickListener) {
                    onOperateClickListener.onClickName(arrayListUsers.get(position), position, UserAdapterRequestAdapter.this, isMyRequest);
                }
            }
        });


        if (isMyRequest) {
            viewHolder.imaveViewAgree.setVisibility(View.INVISIBLE);
            viewHolder.imaveViewDelay.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.imaveViewAgree.setVisibility(View.VISIBLE);
            viewHolder.imaveViewDelay.setVisibility(View.VISIBLE);
        }


        if (arrayListUsers.get(position).isBuiness()) {
            viewHolder.textViewName.setText(arrayListUsers.get(position).getBussness());
        } else {
            viewHolder.textViewName.setText(arrayListUsers.get(position).getName());
        }


        return convertView;
    }

    public void removeAtIndex(int index) {

        arrayListUsers.remove(index);

        notifyDataSetChanged();

    }


    public void desctory() {

        targets.clear();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();

        desctory();
    }

    class ViewHolder {

        @InjectView(R.id.hexImageViewInRequest)
        HexImageView hexImageView;

        @InjectView(R.id.textViewName)
        TextView textViewName;

        @InjectView(R.id.viewOperration)
        View viewOperration;


        @InjectView(R.id.imaveViewAgree)
        View imaveViewAgree;

        @InjectView(R.id.imaveViewDelay)
        View imaveViewDelay;

        public ViewHolder(View v) {

            ButterKnife.inject(this, v);
        }

    }


    public OnOperateClickListener getOnOperateClickListener() {
        return onOperateClickListener;
    }

    public void setOnOperateClickListener(OnOperateClickListener onOperateClickListener) {
        this.onOperateClickListener = onOperateClickListener;
    }

    public static interface OnOperateClickListener {

        public void onClickAgree(User user, int index, UserAdapterRequestAdapter adapterRequestAdapter);

        public void onClickDelay(User user, int index, UserAdapterRequestAdapter adapterRequestAdapter, boolean isMyRequest);
        public void onClickName(User user, int index, UserAdapterRequestAdapter adapterRequestAdapter, boolean isMyRequest);
    }


}

