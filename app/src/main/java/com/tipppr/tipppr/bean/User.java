package com.tipppr.tipppr.bean;

import com.common.bean.IBeanInterface;
import com.common.util.StringUtils;
import com.tipppr.android.App;
import com.tipppr.bean.Card;

import java.util.ArrayList;

/**
 * Created by wangzy on 16/1/26.
 */
public class User implements IBeanInterface {

    private String id;
    private String userId;
    private String facebookId;

    private String profileName;
    private String profileEmail;


    private String email;
    private String payPalEmail;

    private String level;
    private String name;
    private String phone;
    private String headerImage;
    private String bussness;
    private String postcode;
    private String lat;
    private String lng;

    private String amount;
    private String createDate;
    private String isEmailVerify;

    private String country;
    private String currency;
    private String recemethod;


    private int review;
    private String amount2me;

    public static final String ROLE_PERSONAL = "1";
    public static final String ROLE_BUSINESS = "2";

    private ArrayList<User> arrayListFriends;

    public String distance;

    private Card card;


    public User() {

        this.level = ROLE_PERSONAL;
        this.arrayListFriends = new ArrayList<>();
    }


    public boolean isEmailVerify() {
        return "0".equals(isEmailVerify) ? false : true;

    }

    public boolean isUpdateProfile() {


//        return !StringUtils.isEmpty(payPalEmail) && !StringUtils.isEmpty(phone);

        return true;
    }


    public ArrayList<User> getAllFriends() {

        return this.arrayListFriends;
    }


    public void addFriend(User user) {

        this.arrayListFriends.add(user);
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getName() {

        try {
            return name.replace(App.getApp().getSplit(), " ");
        } catch (Exception e) {
            return name;
        }


    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeaderImage() {

        if(!StringUtils.isEmpty(headerImage)){

            return  headerImage.replace("\\","");
        }

        return headerImage;
    }

    public void setHeaderImage(String headerImage) {
        this.headerImage = headerImage;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String fbId) {
        this.facebookId = fbId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBussness() {
        return bussness;
    }

    public void setBussness(String bussness) {
        this.bussness = bussness;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getProfileName() {

        try {
            return profileName.replace(App.getApp().getSplit(), " ");
        } catch (Exception e) {
            return profileName;
        }

    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getProfileEmail() {
        return profileEmail;
    }

    public void setProfileEmail(String profileEmail) {
        this.profileEmail = profileEmail;
    }


    public boolean isBuiness() {

        if (ROLE_BUSINESS.equals(level)) {
            return true;
        } else {
            return false;
        }

    }

    public String getResultName() {

        if (!StringUtils.isEmpty(profileName)) {
            return profileName;
        }

        if (!StringUtils.isEmpty(name)) {
            return profileName;
        }

        return "";
    }

    public String getResultEmail() {

        if (!StringUtils.isEmpty(profileEmail)) {
            return profileEmail;
        }

        if (!StringUtils.isEmpty(email)) {
            return email;
        }

        return "";
    }


    public User copyFromPartyUser(User user) {

//        private String id;
//        private String userId;
//
//        private String profileName;
//        private String profileEmail;
//
//
//        private String email;
//        private String level;
//        private String name;
//        private String phone;
//        private String headerImage;
//        private String bussness;
//        private String postcode;

        if (null != user) {

            if (!StringUtils.isEmpty(user.getId())) {
                this.id = user.getId();
            }
            if (!StringUtils.isEmpty(user.userId)) {
                this.userId = user.getUserId();
            }

            if (!StringUtils.isEmpty(user.getProfileName())) {
                this.profileName = user.getProfileName();
            }
            if (!StringUtils.isEmpty(user.getProfileEmail())) {
                this.profileEmail = user.getProfileEmail();
            }
            if (!StringUtils.isEmpty(user.getEmail())) {
                this.email = user.getEmail();
            }

            if (!StringUtils.isEmpty(user.getName())) {
                this.name = user.getName();
            }

            if (!StringUtils.isEmpty(user.getPhone())) {
                this.phone = user.getPhone();
            }
            if (!StringUtils.isEmpty(user.getHeaderImage())) {
                this.headerImage = user.getHeaderImage();
            }

            if (!StringUtils.isEmpty(user.getBussness())) {
                this.bussness = user.getBussness();
            }
            if (!StringUtils.isEmpty(user.getPostcode())) {
                this.postcode = user.getPostcode();
            }
            if (!StringUtils.isEmpty(user.getIsEmailVerify())) {
                this.setIsEmailVerify(user.getIsEmailVerify());
            }
            if (!StringUtils.isEmpty(user.getCountry())) {
                this.setCountry(user.getCountry());
            }

        }

        return this;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public boolean hasLatlng() {

        if (!StringUtils.isEmpty(lat) && !StringUtils.isEmpty(lng)) {
            return true;
        }
        return false;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getReview() {
        return review;
    }

    public void setReview(int review) {
        this.review = review;
    }

    public String getPayPalEmail() {
        return payPalEmail;
    }

    public void setPayPalEmail(String payPalEmail) {
        this.payPalEmail = payPalEmail;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getIsEmailVerify() {
        return isEmailVerify;
    }

    public void setIsEmailVerify(String isEmailVerify) {
        this.isEmailVerify = isEmailVerify;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {

        if (StringUtils.isEmpty(currency)) {
            return "USD";
        }

        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }


    public String getAmount2me() {
        return amount2me;
    }

    public void setAmount2me(String amount2me) {
        this.amount2me = amount2me;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public String getRecemethod() {
        return recemethod;
    }

    public void setRecemethod(String recemethod) {
        this.recemethod = recemethod;
    }
}
