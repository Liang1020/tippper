package com.tipppr.tipppr.json;

import com.common.net.NetResult;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.tipppr.android.App;
import com.tipppr.bean.Card;
import com.tipppr.bean.TransAction;
import com.tipppr.bean.TransActionMore;
import com.tipppr.tipppr.bean.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by wangzy on 16/1/26.
 */
public class JSONHelper {


    public final static String tag_data = "data";
    public final static String tag_errors = "errors";
    public final static String tag_token = "api_token";


    public static NetResult parseRegist(InputStream inputStream) throws JSONException {

        JSONObject jobj = new JSONObject(getJsonFromInputStream(inputStream));
        LogUtil.i(App.tag, jobj.toString());
        NetResult netResult = new NetResult();

        String erros = jobj.getString(tag_errors);

        if (StringUtils.isEmpty(erros)) {


            JSONObject dataObj = jobj.getJSONObject(tag_data);

            String token = getStringFroJso(tag_token, dataObj);

            User user = new User();

            user.setEmail(getStringFroJso("email", dataObj));
            user.setLevel(getStringFroJso("level", dataObj));
            user.setName(getStringFroJso("name", dataObj));
            user.setId(getStringFroJso("id", dataObj));

            netResult.setData(new Object[]{user});

            App.getApp().setToken(token);

        } else {
            netResult.setMessage(erros);
        }


        return netResult;

    }


    public static NetResult parseLogin(InputStream jsonInputStream, boolean needToken) throws JSONException {

        JSONObject jobj = new JSONObject(getJsonFromInputStream(jsonInputStream));
        LogUtil.i(App.tag, jobj.toString());
        NetResult netResult = new NetResult();

        String erros = jobj.getString(tag_errors);

        if (StringUtils.isEmpty(erros)) {

            String token = jobj.getString(tag_data);

            App.getApp().setToken(token);
            if (!needToken) {
                App.getApp().setUser(new User());
            }

        } else {
            netResult.setMessage(erros);
        }

        return netResult;

    }

    public static NetResult parseUpdate(InputStream jsonInputStream) throws JSONException {

        JSONObject jobj = new JSONObject(getJsonFromInputStream(jsonInputStream));
        LogUtil.i(App.tag, jobj.toString());
        NetResult netResult = new NetResult();

        String erros = jobj.getString(tag_errors);

        if (StringUtils.isEmpty(erros)) {

            JSONObject dataObj = jobj.getJSONObject(tag_data);


            User user = new User();

            user.setProfileEmail(getStringFroJso("email", dataObj));
            user.setProfileName(getStringFroJso("name", dataObj));
            user.setId(getStringFroJso("id", dataObj));
            user.setUserId(getStringFroJso("user_id", dataObj));
            user.setBussness(getStringFroJso("bussness", dataObj));
            user.setHeaderImage(getStringFroJso("avatar", dataObj));
            user.setPhone(getStringFroJso("phone", dataObj));
            user.setPostcode(getStringFroJso("postcode", dataObj));
            user.setIsEmailVerify(getStringFroJso("email_verify", dataObj));
            user.setLat(getStringFroJso("lat", dataObj));
            user.setLng(getStringFroJso("lng", dataObj));
            user.setCountry(getStringFroJso("country", dataObj));
            user.setCurrency(getStringFroJso("currency", dataObj));

            try {
                user.setReview(dataObj.getInt("review"));
            } catch (Exception e) {

            }

            netResult.setData(new Object[]{user});


        } else {
            netResult.setMessage(erros);
        }

        return netResult;

    }

    public static NetResult parseMe(InputStream jsonInputStream) throws JSONException {

        JSONObject jobj = new JSONObject(getJsonFromInputStream(jsonInputStream));
        LogUtil.i(App.tag, jobj.toString());
        NetResult netResult = new NetResult();

        String erros = getStringFroJso(tag_errors, jobj);


        JSONObject dataObj = jobj.getJSONObject(tag_data);


        User user = new User();
        user.setLevel(getStringFroJso("level", dataObj));
        user.setName(getStringFroJso("name", dataObj));
        user.setEmail(getStringFroJso("email", dataObj));
        user.setFacebookId(getStringFroJso("facebook_id", dataObj));

        netResult.setData(new Object[]{user});
        App.getApp().setUser(user);
        netResult.setMessage(erros);

        if (StringUtils.isEmpty(erros) && dataObj.has("profile") && !"null".equals(dataObj.getString("profile"))) {

            JSONObject profileObject = dataObj.getJSONObject("profile");


            user.setId(getStringFroJso("id", profileObject));

            user.setUserId(getStringFroJso("user_id", profileObject));
            user.setProfileName(getStringFroJso("name", profileObject));//profileObject.getString("name"));
            user.setProfileEmail(getStringFroJso("email", profileObject));//profileObject.getString("email"));
            user.setHeaderImage(getStringFroJso("avatar", profileObject));//profileObject.getString("avatar"));
            user.setPhone(getStringFroJso("phone", profileObject));
            user.setBussness(getStringFroJso("bussness", profileObject));//profileObject.getString("bussness"));
            user.setPostcode(getStringFroJso("postcode", profileObject));//profileObject.getString("postcode"));
            user.setPayPalEmail(getStringFroJso("paypal_email", profileObject));
            user.setLat(getStringFroJso("lat", profileObject));
            user.setLng(getStringFroJso("lng", profileObject));
            user.setIsEmailVerify(getStringFroJso("email_verify", profileObject));
            user.setCountry(getStringFroJso("country", profileObject));
            user.setCurrency(getStringFroJso("currency", profileObject));


            if (!"null".equals(profileObject.getString("cardinfo")) && profileObject.has("cardinfo")) {


                JSONObject cardIndoNode = profileObject.getJSONObject("cardinfo");

                Card card = new Card();

                card.setCardNumber(getStringFroJso("cardNumber", cardIndoNode));
                card.setCardEmail(getStringFroJso("cardEmail", cardIndoNode));
                card.setCardPhone(getStringFroJso("cardPhone", cardIndoNode));
                card.setCardPostcode(getStringFroJso("cardPostcode", cardIndoNode));

                card.setCardCurrency(getStringFroJso("cardCurrency", cardIndoNode));
                card.setCardCountry(getStringFroJso("cardCountry", cardIndoNode));

                card.setCardCity(getStringFroJso("cardCity", cardIndoNode));
                card.setCardState(getStringFroJso("cardState", cardIndoNode));

                card.setCardFirstname(getStringFroJso("cardFirstname", cardIndoNode));
                card.setCardLastname(getStringFroJso("cardLastname", cardIndoNode));

                user.setCard(card);

            }

            try {
                user.setReview(profileObject.getInt("review"));
            } catch (Exception e) {

            }

        }
        if (StringUtils.isEmpty(erros) && dataObj.has("paymethod") && !"null".equals(dataObj.getString("paymethod"))) {

            JSONObject paymethodObject = dataObj.getJSONObject("paymethod");

            String methodText = paymethodObject.getString("method");
            user.setRecemethod(methodText);
            LogUtil.i(App.tag, methodText);

        }


        return netResult;

    }


    public static NetResult parseSearch(InputStream jsonInputStream) throws JSONException {

        JSONObject jobj = new JSONObject(getJsonFromInputStream(jsonInputStream));
        LogUtil.i(App.tag, jobj.toString());
        NetResult netResult = new NetResult();

        String erros = getStringFroJso(tag_errors, jobj);
        netResult.setMessage(erros);
        if (StringUtils.isEmpty(erros)) {

            JSONObject objectDatas = jobj.getJSONObject(tag_data);

            String total = getStringFroJso("total", objectDatas);// objectDatas.getInt("total");
            String current = getStringFroJso("current_page", objectDatas);//objectDatas.getInt("current_page");
//            int from = objectDatas.getInt("from");
//            int to = objectDatas.getInt("to");


            JSONArray dataArray = objectDatas.getJSONArray(tag_data);
            ArrayList<User> users = new ArrayList<User>();

            if (null != dataArray && dataArray.length() > 0) {
                for (int i = 0, isize = dataArray.length(); i < isize; i++) {

                    JSONObject jo = dataArray.getJSONObject(i);

                    User user = new User();

                    user.setUserId(getStringFroJso("user_id", jo));
                    user.setProfileName(getStringFroJso("name", jo));
                    user.setProfileEmail(getStringFroJso("email", jo));
                    user.setHeaderImage(getStringFroJso("avatar", jo));
                    user.setPhone(getStringFroJso("phone", jo));
                    user.setBussness(getStringFroJso("bussness", jo));
                    user.setPostcode(getStringFroJso("postcode", jo));
                    user.setLat(getStringFroJso("lat", jo));
                    user.setLng(getStringFroJso("lng", jo));
                    user.setDistance(getStringFroJso("distance", jo));
                    user.setLevel(getStringFroJso("level", jo));
                    user.setPayPalEmail(getStringFroJso("paypal_email", jo));

                    try {
                        user.setReview(jo.getInt("review"));
                    } catch (Exception e) {

                    }

                    users.add(user);

                }
            }

            netResult.setData(new Object[]{users, total, current});

        }

        return netResult;

    }


    public static NetResult parseBefriend(InputStream jsonInputStream) throws JSONException {

        JSONObject jobj = new JSONObject(getJsonFromInputStream(jsonInputStream));
        LogUtil.i(App.tag, jobj.toString());
        NetResult netResult = new NetResult();

        String erros = getStringFroJso(tag_errors, jobj);
        netResult.setMessage(erros);
        if (StringUtils.isEmpty(erros)) {

        }

        return netResult;

    }


    public static NetResult parsePendingFriends(InputStream jsonInputStream) throws JSONException {

        JSONObject jobj = new JSONObject(getJsonFromInputStream(jsonInputStream));
        LogUtil.i(App.tag, jobj.toString());
        NetResult netResult = new NetResult();

        String erros = getStringFroJso(tag_errors, jobj);
        netResult.setMessage(erros);


        ArrayList<User> userListOutGoing = new ArrayList<>();
        ArrayList<User> incomingList = new ArrayList<>();

        if (StringUtils.isEmpty(erros)) {
            if (jobj.has(tag_data) && !StringUtils.isEmpty(jobj.getString(tag_data))) {

                JSONObject allRequestObj = jobj.getJSONObject(tag_data);

                JSONArray outgoingArray = allRequestObj.getJSONArray("outgoing");

                netResult.setData(new Object[]{userListOutGoing, incomingList});

                if (null != outgoingArray && outgoingArray.length() > 0) {

                    for (int i = 0, isize = outgoingArray.length(); i < isize; i++) {

                        JSONObject userObj = outgoingArray.getJSONObject(i);

                        User user = new User();

                        user.setHeaderImage(getStringFroJso("avatar", userObj));
                        user.setBussness(getStringFroJso("bussness", userObj));
                        user.setEmail(getStringFroJso("email", userObj));
                        user.setLat(getStringFroJso("lat", userObj));
                        user.setLng(getStringFroJso("lng", userObj));

                        user.setName(getStringFroJso("name", userObj));
                        user.setPhone(getStringFroJso("phone", userObj));

                        user.setPostcode(getStringFroJso("postcode", userObj));

                        user.setUserId(getStringFroJso("user_id", userObj));
                        user.setLevel(getStringFroJso("level", userObj));

                        try {
                            user.setReview(userObj.getInt("review"));
                        } catch (Exception e) {

                        }

                        userListOutGoing.add(user);

                    }
                }
                JSONArray incomingArray = allRequestObj.getJSONArray("incoming");

                if (null != incomingArray && incomingArray.length() > 0) {

                    for (int i = 0, isize = incomingArray.length(); i < isize; i++) {

                        JSONObject userObj = incomingArray.getJSONObject(i);

                        User user = new User();

                        user.setHeaderImage(getStringFroJso("avatar", userObj));
                        user.setBussness(getStringFroJso("bussness", userObj));
                        user.setEmail(getStringFroJso("email", userObj));
                        user.setLat(getStringFroJso("lat", userObj));
                        user.setLng(getStringFroJso("lng", userObj));

                        user.setName(getStringFroJso("name", userObj));
                        user.setPhone(getStringFroJso("phone", userObj));

                        user.setPostcode(getStringFroJso("postcode", userObj));

                        user.setUserId(getStringFroJso("user_id", userObj));
                        user.setLevel(getStringFroJso("level", userObj));

                        try {
                            user.setReview(userObj.getInt("review"));
                        } catch (Exception e) {

                        }

                        incomingList.add(user);

                    }
                }
            }
        }

        return netResult;

    }

    public static NetResult parseFriendList(InputStream jsonInputStream) throws JSONException {

        JSONObject jobj = new JSONObject(getJsonFromInputStream(jsonInputStream));
        LogUtil.i(App.tag, jobj.toString());
        NetResult netResult = new NetResult();

        String erros = getStringFroJso(tag_errors, jobj);
        netResult.setMessage(erros);

        ArrayList<User> userList = new ArrayList<>();

        if (StringUtils.isEmpty(erros)) {


            try {
                if (jobj.has(tag_data) && null != jobj.getJSONArray(tag_data)) {

                    JSONArray userArray = jobj.getJSONArray(tag_data);

                    if (null != userArray && userArray.length() > 0) {

                        for (int i = 0, isize = userArray.length(); i < isize; i++) {

                            JSONObject userObj = userArray.getJSONObject(i);

                            User user = new User();

                            user.setHeaderImage(getStringFroJso("avatar", userObj));
                            user.setBussness(getStringFroJso("bussness", userObj));
                            user.setEmail(getStringFroJso("email", userObj));
                            user.setLat(getStringFroJso("lat", userObj));
                            user.setLng(getStringFroJso("lng", userObj));

                            user.setName(getStringFroJso("name", userObj));
                            user.setPhone(getStringFroJso("phone", userObj));
                            user.setPostcode(getStringFroJso("postcode", userObj));
                            user.setUserId(getStringFroJso("user_id", userObj));
                            user.setLevel(getStringFroJso("level", userObj));

                            try {
                                user.setReview(userObj.getInt("review"));
                            } catch (Exception e) {

                            }

                            userList.add(user);

                            netResult.setData(new Object[]{userList});

                        }
                    }
                }


            } catch (Exception e) {

                LogUtil.i(App.tag, "read json array error!");
            }

        }

        netResult.setData(new Object[]{userList});

        return netResult;

    }


    public static NetResult parseRequest(InputStream jsonInputStream) throws JSONException {

        JSONObject jobj = new JSONObject(getJsonFromInputStream(jsonInputStream));
        LogUtil.i(App.tag, jobj.toString());
        NetResult netResult = new NetResult();

        String erros = getStringFroJso(tag_errors, jobj);
        netResult.setMessage(erros);

        if (StringUtils.isEmpty(erros)) {

            if (jobj.has(tag_data) && !StringUtils.isEmpty(jobj.getString(tag_data))) {

                JSONObject userObj = jobj.getJSONObject(tag_data);

                User user = new User();

                user.setHeaderImage(getStringFroJso("avatar", userObj));
                user.setBussness(getStringFroJso("bussness", userObj));
                user.setEmail(getStringFroJso("email", userObj));
                user.setLat(getStringFroJso("lat", userObj));
                user.setLng(getStringFroJso("lng", userObj));

                user.setName(getStringFroJso("name", userObj));
                user.setPhone(getStringFroJso("phone", userObj));

                user.setPostcode(getStringFroJso("postcode", userObj));

                user.setUserId(getStringFroJso("user_id", userObj));
                user.setLevel(getStringFroJso("level", userObj));


                try {
                    user.setReview(userObj.getInt("review"));
                } catch (Exception e) {

                }


                netResult.setData(new Object[]{user});

            }
        }
        return netResult;
    }


    public static NetResult parseUserAllProfile(InputStream jsonInputStream) throws JSONException {

        JSONObject jobj = new JSONObject(getJsonFromInputStream(jsonInputStream));
        LogUtil.i(App.tag, jobj.toString());
        NetResult netResult = new NetResult();

        String erros = getStringFroJso(tag_errors, jobj);
        netResult.setMessage(erros);

        if (StringUtils.isEmpty(erros)) {
            if (jobj.has(tag_data) && !StringUtils.isEmpty(jobj.getString(tag_data))) {

                JSONObject userObj = jobj.getJSONObject(tag_data);

                User user = new User();

                user.setHeaderImage(getStringFroJso("avatar", userObj));
                user.setBussness(getStringFroJso("bussness", userObj));
                user.setEmail(getStringFroJso("email", userObj));
                user.setLat(getStringFroJso("lat", userObj));
                user.setLng(getStringFroJso("lng", userObj));

                user.setName(getStringFroJso("name", userObj));
                user.setPhone(getStringFroJso("phone", userObj));

                user.setPostcode(getStringFroJso("postcode", userObj));

                user.setUserId(getStringFroJso("user_id", userObj));
                user.setLevel(getStringFroJso("level", userObj));
                user.setPayPalEmail(getStringFroJso("paypal_email", userObj));
                user.setIsEmailVerify(getStringFroJso("email_verify", userObj));

                try {
                    user.setReview(userObj.getInt("review"));
                } catch (Exception e) {

                }


                JSONArray friendsList = userObj.getJSONArray("friends");

                if (null != friendsList && friendsList.length() > 0) {

                    for (int i = 0, isize = friendsList.length(); i < isize; i++) {

                        JSONObject friendObj = friendsList.getJSONObject(i);


                        User friendUser = new User();

                        friendUser.setHeaderImage(getStringFroJso("avatar", friendObj));
                        friendUser.setBussness(getStringFroJso("bussness", friendObj));
                        friendUser.setEmail(getStringFroJso("email", friendObj));
                        friendUser.setLat(getStringFroJso("lat", friendObj));
                        friendUser.setLng(getStringFroJso("lng", friendObj));

                        friendUser.setName(getStringFroJso("name", friendObj));
                        friendUser.setPhone(getStringFroJso("phone", friendObj));

                        friendUser.setPostcode(getStringFroJso("postcode", friendObj));

                        friendUser.setUserId(getStringFroJso("user_id", friendObj));
                        friendUser.setLevel(getStringFroJso("level", friendObj));
                        friendUser.setPayPalEmail(getStringFroJso("paypal_email", friendObj));

                        try {
                            friendUser.setReview(friendObj.getInt("review"));
                        } catch (Exception e) {

                        }


                        user.addFriend(friendUser);

                    }
                }

                netResult.setData(new Object[]{user});

            }
        }
        return netResult;
    }


    public static NetResult parsePaypalInfo(InputStream jsonInputStream) throws JSONException {


        JSONObject jobj = new JSONObject(getJsonFromInputStream(jsonInputStream));
        LogUtil.i(App.tag, jobj.toString());
        NetResult netResult = new NetResult();

        String erros = getStringFroJso(tag_errors, jobj);
        netResult.setMessage(erros);

        if (StringUtils.isEmpty(erros)) {

            String paypalEmail = jobj.getJSONObject(tag_data).getString("paypal_email");

            netResult.setData(new Object[]{paypalEmail});
        }


        return netResult;

    }


    public static NetResult parsePreArovalKey(InputStream jsonInputStream) throws JSONException {

        JSONObject jobj = new JSONObject(getJsonFromInputStream(jsonInputStream));
        LogUtil.i(App.tag, jobj.toString());
        NetResult netResult = new NetResult();

        String tagErrors = "Errors";

        if ("[]".equals(jobj.getString(tagErrors)) || null == jobj.getString(tagErrors)) {
            String preapprovalKey = jobj.getString("PreapprovalKey");
            netResult.setData(new Object[]{preapprovalKey});
        } else {
            netResult.setMessage(jobj.getString(tagErrors));
        }
        return netResult;
    }


    public static NetResult changePwd(InputStream jsonInputStream) throws JSONException {

        JSONObject jobj = new JSONObject(getJsonFromInputStream(jsonInputStream));
        LogUtil.i(App.tag, jobj.toString());
        NetResult netResult = new NetResult();

        String erros = getStringFroJso(tag_errors, jobj);
        netResult.setMessage(erros);
        return netResult;
    }

    public static NetResult parseVerifyToken(InputStream jsonInputStream) throws JSONException {

        JSONObject jobj = new JSONObject(getJsonFromInputStream(jsonInputStream));
        LogUtil.i(App.tag, jobj.toString());
        NetResult netResult = new NetResult();
        String erros = getStringFroJso(tag_errors, jobj);
        netResult.setMessage(erros);

        if (StringUtils.isEmpty(erros)) {

            JSONObject dataObj = jobj.getJSONObject(tag_data);

            String token = getStringFroJso(tag_token, dataObj);

            User user = new User();

            user.setEmail(getStringFroJso("email", dataObj));
            user.setLevel(getStringFroJso("level", dataObj));
            user.setName(getStringFroJso("name", dataObj));
            user.setId(getStringFroJso("id", dataObj));

            App.getApp().setToken(getStringFroJso("api_token", dataObj));

            netResult.setData(new Object[]{user});

            App.getApp().setToken(token);


        }

        return netResult;
    }


    public static NetResult parseUpload(String json) throws JSONException {

//      {"errors":"","data":"http:\/\/192.168.1.121:8000\/avatar\/6.png"}
        LogUtil.i(App.tag, "result upload json:" + json);
        NetResult netResult = new NetResult();

        JSONObject jobj = new JSONObject(json);

        String erros = jobj.getString(tag_errors);
        netResult.setMessage(erros);
        if (StringUtils.isEmpty(erros)) {

            JSONObject dataObj = jobj.getJSONObject("data");

            netResult.setData(new Object[]{dataObj.getString("avatar")});
        }

        return netResult;
    }


    public static NetResult parseHistory(InputStream jsonInputStream) throws JSONException {


        JSONObject jobj = new JSONObject(getJsonFromInputStream(jsonInputStream));

        LogUtil.i(App.tag, "history:" + jobj.toString());
        NetResult netResult = new NetResult();
        String erros = jobj.getString(tag_errors);
        netResult.setMessage(erros);

        if (StringUtils.isEmpty(erros)) {
            JSONObject dataObj = jobj.getJSONObject("data");

            if (null != dataObj.get(tag_data)) {

                ArrayList<TransAction> transList = new ArrayList<>();
                netResult.setData(new Object[]{transList});

                JSONArray historyArray = dataObj.getJSONArray(tag_data);

                if (null != historyArray && historyArray.length() > 0) {

                    for (int i = 0, isize = historyArray.length(); i < isize; i++) {

                        JSONObject hisObj = historyArray.getJSONObject(i);

                        TransAction tran = new TransAction();
                        tran.setAmount(getStringFroJso("amount", hisObj));
                        tran.setEmail(getStringFroJso("email", hisObj));
                        tran.setSendUserId(getStringFroJso("send_userid", hisObj));
                        tran.setCreated_at(getStringFroJso("created_at", hisObj));

                        transList.add(tran);

                        String hisStrings = hisObj.toString().replace("\\", "");

                        hisStrings = hisStrings.replace("\"[", "[");
                        hisStrings = hisStrings.replace("]\"", "]");

                        JSONObject hisObjs = new JSONObject(hisStrings);


                        String receivers = hisObjs.getString("receives");


//                        JSONArray receives = hisObjs.getJSONArray("receives");

                        JSONArray receives = new JSONArray(receivers);

                        if (null != receives && receives.length() > 0) {

                            for (int j = 0, jsize = receives.length(); j < jsize; j++) {


                                JSONObject receObj = receives.getJSONObject(j);

                                User recevier = new User();
                                recevier.setEmail(getStringFroJso("email", receObj));
                                recevier.setName(getStringFroJso("name", receObj));
                                recevier.setUserId(getStringFroJso("userid", receObj));
                                recevier.setAmount(getStringFroJso("amount", receObj));

                                tran.addReceivers(recevier);
                            }
                        }

                    }

                }
            }

        }

        return netResult;
    }

    public static NetResult parseHistory2(InputStream jsonInputStream) throws JSONException {

        JSONObject jobj = new JSONObject(getJsonFromInputStream(jsonInputStream));
        LogUtil.i(App.tag, "history2:" + jobj.toString());
        NetResult netResult = new NetResult();
        String erros = jobj.getString(tag_errors);
        netResult.setMessage(erros);
        if (StringUtils.isEmpty(erros)) {

            JSONObject rootObject = jobj.getJSONObject(tag_data);
            int total = rootObject.getInt("total");


            float amountTotalin = 0;
            float amountTotalout = 0;


            amountTotalin = Float.parseFloat(jobj.getString("amount_in"));
            amountTotalout = Float.parseFloat(jobj.getString("amount_out"));


            JSONArray dataList = rootObject.getJSONArray(tag_data);


            ArrayList<TransActionMore> transActionMoreArrayList = new ArrayList<>();

            if (null != dataList && dataList.length() != 0) {

                for (int i = 0, isize = dataList.length(); i < isize; i++) {

                    JSONObject jdetail = dataList.getJSONObject(i);


                    TransActionMore transActionMore = new TransActionMore();

                    transActionMore.setAmount(getStringFroJso("amount", jdetail));

                    transActionMore.setCreateAt(getStringFroJso("created_at", jdetail));

                    transActionMore.setMtTime(getStringFroJso("mtime", jdetail));

                    transActionMore.setStatus(getStringFroJso("status", jdetail));

                    JSONObject profileObj = jdetail.getJSONObject("user").getJSONObject("profile");
                    transActionMore.setSendder(parseTransUser(profileObj));


                    JSONArray details = jdetail.getJSONArray("details");
                    if (null != details && details.length() > 0) {

                        for (int j = 0, jsize = details.length(); j < jsize; j++) {

                            User user = parseTransUser(details.getJSONObject(j).getJSONObject("user").getJSONObject("profile"));
                            if (j == 0) {
                                transActionMore.setReceiverUser1(user);
                            }
                            if (j == 1) {
                                transActionMore.setReceiverUser2(user);
                            }
                        }
                    }

//                  transActionMore.setReceiverUser1(parseTransUser(jdetail.getJSONObject("user")));


//                    try {
//                        JSONObject user2 = jdetail.getJSONArray("details").getJSONObject(0).getJSONObject("user");
//                        transActionMore.setReceiverUser2(parseTransUser(user2));
//                    } catch (Exception e) {
//
//                    }

                    transActionMoreArrayList.add(transActionMore);
                }

            }

            netResult.setData(new Object[]{transActionMoreArrayList, total, amountTotalin, amountTotalout});
        }

        return netResult;
    }


    public static NetResult parseHistory3(InputStream jsonInputStream) throws JSONException {

        JSONObject jobj = new JSONObject(getJsonFromInputStream(jsonInputStream));
        LogUtil.i(App.tag, "history3:" + jobj.toString());
        NetResult netResult = new NetResult();
        String erros = jobj.getString(tag_errors);
        netResult.setMessage(erros);


        ArrayList<TransActionMore> totalAllHistorys = new ArrayList<>();

        ArrayList<TransActionMore> transActionMoreArrayListin = new ArrayList<>();
        ArrayList<TransActionMore> transActionMoreArrayListout = new ArrayList<>();

        int totalIn = 0;
        int totalOut = 0;

        float amountTotalin = 0;
        float amountTotalout = 0;
        int totalItem = 0;


        if (StringUtils.isEmpty(erros)) {

            JSONObject datasObject = jobj.getJSONObject(tag_data);

//            int total = rootObject.getInt("total");



            try {
                amountTotalin = Float.parseFloat(jobj.getString("amount_in"));
            }catch (Exception e){
                amountTotalin=0;
            }


            try {
                amountTotalout = Float.parseFloat(jobj.getString("amount_out"));
            }catch (Exception e){
                amountTotalout=0;
            }




            JSONObject inObject = datasObject.getJSONObject("in");
            JSONObject outObject = datasObject.getJSONObject("out");


            //=========================
            JSONArray dataList = inObject.getJSONArray(tag_data);//in
            totalIn = inObject.getInt("total");


            if (null != dataList && dataList.length() != 0) {

                for (int i = 0, isize = dataList.length(); i < isize; i++) {

                    JSONObject jdetail = dataList.getJSONObject(i);


                    TransActionMore transActionMore = new TransActionMore();

                    transActionMore.setAmount(getStringFroJso("amount", jdetail));
                    transActionMore.setCreateAt(getStringFroJso("created_at", jdetail));
//                    transActionMore.setMtTime(getStringFroJso("mtime", jdetail));
                    transActionMore.setStatus(getStringFroJso("status", jdetail));

                    JSONObject profileObj = jdetail.getJSONObject("user").getJSONObject("profile");


                    transActionMore.setSendder(parseTransUser(profileObj));


                    JSONArray details = jdetail.getJSONArray("details");


                    if (null != details && details.length() > 0) {

                        for (int j = 0, jsize = details.length(); j < jsize; j++) {

                            JSONObject detailObj = details.getJSONObject(j);
                            String amout = getStringFroJso("amount", detailObj);

                            User user = parseTransUser(detailObj.getJSONObject("user").getJSONObject("profile"));
                            user.setAmount2me(amout);

                            if (j == 0) {
                                transActionMore.setReceiverUser1(user);
                            }
                            if (j == 1) {
                                transActionMore.setReceiverUser2(user);
                            }


                        }
                    }

                    transActionMoreArrayListin.add(transActionMore);
                }

            }

//=========================
            JSONArray dataListout = outObject.getJSONArray(tag_data);//in
            totalOut = outObject.getInt("total");


            if (null != dataListout && dataListout.length() != 0) {

                for (int i = 0, isize = dataListout.length(); i < isize; i++) {

                    JSONObject jdetail = dataListout.getJSONObject(i);


                    TransActionMore transActionMore = new TransActionMore();

                    transActionMore.setAmount(getStringFroJso("amount", jdetail));
                    transActionMore.setCreateAt(getStringFroJso("created_at", jdetail));
//                    transActionMore.setMtTime(getStringFroJso("mtime", jdetail));
                    transActionMore.setStatus(getStringFroJso("status", jdetail));

                    JSONObject profileObj = jdetail.getJSONObject("user").getJSONObject("profile");
                    transActionMore.setSendder(parseTransUser(profileObj));


                    JSONArray details = jdetail.getJSONArray("details");
                    if (null != details && details.length() > 0) {

                        for (int j = 0, jsize = details.length(); j < jsize; j++) {

                            User user = parseTransUser(details.getJSONObject(j).getJSONObject("user").getJSONObject("profile"));
                            if (j == 0) {
                                transActionMore.setReceiverUser1(user);
                            }
                            if (j == 1) {
                                transActionMore.setReceiverUser2(user);
                            }
                        }
                    }
                    transActionMoreArrayListout.add(transActionMore);
                }

            }


            totalAllHistorys.addAll(transActionMoreArrayListin);
            totalAllHistorys.addAll(transActionMoreArrayListout);


//            Collections.sort(totalAllHistorys, new Comparator<TransActionMore>() {
//                @Override
//                public int compare(TransActionMore lhs, TransActionMore rhs) {
//
//                    long l = Long.parseLong(lhs.getMtTime());
//                    long r = Long.parseLong(rhs.getMtTime());
//
//                    return l <= r ? 1 : -1;
//                }
//            });


            totalItem = totalIn + totalOut;


        }

        netResult.setData(new Object[]{totalAllHistorys, totalItem, amountTotalin, amountTotalout,});

        return netResult;
    }

    private static User parseTransUser(JSONObject jsonObject) {
        try {
            User user = new User();
            user.setUserId(getStringFroJso("user_id", jsonObject));
            user.setName(getStringFroJso("name", jsonObject));
            user.setEmail(getStringFroJso("email", jsonObject));
            user.setPayPalEmail(getStringFroJso("paypal_email", jsonObject));
            user.setHeaderImage(getStringFroJso("avatar", jsonObject));
            user.setPhone(getStringFroJso("phone", jsonObject));
            user.setBussness(getStringFroJso("bussness", jsonObject));
            user.setPostcode(getStringFroJso("postcode", jsonObject));
            user.setLat(getStringFroJso("lat", jsonObject));
            user.setLng(getStringFroJso("lng", jsonObject));
            user.setIsEmailVerify(getStringFroJso("email_verify", jsonObject));
            user.setCountry(getStringFroJso("country", jsonObject));
            user.setCurrency(getStringFroJso("currency", jsonObject));
            user.setLevel(getStringFroJso("level", jsonObject));

            try {
                user.setReview(jsonObject.getInt("review"));
            } catch (Exception e) {
            }


            return user;
        } catch (Exception e) {
            LogUtil.e(App.tag, "parse transe error:" + e.getLocalizedMessage());
        }

        return null;
    }


    public static String getJsonFromInputStream(InputStream ins) {
        if (null != ins) {
            StringBuffer sbf = new StringBuffer();
            BufferedInputStream bfris = new BufferedInputStream(ins);
            byte[] buffer = new byte[512];
            int ret = -1;
            try {
                while ((ret = bfris.read(buffer)) != -1) {
                    sbf.append(new String(buffer, 0, ret));
                }
                bfris.close();
                ins.close();
                return sbf.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    public static String getTextFromInputStream(InputStream ins) {
        if (null != ins) {
            StringBuffer sbf = new StringBuffer();
            BufferedInputStream bfris = new BufferedInputStream(ins);
            byte[] buffer = new byte[512];
            int ret = -1;
            try {
                while ((ret = bfris.read(buffer)) != -1) {
                    sbf.append(new String(buffer, 0, ret));
                }
                bfris.close();
                ins.close();
                return sbf.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String getStringFroJso(String key, JSONObject jobj) throws JSONException {
        if (jobj.has(key)) {

            String result = jobj.getString(key);

            if ("null".equals(result)) {
                return "";
            } else {
                return result;
            }

        } else {
            return "";
        }
    }

}
