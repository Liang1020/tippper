package com.tipppr.tipppr.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.tipppr.android.R;

/**
 * Created by wangzy on 16/1/27.
 */
public class DialogVerifyTip extends MyBasePicker {

    private Context context;
    private OnVerifyTipListener onVerifyTipListener;

    public DialogVerifyTip(final Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_verify_tip, contentContainer);
        this.context = context;

        findViewById(R.id.buttonLater).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                if (null != onVerifyTipListener) {
                    onVerifyTipListener.onLaterClick();
                }
            }
        });

        findViewById(R.id.buttonEmailSend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();
                if (null != onVerifyTipListener) {
                    onVerifyTipListener.onEmailVeriyClick();
                }
            }
        });
    }


    public static interface OnVerifyTipListener {

        public void onEmailVeriyClick();

        public void onLaterClick();
    }

    public OnVerifyTipListener getOnVerifyTipListener() {
        return onVerifyTipListener;
    }

    public void setOnVerifyTipListener(OnVerifyTipListener onVerifyTipListener) {
        this.onVerifyTipListener = onVerifyTipListener;
    }
}
