package com.tipppr.tipppr.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.common.util.ImageUtils;
import com.tipppr.android.R;

/**
 * Created by wangzy on 16/1/26.
 */
public class HexImageView extends ImageView {

    public HexImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public void setImageResource(int resId) {
        Bitmap bitmap = BitmapFactory.decodeResource(getContext().getResources(), resId);
        setImageBitmap(bitmap);

    }


    @Override
    public void setImageBitmap(Bitmap bm) {
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);

//        LogUtil.i(App.tag, "setImageBitmap on url...");

        if (null != bm) {

            Bitmap result = ImageUtils.clipPatchImageNorecyleOriginal(bm, BitmapFactory.decodeResource(getContext().getResources(), R.drawable.bg_mask_header));

            if (null != result) {
                super.setImageBitmap(result);
            } else {
//                LogUtil.i(App.tag, "setImageBitmap on url.err2..");
            }

        } else {
//            LogUtil.i(App.tag, "setImageBitmap on url.err1..");
        }

    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
