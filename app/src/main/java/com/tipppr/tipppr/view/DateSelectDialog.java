package com.tipppr.tipppr.view;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.MySimpleTimePickerView;
import com.bigkoo.pickerview.TimePickerView;
import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.util.LogUtil;
import com.common.util.Tool;
import com.tipppr.android.App;
import com.tipppr.android.R;

import java.util.Date;

/**
 * Created by wangzy on 16/3/24.
 */
public class DateSelectDialog extends MyBasePicker {


    LinearLayout linearLayoutDateStart;
    LinearLayout linearLayoutDateEnd;
    MySimpleTimePickerView timePickerViewStart = null;
    MySimpleTimePickerView timePickerViewEnd = null;

    private OnDateSelectListener onDateSelectListener;

    public DateSelectDialog(final Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.activity_date_picker_select, contentContainer);

        linearLayoutDateStart = (LinearLayout) findViewById(R.id.linearLayoutDateStart);

        timePickerViewStart = new MySimpleTimePickerView(context, TimePickerView.Type.YEAR_MONTH_DAY);

        LinearLayout.LayoutParams layoutParamsStart = new LinearLayout.LayoutParams(-1, -1);
        linearLayoutDateStart.addView(timePickerViewStart.getRootView(), layoutParamsStart);


        timePickerViewEnd = new MySimpleTimePickerView(context, TimePickerView.Type.YEAR_MONTH_DAY);

        LinearLayout.LayoutParams layoutParamsEnd = new LinearLayout.LayoutParams(-1, -1);

        linearLayoutDateEnd = (LinearLayout) findViewById(R.id.linearLayoutDateEnd);
        linearLayoutDateEnd.addView(timePickerViewEnd.getRootView(), layoutParamsEnd);

        findViewById(R.id.viewBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        findViewById(R.id.buttonSetDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date dateStart = timePickerViewStart.getTime();
                Date dateEnd = timePickerViewEnd.getTime();

                if (dateEnd.getTime() < dateStart.getTime()) {
                    Tool.showMessageDialog("End time must after start time! ", (Activity) context);
                    return;
                }
                if (null != onDateSelectListener) {
                    onDateSelectListener.onDateSelect(dateStart, dateEnd);

                }
                LogUtil.i(App.tag, "date Start:" + dateStart.toString());
                LogUtil.i(App.tag, "date End:" + dateEnd.toString());
                dismiss();

            }
        });

    }

    public void setDate(Date dateStart, Date dateEnd) {

        timePickerViewStart.setTime(dateStart);
        timePickerViewEnd.setTime(dateEnd);

    }

    public static interface OnDateSelectListener {
        public void onDateSelect(Date date1, Date date2);
    }

    public OnDateSelectListener getOnDateSelectListener() {
        return onDateSelectListener;
    }

    public void setOnDateSelectListener(OnDateSelectListener onDateSelectListener) {
        this.onDateSelectListener = onDateSelectListener;
    }
}
