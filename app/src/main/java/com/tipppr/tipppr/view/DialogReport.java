package com.tipppr.tipppr.view;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.BaseActivity;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.net.NetInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by wangzy on 16/1/27.
 */
public class DialogReport extends MyBasePicker {


    private EditText editTextReportContent;
    private TextView textViewFraudulent;
    private TextView textViewUnethical;
    private TextView textViewAbusive;
    private ArrayList<TextView> textViews;
    private Context context;
    private String tag;
    private String userId;


    public DialogReport(final Context context, final String userId) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_report, contentContainer);
        this.context = context;
        this.userId = userId;

        this.textViews = new ArrayList<>();
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkText((TextView) v);
            }
        };


        editTextReportContent = (EditText) findViewById(R.id.editTextReportContent);
        editTextReportContent.setOnClickListener(onClickListener);

        textViewFraudulent = (TextView) findViewById(R.id.textViewFraudulent);
        textViewFraudulent.setOnClickListener(onClickListener);
        textViews.add(textViewFraudulent);

        textViewAbusive = (TextView) findViewById(R.id.textViewAbusive);
        textViewAbusive.setOnClickListener(onClickListener);
        textViews.add(textViewAbusive);

        textViewUnethical = (TextView) findViewById(R.id.textViewUnethical);
        textViewUnethical.setOnClickListener(onClickListener);
        textViews.add(textViewUnethical);

        findViewById(R.id.viewCloseDialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editTextReportContent.getWindowToken(), 0);
                dismiss();
            }
        });

        findViewById(R.id.textViewSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String content = ((BaseActivity) context).getInput(editTextReportContent);
                if (StringUtils.isEmpty(content)) {
                    Tool.showMessageDialog("Please fill content!", (Activity) context);
                    return;
                }

                if (StringUtils.isEmpty(tag)) {
                    Tool.showMessageDialog("Please choose type!", (Activity) context);
                    return;
                }


                try {

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("content", content);
                    jsonObject.put("rtype", tag);
                    jsonObject.put("userid", userId);

                    reportUser(true, jsonObject);

                } catch (JSONException e) {
                    e.printStackTrace();
                    LogUtil.e(App.tag, "report user error:" + e.getLocalizedMessage());
                }


            }
        });

        findViewById(R.id.linearLayoutOuter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseTippprActivity)context).closeSoftWareKeyBoradInBase();
            }
        });
//        contentContainer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                ((BaseTippprActivity)context).closeSoftWareKeyBoradInBase();
//            }
//        });
    }


    BaseTask reportTask = null;

    public void reportUser(final boolean showDialog, final JSONObject jsonObject) {

        if (null != reportTask && reportTask.getStatus() == AsyncTask.Status.RUNNING) {
            reportTask.cancel(true);
        }

        reportTask = new BaseTask(context, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    baseTask.showDialogForSelf(true);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.reportUser(context, jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (showDialog) {
                    baseTask.hideDialogSelf();
                }

                if (null != result) {

                    if (StringUtils.isEmpty(result.getMessage())) {

                        Tool.showMessageDialog("Success!", "Report successful", (Activity) context, new DialogCallBackListener() {
                            @Override
                            public void onDone(boolean yesOrNo) {

                                dismiss();
                            }
                        });


                    } else {
                        Tool.showMessageDialog(result.getMessage(), (Activity) context);
                    }

                } else {

                    Tool.showMessageDialog("Net error!", (Activity) context);
                }
            }
        });

        reportTask.execute(new HashMap<String, String>());


    }


    public void checkText(TextView tv) {
        for (TextView tvs : textViews) {
            if (tv.getId() == tvs.getId()) {
                tvs.setTextColor(context.getResources().getColor(R.color.blue_text_color));
                this.tag = String.valueOf(tv.getTag());
            } else {
                tvs.setTextColor(context.getResources().getColor(R.color.gray_text_color));
            }
        }

    }
}
