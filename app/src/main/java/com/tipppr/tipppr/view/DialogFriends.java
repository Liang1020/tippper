package com.tipppr.tipppr.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.util.LogUtil;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.adapter.FriendAdapter;
import com.tipppr.tipppr.bean.User;

/**
 * Created by wangzy on 16/1/27.
 */
public class DialogFriends extends MyBasePicker {


    private ListView friendList;
    private FriendAdapter friendAdapter;
    private OnContinuClickListener onContinuClickListener;
    private User selectUser;


    public DialogFriends(final Context context, final User user) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_friends, contentContainer);
        this.context = context;
        this.friendList = (ListView) findViewById(R.id.friendList);

        this.friendAdapter = new FriendAdapter(context, true, user.getAllFriends());
        friendList.setAdapter(friendAdapter);

        friendList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectUser = (User) parent.getAdapter().getItem(position);
                friendAdapter.selectIndex(position);
                LogUtil.e(App.tag, "click ...");

            }
        });

        findViewById(R.id.viewCloseDialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        findViewById(R.id.viewContinue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onContinuClickListener) {
                    onContinuClickListener.onClickDone(selectUser);
                }
            }
        });

    }


    public static interface OnContinuClickListener {

        public void onClickDone(User user);

    }

    public OnContinuClickListener getOnContinuClickListener() {
        return onContinuClickListener;
    }

    public void setOnContinuClickListener(OnContinuClickListener onContinuClickListener) {
        this.onContinuClickListener = onContinuClickListener;
    }
}
