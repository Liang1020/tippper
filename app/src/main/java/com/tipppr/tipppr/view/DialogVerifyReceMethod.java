package com.tipppr.tipppr.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.tipppr.android.R;

/**
 * Created by wangzy on 16/3/30.
 */
public class DialogVerifyReceMethod extends MyBasePicker {


    private TextView textViewTitleDesc;
    private TextView textViewReceiveMethod;
    private TextView textViewEdit;
    private TextView textViewDel;
    private TextView textViewCancel;
    private String title;
    private String method;
    private OnDialogEventListener onDialogEventListener;

    public DialogVerifyReceMethod(Context context, String title, String method) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_receive_method, contentContainer);

        this.textViewTitleDesc = (TextView) findViewById(R.id.textViewTitleDesc);
        this.textViewReceiveMethod = (TextView) findViewById(R.id.textViewReceiveMethod);

        this.textViewEdit = (TextView) findViewById(R.id.textViewEdit);
        this.textViewDel = (TextView) findViewById(R.id.textViewDel);
        this.textViewCancel = (TextView) findViewById(R.id.textViewCancel);

        this.title = title;
        this.method = method;

        this.textViewTitleDesc.setText(title);
        this.textViewReceiveMethod.setText(method2Label(method));

        if ("receiving method".equalsIgnoreCase(title)) {
            this.textViewDel.setVisibility(View.GONE);
        } else {
            this.textViewDel.setVisibility(View.VISIBLE);
            this.textViewEdit.setVisibility(View.GONE);
        }

        this.textViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });


        this.textViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();
                if (null != onDialogEventListener) {
                    onDialogEventListener.onEditClick();
                }
            }
        });


        this.textViewDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                if (null != onDialogEventListener) {
                    onDialogEventListener.onDelClick();
                }
            }
        });


    }


    private String method2Label(final String method) {

        switch (method){

            case "bank_transfer":
                return "Bank Transfer";
            case "cheque":
                return "Cheque / Check Payment";
            case "paypal":
                return "Paypal";
            case "tipppr_debit":
                return "Tipppr Debit Card";

        }

        return method;
    }


    public static abstract class OnDialogEventListener {

        public void onDelClick() {
        }

        public void onEditClick() {
        }
    }

    public OnDialogEventListener getOnDialogEventListener() {
        return onDialogEventListener;
    }

    public void setOnDialogEventListener(OnDialogEventListener onDialogEventListener) {
        this.onDialogEventListener = onDialogEventListener;
    }
}
