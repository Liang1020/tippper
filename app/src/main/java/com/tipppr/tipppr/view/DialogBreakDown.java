package com.tipppr.tipppr.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.util.NumberHelper;
import com.tipppr.android.R;

/**
 * Created by wangzy on 16/1/27.
 */
public class DialogBreakDown extends MyBasePicker {


    private SeekBar seekBarBreakDown;
    private float total;

    private TextView textViewPercent;
    private TextView textViewPercent2;

    private TextView textViewMoney1, textViewMoney2;
    private float percent = 0.75f;
    private OnDoneClickListener onDoneClickListener;


    public DialogBreakDown(Context context, final float total, final float percentIn) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_break_down, contentContainer);
        findViewById(R.id.textViewDone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (null != onDoneClickListener) {
                    onDoneClickListener.onDoneClick(percent);
                }

                dismiss();
            }
        });

        this.percent = percentIn;
        this.total = total;

        textViewPercent = (TextView) findViewById(R.id.textViewPercent);
        textViewPercent2 = (TextView) findViewById(R.id.textViewPercent2);


        ((TextView) findViewById(R.id.textViewAmount)).setText("$" + NumberHelper.keepDecimal2(String.valueOf(this.total)));
        textViewMoney1 = (TextView) findViewById(R.id.textViewMoney1);
        textViewMoney2 = (TextView) findViewById(R.id.textViewMoney2);

        seekBarBreakDown = (SeekBar) findViewById(R.id.seekBarBreakDown);

        seekBarBreakDown.setProgress((int) (100 * (percent)));

        seekBarBreakDown.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                percent = progress / 100.0f;
                textViewPercent.setText(progress + "%");
                textViewPercent2.setText((100 - progress) + "%");

                float m1 = total * (percent);

                textViewMoney1.setText("$" + NumberHelper.keepDecimal2(String.valueOf(m1)));
                textViewMoney2.setText("$" + NumberHelper.keepDecimal2(String.valueOf((total - m1))));

//                if (null != onDoneClickListener) {
//                    onDoneClickListener.onDoneClick(percent);
//                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        showDefault();
    }

    private void showDefault() {
        textViewPercent.setText((int) (percent * 100) + "%");
        textViewPercent2.setText((int) (100 - percent * 100) + "%");

        float m1 = total * (percent);
        textViewMoney1.setText("$" + NumberHelper.keepDecimal2(String.valueOf(m1)));
        textViewMoney2.setText("$" + NumberHelper.keepDecimal2(String.valueOf((total - m1))));
    }

    public static interface OnDoneClickListener {

        public void onDoneClick(float percent);
    }

    public OnDoneClickListener getOnDoneClickListener() {
        return onDoneClickListener;
    }

    public void setOnDoneClickListener(OnDoneClickListener onDoneClickListener) {
        this.onDoneClickListener = onDoneClickListener;
    }
}
