package com.tipppr.tipppr.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.tipppr.android.R;

/**
 * Created by wangzy on 16/3/30.
 */
public class DialogVerifyNotify extends MyBasePicker {

    public OnClickOkListener onClickOkListener;

    public DialogVerifyNotify(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_verify_notify, contentContainer);

        findViewById(R.id.textViewSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onClickOkListener) {
                    onClickOkListener.onVerifyClick();
                }
            }
        });

        findViewById(R.id.viewCloseDialog).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        findViewById(R.id.textViewCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();
            }
        });
    }


    public static interface OnClickOkListener {
        public void onVerifyClick();

    }

    public OnClickOkListener getOnClickOkListener() {
        return onClickOkListener;
    }

    public void setOnClickOkListener(OnClickOkListener onClickOkListener) {
        this.onClickOkListener = onClickOkListener;
    }
}
