package com.tipppr.tipppr.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.OptionsPickerViewSimple;
import com.bigkoo.pickerview.view.BasePickerView;
import com.common.util.StringUtils;
import com.tipppr.android.R;

import java.util.ArrayList;

/**
 * Created by wangzy on 16/4/1.
 */
public class DialogSingleOption extends BasePickerView {


    private LinearLayout linearLayoutCountry;
    private LinearLayout linearLayoutCurrency;

    private OptionsPickerViewSimple optionsCurrency;

    private OnOptionSelectDoneListener onOptionSelectDoneListener;

    private ArrayList<String> arrayListCurrecy = new ArrayList<>();


    public DialogSingleOption(Context context, String[] options, String currentSelecet) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_country_currency, contentContainer);

        linearLayoutCountry = (LinearLayout) findViewById(R.id.linearLayoutCountry);
        linearLayoutCountry.setVisibility(View.GONE);

        linearLayoutCurrency = (LinearLayout) findViewById(R.id.linearLayoutCurrency);


        //====================================

        optionsCurrency = new OptionsPickerViewSimple(context);


        for (String option : options) {
            arrayListCurrecy.add(option);
        }


        optionsCurrency.setPicker(arrayListCurrecy);

        optionsCurrency.setCyclic(false);

        linearLayoutCurrency.addView(optionsCurrency.getRootView());



        for (int i = 0, isize = arrayListCurrecy.size(); i < isize; i++) {

            if (StringUtils.isEmpty(currentSelecet)) {
                break;
            } else if (currentSelecet.equals(arrayListCurrecy.get(i))) {

                optionsCurrency.setSelectOptions(i);
                break;
            }

        }



        findViewById(R.id.textViewCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        findViewById(R.id.textViewDone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onOptionSelectDoneListener) {
                    int currency = optionsCurrency.getCurrent();
                    onOptionSelectDoneListener.onSelectDonw(arrayListCurrecy.get(currency));
                }
            }
        });


    }


    public void setTitle(String title) {
        ((TextView) contentContainer.findViewById(R.id.textViewTitle)).setText(title);
    }

    public static interface OnOptionSelectDoneListener {

        public void onSelectDonw(String seelct);

    }

    public OnOptionSelectDoneListener getOnOptionSelectDoneListener() {
        return onOptionSelectDoneListener;
    }

    public void setOnOptionSelectDoneListener(OnOptionSelectDoneListener onOptionSelectDoneListener) {
        this.onOptionSelectDoneListener = onOptionSelectDoneListener;
    }
}
