package com.tipppr.tipppr.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.OptionsPickerViewSimple;
import com.bigkoo.pickerview.view.BasePickerView;
import com.common.util.StringUtils;
import com.tipppr.android.R;

import java.util.ArrayList;

/**
 * Created by wangzy on 16/4/1.
 */
public class DialogCountryCurrency extends BasePickerView {


    private LinearLayout linearLayoutCountry;
    private LinearLayout linearLayoutCurrency;

    private OptionsPickerViewSimple optionsPickerView;
    private OptionsPickerViewSimple optionsCurrency;

    private OnOptionSelectDoneListener onOptionSelectDoneListener;

    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayList<String> arrayListCurrecy = new ArrayList<>();

    public DialogCountryCurrency(Context context, String countryCurrency) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_country_currency, contentContainer);

        linearLayoutCountry = (LinearLayout) findViewById(R.id.linearLayoutCountry);
        linearLayoutCurrency = (LinearLayout) findViewById(R.id.linearLayoutCurrency);


        optionsPickerView = new OptionsPickerViewSimple(context);

        arrayList.add("USA");
        arrayList.add("Canada");
        arrayList.add("Australia");

        optionsPickerView.setPicker(arrayList);
        optionsPickerView.setCyclic(false);


        linearLayoutCountry.addView(optionsPickerView.getRootView());

        //====================================

        optionsCurrency = new OptionsPickerViewSimple(context);

        arrayListCurrecy.add("USD");
        arrayListCurrecy.add("CAD");
        arrayListCurrecy.add("AUD");
        optionsCurrency.setPicker(arrayListCurrecy);

        optionsCurrency.setCyclic(false);

        linearLayoutCurrency.addView(optionsCurrency.getRootView());


        if (!StringUtils.isEmpty(countryCurrency)) {
            String[] countryCurrencs = countryCurrency.split("/");
            for (int i = 0, isize = arrayList.size(); i < isize; i++) {
                if (countryCurrencs[0].equals(arrayList.get(i))) {
                    optionsPickerView.setSelectOptions(i);
                    break;
                }
            }
            for (int i = 0, isize = arrayListCurrecy.size(); i < isize; i++) {
                if (countryCurrencs[1].equals(arrayListCurrecy.get(i))) {
                    optionsCurrency.setSelectOptions(i);
                    break;
                }
            }


        }


        findViewById(R.id.textViewCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        findViewById(R.id.textViewDone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();

                if (null != onOptionSelectDoneListener) {

                    int country = optionsPickerView.getCurrent();
                    int currency = optionsCurrency.getCurrent();

                    onOptionSelectDoneListener.onSelectDonw(arrayList.get(country), arrayListCurrecy.get(currency));
                }
            }
        });


    }



    public DialogCountryCurrency(Context context, String countryCurrency,String[] country,String[] currency) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_country_currency, contentContainer);

        linearLayoutCountry = (LinearLayout) findViewById(R.id.linearLayoutCountry);
        linearLayoutCurrency = (LinearLayout) findViewById(R.id.linearLayoutCurrency);


        optionsPickerView = new OptionsPickerViewSimple(context);


        for(String coun:country){
            arrayList.add(coun);
        }


        optionsPickerView.setPicker(arrayList);
        optionsPickerView.setCyclic(false);


        linearLayoutCountry.addView(optionsPickerView.getRootView());

        //====================================

        optionsCurrency = new OptionsPickerViewSimple(context);


        for(String curr:currency){
            arrayListCurrecy.add(curr);
        }

        optionsCurrency.setPicker(arrayListCurrecy);

        optionsCurrency.setCyclic(false);

        linearLayoutCurrency.addView(optionsCurrency.getRootView());


        if (!StringUtils.isEmpty(countryCurrency)) {
            String[] countryCurrencs = countryCurrency.split("/");
            for (int i = 0, isize = arrayList.size(); i < isize; i++) {
                if (countryCurrencs[0].equals(arrayList.get(i))) {
                    optionsPickerView.setSelectOptions(i);
                    break;
                }
            }
            for (int i = 0, isize = arrayListCurrecy.size(); i < isize; i++) {
                if (countryCurrencs[1].equals(arrayListCurrecy.get(i))) {
                    optionsCurrency.setSelectOptions(i);
                    break;
                }
            }


        }


        findViewById(R.id.textViewCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        findViewById(R.id.textViewDone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();

                if (null != onOptionSelectDoneListener) {

                    int country = optionsPickerView.getCurrent();
                    int currency = optionsCurrency.getCurrent();

                    onOptionSelectDoneListener.onSelectDonw(arrayList.get(country), arrayListCurrecy.get(currency));
                }
            }
        });


    }


    public static interface OnOptionSelectDoneListener {

        public void onSelectDonw(String country, String currency);

    }

    public OnOptionSelectDoneListener getOnOptionSelectDoneListener() {
        return onOptionSelectDoneListener;
    }

    public void setOnOptionSelectDoneListener(OnOptionSelectDoneListener onOptionSelectDoneListener) {
        this.onOptionSelectDoneListener = onOptionSelectDoneListener;
    }
}
