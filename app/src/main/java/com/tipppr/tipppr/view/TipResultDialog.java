package com.tipppr.tipppr.view;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.tipppr.android.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by wangzy on 16/1/27.
 */
public class TipResultDialog extends MyBasePicker {


    private boolean isSuccess = false;
    private ImageView imageViewPayResultBigImg;
    private ImageView imageViewIconFace;
    private TextView textViewTipText;
    private Timer timer = null;

    public TipResultDialog(final Context context, boolean isSuccess) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_tip_result, contentContainer);
        this.isSuccess = isSuccess;

        imageViewPayResultBigImg = (ImageView) findViewById(R.id.imageViewPayResultBigImg);
        imageViewIconFace = (ImageView) findViewById(R.id.imageViewIconFace);
        textViewTipText = (TextView) findViewById(R.id.textViewTipText);

        if (!isSuccess) {
            imageViewPayResultBigImg.setImageResource(R.drawable.icon_tip_fail_mask);
            imageViewIconFace.setImageResource(R.drawable.icon_face_fail);
            textViewTipText.setText("Sending tip failed!");
        }

        contentContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (isShowing()) {

                    ((Activity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dismiss();
                        }
                    });

                }
                timer.cancel();
                cancel();
            }
        }, 3 * 1000);


    }


}

