package com.tipppr.trans;

import android.content.Context;
import android.graphics.Bitmap;

import com.example.gaussblurtest.GaussBlur;

/**
 * Created by wangzy on 16/3/22.
 */
public class MyBlurTransform implements com.squareup.picasso.Transformation {


    private static int MAX_RADIUS = 25;
    private static int DEFAULT_DOWN_SAMPLING = 1;

    private Context mContext;

    private int mRadius;
    private int mSampling;

    public MyBlurTransform(Context context) {
        this(context, MAX_RADIUS, DEFAULT_DOWN_SAMPLING);
    }

    public MyBlurTransform(Context context, int radius) {
        this(context, radius, DEFAULT_DOWN_SAMPLING);
    }

    public MyBlurTransform(Context context, int radius, int sampling) {
        mContext = context.getApplicationContext();
        mRadius = radius;
        mSampling = sampling;
    }


    GaussBlur gaussBlur = null;

    @Override
    public Bitmap transform(Bitmap source) {

        gaussBlur = new GaussBlur();

        int radius = 10;

      Bitmap bitmap = gaussBlur.gaussBlur1(source, radius);

//        Bitmap bitmap = GaussBlur.fastblur(mContext,source,radius);

        source.recycle();


//        int scaledWidth = source.getWidth() / mSampling;
//        int scaledHeight = source.getHeight() / mSampling;
//
//        Bitmap bitmap = Bitmap.createBitmap(scaledWidth, scaledHeight, Bitmap.Config.ARGB_8888);
//
//        Canvas canvas = new Canvas(bitmap);
//        canvas.scale(1 / (float) mSampling, 1 / (float) mSampling);
//        Paint paint = new Paint();
//        paint.setFlags(Paint.FILTER_BITMAP_FLAG);
//        canvas.drawBitmap(source, 0, 0, paint);


//        RenderScript rs = RenderScript.create(mContext);
//        Allocation input = Allocation.createFromBitmap(rs, bitmap, Allocation.MipmapControl.MIPMAP_NONE,
//                Allocation.USAGE_SCRIPT);
//        Allocation output = Allocation.createTyped(rs, input.getType());
//        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
//
//        blur.setInput(input);
//        blur.setRadius(mRadius);
//        blur.forEach(output);
//        output.copyTo(bitmap);
//
//        source.recycle();
//        rs.destroy();

        return bitmap;
    }

    @Override
    public String key() {
        return "BlurTransformation(radius=" + mRadius + ", sampling=" + mSampling + ")";
    }
}
