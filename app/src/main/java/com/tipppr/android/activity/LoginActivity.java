package com.tipppr.android.activity;

import android.os.Bundle;
import android.text.Html;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.callback.OnRequestEndCallBack;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.util.ValidateTool;
import com.push.SendTagsCallBack;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.net.NetInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class LoginActivity extends BaseTippprActivity implements SendTagsCallBack {


    @InjectView(R.id.textViewSignUp)
    TextView textViewSignUp;

    @InjectView(R.id.editTextEmail)
    EditText editTextEmail;

    @InjectView(R.id.editTextPwd)
    EditText editTextPwd;


    @InjectView(R.id.checkBoxRemeber)
    CheckBox checkBoxRemeber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);
        textViewSignUp.setText(Html.fromHtml("Not a user yet?&nbsp;&nbsp;<b><font color=\"#40b9f0\">Sign up</font></b>&nbsp;&nbsp;here"));

        User user = App.getApp().getUser();

        if (null != user && !StringUtils.isEmpty(user.getFacebookId())) {
            beforGo();
        } else {
            autoLogin();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();

        if (SharePersistent.getBoolean(this, App.KEY_REMEBERPWD)) {
            checkBoxRemeber.setChecked(true);

            String oldUser = (String) SharePersistent.getObjectValue(this, App.KEY_USER);
            if (!StringUtils.isEmpty(oldUser)) {
                editTextEmail.setText(oldUser);
            }
//            String pwd = (String) SharePersistent.getObjectValue(this, App.KE_PWD);
//            if (!StringUtils.isEmpty(pwd)) {
//                editTextPwd.setText(pwd);
//            }
        } else {
            checkBoxRemeber.setChecked(false);
        }


    }

    private void autoLogin() {

        try {

            if (null != App.getApp().getUser()) {

                String user = (String) SharePersistent.getObjectValue(this, App.KEY_USER);
                String pwd = (String) SharePersistent.getObjectValue(this, App.KE_PWD);

                if (!StringUtils.isEmpty(user) && !StringUtils.isEmpty(pwd)) {

                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("email", user);
                    jsonObject.put("password", pwd);

                    login(jsonObject);
                }


            }

        } catch (Exception e) {

            LogUtil.e(App.tag, "auto login error");
        }

    }


    @OnClick(R.id.textViewForget)
    public void onClickViewForget() {

        DialogUtils.showInputDialog2(this, "", "Confirm", "Cancel", "", "Input your email", new DialogCallBackListener() {

            @Override
            public void onCallBack(boolean yesNo, String text) {
                if (yesNo) {
                    resetpassword(text);
                }
            }
        });

    }


    @OnClick(R.id.viewBack)
    public void onBackClick() {
        closeSoftWareKeyBoradInBase();
        finish();
    }


    @OnCheckedChanged(R.id.checkBoxRemeber)
    public void onChecked() {
        SharePersistent.saveBoolean(this, App.KEY_REMEBERPWD, checkBoxRemeber.isChecked());
    }

    @OnClick(R.id.textViewSignUp)
    public void onClickSignUp() {
        Tool.startActivity(this, SignUpActivity.class);
    }

    @OnClick(R.id.buttonLogin)
    public void onLoginClick() {
        try {

            if (checkValue()) {

                String userName = getInput(editTextEmail);
                String pwd = getInput(editTextPwd);

                if (checkBoxRemeber.isChecked()) {
                    SharePersistent.setObjectValue(this, App.KEY_USER, userName);
                    SharePersistent.setObjectValue(this, App.KE_PWD, pwd);
                } else {
                    SharePersistent.setObjectValue(this, App.KEY_USER, "");
                    SharePersistent.setObjectValue(this, App.KE_PWD, "");
                }

                JSONObject jsonObject = new JSONObject();

                jsonObject.put("email", userName);
                jsonObject.put("password", pwd);

                login(jsonObject);

                SharePersistent.setObjectValue(this, keyemailPwd, userName + "," + pwd);

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private boolean checkValue() {

        String userName = getInput(editTextEmail);
        if (!ValidateTool.checkEmail(userName)) {
            Tool.showMessageDialog("Email format error!", this);
            return false;
        }

        String pwd = getInput(editTextPwd);
        if (StringUtils.isEmpty(pwd)) {
            Tool.showMessageDialog("Password is empty!", this);

            return false;
        }

        return true;
    }


    private void beforGo() {

        getProfile(this, new OnRequestEndCallBack() {
            @Override
            public void onRequestEnd(NetResult netResult) {
                if (null != netResult) {
                    User user = (User) netResult.getData()[0];
                    App.getApp().setUser(user);

                    setUpPushWoosh(user);

                    //set user id to ur airship
//                    UAirship.shared().getPushManager().setAlias(user.getUserId());

                    if (user.isUpdateProfile()) {
                        gotoProfilePage();
                    } else {
                        gotoProfilePageFill();
                    }

                } else {
                    Tool.showMessageDialog("Net error!", LoginActivity.this);
                }
            }
        });


    }


    private void gotoProfilePageFill() {

        Tool.startActivity(this, ProfileFillActivity.class);
    }


    private void gotoProfilePage() {

//        Tool.startActivity(this, ProfileUserActivity.class);

        Tool.startActivity(this, HomeScreenActivity.class);
    }


    private void resetpassword(final String email) {

        reSetTask();

        baseTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("email", email);
                    netResult = NetInterface.resetpassword(LoginActivity.this, jsonObject);
                } catch (Exception e) {
                    LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (!isFinishing()) {
                    if (null != result) {
                        if (StringUtils.isEmpty(result.getMessage())) {
                            Tool.showMessageDialog("We have sent a link to your email address so you can reset your password.", LoginActivity.this);
                        } else {
                            Tool.showMessageDialog(result.getMessage(), LoginActivity.this);
                        }
                    } else {
                        Tool.showMessageDialog("Net error", LoginActivity.this);
                    }
                }
            }
        }, this);

        baseTask.execute(new HashMap<String, String>());

    }


    private void login(final JSONObject jsonObject) {

        reSetTask();

        baseTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.login(LoginActivity.this, jsonObject, false);
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (!isFinishing()) {
                    if (null != result) {
                        if (StringUtils.isEmpty(result.getMessage())) {
                            beforGo();
                        } else {
                            Tool.showMessageDialog(result.getMessage(), LoginActivity.this);
                        }
                    } else {
                        Tool.showMessageDialog("Net error", LoginActivity.this);
                    }
                }
            }
        }, this);

        baseTask.execute(new HashMap<String, String>());

    }

    @Override
    public void onStatusChange(int sendTagsStatus) {

    }

    @Override
    public void onTaskEnds() {

    }

    @Override
    public void onTaskStarts() {

    }
}
