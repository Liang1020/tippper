package com.tipppr.android.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.net.NetInterface;
import com.tipppr.tipppr.view.HexImageView;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class ReviewActivity extends BaseTippprActivity {


    @InjectView(R.id.hexImage1)
    HexImageView hexImage1;

    @InjectView(R.id.textViewName1)
    TextView textViewName1;

    @InjectView(R.id.linearLayoutStar1)
    LinearLayout linearLayoutStar1;

    @InjectView(R.id.rateBar1)
    RatingBar rateBar1;

    @InjectView(R.id.ratBar2)
    RatingBar rateBar2;

    Bitmap bitmapMaskHeader;

    @InjectView(R.id.hexImage2)
    HexImageView hexImage2;

    @InjectView(R.id.textViewName2)
    TextView textViewName2;

    @InjectView(R.id.linearLayoutStar2)
    LinearLayout linearLayoutStar2;

    @InjectView(R.id.viewReviewContainer1)
    View viewReviewContainer1;

    @InjectView(R.id.viewReviewContainer2)
    View viewReviewContainer2;


    private boolean rateUser1Success = false;
    private boolean rateUser2Success = false;


    private int totalCount = 0;
    private int totalRateSuccessCount = 0;

    private boolean isRateSuccess = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        ButterKnife.inject(this);

        this.bitmapMaskHeader = BitmapFactory.decodeResource(getResources(), R.drawable.bg_mask_header);

        if (App.getApp().hasTempKey("user1")) {
            User u1 = (User) App.getApp().getTempObject("user1");
            addUser1Info(u1);
        }

        if (App.getApp().hasTempKey("user2")) {
            User u2 = (User) App.getApp().getTempObject("user2");
            if (null == rateBar1.getTag()) {
                addUser1Info(u2);
            } else {
                addUser1Info2(u2);
            }
        }


//        rateBar1.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
//            @Override
//            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
//
//                if (null != ratingBar.getTag()) {
//                    rateService((User) ratingBar.getTag(), (int) rating, ratingBar);
//                }
//            }
//        });
//
//        rateBar2.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
//            @Override
//            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
//
//                if (null != ratingBar.getTag()) {
//                    rateService((User) ratingBar.getTag(), (int) rating, ratingBar);
//                }
//            }
//        });
//


    }


    private void rateService(final User user, final int score, final RatingBar rbar) {
        reSetTask();
        baseTask = new BaseTask(this, new NetCallBack() {
            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("userid", user.getUserId());
                    jsonObject.put("score", score);
                    netResult = NetInterface.review(ReviewActivity.this, jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null == result) {
                    Tool.showMessageDialog("Net error!", ReviewActivity.this);
                } else {
                    if (StringUtils.isEmpty(result.getMessage())) {
                        showToastWithTime("Rating successful.", 3);


                        if (rbar.getId() == rateBar1.getId()) {
                            rateUser1Success = true;
                        } else {
                            rateUser2Success = true;
                        }

                    } else {
                        showToastWithTime(result.getMessage(), 3);
                    }
                }
            }
        });

        baseTask.execute(new HashMap<String, String>());

    }


    private void rateService(final User user, final int score, final User user2, final int score2) {
        reSetTask();
        baseTask = new BaseTask(this, new NetCallBack() {
            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("userid", user.getUserId());
                    jsonObject.put("score", score);
                    netResult = NetInterface.review(ReviewActivity.this, jsonObject);

                    //===========
                    if (null != user2) {
                        JSONObject jsonObject2 = new JSONObject();
                        jsonObject2.put("userid", user2.getUserId());
                        jsonObject2.put("score", score2);
                        netResult = NetInterface.review(ReviewActivity.this, jsonObject2);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null == result) {
                    Tool.showMessageDialog("Net error!", ReviewActivity.this);
                } else {
                    if (StringUtils.isEmpty(result.getMessage())) {
                        showToastWithTime("Rating successful.", 3);

                        setResult(RESULT_OK);
                        finish();

                    } else {
                        showToastWithTime(result.getMessage(), 3);
                    }
                }
            }
        });

        baseTask.execute(new HashMap<String, String>());

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (null != bitmapMaskHeader && !bitmapMaskHeader.isRecycled()) {
                bitmapMaskHeader.recycle();
            }
        } catch (Exception e) {
        }

    }


    @OnClick(R.id.viewBack)
    public void onViewBack() {
//        checkRate();

        if (checkValue()) {

        }
    }


    @Override
    public void onBackPressed() {
        Tool.showMessageDialog("Pelase submit rating first",this);
//        if (checkValue()) {
//
//        }

    }

    @OnClick(R.id.viewDone)
    public void onClickReviewDone() {

//        checkRate();

        if (checkValue()) {

            if (totalCount == 1) {

//                rateService((User) rateBar1.getTag(), (int) rateBar1.getRating(), rateBar1);

                rateService((User) rateBar1.getTag(), (int) rateBar1.getRating(), null, 0);

            } else if (totalCount == 2) {
                rateService(
                        (User) rateBar1.getTag(), (int) rateBar1.getRating(),
                        (User) rateBar2.getTag(), (int) rateBar2.getRating());

//                rateService((User) rateBar1.getTag(), (int) rateBar1.getRating(), rateBar1);
//                rateService((User) rateBar2.getTag(), (int) rateBar2.getRating(), rateBar2);
            }

        }

    }


    private boolean checkValue() {


        if (totalCount == 1) {
            float r1 = rateBar1.getRating();
            if (0 != r1) {
                return true;
            } else {
                Tool.showMessageDialog("Please rate first.", this);
                return false;
            }

        } else if (totalCount == 2) {

            float r1 = rateBar1.getRating();
            float r2 = rateBar2.getRating();

            if ((r1 + r2) == 0) {
                Tool.showMessageDialog("Please rate user first.", this);
                return false;
            }

            if (r1 == 0) {
                Tool.showMessageDialog("Please rate " + getInput(textViewName1), this);
                return false;
            }
            if (r2 == 0) {
                Tool.showMessageDialog("Please rate " + getInput(textViewName2), this);
                return false;
            }
        }

        return true;

    }


    private void checkRate() {

        totalRateSuccessCount = 0;

        if (rateUser1Success == true) {
            totalRateSuccessCount++;
        }
        if (rateUser2Success == true) {
            totalRateSuccessCount++;
        }

        if (totalCount == totalRateSuccessCount) {
            setResult(RESULT_OK);
            finish();
        } else {
            showNotifyTextIn5Seconds("Please rate first.");
        }
    }


    private void addUser1Info(User user) {
        totalCount++;

        if (!StringUtils.isEmpty(user.getHeaderImage())) {

            Target target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    hexImage1.setImageBitmap(bitmap);
//                LogUtil.i(App.tag, "load url sucess");
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };

            hexImage1.setTag(target);

            Picasso.with(ReviewActivity.this).load(user.getHeaderImage()).
                    placeholder(R.drawable.hary).
                    resize(bitmapMaskHeader.getWidth(), bitmapMaskHeader.getHeight()).
                    into(target);

        }

        textViewName1.setText((user.isBuiness() ? user.getBussness() : user.getName()));

        rateBar1.setTag(user);

        viewReviewContainer1.setVisibility(View.VISIBLE);

//        if (user.getReview() >= 0 && user.getReview() <= 5) {
//
//            rateBar1.setRating(user.getReview());
//        }

    }

    private boolean rateUser2 = false;

    private void addUser1Info2(User user) {
        totalCount++;

        if (!StringUtils.isEmpty(user.getHeaderImage())) {

            Target target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    hexImage2.setImageBitmap(bitmap);
//                LogUtil.i(App.tag, "load url sucess");
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };

            hexImage2.setTag(target);

            Picasso.with(ReviewActivity.this).load(user.getHeaderImage()).
                    placeholder(R.drawable.hary).
                    resize(bitmapMaskHeader.getWidth(), bitmapMaskHeader.getHeight()).
                    into(target);

        }

        textViewName2.setText((user.isBuiness() ? user.getBussness() : user.getName()));

        rateBar2.setTag(user);

        viewReviewContainer2.setVisibility(View.VISIBLE);


//        if (user.getReview() >= 0 && user.getReview() <= 5) {
//
//            rateBar2.setRating(user.getReview());
//        }

    }

}
