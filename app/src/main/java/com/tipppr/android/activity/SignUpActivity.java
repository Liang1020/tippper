package com.tipppr.android.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.callback.OnRequestEndCallBack;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.util.ValidateTool;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.net.NetInterface;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class SignUpActivity extends BaseTippprActivity {


    @InjectView(R.id.textViewSignUpPrivacy)
    TextView textViewSignUpPP;

    @InjectView(R.id.editTextEmail)
    EditText editTextEmail;

    @InjectView(R.id.editTextPwd)
    EditText editTextPwd;

    @InjectView(R.id.editTextPwdConfirm)
    EditText editTextPwdConfirm;

    @InjectView(R.id.radioGroupType)
    RadioGroup radioGroup;

    private String linkText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.inject(this);
        setLinkStype();

    }

    private void setLinkStype() {

        linkText = "By tapping to Sign Up, you are indicating that you have read the <font color=\"#ff000000\">" +
                "<B><a href=\"pp\"> Privacy Policy</a></B>" +
                "</font> and agree to the " +
                "<b><a href=\"terms\">Terms and Conditions</a></b>";
        textViewSignUpPP.setText(getClickableHtml(linkText));
        textViewSignUpPP.setAutoLinkMask(Linkify.WEB_URLS);
        textViewSignUpPP.setMovementMethod(LinkMovementMethod.getInstance());
    }


    private void setLinkClickable(final SpannableStringBuilder clickableHtmlBuilder,
                                  final URLSpan urlSpan) {
        int start = clickableHtmlBuilder.getSpanStart(urlSpan);
        int end = clickableHtmlBuilder.getSpanEnd(urlSpan);
        int flags = clickableHtmlBuilder.getSpanFlags(urlSpan);
        ClickableSpan clickableSpan = new ClickableSpan() {
            public void onClick(View view) {

                setLinkStype();
                String url = urlSpan.getURL();
//                Tool.startUrl(SignUpActivity.this, "http://www.tipppr.com");


                if ("pp".equals(url)) {
                    Tool.startUrl(SignUpActivity.this, "http://27.124.116.193/~tippprco/wp-content/uploads/2016/06/Latest_Latest_Privacy_Policy_TIPPPR.pdf");
                } else {
                    Tool.startUrl(SignUpActivity.this, "http://27.124.116.193/~tippprco/wp-content/uploads/2016/06/Latest_Latest_Terms_and_Conditions_TIPPPR.pdf");
                }
                LogUtil.i(App.tag, "click url:" + urlSpan.getURL());

//                Editable edit = textViewSignUpPP.getEditableText();
//                if (null != edit) {
//                    edit.clear();
//                }
            }
        };
        clickableHtmlBuilder.setSpan(clickableSpan, start, end, flags);
    }

    private CharSequence getClickableHtml(String html) {
        Spanned spannedHtml = Html.fromHtml(html);
        SpannableStringBuilder clickableHtmlBuilder = new SpannableStringBuilder(spannedHtml);
        URLSpan[] urls = clickableHtmlBuilder.getSpans(0, spannedHtml.length(), URLSpan.class);
        for (final URLSpan span : urls) {
            setLinkClickable(clickableHtmlBuilder, span);
        }
        return clickableHtmlBuilder;
    }

    @OnClick(R.id.viewBack)
    public void onBackClick() {

        closeSoftWareKeyBoradInBase();

        finish();
    }


    private boolean checkValue() {


        int checkedId = radioGroup.getCheckedRadioButtonId();
        if (-1 == checkedId) {

            Tool.showMessageDialog("Please select account type!", this);
            return false;
        }


        String email = getInput(editTextEmail);

        if (!ValidateTool.checkEmail(email)) {
            Tool.showMessageDialog("Email format error!", this);
            return false;
        }

        String pwd = getInput(editTextPwd);
        if (StringUtils.isEmpty(pwd)) {
            Tool.showMessageDialog(getResources().getString(R.string.password_error_notify), this);
            return false;
        }

        String conpwd = getInput(editTextPwdConfirm);

        if (!conpwd.equals(pwd)) {

            Tool.showMessageDialog("Confirm password error!", this);

            return false;
        }

        return true;
    }

    @OnClick(R.id.buttonSingUp)
    public void onClickSignUp() {

        if (checkValue()) {

            try {
                JSONObject jsonObject = new JSONObject();

                jsonObject.put("email", getInput(editTextEmail));
                jsonObject.put("password", getInput(editTextPwdConfirm));
                jsonObject.put("password_confirmation", getInput(editTextPwdConfirm));

                String type = String.valueOf(radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag());

                jsonObject.put("level", type);

                regist(jsonObject);
            } catch (Exception e) {

            }


        }

//        Tool.startActivity(this, VerifyActivity.class);

    }

    private void regist(final JSONObject jsonObject) {
        reSetTask();

        baseTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
//                    netResult = NetInterface.getTokens(baseTask.weakReferenceContext.get(), jsonObject);

                    netResult = NetInterface.regist(baseTask.weakReferenceContext.get(), jsonObject);

                } catch (Exception e) {

                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (isFinishing()) {
                    return;
                }
                if (null != result) {
                    if (StringUtils.isEmpty(result.getMessage())) {

                        User user=(User) (result.getData()[0]);
                        App.getApp().setUser(user);
                        //set user id to ur airship

                        LogUtil.e(App.tag,"airship userid:"+user.getUserId());


//                        UAirship.shared().getPushManager().setAlias(user.getId());

                        login();
//                        gotoProfilePage();

                    } else {
                        Tool.showMessageDialog(result.getMessage(), (Activity) baseTask.weakReferenceContext.get());
                    }
                } else {
                    Tool.showMessageDialog("Net error", (Activity) baseTask.weakReferenceContext.get());
                }

            }
        }, this);

        baseTask.execute(new HashMap<String, String>());

    }


    private void login() {

        reSetTask();

        baseTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {



                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("email", getInput(editTextEmail));
                    jsonObject.put("password", getInput(editTextPwd));

                    netResult = NetInterface.login(SignUpActivity.this, jsonObject,false);
                } catch (Exception e) {
                    LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
                }


                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (!isFinishing()) {
                    if (null != result) {
                        if (StringUtils.isEmpty(result.getMessage())) {

                            beforGo();//

//                            gotoProfilePage();
                        } else {
                            Tool.showMessageDialog(result.getMessage(), SignUpActivity.this);
                        }
                    } else {
                        Tool.showMessageDialog("Net error", SignUpActivity.this);
                    }
                }
            }
        }, this);

        baseTask.execute(new HashMap<String, String>());

    }


    private void beforGo() {
        getProfile(this, new OnRequestEndCallBack() {
            @Override
            public void onRequestEnd(NetResult netResult) {
                if (null != netResult) {
                    User user = (User) netResult.getData()[0];
                    App.getApp().setUser(user);

                    setUpPushWoosh(user);

                    gotoProfilePage();

//                    if (user.isUpdateProfile()) {
//                        gotoProfilePage();
//                    } else {
//                        gotoProfilePageFill();
//                    }

                } else {
                    Tool.showMessageDialog("Net error!", SignUpActivity.this);
                }
            }
        });


    }

    private void gotoProfilePage() {

        App.getApp().putTemPObject("tag", true);
        App.getApp().putTemPObject("regist_email",getInput(editTextEmail));
        Tool.startActivity(SignUpActivity.this, ProfileFillActivity.class);

//        App.getApp().putTemPObject("email", getInput(editTextEmail));
//        App.getApp().putTemPObject("pwd", getInput(editTextPwd));

//        Tool.startActivity(this, VerifyActivity.class);
        finish();
    }

}
