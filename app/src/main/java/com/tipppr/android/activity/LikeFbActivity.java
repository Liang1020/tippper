package com.tipppr.android.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import com.facebook.share.widget.LikeView;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class LikeFbActivity extends BaseTippprActivity {


    @InjectView(R.id.textViewLater)
    TextView textViewLater;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_like_fb);
        ButterKnife.inject(this);

        try {
            Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/gotham_book.ttf");
            textViewLater.setTypeface(tf);
        } catch (Exception e) {
        }


        LikeView likeView = (LikeView) findViewById(R.id.likeView);

        likeView.setLikeViewStyle(LikeView.Style.BOX_COUNT);
        likeView.setObjectIdAndType(
                "https://www.facebook.com/tippprmobile",
                LikeView.ObjectType.PAGE);

    }

    @OnClick(R.id.textViewLater)
    public void onClickLater() {
        setResult(RESULT_OK);
        finish();
    }


    @Override
    public void onBackPressed() {

    }

    int resumeCount = 0;

    @Override
    protected void onResume() {
        super.onResume();
        if (resumeCount >= 1) {
            textViewLater.setText("Skip");
            setResult(RESULT_OK);
            finish();
        }
        resumeCount++;
    }

}
