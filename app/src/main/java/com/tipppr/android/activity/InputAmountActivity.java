package com.tipppr.android.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.common.util.LogUtil;
import com.common.util.NumberHelper;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.squareup.picasso.Picasso;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class InputAmountActivity extends BaseTippprActivity {


    @InjectView(R.id.textViewAmount)
    TextView textViewAmount;


    @InjectView(R.id.linearLayoutNumbers)
    LinearLayout linearLayoutNumbers;

    @InjectView(R.id.imageViewBg)
    ImageView imageViewBg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_input);
        ButterKnife.inject(this);
        initView();
    }

    public void initView() {

        Point p = Tool.getDisplayMetrics(this);

        if (null != App.getApp().getUser() && !StringUtils.isEmpty(App.getApp().getUser().getHeaderImage())) {

            Picasso.with(App.getApp()).load(App.getApp().getUser().getHeaderImage())
                    .config(Bitmap.Config.RGB_565)
                    .resize(p.x, p.y)
                    .centerCrop()
                    .into(imageViewBg);
        }


        for (int i = 0; i < 10; i++) {
            Button button = (Button) linearLayoutNumbers.findViewWithTag(String.valueOf(i));
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String input = getInput(textViewAmount);
                    if (input.contains(".") && !input.endsWith(".")) {
                        int dindex = input.lastIndexOf(".");
                        String afterDot = input.substring(dindex + 1, input.length());
                        LogUtil.i(App.tag, "afterdot:" + afterDot);
                        if (afterDot.length() >= 2) {
                            return;
                        }

                    }

//                    if(input.contains("$0")){
//                        textViewAmount.setText("$");
//                    }

                    String tag = (String) v.getTag();
                    textViewAmount.append(tag);

                    String inputText = getInput(textViewAmount);
                    if (inputText.startsWith("$00")) {
                        textViewAmount.setText("$0");
                    }

                    if (inputText.length() >= 3 && input.startsWith("$0") && !input.startsWith("$0.")) {
                        textViewAmount.setText(inputText.replace("$0", "$"));
                    }




                }
            });
        }

        try {

            float money = (Float) App.getApp().getTempObject("amount");
            String moneyText = String.valueOf(money);
            if ("0.0".equals(moneyText)) {
                textViewAmount.setText("$0");
            } else if (moneyText.endsWith(".0")) {
                textViewAmount.setText("$" + (int) money);
            } else if ("0".equals(moneyText)) {
                textViewAmount.setText("$0");
            } else {
                textViewAmount.setText("$" + NumberHelper.keepDecimal2(String.valueOf(money)));
            }

        } catch (Exception e) {
            textViewAmount.setText("$0");
            LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
        }


    }


    @OnClick(R.id.viewBack)
    public void onBackClick() {
        finish();
    }

    @OnClick(R.id.buttonTip)
    public void onClickBackRigh() {

        if (StringUtils.isEmpty(getInput(textViewAmount).replace("$",""))) {
            showNotifyTextIn5Seconds("Please input amount first.");
            return;
        }

        float input = Float.parseFloat(getInput(textViewAmount).replace("$", ""));

        if (input < 1) {
            showNotifyTextIn5Seconds(R.string.notify_less_money);
            return;
        }

        if (input > maxMoney) {
            Tool.showMessageDialog("Oops!", "You really are generous! However Tipppr only accepts tips up to $200. Please adjust your tip before continuing.", this);
            return;
        }


        if (StringUtils.isEmpty(String.valueOf(input))) {
            showNotifyTextIn5Seconds(" Please input amount first.");
            return;
        }


        Intent intent = new Intent();
        intent.putExtra("amount", input);
        setResult(RESULT_OK, intent);
        finish();
    }

    @OnClick(R.id.buttonDot)
    public void onClickDot() {
        String input = getInput(textViewAmount);
        if (!StringUtils.isEmpty(input) && StringUtils.computStrlen(input) > 1 && !input.contains(".")) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(input + ".");
            textViewAmount.setText(stringBuilder);
        }

    }

    @OnClick(R.id.buttonIdDel)
    public void onClickDel() {
        String inputText = getInput(textViewAmount);
        textViewAmount.setText("$0");

//        if (!StringUtils.isEmpty(inputText) && (StringUtils.computStrlen(inputText) > 1)) {
//            textViewAmount.setText(inputText.substring(0, inputText.length() - 1));
//        }
    }

//    @OnClick(R.id.buttonTip)
//    public void onTipClick() {
//
//        String input = getInput(textViewAmount);
//        if (!"$".equals(input)) {
//            App.getApp().putTemPObject("money", input.replace("$", ""));
//            Tool.startActivity(InputAmountActivity.this, TippprOptionActivity.class);
//        }
//    }

}
