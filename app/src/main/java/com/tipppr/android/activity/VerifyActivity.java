package com.tipppr.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.net.NetInterface;
import com.tipppr.tipppr.view.DialogVerifyTip;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class VerifyActivity extends BaseTippprActivity {


    @InjectView(R.id.editTextVerifyCode)
    EditText editTextVerifyCode;

    @InjectView(R.id.textViewStep)
    TextView textViewStep;


    @InjectView(R.id.viewBack)
    View viewBack;

    DialogVerifyTip dialogVerifyTip;

    private boolean fromRegist = false;

    private int SHORT_GO_FB = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);
        ButterKnife.inject(this);

//        email = (String) App.getApp().getTempObject("email");
//        pwd = (String) App.getApp().getTempObject("pwd");

//        App.getApp().putTemPObject("tag", true);


        if (App.getApp().hasTempKey("tag") && true == (boolean) App.getApp().getTempObject("tag")) {
            textViewStep.setVisibility(View.VISIBLE);
            viewBack.setVisibility(View.INVISIBLE);
            fromRegist = true;
            dialogVerifyTip = new DialogVerifyTip(this);

            dialogVerifyTip.setOnVerifyTipListener(new DialogVerifyTip.OnVerifyTipListener() {
                @Override
                public void onEmailVeriyClick() {
                    getTokens();
                }

                @Override
                public void onLaterClick() {

                    if (fromRegist) {
                        beforGotoProfileActivity();
                    } else {
                        setResult(SHORT_REQUEST_CANCEL_VERIFY);
                        finish();
                    }

                }
            });

            dialogVerifyTip.show();


        } else {
            textViewStep.setVisibility(View.INVISIBLE);
            viewBack.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onBackPressed() {
        if (null != dialogVerifyTip && dialogVerifyTip.isShowing()) {
            dialogVerifyTip.dismiss();
            return;
        }
        super.onBackPressed();

    }

    @OnClick(R.id.buttonSend)
    public void onSendClick() {
        getTokens();
    }


    @OnClick(R.id.buttonLater)
    public void onClickButtonLater() {
        if (fromRegist) {
            beforGotoProfileActivity();
        } else {
            setResult(SHORT_REQUEST_CANCEL_VERIFY);
            finish();
        }

    }

    private void beforGotoProfileActivity() {
        if (fromRegist) {
            Tool.startActivityForResult(this, LikeFbActivity.class,SHORT_GO_FB);
        } else {
            gotoProfileActivity();
        }
    }

    private void gotoProfileActivity(){
        Tool.startActivity(this, ProfileUserActivity.class);
        finish();
    }



    @OnClick(R.id.viewBack)
    public void onBackClick() {
        if (fromRegist) {

        } else {
            finish();
        }

    }

    @OnClick(R.id.buttonVerify)
    public void onClickVerify() {

//        boolean test = true;
//        if (test) {
//            Tool.startActivity(VerifyActivity.this, ProfileFillActivity.class);
//            return;
//        }

        {
            String verify = getInput(editTextVerifyCode);
            if (!StringUtils.isEmpty(verify)) {
                verifyToken(verify);
            } else {
//                editTextVerifyCode.setError("verify code is empty!");
                showNotifyTextIn5Seconds("Verification code is empty.");
            }
        }


    }

    private void getTokens() {
        reSetTask();
        baseTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {

                    JSONObject jsonObject = new JSONObject();
                    netResult = NetInterface.getTokens(VerifyActivity.this, jsonObject);
                } catch (Exception e) {
                    LogUtil.e(App.tag, "error in veriy:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (StringUtils.isEmpty(result.getMessage())) {
                        LogUtil.i(App.tag, "get verify token end success!");
                        showNotifyTextIn5Seconds("Verification code has been sent to your email address.");
//                        showToast("Verification code has been sent to your email address.",4);
                    }

//                    String token = (String) result.getData()[0];
//                    App.getApp().setToken(token);
//                    Tool.startActivity(VerifyActivity.this, ProfileFillActivity.class);
                } else {
//                    showNotifyTextIn5Seconds(R.string.error_net);
                }

            }
        });
        baseTask.execute(new HashMap<String, String>());


    }

    private void verifyToken(final String token) {

        reSetTask();

        baseTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("token", token);
                    netResult = NetInterface.vrify(VerifyActivity.this, jsonObject);
                } catch (Exception e) {
                    LogUtil.e(App.tag, "error in veriy:" + e.getLocalizedMessage());

                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null != result) {

                    if (StringUtils.isEmpty(result.getMessage())) {

                        User user = App.getApp().getUser();
                        user.setIsEmailVerify("1");
                        App.getApp().setUser(user);
                        login();

                    } else {
                        Tool.showMessageDialog(result.getMessage(), (Activity) baseTask.weakReferenceContext.get());
                    }
                } else {
                    showNotifyTextIn5Seconds(R.string.error_net);
                }

            }
        });
        baseTask.execute(new HashMap<String, String>());
    }


    private BaseTask loginTask;

    private void login() {
        reSetTask();

        final String[] userNamepwd = ((String) SharePersistent.getObjectValue(this, keyemailPwd)).split(",");


        loginTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("email", userNamepwd[0]);
                    jsonObject.put("password", userNamepwd[1]);
                    netResult = NetInterface.login(VerifyActivity.this, jsonObject, true);

                } catch (Exception e) {
                    LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (!isFinishing()) {
                    if (null != result) {

                        if (StringUtils.isEmpty(result.getMessage())) {
                            if (fromRegist) {
                                beforGotoProfileActivity();
                            } else {
                                setResult(RESULT_OK);
                                finish();
                            }
                        } else {
                            gotoLoginReLogin();
                        }
                    } else {
                        gotoLoginReLogin();
                    }
                }
            }
        }, this);

        loginTask.execute(new HashMap<String, String>());

    }


    private void gotoLoginReLogin() {

        Tool.showMessageDialog("Please login again", VerifyActivity.this, new DialogCallBackListener() {
            @Override
            public void onDone(boolean yesOrNo) {

                Intent intent = new Intent(VerifyActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==SHORT_GO_FB && RESULT_OK ==resultCode){
            gotoProfileActivity();
        }

    }
}
