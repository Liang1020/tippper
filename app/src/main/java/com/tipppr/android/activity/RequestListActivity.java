package com.tipppr.android.activity;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.push.PushTool;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.adapter.UserAdapterRequestAdapter;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.net.NetInterface;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class RequestListActivity extends BaseTippprActivity {


    @InjectView(R.id.listViewRequestOut)
    ListView listViewRequestOut;

    @InjectView(R.id.listViewRequestIn)
    ListView listViewRequestIn;


    @InjectView(R.id.buttonShowOutgoing)
    Button buttonShowOutgoing;

    @InjectView(R.id.buttonIncomeing)
    Button buttonIncomeing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_list);
        ButterKnife.inject(this);
        ArrayList<User> usersList = (ArrayList<User>) App.getApp().getTempObject("requestlist");
        ArrayList<User> usersListin = (ArrayList<User>) App.getApp().getTempObject("requestListIn");
        buildList(usersList, usersListin);
    }


    private void showOutGoing() {

        buttonShowOutgoing.setBackgroundResource(R.drawable.shape_tab_left_select);
        buttonShowOutgoing.setTextColor(Color.WHITE);

        buttonIncomeing.setBackgroundResource(R.drawable.shape_tab_right_normal);
        buttonIncomeing.setTextColor(Color.parseColor("#46b5db"));

        listViewRequestOut.setVisibility(View.VISIBLE);
        listViewRequestIn.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.buttonShowOutgoing)
    public void onShowOutGoing() {

        showOutGoing();

    }

    private void showInComing() {

        buttonShowOutgoing.setBackgroundResource(R.drawable.shape_tab_left_normal);
        buttonShowOutgoing.setTextColor(Color.parseColor("#46b5db"));

        buttonIncomeing.setBackgroundResource(R.drawable.shape_tab_right_select);
        buttonIncomeing.setTextColor(Color.WHITE);

        listViewRequestOut.setVisibility(View.INVISIBLE);
        listViewRequestIn.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.buttonIncomeing)
    public void setButtonIncomeing() {
        showInComing();
    }


    private void buildList(ArrayList<User> usersList, ArrayList<User> incomeList) {

        if (null != usersList) {
            UserAdapterRequestAdapter userAdapterRequestAdapter = new UserAdapterRequestAdapter(this, usersList, true);

            userAdapterRequestAdapter.setOnOperateClickListener(new UserAdapterRequestAdapter.OnOperateClickListener() {
                @Override
                public void onClickAgree(final User user, final int index, final UserAdapterRequestAdapter adapterRequestAdapter) {

                    DialogUtils.showConfirmDialog(RequestListActivity.this, "", "Accept this request?", "Confirm", "Cancel", new DialogCallBackListener() {
                        @Override
                        public void onDone(boolean yesOrNo) {
                            if (yesOrNo) {
                                requestAcceptRequest(true, user, index, adapterRequestAdapter);
                            }
                        }
                    });
                }

                @Override
                public void onClickDelay(final User user, final int index, final UserAdapterRequestAdapter adapterRequestAdapter, boolean isMyRequest) {


                    String title = "Decline this request?";
                    if (isMyRequest) {
                        title = "Cancel this request?";
                    }

                    DialogUtils.showConfirmDialog(RequestListActivity.this, "", title, "Confirm", "Cancel", new DialogCallBackListener() {
                        @Override
                        public void onDone(boolean yesOrNo) {
                            if (yesOrNo) {
                                requestdenyRequest(true, user, index, adapterRequestAdapter);
                            }
                        }
                    });

                }

                @Override
                public void onClickName(User user, int index, UserAdapterRequestAdapter adapterRequestAdapter, boolean isMyRequest) {

                    App.getApp().putTemPObject("user", user);
                    Tool.startActivity(RequestListActivity.this, ProfileAnyUserActivity.class);
                }
            });

            listViewRequestOut.setAdapter(userAdapterRequestAdapter);
//            listViewRequestOut.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    User user = (User) parent.getAdapter().getItem(position);
//                    App.getApp().putTemPObject("user", user);
//                    Tool.startActivity(RequestListActivity.this, ProfileAnyUserActivity.class);
//                }
//            });

        }

        if (null != incomeList) {
            UserAdapterRequestAdapter userAdapterRequestAdapter = new UserAdapterRequestAdapter(this, incomeList, false);
            userAdapterRequestAdapter.setOnOperateClickListener(new UserAdapterRequestAdapter.OnOperateClickListener() {
                @Override
                public void onClickAgree(final User user, final int index, final UserAdapterRequestAdapter adapterRequestAdapter) {
                    DialogUtils.showConfirmDialog(RequestListActivity.this, "", "Accept this request?", "Confirm", "Cancel", new DialogCallBackListener() {
                        @Override
                        public void onDone(boolean yesOrNo) {
                            if (yesOrNo) {
                                requestAcceptRequest(true, user, index, adapterRequestAdapter);
                            }
                        }
                    });
                }

                @Override
                public void onClickDelay(final User user, final int index, final UserAdapterRequestAdapter adapterRequestAdapter, boolean isMyRequest) {

                    String title = "Decline this request?";
                    if (isMyRequest) {
                        title = "Cancel this request?";
                    }

                    DialogUtils.showConfirmDialog(RequestListActivity.this, "", title, "Confirm", "Cancel", new DialogCallBackListener() {
                        @Override
                        public void onDone(boolean yesOrNo) {
                            if (yesOrNo) {
                                requestdenyRequest(true, user, index, adapterRequestAdapter);
                            }
                        }
                    });
                }

                @Override
                public void onClickName(User user, int index, UserAdapterRequestAdapter adapterRequestAdapter, boolean isMyRequest) {
                    App.getApp().putTemPObject("user", user);
                    Tool.startActivity(RequestListActivity.this, ProfileAnyUserActivity.class);
                }
            });

            listViewRequestIn.setAdapter(userAdapterRequestAdapter);
//            listViewRequestIn.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    User user = (User) parent.getAdapter().getItem(position);
//                    App.getApp().putTemPObject("user", user);
//                    Tool.startActivity(RequestListActivity.this, ProfileAnyUserActivity.class);
//                }
//            });
        }
    }


    @OnClick(R.id.viewBack)
    public void onBackClick() {
        setResult(RESULT_OK);
        finish();
    }


    @Override
    public void onBackPressed() {

        setResult(RESULT_OK);
        finish();


        super.onBackPressed();


    }

    private void requestAcceptRequest(final boolean showDialog, final User user, final int index, final UserAdapterRequestAdapter adapterRequestAdapter) {

        reSetTask();
        baseTask = new BaseTask(new NetCallBack() {


            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    baseTask.showDialogForSelf(true);
                }

            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {

                    JSONObject jso = new JSONObject();

                    jso.put("userid", user.getUserId());

                    netResult = NetInterface.acceptFriendRequest(RequestListActivity.this, jso);


                    ArrayList<String> ids = new ArrayList<>();
                    ids.add(user.getUserId());


                    User currentUser = App.getApp().getUser();

                    String content = currentUser.isBuiness() ? currentUser.getBussness() : currentUser.getProfileName();

                    content += " accepted your request.";

                    JSONObject jsonObjetType = new JSONObject();
                    jsonObjetType.put("type", 2);

                    PushTool.sendPushWithPushwoosh(ids, content, jsonObjetType);

                } catch (Exception e) {
                    LogUtil.e(App.tag, "accepet request list");
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (showDialog) {
                    baseTask.hideDialogSelf();
                }

                if (null != result) {
                    if (!StringUtils.isEmpty(result.getMessage())) {
                        Tool.showMessageDialog(result.getMessage(), RequestListActivity.this);
                    } else {
//                        Tool.showMessageDialog("Accept success!", RequestListActivity.this);
                        adapterRequestAdapter.removeAtIndex(index);
                        showToast("Accept success!", 2);
                    }
                } else {
                    Tool.showMessageDialog("Net error!", RequestListActivity.this);
                }

            }
        }, this);

        baseTask.execute(new HashMap<String, String>());

    }


    private void requestdenyRequest(final boolean showDialog, final User user, final int index, final UserAdapterRequestAdapter adapterRequestAdapter) {

        reSetTask();
        baseTask = new BaseTask(new NetCallBack() {


            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    baseTask.showDialogForSelf(true);
                }

            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {

                    JSONObject jso = new JSONObject();

                    jso.put("userid", user.getUserId());

                    netResult = NetInterface.denyFriendRequest(RequestListActivity.this, jso);

                } catch (Exception e) {
                    LogUtil.e(App.tag, "accepet request list");
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (showDialog) {
                    baseTask.hideDialogSelf();
                }

                if (null != result) {
                    if (!StringUtils.isEmpty(result.getMessage())) {
                        Tool.showMessageDialog(result.getMessage(), RequestListActivity.this);
                    } else {
//                        Tool.showMessageDialog("Dely success!", RequestListActivity.this);
                        showToast("Dely success!", 2);
                        adapterRequestAdapter.removeAtIndex(index);
                    }
                } else {
                    Tool.showMessageDialog("Net error!", RequestListActivity.this);
                }

            }
        }, this);

        baseTask.execute(new HashMap<String, String>());

    }


    private BaseTask friendTask = null;

    private void loadPendingFriendList(final boolean showDialog) {

        if (null != friendTask && friendTask.getStatus() == AsyncTask.Status.RUNNING) {
            friendTask.cancel(true);
        }

        friendTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

                if (showDialog) {
                    baseTask.showDialogForSelf(true);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {
                    netResult = NetInterface.getpendingFriendList(RequestListActivity.this, new JSONObject());
                } catch (Exception e) {
                    LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
                }

                return netResult;

            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result && StringUtils.isEmpty(result.getMessage())) {
                    try {

                        ArrayList<User> arrayListRequestList = (ArrayList<User>) result.getData()[0];
                        ArrayList<User> incompeList = (ArrayList<User>) result.getData()[1];
                        buildList(arrayListRequestList, incompeList);

                    } catch (Exception e) {
                        LogUtil.e(App.tag, "exception:in get count");
                    }
                }
            }
        }, this);

        friendTask.execute(new HashMap<String, String>());

    }
}
