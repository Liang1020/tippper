package com.tipppr.android.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.callback.OnRequestEndCallBack;
import com.callback.OnUploadCallBack;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.ImageUtils;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.util.ValidateTool;
import com.dialog.DialogCreditCardEdit;
import com.dialog.DialogCreditCardFill;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.theartofdev.edmodo.cropper.CropImage;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.bean.Card;
import com.tipppr.bean.SmartyStreet;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.net.NetInterface;
import com.tipppr.tipppr.photo.lib.PhotoLibUtils;
import com.tipppr.tipppr.view.DialogSingleOption;
import com.tipppr.tipppr.view.PhotoSelectDialog;
import com.tipppr.trans.MyBlurTransform;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class ProfileFillActivity extends BaseTippprActivity {


    @InjectView(R.id.imageViewBigHeader)
    ImageView imageViewBigHeader;

    @InjectView(R.id.imageViewBottomGlass)
    ImageView imageViewBottomGlass;

    @InjectView(R.id.imageViewSmallHeader)
    ImageView imageViewSmallHeader;

    Bitmap bitmapMaskHeader;

    Point p = null;

    int maskWidth, maskHeight;

//    @InjectView(R.id.editTextFullName)
//    EditText editTextFullName;

    @InjectView(R.id.editTextEmail)
    EditText editTextEmail;

    @InjectView(R.id.editTextMobileNumber)
    EditText editTextMobileNumber;

    @InjectView(R.id.editAddress)
    EditText editAddress;

    @InjectView(R.id.textViewAddPhoto)
    TextView textViewAddPhoto;


    @InjectView(R.id.viewBusName)
    View viewBusName;

    @InjectView(R.id.viewChangepwd)
    View viewChangepwd;

    @InjectView(R.id.viewBusNameLine)
    View viewBusNameLine;


    @InjectView(R.id.viewContactName)
    View viewContactName;

    @InjectView(R.id.viewContactNameLine)
    View viewContactNameLine;


    @InjectView(R.id.viewContactName)
    View getViewContactNameLine;

    @InjectView(R.id.viewContactNumber)
    View viewContactNumber;


    @InjectView(R.id.viewFuallName)
    View viewFuallName;

    @InjectView(R.id.viewFuallNameLine)
    View viewFuallNameLine;

    @InjectView(R.id.viewMobileNumber)
    View viewMobileNumber;

    @InjectView(R.id.viewMobileNumberLine)
    View viewMobileNumberLine;


    @InjectView(R.id.viewPostAddress)
    View viewPostAddress;


    @InjectView(R.id.editTextContactName)
    EditText editTextContactName;

    @InjectView(R.id.editTextContactNumber)
    EditText editTextContactNumber;


    @InjectView(R.id.editTextBusname)
    EditText editTextBusname;

    @InjectView(R.id.scrollView)
    ScrollView scrollView;

    @InjectView(R.id.viewCheckPostAddress)
    View viewCheckPostAddress;


    @InjectView(R.id.textViewPaypalEmail)
    TextView textViewPaypalEmail;

    @InjectView(R.id.textViewStepText)
    TextView textViewStepText;


    @InjectView(R.id.editTextFirstName)
    EditText editTextFirstName;

    @InjectView(R.id.editTextLastName)
    EditText editTextLastName;

    @InjectView(R.id.editTextCity)
    EditText editTextCity;

    @InjectView(R.id.editTextState)
    EditText editTextState;

    @InjectView(R.id.editTextPostCode)
    EditText editTextPostCode;

    @InjectView(R.id.textViewCreditCar)
    TextView textViewCreditCar;

    @InjectView(R.id.textViewCurrency)
    TextView textViewCurrency;

    @InjectView(R.id.textViewCountry)
    TextView textViewCountry;

    @InjectView(R.id.viewContactNumberLine)
    View viewContactNumberLine;

    @InjectView(R.id.rateBar)
    RatingBar rateBar;


    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PROFILE_SHARING = 3;
    private static final int INT_GO_PAYPAL_LOGIN = 4;
    private static final int INT_GO_VERIFY = 5;

    private boolean isFromRegist = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_fill);
        ButterKnife.inject(this);
        p = Tool.getDisplayMetrics(this);
        initHeaderLayoutParam();

        showHideView();

        if (App.getApp().hasTempKey("tag")) {
            initUserView(App.getApp().getUser());
            textViewStepText.setVisibility(View.VISIBLE);
            isFromRegist = true;
            viewChangepwd.setVisibility(View.INVISIBLE);

        } else {
            textViewStepText.setVisibility(View.INVISIBLE);
            viewChangepwd.setVisibility(View.VISIBLE);

            getProfile(this, true, false, new OnRequestEndCallBack() {
                @Override
                public void onRequestEnd(NetResult netResult) {
                    if (null != netResult) {
                        User user = (User) netResult.getData()[0];
                        App.getApp().setUser(user);
                        initUserView(user);
                    } else {
                        Tool.showMessageDialog("Net error!", ProfileFillActivity.this);
                    }
                }
            });
        }

    }


    @OnTextChanged(R.id.editTextState)
    public void onTextValueChagned() {
        String state = getInput(editTextState);

        if(StringUtils.isEmpty(state)){
            editTextState.setHint(getResources().getString(R.string.address_hint));
        }else{
            editTextState.setHint("");
        }

    }


    DialogCreditCardEdit dialogCreditCardEdit;
    DialogCreditCardFill dialogCreditCardFill;

    @OnClick(R.id.linearLayoutCreditCard)
    public void onCredtCardClick() {

        if (checkValue() == false) {
            Tool.showMessageDialog("Please complete your profile first.", ProfileFillActivity.this);
            return;
        }


        User user = App.getApp().getUser();
        if (null != user && null != user.getCard() && !StringUtils.isEmpty(user.getCard().getCardNumber())) {
            dialogCreditCardEdit = new DialogCreditCardEdit(this, true);
        } else {
            dialogCreditCardEdit = new DialogCreditCardEdit(this, false);
        }

        dialogCreditCardEdit.setOnCreditDialogListener(new DialogCreditCardEdit.OnCreditDialogListener() {

            @Override
            public void onAddClick() {

                gotoFillCreditCardActivity();

            }

            @Override
            public void onDeleteClick() {

                LogUtil.i(App.tag, "on delete click");

                DialogUtils.showConfirmDialog(ProfileFillActivity.this, "", "Unbundle this credit card?", "Confirm", "Cancel", new DialogCallBackListener() {
                    @Override
                    public void onDone(boolean yesOrNo) {

                        if (yesOrNo) {
                            deleteCardInfo();
                        }

                    }
                });

//                deleteCardInfo();

            }
        });


        dialogCreditCardEdit.show();


    }


    private void gotoFillCreditCardActivity() {

        Tool.startActivityForResult(this, CreditCardActivity.class, SHORT_REQUEST_FILL_CARD);


    }


    private void saveCardNumber(final String cardNunber, final String exp, final String cvv) {

        reSetTask();
        baseTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {

                    JSONObject jso = new JSONObject();

                    jso.put("cardNumber", cardNunber.replace("-", ""));
                    jso.put("cardExp", exp);
                    jso.put("cvv", cvv);

                    netResult = NetInterface.getBillpro(ProfileFillActivity.this, jso);

                } catch (Exception e) {

                }


                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();


                if (null != result) {

                    if (StringUtils.isEmpty(result.getMessage())) {

                        if (null != dialogCreditCardEdit) {
                            dialogCreditCardEdit.dismiss();
                            dialogCreditCardEdit = null;
                        }

                        if (null != dialogCreditCardFill) {
                            dialogCreditCardFill.dismiss();
                            dialogCreditCardFill = null;
                        }

                        Tool.showMessageDialog("Bind credit card successfully.", ProfileFillActivity.this);

                        getProfile(ProfileFillActivity.this, true, false, new OnRequestEndCallBack() {
                            @Override
                            public void onRequestEnd(NetResult netResult) {
                                if (null != netResult) {
                                    User user = (User) netResult.getData()[0];
                                    App.getApp().setUser(user);
                                    initUserView(user);
                                }
                            }
                        });

                    } else {
                        Tool.showMessageDialog(result.getMessage(), ProfileFillActivity.this);
                    }
                } else {

                    showNotifyTextIn5Seconds(R.string.error_net);
                }


            }
        });

        baseTask.execute(new HashMap<String, String>());

    }

    private void deleteCardInfo() {

        reSetTask();
        baseTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {


                NetResult netResult = null;

                try {

                    netResult = NetInterface.deleteBillInfo(ProfileFillActivity.this, new JSONObject());

                } catch (Exception e) {
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();


                if (null != result) {

                    if (StringUtils.isEmpty(result.getMessage())) {

                        if (null != dialogCreditCardEdit) {
                            dialogCreditCardEdit.dismiss();
                            dialogCreditCardEdit = null;
                        }

                        Tool.showMessageDialog("Unbind credit card successfully.", ProfileFillActivity.this);

                        getProfile(ProfileFillActivity.this, new OnRequestEndCallBack() {
                            @Override
                            public void onRequestEnd(NetResult netResult) {
                                if (null != netResult) {
                                    User user = (User) netResult.getData()[0];
                                    App.getApp().setUser(user);
                                    initUserView(user);
                                }
                            }
                        });

                    } else {
                        Tool.showMessageDialog(result.getMessage(), ProfileFillActivity.this);
                    }
                } else {

                    showNotifyTextIn5Seconds(R.string.error_net);
                }


            }
        });

        baseTask.execute(new HashMap<String, String>());

    }


    private BaseTask verifyTask;

    private void verifyTask(final String allAddress, final EditText editText, final User user, final OnAddressVerifyListener onAddressVerifyListener) {
        if (null != verifyTask && verifyTask.getStatus() == AsyncTask.Status.RUNNING) {
            verifyTask.cancel(true);
        }

        verifyTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {


                NetResult netResult = null;
                try {

                    String country = getInput(textViewCountry);
                    if (StringUtils.isEmpty(country)) {
                        if (!StringUtils.isEmpty(getInput(textViewCurrency))) {
                            country = user.getCountry();
                        } else {
                            country = Locale.getDefault().getCountry();
                        }
                        LogUtil.e(App.tag, "country move:" + country);
                    }
                    boolean isUsa = "USA".equals(country) ? true : false;
                    SmartyStreet smartyStreet = getAddressListFromSmartyAPI(allAddress, country, isUsa);
                    netResult = new NetResult();
                    netResult.setTag(smartyStreet);
                } catch (Exception e) {
                    LogUtil.e(App.tag, "error parse address:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null != result) {
                    SmartyStreet smartyStreet = (SmartyStreet) result.getTag();

                    if (null != smartyStreet) {

                        if (smartyStreet.isValidate()) {
                            editText.setTextColor(Color.parseColor("#27abec"));
                            editText.setTag(smartyStreet);
                            if (null != onAddressVerifyListener) {
                                onAddressVerifyListener.onInterFaceVerify(true);
                            }
                            lastVerifySuccess = true;
                        } else {
                            editText.setTag(null);
                            editText.setTextColor(getResources().getColor(R.color.red));
                            showNotifyTextIn5Seconds("Please input a valid address.");
                            if (null != onAddressVerifyListener) {
                                onAddressVerifyListener.onInterFaceVerify(false);
                            }
                            lastVerifySuccess = false;
                        }

                    }
                } else {
                    lastVerifySuccess = false;

                    showNotifyTextIn5Seconds(R.string.error_net);
                    if (null != onAddressVerifyListener) {
                        onAddressVerifyListener.onInterFaceVerify(false);
                    }
                }
            }
        });

        verifyTask.execute(new HashMap<String, String>());

    }


    @OnClick(R.id.lienarlayoutCurrency)
    public void onClickCountoryCurrency() {

        closeSoftWareKeyBoradInBase();

        String[] array = {"USD", "AUD", "CAD"};

        DialogSingleOption dialogSingleOption = new DialogSingleOption(this, array, getInput(textViewCurrency));

        dialogSingleOption.setTitle("Currency");

        dialogSingleOption.setOnOptionSelectDoneListener(new DialogSingleOption.OnOptionSelectDoneListener() {
            @Override
            public void onSelectDonw(String seelct) {

                textViewCurrency.setText(seelct);

            }
        });

        dialogSingleOption.show();

    }


    @OnClick(R.id.linearLayoutCountry)
    public void onClickCountry() {

        closeSoftWareKeyBoradInBase();


        String[] array = {"USA", "Australia", "Cannada"};

        DialogSingleOption dialogSingleOption = new DialogSingleOption(this, array, getInput(textViewCountry));

        dialogSingleOption.setTitle("Country");

        dialogSingleOption.setOnOptionSelectDoneListener(new DialogSingleOption.OnOptionSelectDoneListener() {
            @Override
            public void onSelectDonw(String seelct) {
                textViewCountry.setText(seelct);
            }
        });


        dialogSingleOption.show();

    }


    @OnClick(R.id.linearLayoutLinkButton)
    public void onClickLinkButton() {

        User user = App.getApp().getUser();

        if (StringUtils.isEmpty(user.getPayPalEmail())) {
            Tool.startActivityForResult(ProfileFillActivity.this, PaypalLoginActivity.class, INT_GO_PAYPAL_LOGIN);
        } else {

            DialogUtils.showConfirmDialog(this,
                    "Rebinding Paypal?", "Current:\n" + user.getPayPalEmail(), "Confirm", "Cancel", new DialogCallBackListener() {
                        @Override
                        public void onDone(boolean yesOrNo) {
                            if (yesOrNo) {
                                Tool.startActivityForResult(ProfileFillActivity.this, PaypalLoginActivity.class, INT_GO_PAYPAL_LOGIN);
                            }
                        }
                    }
            );
        }

    }

    private void backAction() {
        setResult(RESULT_OK);
        finish();
    }

    @OnClick(R.id.viewBack)
    public void onBackClick() {
        backAction();
    }

    public void showHideView() {

        User user = App.getApp().getUser();
        if (user.isBuiness()) {

//            if (user.hasLatlng()) {
//
//                Address address = new Address(Locale.getDefault());
//                address.setLatitude(Float.parseFloat(user.getLat()));
//                address.setLongitude(Float.parseFloat(user.getLng()));
//
//                textViewAddress.setTag(address);
//            }

            //================================

            viewBusName.setVisibility(View.VISIBLE);
            viewBusNameLine.setVisibility(View.VISIBLE);

//          viewBusAddress.setVisibility(View.VISIBLE);


            viewContactName.setVisibility(View.VISIBLE);
            viewContactNameLine.setVisibility(View.VISIBLE);
            viewContactNumber.setVisibility(View.VISIBLE);


            viewMobileNumber.setVisibility(View.GONE);
            viewMobileNumberLine.setVisibility(View.GONE);


            viewFuallName.setVisibility(View.GONE);
            viewFuallNameLine.setVisibility(View.GONE);
            //=======================================
            viewContactNumberLine.setVisibility(View.VISIBLE);

        } else {

//            editTextFullName.setText(user.getResultName());
//            editTextEmail.setText(user.getProfileEmail());
//            editTextMobileNumber.setText(user.getPhone());
//            textViewPostalAddress.setText(user.getPostcode());
//            //========
//            if (user.hasLatlng()) {
//                Address address = new Address(Locale.getDefault());
//                address.setLatitude(Float.parseFloat(user.getLat()));
//                address.setLongitude(Float.parseFloat(user.getLng()));
//                textViewPostalAddress.setTag(address);
//            }

            viewBusName.setVisibility(View.GONE);
            viewBusNameLine.setVisibility(View.GONE);

            viewContactName.setVisibility(View.GONE);
            viewContactNameLine.setVisibility(View.GONE);
            viewContactNumber.setVisibility(View.GONE);


            viewMobileNumber.setVisibility(View.VISIBLE);
            viewMobileNumberLine.setVisibility(View.VISIBLE);


            viewFuallName.setVisibility(View.VISIBLE);
            viewFuallNameLine.setVisibility(View.VISIBLE);
            viewContactNumberLine.setVisibility(View.GONE);

        }

    }


    private void initUserView(User user) {

        String country = user.getCountry();
        String currency = user.getCurrency();

        if (!StringUtils.isEmpty(country) && !StringUtils.isEmpty(currency)) {

            textViewCountry.setText(country);
            textViewCurrency.setText(currency);
        }

        rateBar.setRating(user.getReview());


//        String saveAddress = getInput(editAddress) + "|" + getInput(editTextCity) + "|" + getInput(editTextState) + "|" + getInput(textViewCountry) + "|" + postcode;


        textViewCountry.setText(user.getCountry());

        String address = user.getPostcode();
        if (!StringUtils.isEmpty(address) && address.contains(getResources().getString(R.string.split))) {
            String[] fullAddress = user.getPostcode().split(getResources().getString(R.string.split_r));
            if (null != fullAddress) {
                if (fullAddress.length >= 4) {
                    editAddress.setText(fullAddress[0]);
                    editTextCity.setText(fullAddress[1]);
                    editTextState.setText(fullAddress[2]);
                    editTextPostCode.setText(fullAddress[3]);
                } else {
                    editAddress.setText(address);
                }
            } else {
                editAddress.setText(address);
            }
        } else {
            editAddress.setText(address);
        }


        if (null != user.getCard()) {
            Card card = user.getCard();
            if (!StringUtils.isEmpty(card.getCardNumber())) {
                textViewCreditCar.setText("**** **** **** **** " + card.getCardNumber());
            } else {
                textViewCreditCar.setText("Credit Card");
            }

        }


        if (user.isBuiness()) {

            editTextContactNumber.setText(user.getPhone());
            editTextContactName.setText(user.getProfileName());

            editTextEmail.setText(user.getResultEmail());
            editTextBusname.setText(user.getBussness());
            if (user.hasLatlng()) {
                SmartyStreet addressSnart = new SmartyStreet();
                addressSnart.setLatitude((user.getLat()));
                addressSnart.setLongtitude(user.getLng());

                editAddress.setTag(addressSnart);
            }

            //================================

            viewBusName.setVisibility(View.VISIBLE);
            viewBusNameLine.setVisibility(View.VISIBLE);


            viewContactName.setVisibility(View.VISIBLE);
            viewContactNameLine.setVisibility(View.VISIBLE);

            viewContactNumber.setVisibility(View.VISIBLE);


            viewMobileNumber.setVisibility(View.GONE);
            viewMobileNumberLine.setVisibility(View.GONE);


            viewFuallName.setVisibility(View.GONE);
            viewFuallNameLine.setVisibility(View.GONE);
            //=======================================

        } else {

            String name = user.getResultName();

            if (!StringUtils.isEmpty(name)) {

                if (name.contains(getResources().getString(R.string.split))) {
                    String[] names = name.split(getResources().getString(R.string.split_r));
                    if (names.length == 2) {
                        editTextFirstName.setText(names[0]);
                        editTextLastName.setText(names[1]);
                    } else {
                        editTextFirstName.setText(name.replace(getResources().getString(R.string.split), ""));
                    }
                } else {

                }

            }

            editTextEmail.setText(user.getResultEmail());
            editTextMobileNumber.setText(user.getPhone());
            //========
            if (user.hasLatlng()) {
                SmartyStreet smartyStreet = new SmartyStreet();

                smartyStreet.setLatitude((user.getLat()));
                smartyStreet.setLongtitude((user.getLng()));
                editAddress.setTag(smartyStreet);

            }


            viewBusName.setVisibility(View.GONE);
            viewBusNameLine.setVisibility(View.GONE);


            viewContactName.setVisibility(View.GONE);
            viewContactNameLine.setVisibility(View.GONE);
            viewContactNumber.setVisibility(View.GONE);


            viewMobileNumber.setVisibility(View.VISIBLE);
            viewMobileNumberLine.setVisibility(View.VISIBLE);


            viewFuallName.setVisibility(View.VISIBLE);
            viewFuallNameLine.setVisibility(View.VISIBLE);

        }


        if (StringUtils.isEmpty(getInput(editTextEmail))) {
            editTextEmail.setText(user.getResultEmail());
            if (StringUtils.isEmpty(getInput(editTextEmail)) && App.getApp().hasTempKey("regist_email")) {
                editTextEmail.setText((String) App.getApp().getTempObject("regist_email"));
            }
        }


        if (!StringUtils.isEmpty(App.getApp().getUser().getHeaderImage())) {

            String headerUrl = App.getApp().getUser().getHeaderImage();

            Picasso.with(App.getApp()).load(headerUrl)
                    .resize(maskWidth, maskHeight)
                    .transform(new MyBlurTransform(App.getApp()))
                    .centerCrop()
                    .config(Bitmap.Config.RGB_565)
                    .into(imageViewBigHeader, new Callback() {
                        @Override
                        public void onSuccess() {
                            imageViewBottomGlass.setBackgroundResource(R.drawable.bg_cover_photo_mask_cover);
                        }

                        @Override
                        public void onError() {

                        }
                    });

            Target target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    Bitmap bitmapResult = ImageUtils.clipPatchImageNorecyle(bitmap, bitmapMaskHeader);
                    imageViewSmallHeader.setImageBitmap(bitmapResult);
                    textViewAddPhoto.setVisibility(View.GONE);
                    imageViewSmallHeader.setBackgroundResource(R.drawable.bg_back_header);

                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            };

            imageViewSmallHeader.setTag(target);
            Picasso.with(App.getApp()).load(headerUrl)
                    .resize(maskWidth, maskHeight).config(Bitmap.Config.RGB_565)
                    .centerCrop()
                    .into(target);

        }

        if (!StringUtils.isEmpty(user.getPayPalEmail())) {
            textViewPaypalEmail.setText("Paypal:" + user.getPayPalEmail());
        }

    }


    @Override
    public void onBackPressed() {
        backAction();
    }

    private int pic_width = 512;
    private int pic_height = 512;


    @OnClick(R.id.imageViewSmallHeader)
    public void onClickHeaderImage() {
        PhotoSelectDialog photoSelectDialog = new PhotoSelectDialog(this);
        photoSelectDialog.setOnAddCatchListener(new PhotoSelectDialog.OnPhotoSelectionActionListener() {
            @Override
            public void onTakePhotoClick() {

                onTakePhtoClick(maskWidth, maskHeight);


            }

            @Override
            public void onChoosePhotoClick() {
                onChosePhtoClick(maskWidth, maskHeight);
            }

            @Override
            public void onDismis() {

            }
        });
        photoSelectDialog.show();
    }


    @OnTextChanged(R.id.editAddress)
    public void onAddressChaned() {
        showNeedVerify();
    }

//    @OnTextChanged(R.id.editTextCity)
//    public void onTextvaluchagnedForcity() {
//        showNeedVerify();
//    }
//
//    @OnTextChanged(R.id.textViewCountry)
//    public void onCountryChanged() {
//        showNeedVerify();
//    }
//
//    @OnTextChanged(R.id.editTextState)
//    public void onStateChanged() {
//        showNeedVerify();
//    }

    private void showNeedVerify() {

        try {

            User user = App.getApp().getUser();

            String old = user.getPostcode().split("\\|")[0];
            String now = getInput(editAddress);
            if (old.equals(now)) {
                return;
            }

        } catch (Exception e) {
            LogUtil.i(App.tag, "showNeedVerify:" + e.getLocalizedMessage());

        }
        lastVerifySuccess = false;
        editAddress.setTag(null);
        editAddress.setTextColor(getResources().getColor(R.color.red));


    }


    @OnClick(R.id.buttonSave)
    public void onSaveClick() {

        User user = App.getApp().getUser();


        String postcode = getInputFromId(R.id.editTextPostCode);

        String email = getInput(editTextEmail);
        String mobileNumber = getInput(editTextMobileNumber);

        String postAddress = getInput(editAddress) + "," + getInput(editTextCity) + "," + getInput(editTextState) + "," + getInput(textViewCountry);


        String split = App.getApp().getSplit();

        String saveAddress = getInput(editAddress) + split + getInput(editTextCity) + split + getInput(editTextState) + split + postcode;

        if (StringUtils.isEmpty(textViewCountry.getText().toString()) || StringUtils.isEmpty(textViewCurrency.getText().toString())) {
            Tool.showMessageDialog("Please select Country/Currency", ProfileFillActivity.this);
            return;
        }

        String countryCurrency = getInput(textViewCountry) + "/" + getInput(textViewCurrency);

        String fname = getInputFromId(R.id.editTextFirstName) + split + getInputFromId(R.id.editTextLastName);


        if (user.isBuiness()) {

            String businessName = getInput(editTextBusname);
            if (StringUtils.isEmpty(businessName)) {
                Tool.showMessageDialog("Business name should not be empty!", ProfileFillActivity.this);
                return;
            }


            //============================================================================

            String address = getInputFromId(R.id.editAddress);

            if (StringUtils.isEmpty(address)) {
                Tool.showMessageDialog("Address should not be empty!", ProfileFillActivity.this);
                return;
            }


            //============================================================================

            if (!ValidateTool.checkEmail(email)) {
                Tool.showMessageDialog("Please enter a properly formatted email", ProfileFillActivity.this);
                return;
            }


            String contactName = getInput(editTextContactName);
            if (StringUtils.isEmpty(contactName)) {
                Tool.showMessageDialog("Contact name should not be empty!", ProfileFillActivity.this);
                return;
            }

            String contactNumber = getInput(editTextContactNumber);
            if (StringUtils.isEmpty(contactNumber)) {
                Tool.showMessageDialog("Contact number should not be empty!", ProfileFillActivity.this);
                return;
            }


            if (StringUtils.isEmpty(countryCurrency)) {
                Tool.showMessageDialog("Please select Country/Currency", ProfileFillActivity.this);
                return;
            }

            if (needVerify(editAddress, user)) {

                verifyTask(postAddress, editAddress, user, new OnAddressVerifyListener() {

                    @Override
                    public void onInterFaceVerify(boolean success) {
                        if (success) {
                            onSaveClick();
                        }
                    }
                });

                return;
            }

            String country = countryCurrency.split("/")[0];
            String currency = countryCurrency.split("/")[1];


            try {

                JSONObject jsonObject = new JSONObject();

                jsonObject.put("name", getInput(editTextContactName));
                jsonObject.put("phone", getInput(editTextContactNumber));
                jsonObject.put("email", getInput(editTextEmail));
                jsonObject.put("bussness", getInput(editTextBusname));
                jsonObject.put("postcode", saveAddress);

                jsonObject.put("country", country);
                jsonObject.put("currency", currency);


                SmartyStreet address1 = (SmartyStreet) editAddress.getTag();
                if (null != address1) {
                    jsonObject.put("lat", address1.getLatitude());
                    jsonObject.put("lng", address1.getLongtitude());
                }
                updateProfile(ProfileFillActivity.this, jsonObject, new OnRequestEndCallBack() {
                    @Override
                    public void onRequestEnd(NetResult netResult) {

                        if (null != netResult) {
                            if (!StringUtils.isEmpty(netResult.getMessage())) {
                                Tool.showMessageDialog(netResult.getMessage(), ProfileFillActivity.this);
                            } else {
                                updateProfileSuccess(netResult);
                            }
                        } else {
                            Tool.showMessageDialog("Net error!", ProfileFillActivity.this);
                        }

                    }
                });

            } catch (Exception e) {
                LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
            }


        } else {


            if (StringUtils.isEmpty(fname.replace(" ", ""))) {
                Tool.showMessageDialog("Fullname should not be empty!", ProfileFillActivity.this);
                return;
            }

            if (!ValidateTool.checkEmail(getInput(editTextEmail))) {
                Tool.showMessageDialog("Please enter a properly formatted email", ProfileFillActivity.this);
                return;
            }

            if (StringUtils.isEmpty(getInput(editTextMobileNumber))) {
                Tool.showMessageDialog("Mobile number should not be empty!", ProfileFillActivity.this);
                return;
            }

            if (StringUtils.isEmpty(countryCurrency)) {
                Tool.showMessageDialog("Please select Country/Currency", ProfileFillActivity.this);
                return;
            }

            String country = countryCurrency.split("/")[0];
            String currency = countryCurrency.split("/")[1];


            if (StringUtils.isEmpty(getInput(editAddress))) {
                Tool.showMessageDialog("Postal address should not be empty!", ProfileFillActivity.this);
                return;
            }

            if (needVerify(editAddress, user)) {

                verifyTask(postAddress, editAddress, user, new OnAddressVerifyListener() {
                    @Override
                    public void onInterFaceVerify(boolean success) {

                        if (success) {
                            onSaveClick();
                        }
                    }
                });

                return;
            }


            try {

                JSONObject jsonObject = new JSONObject();


                jsonObject.put("name", fname);
                jsonObject.put("phone", mobileNumber);
                jsonObject.put("email", email);
                jsonObject.put("postcode", saveAddress);

                jsonObject.put("country", country);
                jsonObject.put("currency", currency);


                //=========

                SmartyStreet address1 = (SmartyStreet) editAddress.getTag();
                if (null != address1) {
                    jsonObject.put("lat", address1.getLatitude());
                    jsonObject.put("lng", address1.getLongtitude());
                }

                updateProfile(ProfileFillActivity.this, jsonObject, new OnRequestEndCallBack() {
                    @Override
                    public void onRequestEnd(NetResult netResult) {

                        if (null != netResult) {
                            if (!StringUtils.isEmpty(netResult.getMessage())) {
                                Tool.showMessageDialog(netResult.getMessage(), ProfileFillActivity.this);
                            } else {

                                updateProfileSuccess(netResult);
                            }
                        } else {
                            Tool.showMessageDialog("Net error!", ProfileFillActivity.this);
                        }

                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }


    private boolean checkValue() {


        User user = App.getApp().getUser();


        String postcode = getInputFromId(R.id.editTextPostCode);

        String email = getInput(editTextEmail);
        String mobileNumber = getInput(editTextMobileNumber);

        String postAddress = getInput(editAddress) + "," + getInput(editTextCity) + "," + getInput(editTextState) + "," + getInput(textViewCountry);


        String split = App.getApp().getSplit();

        String saveAddress = getInput(editAddress) + split + getInput(editTextCity) + split + getInput(editTextState) + split + postcode;

        if (StringUtils.isEmpty(textViewCountry.getText().toString()) || StringUtils.isEmpty(textViewCurrency.getText().toString())) {

            return false;
        }

        String countryCurrency = getInput(textViewCountry) + "/" + getInput(textViewCurrency);

        String fname = getInputFromId(R.id.editTextFirstName) + split + getInputFromId(R.id.editTextLastName);


        if (user.isBuiness()) {

            String businessName = getInput(editTextBusname);
            if (StringUtils.isEmpty(businessName)) {
//                Tool.showMessageDialog("Business name should not be empty!", ProfileFillActivity.this);
                return false;
            }


            //============================================================================

            String address = getInputFromId(R.id.editAddress);

            if (StringUtils.isEmpty(address)) {
//                Tool.showMessageDialog("Address should not be empty!", ProfileFillActivity.this);
//                return;
                return false;
            }


            //============================================================================

            if (!ValidateTool.checkEmail(email)) {
//                Tool.showMessageDialog("Please enter a properly formatted email", ProfileFillActivity.this);
//                return;
                return false;
            }


            String contactName = getInput(editTextContactName);
            if (StringUtils.isEmpty(contactName)) {
//                Tool.showMessageDialog("Contact name should not be empty!", ProfileFillActivity.this);
//                return;
                return false;
            }

            String contactNumber = getInput(editTextContactNumber);
            if (StringUtils.isEmpty(contactNumber)) {
//                Tool.showMessageDialog("Contact number should not be empty!", ProfileFillActivity.this);
                return false;
            }


            if (StringUtils.isEmpty(countryCurrency)) {
//                Tool.showMessageDialog("Please select Country/Currency", ProfileFillActivity.this);
                return false;
            }

            if (needVerify(editAddress, user)) {

//                verifyTask(postAddress, editAddress, user, new OnAddressVerifyListener() {
//
//                    @Override
//                    public void onInterFaceVerify(boolean success) {
//                        if (success) {
//                            onSaveClick();
//                        }
//                    }
//                });

                return false;
            }


        } else {


            if (StringUtils.isEmpty(fname.replace(" ", ""))) {
//                Tool.showMessageDialog("Fullname should not be empty!", ProfileFillActivity.this);
                return false;
            }

            if (!ValidateTool.checkEmail(getInput(editTextEmail))) {
//                Tool.showMessageDialog("Please enter a properly formatted email", ProfileFillActivity.this);
                return false;
            }

            if (StringUtils.isEmpty(getInput(editTextMobileNumber))) {
//                Tool.showMessageDialog("Mobile number should not be empty!", ProfileFillActivity.this);
                return false;
            }

            if (StringUtils.isEmpty(countryCurrency)) {
//                Tool.showMessageDialog("Please select Country/Currency", ProfileFillActivity.this);
                return false;
            }

            String country = countryCurrency.split("/")[0];
            String currency = countryCurrency.split("/")[1];


            if (StringUtils.isEmpty(getInput(editAddress))) {
//                Tool.showMessageDialog("Postal address should not be empty!", ProfileFillActivity.this);
                return false;
            }

            if (needVerify(editAddress, user)) {

//                verifyTask(postAddress, editAddress, user, new OnAddressVerifyListener() {
//                    @Override
//                    public void onInterFaceVerify(boolean success) {
//
//                        if (success) {
//                            onSaveClick();
//                        }
//                    }
//                });

                return false;
            }

        }

        return true;

    }


    private boolean lastVerifySuccess = false;

    private boolean needVerify(EditText editAddress, User user) {

        if (null == editAddress.getTag()) {
            return true;
        }

        if (lastVerifySuccess == true) {
            return false;
        }


        try {
            String addres = user.getPostcode().split("\\|")[0];
            if (!getInput(editAddress).equals(addres)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return true;
        }

    }

    public void updateProfileSuccess(NetResult netResult) {
        User user = (User) netResult.getData()[0];

        User oldUser = App.getApp().getUser();
        oldUser.copyFromPartyUser(user);
        App.getApp().setUser(oldUser);
        initUserView(oldUser);

        try {
            //save new email& password ,aftered verify auto login
            SharePersistent.setObjectValue(this, App.KEY_USER, getInput(editTextEmail));
            String kyserPwd = (String) SharePersistent.getObjectValue(this, keyemailPwd);
            if (null != kyserPwd) {
                String pwd = kyserPwd.split(",")[1];
                SharePersistent.setObjectValue(this, keyemailPwd, getInput(editTextEmail) + "," + pwd);
            }
        } catch (Exception e) {
            LogUtil.i(App.tag, "save new email fail on ProfileFillActivity");
        }

        if (App.getApp().hasTempKey("tag") && true == (Boolean) App.getApp().getTempObject("tag")) {

            //===============================================xxxx LikeFb================================================================
            App.getApp().putTemPObject("tag", true);
//            Tool.startActivityForResult(ProfileFillActivity.this, VerifyActivity.class, INT_GO_VERIFY);

            Tool.startActivity(this, ReceivingMethodActivity.class);
            finish();


            //===============================================xxxx LikeFb================================================================
        } else if (App.getApp().hasTempKey("fromProfile") && (boolean) App.getApp().getTempObject("fromProfile") == true) {

//           String oldUser = (String) SharePersistent.getObjectValue(this, App.KEY_USER);
            //保存新的email
            setResult(RESULT_OK);
            finish();

        } else if (App.getApp().hasTempKey("fromCard") && (boolean) App.getApp().getTempObject("fromCard") == true) {
            setResult(RESULT_OK);
            finish();
        } else {
            gotoPofileUserPage();
        }

    }


    @OnClick(R.id.linearLayoutReceiveMethod)
    public void onClickReceiveMethod() {
        Tool.startActivity(this, ReceivingMethodActivity.class);
    }


    @OnClick(R.id.viewChangepwd)
    public void changePwd() {
        Tool.startActivity(this, ChangePasswordActivity.class);
    }


    private void gotoPofileUserPage() {

        Tool.startActivity(this, ProfileUserActivity.class);
        finish();
    }


    private void initHeaderLayoutParam() {

        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(p.x, p.y / 2);
        imageViewBigHeader.setLayoutParams(rlp);

        RelativeLayout.LayoutParams rlp2 = new RelativeLayout.LayoutParams(p.x, p.y / 2);
        imageViewBottomGlass.setLayoutParams(rlp2);


        bitmapMaskHeader = BitmapFactory.decodeResource(getResources(), R.drawable.bg_mask_header);

        LogUtil.e(App.tag, " mask dimens:" + bitmapMaskHeader.getWidth() + " " + bitmapMaskHeader.getHeight());

        this.maskWidth = bitmapMaskHeader.getWidth();
        this.maskHeight = bitmapMaskHeader.getHeight();

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == SHORT_REQUEST_READ_EXTEAL) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                if (type_request_picture == TYPE_REQEST_PICTURE_SELECT) {
                    startSelectPicture();
                } else {
                    startTakePicture();
                }
            } else {
                showNotifyTextIn5Seconds("Access denied,Please try again!");
            }
        }

    }


    private void getPayPalInfo(final String userId, final String authCode) {

        reSetTask();

        baseTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("code", authCode);
                    netResult = NetInterface.getPayPalInfo(ProfileFillActivity.this, userId, jsonObject);
                } catch (Exception e) {
                    LogUtil.i(App.tag, "getPay payl info error:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (StringUtils.isEmpty(result.getMessage())) {

                        String payPalEmail = (String) result.getData()[0];

                        User user = App.getApp().getUser();

                        user.setPayPalEmail(payPalEmail);

                        App.getApp().setUser(user);

                        showToastWithTime("PayPal email:" + payPalEmail + " link Paypal success!", 4);


                        if (!StringUtils.isEmpty(user.getPayPalEmail())) {
                            textViewPaypalEmail.setText("Paypal:" + user.getPayPalEmail());
                        }

                    } else {
                        showToastWithTime(result.getMessage(), 4);
                    }
                } else {
                    showToastWithTime("Net error!", 4);
                }
            }
        });
        baseTask.execute(new HashMap<String, String>());

    }


    private void onVerifyEnd(int resultCode) {
        if (isFromRegist) {

            //fb sdk error
//            Tool.startActivity(ProfileFillActivity.this, LikeFbActivity.class);

            gotoPofileUserPage();

        } else {
            if (resultCode == RESULT_OK) {
                gotoPofileUserPage();

            } else if (resultCode == SHORT_REQUEST_CANCEL_VERIFY) {
                gotoPofileUserPage();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == INT_GO_VERIFY) {
            onVerifyEnd(resultCode);
            return;
        }


        if (requestCode == SHORT_REQUEST_FILL_CARD) {

            getProfile(this, new OnRequestEndCallBack() {
                @Override
                public void onRequestEnd(NetResult netResult) {
                    if (null != netResult) {
                        User user = (User) netResult.getData()[0];
                        App.getApp().setUser(user);
                        initUserView(user);
                    } else {
                        Tool.showMessageDialog("Net error!", ProfileFillActivity.this);
                    }
                }
            });


            return;
        }


        if (requestCode == INT_GO_PAYPAL_LOGIN) {
            if (resultCode == RESULT_OK) {
                User user = App.getApp().getUser();
                String id = user.getUserId();
                if (StringUtils.isEmpty(id)) {
                    id = user.getId();
                }
                getPayPalInfo(id, (String) App.getApp().getTempObject("code"));
            } else {
                showToastWithTime("Login Paypal failed!", 4);
            }

            return;
        }


//        if (requestCode == short_go_select_postal_address && resultCode == Activity.RESULT_OK) {
//            android.location.Address address = (android.location.Address) App.getApp().getTempObject("address");
//            if (null != address) {
//
//                String featureName = address.getFeatureName();
//                String area = address.getAdminArea();
//                String country = address.getCountryName();
//
//                textViewPostalAddress.setText(featureName + (StringUtils.isEmpty(area) ? "" : "," + area) + (StringUtils.isEmpty(country) ? "" : "," + country));
//                textViewPostalAddress.setTag(address);
//                viewCheckPostAddress.setVisibility(View.VISIBLE);
//
//            }
//            return;
//        }


        //=======deal pick pciture from camera or location store
        String mFileName;
        if (requestCode == SELECT_PIC && resultCode == RESULT_OK) {
            if (resultCode == RESULT_OK && data != null) {
                mFileName = PhotoLibUtils.getDataColumn(getApplicationContext(), data.getData(), null, null);
                PhotoLibUtils.copyFile(mFileName, sharedpreferencesUtil.getImageTempNameString2());
                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
                cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
            }
        }
        if (requestCode == SELECT_PIC_KITKAT && resultCode == RESULT_OK) {
            if (resultCode == RESULT_OK && data != null) {
                mFileName = PhotoLibUtils.getPath(getApplicationContext(), data.getData());
                String newPath = sharedpreferencesUtil.getImageTempNameString2();
                PhotoLibUtils.copyFile(mFileName, newPath);
                Uri uri = sharedpreferencesUtil.getImageTempNameUri();
                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
                cropImageUri(uri, outoutUri, outputX, outputY, CROP_BIG_PICTURE);

            }
        }
        if (requestCode == TAKE_BIG_PICTURE && resultCode == RESULT_OK) {
            Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
            cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
        }


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);


            if (resultCode == RESULT_OK) {
                String imgURI = result.getUri().getPath();
                LogUtil.i(App.tag, "croped img url:" + imgURI);
//                ((ImageView) findViewById(R.id.quick_start_cropped_image)).setImageURI(result.getUri());

                onCropImgSuccess(imgURI);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

//                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();

            }

        }


        //old version code
        if (requestCode == CROP_BIG_PICTURE && resultCode == Activity.RESULT_OK) {
            //after crop img

            if (sharedpreferencesUtil.getImageTempNameUri() != null) {
                Uri result = sharedpreferencesUtil.getImageTempNameUriCroped();
                App.firstRun();

                if (!StringUtils.isEmpty(result.getPath())) {
                    final String picPath = result.getPath();
                    LogUtil.i(App.tag, "src path:" + picPath);
                    onCropImgSuccess(result.getPath());
                }
            }
        }

    }


    private void onCropImgSuccess(String picPath) {

        File selectFile = new File(picPath);
        if (null != selectFile && selectFile.exists()) {
            try {

                upLoadHeaderImg(ProfileFillActivity.this, picPath, new OnUploadCallBack() {
                    @Override
                    public void onUploadEnd(NetResult netResult, Context context) {
                        if (null != context) {
                            if (null != netResult) {
                                if (!StringUtils.isEmpty(netResult.getMessage())) {
                                    Tool.showMessageDialog(netResult.getMessage(), (Activity) context);
                                } else {
//                                                Tool.showMessageDialog("Upload success!", (Activity) context);

                                    String headerUrl = (String) netResult.getData()[0];

                                    User currentUser = App.getApp().getUser();
                                    currentUser.setHeaderImage(headerUrl);

                                    App.getApp().setUser(currentUser);
//                                    Picasso.with(App.getApp()).invalidate(headerUrl);

                                    Picasso.with(App.getApp()).load(headerUrl)
                                            .resize(maskWidth, maskHeight)
//                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .transform(new MyBlurTransform(App.getApp()))
                                            .config(Bitmap.Config.RGB_565)
                                            .into(imageViewBigHeader);


                                    Target target = new Target() {
                                        @Override
                                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                            Bitmap bitmapResult = ImageUtils.clipPatchImageNorecyle(bitmap, bitmapMaskHeader);
                                            imageViewSmallHeader.setImageBitmap(bitmapResult);
                                            textViewAddPhoto.setVisibility(View.GONE);
                                            imageViewSmallHeader.setBackgroundResource(R.drawable.bg_back_header);
                                            imageViewBottomGlass.setBackgroundResource(R.drawable.bg_cover_photo_mask_cover);

                                        }

                                        @Override
                                        public void onBitmapFailed(Drawable errorDrawable) {

                                        }

                                        @Override
                                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                                        }
                                    };

                                    imageViewSmallHeader.setTag(target);
                                    Picasso.with(App.getApp()).load(headerUrl)
//                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .resize(maskWidth, maskHeight).config(Bitmap.Config.RGB_565)
//                                            .noFade()
                                            .into(target);

                                }
                            } else {
                                Tool.showMessageDialog("Net error!", (Activity) context);
                            }
                        }
                    }
                });

            } catch (Exception e) {
                LogUtil.e(App.tag, "compress picture fail!");
            }
        } else {
            LogUtil.i(App.tag, "file not exist!");
        }

    }


    public static interface OnAddressVerifyListener {

        public void onInterFaceVerify(boolean success);
    }


}
