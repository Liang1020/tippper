package com.tipppr.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.callback.OnRequestEndCallBack;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.adapter.SearchedAdapter;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.net.NetInterface;
import com.zbar.lib.CaptureActivity;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class SearchUserActivity extends BaseTippprActivity {

    @InjectView(R.id.listViewSearchResult)
    ListView listView;


    @InjectView(R.id.viewScan)
    View viewScan;

    @InjectView(R.id.editTextInput)
    EditText editTextInput;

    @InjectView(R.id.textViewTitle)
    TextView textViewTitle;

    private ArrayList<User> arrayListUsers;
    private SearchedAdapter searchedAdapter;
    private short SHORT_GO_PROFILE = 30;

    private static final short REQUEST_CODE_SCAN = 112;
    private static final short REQUEST_CODE_SCAN_TIP = 113;

    private static final String DECODED_CONTENT_KEY = "codedContent";
    private static final String DECODED_BITMAP_KEY = "codedBitmap";

    private boolean isfromTip = false;
    private String type_search;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);
        ButterKnife.inject(this);

        currentPage = 1;
        initView();
    }


    @OnClick(R.id.viewScan)
    public void onScanClick() {
        Intent intent = new Intent(this, CaptureActivity.class);
        startActivityForResult(intent, REQUEST_CODE_SCAN);
    }

    @Override
    public void initView() {

        arrayListUsers = new ArrayList<>();
        searchedAdapter = new SearchedAdapter(this, arrayListUsers);
        listView.setAdapter(searchedAdapter);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    // 当不滚动时
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        // 判断滚动到底部
                        if (view.getLastVisiblePosition() == (view.getCount() - 1)) {
                            LogUtil.i(App.tag, "滚动到底部");
                            if (arrayListUsers.size() < totallCount) {
                                String input = getInput(editTextInput);
                                searchByName(input, false);
                            }
                        }
                        break;
                }

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        if (App.getApp().getUser().isBuiness()) {
            textViewTitle.setText("Add Associate");
        } else {
            textViewTitle.setText("Add Workplace");
        }

        if (App.getApp().hasTempKey("fromTip")) {
            App.getApp().getTempObject("fromTip");
            textViewTitle.setText("Search");
            isfromTip = true;
//            viewScan.setVisibility(View.INVISIBLE);
            type_search = (String) App.getApp().getTempObject("type");
        } else {
            viewScan.setVisibility(View.VISIBLE);
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                final User user = (User) parent.getAdapter().getItem(position);
                App.getApp().putTemPObject("user", user);
                App.getApp().putTemPObject("fromTip", isfromTip);

                Tool.startActivityForResult(SearchUserActivity.this, ProfileAnyUserActivity.class, SHORT_GO_PROFILE);


//                final User user = (User) parent.getAdapter().getItem(position);
//
//                DialogUtils.showConfirmDialog(SearchUserActivity.this, "", "Add " + user.getProfileName() + " to friend list ?", "confirm", "cancel", new DialogCallBackListener() {
//                    @Override
//                    public void onDone(boolean yesOrNo) {
//                        if (yesOrNo) {
//                            addUserList(user);
//                        }
//                    }
//                });


            }
        });

    }


    @OnTextChanged(R.id.editTextInput)
    public void onInputChanged() {
        String input = getInput(editTextInput);
        if (!StringUtils.isEmpty(input)) {

        }
    }


    @OnClick(R.id.viewSearch)
    public void onClickSearch() {
        String input = getInput(editTextInput);
        if (!StringUtils.isEmpty(input)) {
            searchByName(input, true);
        }

    }


    public void searchByName(final String name, final boolean isRefresh) {

        reSetTask();

        baseTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {

                    JSONObject json = new JSONObject();
                    json.put("name", name);

                    String type = "";

                    if (App.getApp().getUser().isBuiness()) {
                        type = User.ROLE_PERSONAL;
                    } else {
                        type = User.ROLE_BUSINESS;
                    }

                    if (isfromTip) {
                        json.put("type", type_search);
                    } else {
                        json.put("type", type);
                    }

                    if (null != App.getApp().getCurrentLatlng()) {

                        json.put("lat", App.getApp().getCurrentLatlng().latitude);
                        json.put("lng", App.getApp().getCurrentLatlng().longitude);
                    }

                    json.put("page", currentPage++);
                    json.put("pageSize", pageSize);

                    netResult = NetInterface.search(baseTask.weakReferenceContext.get(), json);

                } catch (Exception e) {
                    LogUtil.e(App.tag, "search error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (!StringUtils.isEmpty(result.getMessage())) {
                        Tool.showMessageDialog(result.getMessage(), (Activity) baseTask.weakReferenceContext.get());
                        currentPage--;
                    } else {
                        buildAdapter((ArrayList<User>) result.getData()[0], isRefresh);
                        totallCount = Integer.parseInt((String) result.getData()[1]);
//                        currentPage=Integer.parseInt((String)result.getData()[2]);
                    }
                } else {
                    currentPage--;
                    Tool.showMessageDialog("Net error!", (Activity) baseTask.weakReferenceContext.get());
                }

            }
        }, this);

        baseTask.execute(new HashMap<String, String>());

    }

    private void buildAdapter(ArrayList<User> newUsers, final boolean isreFresh) {
        if (isreFresh) {
            this.arrayListUsers.clear();
        }

        LogUtil.e(App.tag, "search user count:" + newUsers.size());
        this.arrayListUsers.addAll(newUsers);
        LogUtil.e(App.tag, "total pull:" + ListUtiles.getListSize(arrayListUsers));
        searchedAdapter.notifyDataSetChanged();


    }


    @OnClick(R.id.viewBack)
    public void onClickBack() {
        finish();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SHORT_GO_PROFILE && resultCode == Activity.RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }

        // 扫描二维码/条码回传
        if (requestCode == REQUEST_CODE_SCAN && resultCode == RESULT_OK) {
            if (data != null) {
                final String userId = data.getStringExtra(DECODED_CONTENT_KEY);
//                Bitmap bitmap = data.getParcelableExtra(DECODED_BITMAP_KEY);

                getAllProfile(this, userId, new OnRequestEndCallBack() {
                    @Override
                    public void onRequestEnd(NetResult netResult) {

                        if (null != netResult) {

                            User user = (User) netResult.getData()[0];
                            User me = App.getApp().getUser();

                            if (me.getUserId().equals(userId)) {
                                Tool.showMessageDialog("You can not scan yourself", SearchUserActivity.this);
                                return;
                            }

                            if (user.getLevel().equals(me.getLevel()) && !isfromTip) {
                                Tool.showMessageDialog("Oops", "Not able to add a user of the same type.", SearchUserActivity.this);
//                                Tool.showMessageDialog("You can not scan yourself", SearchUserActivity.this);
                                return;
                            }

                            user.setUserId(userId);
                            App.getApp().putTemPObject("user", user);
                            App.getApp().putTemPObject("fromTip", isfromTip);
                            Tool.startActivityForResult(SearchUserActivity.this, ProfileAnyUserActivity.class, REQUEST_CODE_SCAN_TIP);


                        } else {
                            Tool.showMessageDialog(R.string.error_net, SearchUserActivity.this);
                        }

                    }
                });


            }
        }

        if (requestCode == REQUEST_CODE_SCAN_TIP && resultCode == RESULT_OK) {

            setResult(RESULT_OK);
            finish();

        }


    }
}
