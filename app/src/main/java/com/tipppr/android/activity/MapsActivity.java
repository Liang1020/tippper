package com.tipppr.android.activity;

import android.os.Bundle;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MapsActivity extends BaseTippprActivity implements OnMapReadyCallback {

    private GoogleMap mMap;


    @InjectView(R.id.textViewTitle)
    TextView textViewTitle;

    private String title;
    private LatLng latLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        ButterKnife.inject(this);

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        title = (String) App.getApp().getTempObject("address");
        latLng = (LatLng) App.getApp().getTempObject("latlng");

        textViewTitle.setText(title);

    }

    @OnClick(R.id.viewBack)
    public void onBackClick() {
        finish();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (null != latLng) {

            mMap.getUiSettings().setMapToolbarEnabled(false);

            mMap.addMarker(new MarkerOptions().position(latLng).title(title));

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,googleMap.getMaxZoomLevel()/2));
        }

    }
}
