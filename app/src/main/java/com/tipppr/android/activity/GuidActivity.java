package com.tipppr.android.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.common.util.SharePersistent;
import com.common.util.Tool;
import com.indicator.view.IndicatorView;
import com.tipppr.adapter.GuildAdapter;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class GuidActivity extends BaseTippprActivity {


    @InjectView(R.id.viePager)
    ViewPager viewPager;

    @InjectView(R.id.indicatorView)
    IndicatorView indicatorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guid);
        ButterKnife.inject(this);


        GuildAdapter guildAdapter = new GuildAdapter(this, viewPager);

        indicatorView.setIndicatorSize(5, R.drawable.icon_indicator_uncheck, R.drawable.icon_indicator_check);

        viewPager.setAdapter(guildAdapter);

        guildAdapter.setOnSkipListener(new GuildAdapter.OnSkipListener() {
            @Override
            public void onSkipClick() {
                Tool.startActivity(GuidActivity.this, LoginSelectActivity.class);
                SharePersistent.saveBoolean(GuidActivity.this, "WatchGuid", true);
                finish();
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                indicatorView.checkIndex(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }
}
