package com.tipppr.android.activity;

import android.os.Bundle;

import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class TermsActivity extends BaseTippprActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        ButterKnife.inject(this);


    }

    @OnClick(R.id.viewBack)
    public void onViewBackClick() {
        finish();
    }
}
