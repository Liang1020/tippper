package com.tipppr.android.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.callback.OnRequestEndCallBack;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.tipppr.adapter.SpninnerAdapter;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.json.JSONHelper;
import com.tipppr.tipppr.net.NetInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class ReceivingMethodActivity extends BaseTippprActivity {


    @InjectView(R.id.spinnerCountry)
    Spinner spinnerCountry;

    @InjectView(R.id.linearLayoutBankTransferContent)
    LinearLayout linearLayoutBankTransferContent;

//
//    @InjectView(R.id.checkBoxBankTransfer)
//    CheckBox checkBoxBankTransfer;
//
//    @InjectView(R.id.checkBoxCheque)
//    CheckBox checkBoxCheque;
//
//    @InjectView(R.id.checkBoxTippprDebit)
//    CheckBox checkBoxTippprDebit;
//
//    @InjectView(R.id.checkBoxPaypal)
//    CheckBox checkBoxPaypal;
//
//    @InjectView(R.id.textViewAsk)
//    TextView textViewAsk;

    @InjectView(R.id.textViewBSBTitle)
    TextView textViewBSBTitle;

    @InjectView(R.id.editTextBsb)
    EditText editTextBsb;

    @InjectView(R.id.editTextAccountNumber)
    EditText editTextAccountNumber;

    @InjectView(R.id.edittextAccountName)
    EditText edittextAccountName;

    @InjectView(R.id.editTextCheque)
    EditText editTextCheque;

    @InjectView(R.id.editTextPayPalName)
    EditText editTextPayPalName;

    @InjectView(R.id.editTextPaypalId)
    EditText editTextPaypalId;

    @InjectView(R.id.editTextTipperDebit)
    EditText editTextTipperDebit;

    @InjectView(R.id.textViewTitle)
    TextView textViewTitle;

    @InjectView(R.id.textViewTitleStep)
    TextView textViewTitleStep;

    @InjectView(R.id.textViewSkip)
    TextView textViewSkip;


    @InjectView(R.id.lienarLayoutTipmethods)
    LinearLayout lienarLayoutTipmethods;

    @InjectView(R.id.textViewPleaseSelect)
    TextView textViewPleaseSelect;

    @InjectView(R.id.imageViewMethodIcon)
    ImageView imageViewMethodIcon;


    //=======
    @InjectView(R.id.lineayLayoutBankTransfer)
    LinearLayout lineayLayoutBankTransfer;

    @InjectView(R.id.linearlayoutCheque)
    LinearLayout linearlayoutCheque;


    @InjectView(R.id.linearlayoutPaypal)
    LinearLayout linearlayoutPaypal;

    @InjectView(R.id.linearLayoutTippprDebit)
    LinearLayout linearLayoutTippprDebit;

    ArrayList<View> arrayListPayView = new ArrayList<>();


    private ArrayList<CheckBox> arrayListCheckBoxs;
    private boolean isFromRegist = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiving_method);
        ButterKnife.inject(this);

        arrayListCheckBoxs = new ArrayList<>();

        arrayListPayView.add(lineayLayoutBankTransfer);
        arrayListPayView.add(linearlayoutCheque);
        arrayListPayView.add(linearlayoutPaypal);
        arrayListPayView.add(linearLayoutTippprDebit);


//        arrayListCheckBoxs.add(checkBoxBankTransfer);
//        arrayListCheckBoxs.add(checkBoxCheque);
//        arrayListCheckBoxs.add(checkBoxTippprDebit);
//        arrayListCheckBoxs.add(checkBoxPaypal);


//        App.getApp().putTemPObject("tag", true);

        if (App.getApp().hasTempKey("tag") && ((Boolean) App.getApp().getTempObject("tag")) == true) {
            isFromRegist = true;
        } else {
            isFromRegist = false;
        }

        initView();

    }


    @OnClick(R.id.textViewBankTransfer)
    public void onClickMethodBankTransker() {
        lienarLayoutTipmethods.setVisibility(View.GONE);
        imageViewMethodIcon.setImageResource(R.drawable.icon_arrow_method_down);

        textViewPleaseSelect.setText("Bank Transfer");
        textViewPleaseSelect.setTag("0");
        showByTag("0");

    }

    @OnClick(R.id.textViewCheque)
    public void onCleckMethodCeque() {
        lienarLayoutTipmethods.setVisibility(View.GONE);
        imageViewMethodIcon.setImageResource(R.drawable.icon_arrow_method_down);
        textViewPleaseSelect.setText("Check/Cheque");
        textViewPleaseSelect.setTag("1");
        showByTag("1");
    }

    @OnClick(R.id.textViewPaypal)
    public void onClickpaypal() {
        lienarLayoutTipmethods.setVisibility(View.GONE);
        imageViewMethodIcon.setImageResource(R.drawable.icon_arrow_method_down);
        textViewPleaseSelect.setText("PayPal");
        textViewPleaseSelect.setTag("2");
        showByTag("2");
    }

    @OnClick(R.id.textViewTippprDebit)
    public void onCLickDebitMethod() {
        lienarLayoutTipmethods.setVisibility(View.GONE);
        imageViewMethodIcon.setImageResource(R.drawable.icon_arrow_method_down);
        textViewPleaseSelect.setText("TIPPPR Debit Card");
        textViewPleaseSelect.setTag("3");
        showByTag("3");
    }


    @OnClick(R.id.viewPleaseSelect)
    public void onSelectClick() {
        if (View.VISIBLE == lienarLayoutTipmethods.getVisibility()) {
            lienarLayoutTipmethods.setVisibility(View.GONE);
            imageViewMethodIcon.setImageResource(R.drawable.icon_arrow_method_down);

        } else {
            lienarLayoutTipmethods.setVisibility(View.VISIBLE);
            imageViewMethodIcon.setImageResource(R.drawable.icon_arrow_method_up);
        }
    }


    private void showByTag(String tag) {

        for (View v : arrayListPayView) {
            if (tag.equals((String) v.getTag())) {
                v.setVisibility(View.VISIBLE);
            } else {
                v.setVisibility(View.GONE);
            }
        }


    }


    @OnClick(R.id.buttonSave)
    public void onSaveButtonClick() {

        JSONObject jsonObjectRemethod = new JSONObject();

        try {

            if ("0".equals((String) textViewPleaseSelect.getTag())) {

                //====bank transfer
                String bankContry = ((String) spinnerCountry.getSelectedItem());
                if ("select country".equals(bankContry)) {
                    Tool.showMessageDialog("Please select country.", this);
                    return;
                }

                String bankAccountName = getInputFromId(R.id.edittextAccountName);
                if (StringUtils.isEmpty(bankAccountName)) {
                    Tool.showMessageDialog("Bank account name is empty", this);
                    return;
                }

                String bankAccountNumber = getInputFromId(R.id.editTextAccountNumber);
                if (StringUtils.isEmpty(bankAccountNumber) || bankAccountNumber.replace("-", "").length() != 16) {
                    Tool.showMessageDialog("Please input 16 bank numbers.", this);
                    return;
                }

                String bsb = getInputFromId(R.id.editTextBsb);

                if (StringUtils.isEmpty(bsb)) {
                    Tool.showMessageDialog("BSB is empty.", this);
                    return;
                }


                JSONObject banktransferObject = new JSONObject();


                banktransferObject.put("country", bankContry);
                banktransferObject.put("account_name", bankAccountName);
                banktransferObject.put("account_number", bankAccountNumber);
                banktransferObject.put("bsb", bsb);


                JSONObject bankTrasferObj = new JSONObject();

                bankTrasferObj.put("bank_transfer", banktransferObject);

                jsonObjectRemethod.put("method", bankTrasferObj);

            }

            //=======cheque
            String cheque = getInputFromId(R.id.editTextCheque);
            if ("1".equals((String) textViewPleaseSelect.getTag())) {
                if (StringUtils.isEmpty(cheque)) {

                    Tool.showMessageDialog("Cheque is empty", this);

                    return;
                }

                JSONObject chequeObject = new JSONObject();

                chequeObject.put("cheque", cheque);

                jsonObjectRemethod.put("method", chequeObject);

            }

            if ("2".equals(textViewPleaseSelect.getTag())) {

                //===paypal

                String paypalName = getInputFromId(R.id.editTextPayPalName);

                if (StringUtils.isEmpty(paypalName)) {
                    Tool.showMessageDialog("Paypal name is empty.", this);
                    return;
                }


                String paypalId = getInputFromId(R.id.editTextPaypalId);
                if (StringUtils.isEmpty(paypalId)) {
                    Tool.showMessageDialog("Paypal id is empty.", this);
                    return;
                }

                JSONObject paypalObject = new JSONObject();

                paypalObject.put("name", paypalName);
                paypalObject.put("paypalId", paypalId);


                JSONObject paypMethod = new JSONObject();

                paypMethod.put("paypal", paypalObject);

                jsonObjectRemethod.put("method", paypMethod);

            }

            //=======debit card
            String debitCard = getInputFromId(R.id.editTextTipperDebit);
            if ("3".equals(textViewPleaseSelect.getTag())) {

                if (StringUtils.isEmpty(debitCard)) {
                    Tool.showMessageDialog("Tipppr debit card is empty. ", this);
                    return;
                }


                JSONObject tipprDebitObject = new JSONObject();

                tipprDebitObject.put("tipppr_debit", debitCard);

                jsonObjectRemethod.put("method", tipprDebitObject);

            }


        } catch (Exception e) {

            LogUtil.e(App.tag,"save paymethod json exception");
        }


        saveReceiveMethod(jsonObjectRemethod);

        LogUtil.i(App.tag, jsonObjectRemethod.toString());

    }


    public void saveReceiveMethod(final JSONObject jsonObject) {

        reSetTask();

        baseTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    netResult = NetInterface.saveReceiveMethod(ReceivingMethodActivity.this, jsonObject);
                } catch (Exception e) {
                    LogUtil.i(App.tag, "save method error:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (StringUtils.isEmpty(result.getMessage())) {

                        getProfile(ReceivingMethodActivity.this, new OnRequestEndCallBack() {
                            @Override
                            public void onRequestEnd(NetResult netResult) {

                                if (null != netResult) {

                                    User user = (User) netResult.getData()[0];
                                    App.getApp().setUser(user);

                                    if (isFromRegist) {
                                        gotoCreditCardActivity();
                                    } else {
                                        setResult(RESULT_OK);
                                        finish();
                                    }

                                }

                            }
                        });

                    } else {
                        Tool.showMessageDialog(result.getMessage(), ReceivingMethodActivity.this);
                    }

                } else {
                    LogUtil.i(App.tag, "save method error");
                    showNotifyTextIn5Seconds(R.string.error_net);
                }
            }
        });

        baseTask.execute(new HashMap<String, String>());

    }


    private void gotoCreditCardActivity() {

        App.getApp().putTemPObject("tag", true);
        Tool.startActivity(ReceivingMethodActivity.this, CreditCardActivity.class);
    }


    private void checkId(int id) {

        for (CheckBox cx : arrayListCheckBoxs) {
            if (cx.getId() == id) {
//                cx.setChecked(true);
            } else {
                cx.setChecked(false);
            }
        }

    }


    @OnClick(R.id.textViewSkip)
    public void onSkipClick() {
        gotoCreditCardActivity();
    }


    private int selectItems = 0;

    @Override
    public void initView() {


        if (isFromRegist) {
            textViewTitle.setVisibility(View.INVISIBLE);
            textViewTitleStep.setVisibility(View.VISIBLE);
            textViewSkip.setVisibility(View.VISIBLE);

        } else {
            textViewTitle.setVisibility(View.VISIBLE);
            textViewTitleStep.setVisibility(View.INVISIBLE);
            textViewSkip.setVisibility(View.INVISIBLE);
        }


        String[] values = {"select country", "USA", "Cannada", "Australia"};
        String[] labels = {"select country", "USA", "Cannada", "Australia"};

        SpninnerAdapter spninnerAdapter = new SpninnerAdapter(this, values, labels);
        spinnerCountry.setAdapter(spninnerAdapter);
        spinnerCountry.setSelected(false);
        linearLayoutBankTransferContent.setVisibility(View.VISIBLE);

        spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 1) {
                    textViewBSBTitle.setText("ABA");
                } else if (position == 2) {
                    textViewBSBTitle.setText("Bank Trasit Number");
                } else if (position == 3) {
                    textViewBSBTitle.setText("BSB");
                } else {
                    textViewBSBTitle.setText("BSB");
                }


                if (selectItems != 0 && position != 0) {
                    linearLayoutBankTransferContent.setVisibility(View.VISIBLE);
                    LogUtil.i(App.tag, "on select items");
                }
                if (0 == position) {
                    linearLayoutBankTransferContent.setVisibility(View.GONE);
                }
                selectItems++;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCountry.setSelection(1);

        //===========================================================
        //===========================================================

        User user = App.getApp().getUser();

        if (null != user && !StringUtils.isEmpty(user.getRecemethod())) {

            try {
                JSONObject methodObject = new JSONObject(user.getRecemethod());

                if (methodObject.has("bank_transfer")) {

                    JSONObject banktransFerObject = methodObject.getJSONObject("bank_transfer");

                    String country = JSONHelper.getStringFroJso("country", banktransFerObject);
                    for (int i = 0, isize = labels.length; i < isize; i++) {
                        if (country.equals(labels[i])) {
                            spinnerCountry.setSelection(i);
                            break;
                        }
                    }

                    String bsb = JSONHelper.getStringFroJso("bsb", banktransFerObject);
                    editTextBsb.setText(bsb);

                    String accountname = JSONHelper.getStringFroJso("account_name", banktransFerObject);
                    edittextAccountName.setText(accountname);

                    String accountNumber = JSONHelper.getStringFroJso("account_number", banktransFerObject);
                    editTextAccountNumber.setText(accountNumber);

                    linearLayoutBankTransferContent.setVisibility(View.VISIBLE);

                    onClickMethodBankTransker();
                }
                if (methodObject.has("cheque")) {

                    editTextCheque.setText(JSONHelper.getStringFroJso("cheque", methodObject));
                    onCleckMethodCeque();
                }

                if (methodObject.has("paypal")) {

                    JSONObject paypalObject = methodObject.getJSONObject("paypal");

                    editTextPayPalName.setText(JSONHelper.getStringFroJso("name", paypalObject));
                    editTextPaypalId.setText(JSONHelper.getStringFroJso("paypalId", paypalObject));

                    onClickpaypal();
                }
                if (methodObject.has("tipppr_debit")) {
                    editTextTipperDebit.setText(JSONHelper.getStringFroJso("tipppr_debit", methodObject));
                    onCLickDebitMethod();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }

    @OnClick(R.id.viewBack)
    public void onClickback() {

        finish();
    }

}
