package com.tipppr.android.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.listener.OnDismissListener;
import com.callback.OnRequestEndCallBack;
import com.common.callback.NetCallBackMini;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.LogUtil;
import com.common.util.NumberHelper;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.dialog.DialogShowCardInfo;
import com.paypal.android.MEP.CheckoutButton;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.PayPalAdvancedPayment;
import com.paypal.android.MEP.PayPalInvoiceData;
import com.paypal.android.MEP.PayPalPreapproval;
import com.paypal.android.MEP.PayPalReceiverDetails;
import com.paypal.android.MEP.PayPalResultDelegate;
import com.push.PushTool;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.net.NetInterface;
import com.tipppr.tipppr.view.DialogBreakDown;
import com.tipppr.tipppr.view.DialogVerifyNotify;
import com.tipppr.tipppr.view.TipResultDialog;
import com.zbar.lib.CaptureActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class TippprOptionActivity extends BaseTippprActivity implements PayPalResultDelegate {


    @InjectView(R.id.textViewAmountTotal)
    TextView textViewAmountTotal;

    private float money = 0.f;

    private short SHORT_GO_SEARCH = 0x10;
    private short SHOUR_GO_QR = 0x11;
    private short SHORT_GO_PAYPAL_CONFIRM = 0x12;
    private short SHORT_GO_CREDIT = 0x13;

    private float percent = 0.75f;
    private static final String DECODED_CONTENT_KEY = "codedContent";
    private String paykey = "";


    @InjectView(R.id.textViewBreakDownPercent)
    TextView textViewBreakDownPercent;

    @InjectView(R.id.textViewBus)
    TextView textViewBus;

    @InjectView(R.id.textViewPerson)
    TextView textViewPerson;

    @InjectView(R.id.linearLayoutCheckout)
    LinearLayout linearLayoutCheckout;

    @InjectView(R.id.buttonGetPreApprolval)
    Button buttonGetPreApprolval;

    @InjectView(R.id.dialogShowcardInfo)
    DialogShowCardInfo dialogShowcardInfo;

    @InjectView(R.id.linearLayoutTippprContent)
    LinearLayout linearLayoutTippprContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipppr_option);
        ButterKnife.inject(this);

        try {
            money = Float.parseFloat((String) App.getApp().getTempObject("money"));
        } catch (Exception e) {
        }

        String moneyText = String.valueOf(money);
        if (moneyText.endsWith(".0")) {
            textViewAmountTotal.setText("$" + (int) money);
        } else {
            textViewAmountTotal.setText("$" + NumberHelper.keepDecimal2(String.valueOf(money)));
        }
        initPaypalButton();
    }


    private static final int REQUEST_PAYPAL_CHECKOUT = 2;

    private void initPaypalButton() {

        PayPal payPal = PayPal.getInstance();

        if (null != payPal) {
            CheckoutButton checkoutButton = payPal.getCheckoutButton(this, PayPal.BUTTON_194x37, CheckoutButton.TEXT_PAY);

            linearLayoutCheckout.addView(checkoutButton);
            checkoutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pay();

//                    PayPalPayment newPayment = new PayPalPayment();
//                    newPayment.setSubtotal(new BigDecimal(mImg_cost[_value]));

//                    newPayment.setSubtotal(new BigDecimal(13));
//                    newPayment.setCurrencyType("USD");
//                    newPayment.setRecipient("svigra_1322573821_biz@gmail.com");
//                    newPayment.setMerchantName("Picasso and PayPal");
//
//
//                    Intent checkoutIntent = PayPal.getInstance().checkout(newPayment, TippprOptionActivity.this /*, new ResultDelegate()*/);
//                    // Use the android's startActivityForResult() and pass in our
//                    // Intent.
//                    // This will start the library.
//                    startActivityForResult(checkoutIntent, REQUEST_PAYPAL_CHECKOUT);

//============================
//                    PayPalPayment newPayment = new PayPalPayment();
//                    newPayment.setSubtotal(new BigDecimal(mImg_cost[_value]));
//                    newPayment.setCurrencyType("USD");
//                    newPayment.setRecipient("svigra_1322573821_biz@gmail.com");
//                    newPayment.setMerchantName("Picasso and PayPal");
//
//                    Intent checkoutIntent = PayPal.getInstance().checkout(newPayment, this /*, new ResultDelegate()*/);
//                    // Use the android's startActivityForResult() and pass in our
//                    // Intent.
//                    // This will start the library.
//                    if (mPurchase[_value]) {
//                        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
//                        alertDialog.setTitle("Message");
//                        alertDialog.setMessage("You have already purchased this item!");
//                        alertDialog.show();
//                    }
//                    else {
//                        this.startActivityForResult(checkoutIntent, REQUEST_PAYPAL_CHECKOUT);
//                    }

                }
            });

        } else {
            LogUtil.i(App.tag, "paypal object is null");
        }


    }

    @OnClick(R.id.buttonClearForm)
    public void onClickForm() {

        textViewBus.setText(null);
        textViewBus.setTag(null);

        textViewPerson.setText(null);
        textViewPerson.setTag(null);

        textViewBreakDownPercent.setText(null);

        textViewAmountTotal.setText(null);

        money = 0;

        refresh();
    }


    private void refresh() {
        linearLayoutTippprContent.postInvalidate();
    }

    private int requestPermisssionFailCount = 0;

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == SHORT_REQUEST_PHONE_STATE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                getPayPalKey(); //request
                pay();
            } else {
                requestPermisssionFailCount++;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (requestPermisssionFailCount > 1) {
                        showNotifyTextIn5Seconds("Your device deny access info,Please try again!");
                    }
                } else {
                    showNotifyTextIn5Seconds("Your device deny access info,Please try again!");
                }

            }

        }

    }

    @OnClick(R.id.buttonGetPreApprolval)
    public void onClickPreApproval() {

        LogUtil.i(App.tag, "request approval key");


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {

                //get ConfiredApprovalKey now

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE,}, SHORT_REQUEST_PHONE_STATE);

                //request permisson
                return;
            }
        }


//        getPayPalKey();

    }


    @Deprecated
    private void getPayPalKey() {
        getPreApproval(new NetCallBackMini() {
            @Override
            public void onFinish(NetResult netResult) {
                if (null != netResult) {

                    PayPal pp = PayPal.getInstance();

                    PayPalPreapproval preapproval = new PayPalPreapproval();


                    String payPalPreapprovalKey = (String) netResult.getData()[0];

                    buttonGetPreApprolval.setText(payPalPreapprovalKey);

                    preapproval.setCurrencyType("AUD");
                    preapproval.setMerchantName("Tipppr");

                    pp.setPreapprovalKey(payPalPreapprovalKey);

                    Intent intent = pp.preapprove(preapproval, TippprOptionActivity.this);

                    startActivityForResult(intent, SHORT_GO_PAYPAL_CONFIRM);


                } else {
                    showToastWithTime("Error net", 4);
                }
            }
        });


    }


    private short short_go_review = 100;

    String cardNumber = "";

    @OnClick(R.id.buttonTip)
    public void onClickTipButton() {

        User currentUser = App.getApp().getUser();
        if (!currentUser.isEmailVerify()) {

            DialogVerifyNotify dialogVerifyNotify = new DialogVerifyNotify(TippprOptionActivity.this);
            dialogVerifyNotify.setOnClickOkListener(new DialogVerifyNotify.OnClickOkListener() {
                @Override
                public void onVerifyClick() {
                    Tool.startActivityForResult(TippprOptionActivity.this, VerifyActivity.class, SHORT_REQUESET_VERIFY);
                }
            });
            dialogVerifyNotify.show();


            return;
        }

        if (null == currentUser.getCard() || StringUtils.isEmpty(currentUser.getCard().getCardNumber())) {

            DialogUtils.showConfirmDialog(TippprOptionActivity.this, "Oops! No credit card information", "You haven't yet entered your credit card information. Click OK below to update your credit card information on your profile page.", "OK", "Cancel", new DialogCallBackListener() {
                @Override
                public void onDone(boolean yesOrNo) {
                    if (yesOrNo) {
                        Tool.startActivityForResult(TippprOptionActivity.this, CreditCardActivity.class, SHORT_GO_CREDIT);
                    }
                }
            });

            return;
        }

        if (checkValue()) {

            try {
                cardNumber = App.getApp().getUser().getCard().getCardNumber();
            } catch (Exception e) {
            }


            if (StringUtils.isEmpty(cardNumber)) {

                DialogUtils.showConfirmDialog(TippprOptionActivity.this, "", "Please bind your credit card fist!", "Confirm", "Cancel", new DialogCallBackListener() {
                    @Override
                    public void onDone(boolean yesOrNo) {

                        Tool.startActivityForResult(TippprOptionActivity.this, CreditCardActivity.class, SHORT_REQUEST_FILL_CARD);

                    }
                });


                /*
                 bel

                final DialogTipMethodSelect tipMethodSelectDialog = new DialogTipMethodSelect(this, false);

                tipMethodSelectDialog.setOnMethodListener(new DialogTipMethodSelect.OnMethodListener() {


                    @Override
                    public void onCreditCard() {
                        tipMethodSelectDialog.dismiss();
                    }


                    @Override
                    public void onCreditCardInfoConfirm(String number, final String name, final String cvc) {
                        tipMethodSelectDialog.dismiss();


                        dialogShowcardInfo.setCardNumber(number);
                        dialogShowcardInfo.setOnCardInfoUIListener(new DialogShowCardInfo.OnCardInfoUIListener() {
                            @Override
                            public void onConfirm(String cardNumber) {

                                LogUtil.i(App.tag, "You want to use :" + cardNumber);
                                LogUtil.i(App.tag, "You want to name :" + name);
                                LogUtil.i(App.tag, "You want to cvc :" + cvc);

                                try {
                                    payBill(cardNumber, name, cvc);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    LogUtil.e(App.tag, "build josn error.");
                                }

                            }
                        });
                        dialogShowcardInfo.show();

                        LogUtil.d(App.tag, "fill card info done");
                    }

                    @Override
                    public void onPayPalClick() {
                        tipMethodSelectDialog.dismiss();
                        pay();
                    }

                    @Override
                    public void onBindClick() {

                        if (!StringUtils.isEmpty(cardNumber)) {
                            showConfirmCreditcardNumber(cardNumber);
                        } else {

                            DialogUtils.showConfirmDialog(TippprOptionActivity.this, "", "Please bind your credit card fist!", "Confirm", "Cancel", new DialogCallBackListener() {
                                @Override
                                public void onDone(boolean yesOrNo) {

                                    Tool.startActivityForResult(TippprOptionActivity.this, CreditCardActivity.class, SHORT_REQUEST_FILL_CARD);

                                }
                            });

                        }
                    }
                });

                tipMethodSelectDialog.show();
                */

            } else {
                showConfirmCreditcardNumber(cardNumber);
            }


        }

    }


    private void showConfirmCreditcardNumber(String cardNumber) {

        dialogShowcardInfo.setCardNumber("**** **** **** " + cardNumber);
        dialogShowcardInfo.setOnCardInfoUIListener(new DialogShowCardInfo.OnCardInfoUIListener() {
            @Override
            public void onConfirm(String cardNumber) {
                try {
                    payBill("", "", "");
                } catch (JSONException e) {
                    e.printStackTrace();
                    LogUtil.e(App.tag, "build josn error.");
                }
            }
        });
        dialogShowcardInfo.show();

    }


    public void payBill(String cardNumber, String accounName, String cvc) throws JSONException {


        final float percentTippr = 0.04f;

        if (checkValue()) {

            if (null != textViewPerson.getTag() && null != textViewBus.getTag()) {
                if ("100%".equals(getInput(textViewBreakDownPercent)) || StringUtils.isEmpty(getInput(textViewBreakDownPercent))) {
                    showNotifyTextIn5Seconds("Please select breakdown!");
                    return;
                }
            }

            final User currentUser = App.getApp().getUser();

            final String totalmoney = getInput(textViewAmountTotal).replace("$", "");
            final float totalMoney = Float.parseFloat(totalmoney);
            final String currentcy = currentUser.getCurrency();

            final JSONObject jsonObjectPay = new JSONObject();

            jsonObjectPay.put("amount", totalMoney);

            jsonObjectPay.put("currency", currentcy);
            jsonObjectPay.put("cardNumber", cardNumber.replace("-", ""));
            jsonObjectPay.put("cardExp", accounName);
            jsonObjectPay.put("cvv", cvc);


            //======================================================================================

            //when all type user exist
            if (null != textViewPerson.getTag() && null != textViewBus.getTag() && totalMoney > 0) {

                User userPeronal = (User) textViewPerson.getTag();
                User userBus = (User) textViewBus.getTag();

                String[] percent = ((String) textViewBreakDownPercent.getTag()).split(",");
                LogUtil.i(App.tag, "total:" + totalmoney + " personal:" + percent[0] + " buiness:" + percent[1]);

                final float percentPersonal = Float.parseFloat(percent[0]);

                final float money4Tipper = totalMoney * percentTippr;
                final float money4Peronal = (totalMoney - money4Tipper) * percentPersonal;
                final float money4Buiness = totalMoney - money4Tipper - money4Peronal;


                if (money4Buiness > 0 && money4Peronal > 0) {

                    LogUtil.i(App.tag, "m4p:" + money4Peronal);
                    LogUtil.i(App.tag, "m4b:" + money4Buiness);
                    LogUtil.i(App.tag, "m4t:" + money4Tipper);

                    JSONArray array = new JSONArray();

                    JSONObject personReceiver = new JSONObject();
                    personReceiver.put("userId", userPeronal.getUserId());
                    personReceiver.put("percent", percent[0]);

                    JSONObject personBus = new JSONObject();
                    personBus.put("userId", userBus.getUserId());
                    personBus.put("percent", percent[1]);

                    array.put(personReceiver);
                    array.put(personBus);

                    jsonObjectPay.put("receivers", array);

                }

            } else {

                if (null != textViewPerson.getTag() && totalMoney > 0) { //for just personal

                    final float money4Tipper = totalMoney * percentTippr;
                    final float money4Peronal = (totalMoney - money4Tipper);

                    User userPeronal = (User) textViewPerson.getTag();

                    JSONObject personReceiver = new JSONObject();

                    personReceiver.put("userId", userPeronal.getUserId());
                    personReceiver.put("percent", "1");

                    JSONArray array = new JSONArray();

                    array.put(personReceiver);

                    jsonObjectPay.put("receivers", array);

                } else if (null != textViewBus.getTag() && totalMoney > 0) { //for just buiness

                    User userBus = (User) textViewBus.getTag();
                    final float money4Tipper = totalMoney * percentTippr;
                    final float money4Buiness = totalMoney - money4Tipper;


                    JSONObject personReceiver = new JSONObject();

                    personReceiver.put("userId", userBus.getUserId());
                    personReceiver.put("percent", "1");

                    JSONArray array = new JSONArray();

                    array.put(personReceiver);

                    jsonObjectPay.put("receivers", array);

                }
            }

            //==========================

            LogUtil.i(App.tag, "pay json:" + jsonObjectPay.toString());

            //==========================

            reSetTask();

            baseTask = new BaseTask(TippprOptionActivity.this, new NetCallBack() {

                @Override
                public void onPreCall(BaseTask baseTask) {

                    baseTask.showDialogForSelf(true);
                }

                @Override
                public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                    NetResult netResult = null;
                    try {
                        netResult = NetInterface.payBillPro(TippprOptionActivity.this, jsonObjectPay);
                    } catch (Exception e) {
                        LogUtil.e(App.tag, "pay bill error:" + e.getLocalizedMessage());
                    }

                    return netResult;
                }

                @Override
                public void onFinish(NetResult result, BaseTask baseTask) {
                    baseTask.hideDialogSelf();
                    if (null != result) {

                        if (!StringUtils.isEmpty(result.getMessage())) {
                            showNotifyTextIn5Seconds(result.getMessage());
                        } else {
//                            showNotifyTextIn5Seconds("Pay successful!");

                            sendPayPush();

                            playMp3();

                            TipResultDialog tipResultDialog = new TipResultDialog(TippprOptionActivity.this, true);
                            tipResultDialog.setOnDismissListener(new OnDismissListener() {
                                @Override
                                public void onDismiss(Object o) {

                                    boolean canGo = false;

                                    if (null != textViewBus.getTag()) {
                                        canGo = true;
                                        App.getApp().putTemPObject("user1", (User) textViewBus.getTag());
                                    }

                                    if (null != textViewPerson.getTag()) {
                                        canGo = true;
                                        App.getApp().putTemPObject("user2", (User) textViewPerson.getTag());
                                    }

                                    if (canGo) {
                                        Tool.startActivityForResult(TippprOptionActivity.this, ReviewActivity.class, short_go_review);
                                    }

                                    //success
                                    payKeyObject = null;
                                    onClickForm();


//                                setResult(RESULT_OK);
//                                finish();

                                }
                            });
                            tipResultDialog.show();


                        }

                    } else {

                        showNotifyTextIn5Seconds(R.string.error_net);
                    }

                }
            });

            baseTask.execute(new HashMap<String, String>());


        }


    }


    /**
     * pay with paypal
     */
    private void pay() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE,}, SHORT_REQUEST_PHONE_STATE);
                return;
            }
        }

        final float percentTippr = 0.04f;

        if (checkValue()) {

            if (null != textViewPerson.getTag() && null != textViewBus.getTag()) {
                if ("100%".equals(getInput(textViewBreakDownPercent)) || StringUtils.isEmpty(getInput(textViewBreakDownPercent))) {
                    showNotifyTextIn5Seconds("Please select breakdown!");
                    return;
                }
            }


            final User currentUser = App.getApp().getUser();

            PayPal paypal = PayPal.getInstance();

            paypal.setShippingEnabled(false);
            paypal.setDynamicAmountCalculationEnabled(true);
            paypal.setFeesPayer(PayPal.FEEPAYER_EACHRECEIVER);

            PayPalAdvancedPayment advancedPayment = new PayPalAdvancedPayment();


            advancedPayment.setCurrencyType(currentUser.getCurrency());

            final String totalmoney = getInput(textViewAmountTotal).replace("$", "");
            final float totalMoney = Float.parseFloat(totalmoney);
            //======================================================================================

            //=======================can not tip your self check====================================
            String currentPaypalEmail = currentUser.getPayPalEmail();

            if (null != textViewPerson.getTag()) {
                User personalUser = (User) textViewPerson.getTag();
                if (currentUser.getUserId().equals(personalUser.getUserId())) {
                    showNotifyTextIn5Seconds("You can't tip yourself!");
                    return;
                }
            }

            if (null != textViewBus.getTag()) {
                User buinessUser = (User) textViewBus.getTag();
                if (currentUser.getUserId().equals(buinessUser.getUserId())) {
                    showNotifyTextIn5Seconds("You can't tip yourself!");
                    return;
                }
            }


            //======================================================================================

            PayPalReceiverDetails paypalUserTipprpr = new PayPalReceiverDetails();

            PayPalInvoiceData invoiceDataForTippr = new PayPalInvoiceData();
            invoiceDataForTippr.setShipping(new BigDecimal(0));
            invoiceDataForTippr.setTax(new BigDecimal(0.0f));
            paypalUserTipprpr.setInvoiceData(invoiceDataForTippr);
            paypalUserTipprpr.setSubtotal(new BigDecimal(totalMoney));
            paypalUserTipprpr.setRecipient("sales@tipppr.com");
            paypalUserTipprpr.setMerchantName("Tipppr");
            paypalUserTipprpr.setDescription("Tip from " + App.getApp().getUser().getProfileEmail());
            paypalUserTipprpr.setIsPrimary(true);

            advancedPayment.getReceivers().add(paypalUserTipprpr);


            //======================================================================================

            //when all type user exist
            if (null != textViewPerson.getTag() && null != textViewBus.getTag() && totalMoney > 0) {

                User userPeronal = (User) textViewPerson.getTag();
                User userBus = (User) textViewBus.getTag();


                if (!(userPeronal.isUpdateProfile() && userBus.isUpdateProfile())) {
                    showNotifyTextIn5Seconds("The user has not bound PayPal account.");
                    return;
                }
                if (userPeronal.getPayPalEmail().equals(userBus.getPayPalEmail())) {
                    LogUtil.i(App.tag, "same receiver paypal:" + userBus.getPayPalEmail());
                    showNotifyTextIn5Seconds("Business and personal email cannot be the same!");
                    return;
                }

                String[] percent = ((String) textViewBreakDownPercent.getTag()).split(",");
                LogUtil.i(App.tag, "total:" + totalmoney + " personal:" + percent[0] + " buiness:" + percent[1]);

                final float percentPersonal = Float.parseFloat(percent[0]);

                final float money4Tipper = totalMoney * percentTippr;
                final float money4Peronal = (totalMoney - money4Tipper) * percentPersonal;
                final float money4Buiness = totalMoney - money4Tipper - money4Peronal;

                if (money4Buiness > 0 && money4Peronal > 0) {

                    LogUtil.i(App.tag, "m4p:" + money4Peronal);
                    LogUtil.i(App.tag, "m4b:" + money4Buiness);
                    LogUtil.i(App.tag, "m4t:" + money4Tipper);

                    //======create receive detail for personal===
                    PayPalReceiverDetails paypalUserPersonal = new PayPalReceiverDetails();
                    paypalUserPersonal.setSubtotal(new BigDecimal(money4Peronal));

                    PayPalInvoiceData invoiceData = new PayPalInvoiceData();
                    invoiceData.setShipping(new BigDecimal(0));
                    invoiceData.setTax(new BigDecimal(0));
                    paypalUserPersonal.setInvoiceData(invoiceData);

                    paypalUserPersonal.setDescription("Tip from " + ((currentUser.isBuiness()) ? currentUser.getBussness() : currentUser.getName()));

                    String personalEmail = userPeronal.getPayPalEmail();
                    LogUtil.i(App.tag, "personal email:" + personalEmail);
                    paypalUserPersonal.setRecipient(personalEmail);
                    paypalUserPersonal.setMerchantName("Tipppr");
                    paypalUserPersonal.setIsPrimary(false);

                    advancedPayment.getReceivers().add(paypalUserPersonal);

                    //====create receive detail for buiness=====

                    PayPalReceiverDetails paypalUserBuiness = new PayPalReceiverDetails();
                    paypalUserBuiness.setSubtotal(new BigDecimal(money4Buiness));
                    PayPalInvoiceData invoiceDataForBuiness = new PayPalInvoiceData();
                    invoiceDataForBuiness.setShipping(new BigDecimal(0));
                    invoiceDataForBuiness.setTax(new BigDecimal(0));
                    paypalUserBuiness.setInvoiceData(invoiceDataForBuiness);
                    paypalUserBuiness.setDescription("Tip from " + ((currentUser.isBuiness()) ? currentUser.getBussness() : currentUser.getName()));
                    paypalUserBuiness.setMerchantName("Tipppr");
                    paypalUserBuiness.setIsPrimary(false);

                    String buinessEmail = userBus.getPayPalEmail();
                    LogUtil.i(App.tag, "buinessemail:" + buinessEmail);

                    paypalUserBuiness.setRecipient(buinessEmail);
                    advancedPayment.getReceivers().add(paypalUserBuiness);

                }

            } else {
                //onley one receiver=======this is for test do not change anything for test============================

                if (null != textViewPerson.getTag() && totalMoney > 0) { //for just personal

                    final float money4Tipper = totalMoney * percentTippr;
                    final float money4Peronal = (totalMoney - money4Tipper);
//                    final float money4Peronal = (totalMoney - money4Tipper) * percent;
//                    final float money4Buiness = totalMoney - money4Tipper - money4Peronal;

                    User userPeronal = (User) textViewPerson.getTag();
                    if (!userPeronal.isUpdateProfile()) {
                        showNotifyTextIn5Seconds("The user which you select PayPal email not bind!");
                        return;
                    }


                    //============================person============================================

                    PayPalReceiverDetails paypalReceiverDetailsSingle4Personal = new PayPalReceiverDetails();
                    PayPalInvoiceData invoiceDataForPersonal = new PayPalInvoiceData();
                    invoiceDataForPersonal.setShipping(new BigDecimal(0));
                    invoiceDataForPersonal.setTax(new BigDecimal(0));

                    paypalReceiverDetailsSingle4Personal.setInvoiceData(invoiceDataForPersonal);
                    paypalReceiverDetailsSingle4Personal.setSubtotal(new BigDecimal(money4Peronal));
                    paypalReceiverDetailsSingle4Personal.setDescription("Tip from " + ((currentUser.isBuiness()) ? currentUser.getBussness() : currentUser.getName()));

                    String receiverEmail = userPeronal.getPayPalEmail();

                    LogUtil.i(App.tag, "recever personal email:" + receiverEmail);
                    paypalReceiverDetailsSingle4Personal.setRecipient(receiverEmail);
                    paypalReceiverDetailsSingle4Personal.setMerchantName("Tipppr");
                    paypalReceiverDetailsSingle4Personal.setIsPrimary(false);

                    advancedPayment.getReceivers().add(paypalReceiverDetailsSingle4Personal);


                    //============================buiness===========================================

//                    PayPalInvoiceData payPalInvoiceData4bus = new PayPalInvoiceData();
//
//                    payPalInvoiceData4bus.setTax(new BigDecimal(0));
//                    payPalInvoiceData4bus.setShipping(new BigDecimal(0));
//
//                    PayPalReceiverDetails testRBuiness = new PayPalReceiverDetails();
//                    testRBuiness.setInvoiceData(payPalInvoiceData4bus);
//                    testRBuiness.setSubtotal(new BigDecimal(money4Buiness));
//                    testRBuiness.setRecipient("sandp1@domain.com");
//                    testRBuiness.setMerchantName("Tipppr");
//                    testRBuiness.setDescription("Tip from " + App.getApp().getUser().getProfileName());
//                    testRBuiness.setIsPrimary(false);
//
//                    advancedPayment.getReceivers().add(testRBuiness);


                } else if (null != textViewBus.getTag() && totalMoney > 0) { //for just buiness

                    User userBus = (User) textViewBus.getTag();
                    final float money4Tipper = totalMoney * percentTippr;
                    final float money4Buiness = totalMoney - money4Tipper;

                    if (!userBus.isUpdateProfile()) {
                        showNotifyTextIn5Seconds("The user which you select PayPal email not bind!");
                        return;
                    }


                    PayPalReceiverDetails paypalUserBuiness = new PayPalReceiverDetails();

                    paypalUserBuiness.setSubtotal(new BigDecimal(money4Buiness));

                    PayPalInvoiceData invoiceDataForBuiness = new PayPalInvoiceData();
                    invoiceDataForBuiness.setShipping(new BigDecimal(0));
                    invoiceDataForBuiness.setTax(new BigDecimal(0));
                    paypalUserBuiness.setInvoiceData(invoiceDataForBuiness);
                    paypalUserBuiness.setDescription("Tip from " + ((currentUser.isBuiness()) ? currentUser.getBussness() : currentUser.getName()));
                    paypalUserBuiness.setMerchantName("Tipppr");

                    String receiverEmail = userBus.getPayPalEmail();
                    LogUtil.i(App.tag, "recever personal bus:" + receiverEmail);

                    paypalUserBuiness.setRecipient(receiverEmail);
                    advancedPayment.getReceivers().add(paypalUserBuiness);
                }
            }

            Intent payIntent = paypal.checkout(advancedPayment, this);
            startActivityForResult(payIntent, SHORT_GO_PAYPAL_CONFIRM);
        }

    }


    private boolean checkValue() {

        if (StringUtils.isEmpty(getInput(textViewPerson)) && StringUtils.isEmpty(getInput(textViewBus))) {
//            showToast("Please select at leat one receiver!", 4);

            showNotifyTextIn5Seconds("Please select at least one receiver.");
            return false;
        }

//        if (StringUtils.isEmpty(getInput(textViewBus))) {
//
//            showToast("Please select buiness!", 3);
//
//            return false;
//        }


        if (null != textViewPerson.getTag() && null != textViewBus.getTag() && "100%".equals(getInput(textViewBreakDownPercent))) {
            showNotifyTextIn5Seconds("Please select breakdown!");
            return false;
        }


        if (StringUtils.isEmpty(getInput(textViewBreakDownPercent))) {
            showNotifyTextIn5Seconds("Please select breakdown!");
            return false;
        }


        if (StringUtils.isEmpty(getInput(textViewAmountTotal))) {
            showNotifyTextIn5Seconds("Please select total amount!");
            return false;
        }

        return true;
    }


    MediaPlayer mediaPlayer;

    public void playMp3() {

        mediaPlayer = MediaPlayer.create(TippprOptionActivity.this, R.raw.applause);

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

                if (null != mediaPlayer) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            }
        });
        mediaPlayer.start();


    }


    @Override
    public void onBackPressed() {
//
//        if (null != tipResultDialog && tipResultDialog.isShowing()) {
//            tipResultDialog.dismiss();
//            return;
//        }
        if (StringUtils.isEmpty(getInput(textViewAmountTotal))) {
            setResult(RESULT_OK);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @OnClick(R.id.viewBackQr)
    public void onClickQRScan() {
        Tool.startActivityForResult(this, CaptureActivity.class, SHOUR_GO_QR);
    }

    @OnClick(R.id.viewToBusiness)
    public void onClickToBuiness() {
        gotoSearchWityType(User.ROLE_BUSINESS);
    }


    @OnClick(R.id.linearLayout2PersonView)
    public void onClickPersonView() {
        gotoSearchWityType(User.ROLE_PERSONAL);
    }


    private void gotoSearchWityType(String type) {

        App.getApp().putTemPObject("fromTip", true);
        App.getApp().putTemPObject("type", type);
        Tool.startActivityForResult(this, SearchUserActivity.class, SHORT_GO_SEARCH);
    }


    @OnClick(R.id.viewAmountBreak)
    public void onAmontClick() {

        if (null == textViewBus.getTag() ^ null == textViewPerson.getTag()) {
            showToastWithTime("Do not need to select breakdown for 1 receiver.", 3);
            return;
        }

        if (null == textViewBus.getTag() && null == textViewPerson.getTag()) {
            showToastWithTime("Please select person or business first!", 3);
            return;
        }

        if (1 == getSelectedCount()) {
            showToastWithTime("Breakdown is 100%  for one recipient!", 3);
            return;
        }

        DialogBreakDown dialogBreakDown = new DialogBreakDown(this, money, percent);

        dialogBreakDown.setOnDoneClickListener(new DialogBreakDown.OnDoneClickListener() {
            @Override
            public void onDoneClick(float percent) {

                TippprOptionActivity.this.percent = percent;

                float person = percent;

                float business = 1 - person;
                textViewBreakDownPercent.setTag(String.valueOf(person + "," + business));

                textViewBreakDownPercent.setText("P" + (int) ((100 * percent)) + "% - B" + " " + (100 - (int) ((100 * percent))) + "%");
            }
        });

        dialogBreakDown.show();
    }

    @OnClick(R.id.viewBack)
    public void onViewBackClick() {
        if (StringUtils.isEmpty(getInput(textViewAmountTotal))) {
            setResult(RESULT_OK);
            finish();
        } else {
            finish();
        }
    }


    public static final short RESULT_AMOUNT = 565;

    @OnClick(R.id.linearLayoutAmountView)
    public void onClickAmount() {
        App.getApp().putTemPObject("amount", money);
        Tool.startActivityForResult(this, InputAmountActivity.class, RESULT_AMOUNT);
    }


    private BaseTask taskSavePayKey;

    protected void savePayKey(final String payKey) {

        if (null != taskSavePayKey && taskSavePayKey.getStatus() == AsyncTask.Status.RUNNING) {

            taskSavePayKey.cancel(true);
        }

        taskSavePayKey = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {


                    if (null == payKeyObject) {

                        payKeyObject = new JSONObject();
                        payKeyObject.put("paykey", paykey);
                        payKeyObject.put("api_token", App.getApp().getToken());
                        payKeyObject.put("send_userid", App.getApp().getUser().getUserId());

                        JSONArray receivers = new JSONArray();

                        if (null != textViewPerson.getTag()) {

                            User person = (User) textViewPerson.getTag();
                            JSONObject r1 = new JSONObject();
                            r1.put("userid", person.getUserId());
                            r1.put("email", person.getPayPalEmail());
                            r1.put("name", person.isBuiness() ? person.getBussness() : person.getName());
                            receivers.put(r1);

                        }

                        if (null != textViewBus.getTag()) {

                            User person = (User) textViewBus.getTag();

                            JSONObject r12 = new JSONObject();

                            r12.put("userid", person.getUserId());
                            r12.put("email", person.getPayPalEmail());
                            r12.put("name", person.isBuiness() ? person.getBussness() : person.getName());

                            receivers.put(r12);

                        }
                        payKeyObject.put("receivers", receivers);
                    }

                    netResult = NetInterface.savePayKey(TippprOptionActivity.this, payKeyObject);
                    //=====send push=========
                    sendPayPush();

                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.e(App.tag, " save pay key error:" + e.getLocalizedMessage());
                }


                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (StringUtils.isEmpty(result.getMessage())) {

                        TipResultDialog tipResultDialog = new TipResultDialog(TippprOptionActivity.this, true);
                        tipResultDialog.setOnDismissListener(new OnDismissListener() {
                            @Override
                            public void onDismiss(Object o) {

                                boolean canGo = false;

                                if (null != textViewBus.getTag()) {
                                    canGo = true;
                                    App.getApp().putTemPObject("user1", (User) textViewBus.getTag());
                                }

                                if (null != textViewPerson.getTag()) {
                                    canGo = true;
                                    App.getApp().putTemPObject("user2", (User) textViewPerson.getTag());
                                }

                                if (canGo) {
                                    Tool.startActivityForResult(TippprOptionActivity.this, ReviewActivity.class, short_go_review);
                                }

                                //success
                                payKeyObject = null;


                                onClickForm();
//                                setResult(RESULT_OK);
//                                finish();

                            }
                        });
                        tipResultDialog.show();

                    } else {
                        //false
                        final AlertDialog.Builder builder = new AlertDialog.Builder(TippprOptionActivity.this);
                        builder.setCancelable(false);
                        builder.setTitle(result.getMessage() + " save pay history failed,please retry?");
                        builder.setPositiveButton("Retry",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        savePayKey(payKey);
                                    }
                                });
                        builder.show();
                    }
                } else {
//                    showNotifyTextIn5Seconds(R.string.error_net);

                    final AlertDialog.Builder builder = new AlertDialog.Builder(TippprOptionActivity.this);
                    builder.setCancelable(false);
                    builder.setTitle("Net error,save pay history failed,please retry?");
                    builder.setPositiveButton("Retry",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    savePayKey(payKey);
                                }
                            });
                    builder.show();
                }
            }
        });
        taskSavePayKey.execute(new HashMap<String, String>());


    }


    private void sendPayPush() {


        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    ArrayList<String> targetids = new ArrayList<>();

                    if (null != textViewPerson.getTag()) {
                        User person = (User) textViewPerson.getTag();
                        targetids.add(person.getUserId());
                    }
                    if (null != textViewBus.getTag()) {
                        User person = (User) textViewBus.getTag();
                        targetids.add(person.getUserId());
                    }


                    //=====send push
                    User user = App.getApp().getUser();
                    String content = user.isBuiness() ? user.getBussness() : user.getProfileName();
                    content += " sent you a tip.";

                    JSONObject jsonObjetType = new JSONObject();

                    jsonObjetType.put("type", 0);

                    boolean isSuccess = PushTool.sendPushWithPushwoosh(targetids, content, jsonObjetType);

                    LogUtil.i(App.tag, "send push result:" + isSuccess);

                } catch (JSONException e) {
                    e.printStackTrace();
                    LogUtil.e(App.tag, "send push fail:" + e.getLocalizedMessage());
                }

            }
        }).start();


    }


    JSONObject payKeyObject = null;


    private int payFailCount = 0;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SHORT_REQUEST_FILL_CARD) {

            showProgressDialog();
            getProfile(this, new OnRequestEndCallBack() {
                @Override
                public void onRequestEnd(NetResult netResult) {

                    hideProgressDialog();

                    if (null != netResult) {
                        User user = (User) netResult.getData()[0];
                        App.getApp().setUser(user);
                    } else {
                        Tool.showMessageDialog("Net error!", TippprOptionActivity.this);
                    }
                }
            });


            return;
        }

        if (requestCode == SHORT_GO_CREDIT && resultCode==RESULT_OK) {
            getProfile(this, new OnRequestEndCallBack() {
                @Override
                public void onRequestEnd(NetResult netResult) {
                    if (null != netResult) {
                        User user = (User) netResult.getData()[0];
                        App.getApp().setUser(user);
                    }
                }
            });
            return;
        }


        if (requestCode == SHORT_REQUESET_VERIFY) {
            if (resultCode == RESULT_OK) {
                User nowUser = App.getApp().getUser();
                nowUser.setIsEmailVerify("1");
                App.getApp().setUser(nowUser);

            }
            return;
        }
        if (requestCode == SHORT_GO_PAYPAL_CONFIRM) {

            switch (resultCode) {
                case Activity.RESULT_OK:
                    //The payment succeeded
//                    showNotifyTextIn5Seconds("Pay successful!");
                    paykey = data.getStringExtra(PayPalActivity.EXTRA_PAY_KEY); //Tell the user their payment succeeded
                    LogUtil.i(App.tag, "confirm payaprrovalkey:" + paykey);
                    payKeyObject = null;
                    savePayKey(paykey);

                    break;

                case Activity.RESULT_CANCELED:

                    break;
                case PayPalActivity.RESULT_FAILURE:
                    String errorID = data.getStringExtra(PayPalActivity.EXTRA_ERROR_ID);
                    String errorMessage = data.getStringExtra(PayPalActivity.EXTRA_ERROR_MESSAGE); //Tell the user their payment was failed.
                    LogUtil.i(App.tag, "erroMessage:" + errorMessage + " erroid:" + errorID);
//                    showToastWithTime(errorMessage, 3);
                    payFailCount++;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (payFailCount > 1) {
                            showPayFailNotify();
                        }
                    } else {
                        showPayFailNotify();
                    }


                    break;
            }

            return;
        }


        if (requestCode == short_go_review && resultCode == RESULT_OK) {

            onClickForm();

            setResult(RESULT_OK);
            finish();

            return;
        }


        if (requestCode == RESULT_AMOUNT && resultCode == RESULT_OK) {

            this.money = (data.getFloatExtra("amount", 0.0f));

            String moneyText = String.valueOf(money);
            if (moneyText.endsWith(".0")) {
                textViewAmountTotal.setText("$" + (int) money);
            } else {
                textViewAmountTotal.setText("$" + NumberHelper.keepDecimal2(String.valueOf(money)));
            }

            return;
        }

        if (requestCode == SHOUR_GO_QR && resultCode == RESULT_OK) {

//            buildResult();

            if (data != null) {
                String content = data.getStringExtra(DECODED_CONTENT_KEY);
//                Bitmap bitmap = data.getParcelableExtra(DECODED_BITMAP_KEY);
                User user = new User();
                user.setUserId(content);
                App.getApp().putTemPObject("user", user);
                App.getApp().putTemPObject("fromTip", true);
                Tool.startActivityForResult(this, ProfileAnyUserActivity.class, SHORT_GO_SEARCH);
            }
        }


        if (requestCode == SHORT_GO_SEARCH && resultCode == RESULT_OK) {

            int selectCount = buildResult();

            if (selectCount == 1) {
                textViewBreakDownPercent.setText("100%");

            } else if (selectCount == 2) {

//                textViewBreakDownPercent.setText("Please select Breakdown");
            }

        }

    }


    private void showPayFailNotify() {
        TipResultDialog tipResultDialog = new TipResultDialog(TippprOptionActivity.this, false);
        tipResultDialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(Object o) {

            }
        });

        tipResultDialog.show();

    }

    private int buildResult() {

        int selectPerson = 0;

        if (App.getApp().hasTempKey("selectUser1")) {

            User u1 = (User) App.getApp().getTempObject("selectUser1");
            if (u1.isBuiness()) {
                textViewBus.setTag(u1);
                textViewBus.setText(u1.getBussness());
                LogUtil.i(App.tag, "1bus:" + u1.getBussness());
            } else {
                textViewPerson.setTag(u1);
                textViewPerson.setText(u1.getName());
                LogUtil.i(App.tag, "1person:" + u1.getName());
            }
            selectPerson++;
        }

        if (App.getApp().hasTempKey("selectUser2")) {
            User u2 = (User) App.getApp().getTempObject("selectUser2");
            if (u2.isBuiness()) {
                textViewBus.setTag(u2);
                textViewBus.setText(u2.getBussness());
                LogUtil.i(App.tag, "2bus:" + u2.getBussness());
            } else {
                textViewPerson.setTag(u2);
                textViewPerson.setText(u2.getName());
                LogUtil.i(App.tag, "2person:" + u2.getName());
            }
            selectPerson++;
        }

        return selectPerson;
    }

    private int getSelectedCount() {
        int count = 0;
        if (null != textViewPerson.getTag()) {
            count++;
        }
        if (null != textViewBus.getTag()) {
            count++;
        }
        return count;
    }

    @Override
    public void onPaymentSucceeded(String s, String s1) {
        LogUtil.i(App.tag, "s:" + s + "  s1:" + s1);
    }

    @Override
    public void onPaymentFailed(String s, String s1, String s2, String s3, String s4) {
        LogUtil.i(App.tag, "s:" + s + "  s1:" + s1 + " s2:" + s2 + " s3:" + s3 + " s4:" + s4);
    }

    @Override
    public void onPaymentCanceled(String s) {
        LogUtil.i(App.tag, "s:" + s);
    }


}
