package com.tipppr.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import com.callback.OnRequestEndCallBack;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.net.NetInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A login screen that offers login via email/password.
 */
public class LoginSelectActivity extends BaseTippprActivity {


    private CallbackManager callbackManager;
    private ProfileTracker profileTracker;
    private LoginManager loginManager;
    private AccessTokenTracker accessTokenTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_select);
        ButterKnife.inject(this);

        initFbLogin();

        autoGotoLogin();

    }


    private void autoGotoLogin() {

        try {

            if (null != App.getApp().getUser()) {
                User user = App.getApp().getUser();
                if (StringUtils.isEmpty(user.getFacebookId())) {
                    String email = (String) SharePersistent.getObjectValue(this, App.KEY_USER);
                    String pwd = (String) SharePersistent.getObjectValue(this, App.KE_PWD);

                    if (!StringUtils.isEmpty(email) && !StringUtils.isEmpty(pwd)) {
                        onLoginClick();
                    }
                } else {
                    onClickloginFb();

                }
            }
        } catch (Exception e) {
            LogUtil.e(App.tag, "auto login error");
        }

    }


    @OnClick(R.id.viewLoginFb)
    public void onClickloginFb() {

//        Collection<String> permissions = Arrays.asList("public_profile", "email", "user_friends", "user_hometown", "user_location");
        Collection<String> permissions = Arrays.asList("public_profile", "email", "user_friends");

        loginManager.setLoginBehavior(LoginBehavior.WEB_ONLY);
        loginManager.logInWithReadPermissions(this, permissions);

    }

    private void initFbLogin() {
        callbackManager = CallbackManager.Factory.create();
        loginManager = LoginManager.getInstance();
        loginManager.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        //fb login 100% call this method

                        LogUtil.i(App.tag, "login onSuccess");
                        final String userId = loginResult.getAccessToken().getUserId();
                        Log.i(App.tag, "login onSuccess uid:" + userId);
                        LogUtil.e(App.tag, "1.loginfb1");


                        if (AccessToken.getCurrentAccessToken() == null) {
                            System.out.println("not logged in yet");

                        } else {
                            System.out.println("Logged in");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (AccessToken.getCurrentAccessToken() == null || AccessToken.getCurrentAccessToken().isExpired()) {
                                        Log.i(App.tag, "not logged in yet");

                                    } else {
                                        Log.i(App.tag, "logged in");
                                        AccessToken.refreshCurrentAccessTokenAsync();
//                                        facebookUserLogin(null);

                                        onLoginFbSuccess(userId);
                                    }
                                }
                            });
                        }


//                        onLoginFbSuccess(userId);


//                       String name = currentProfile.getFirstName() + currentProfile.getMiddleName() + currentProfile.getLastName();
//                       Log.i("wzy", "fuall name:" + name);
//                       profileTracker.startTracking();//获取更加详细的用户数据
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Tool.ToastShow(LoginSelectActivity.this, "LoginFail with FB,Please retry!");
                        LogUtil.e(App.tag, "login fb fail:" + exception.getLocalizedMessage());
                    }
                });

    }


    private void getFacebookUserDetails() {

        showProgressDialog();

        Bundle bundle = new Bundle();
//        bundle.putString("fields", "name, email, locale, picture, hometown, location");
        bundle.putString("fields", "name, email, locale, picture");
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me",
                bundle,
                HttpMethod.GET,
                new GraphRequest.Callback() {

                    public void onCompleted(GraphResponse response) {

                        hideProgressDialog();

                        JSONObject object = response.getJSONObject();
                        if (object == null) {
                            return;
                        }

                        try {
                            User user = new User();

                            if (object.getString("name") != null) {
                                user.setProfileName(object.getString("name"));
                            }

                            user.setFacebookId(object.getString("id"));

                            if (object.getString("email") != null)
                                user.setProfileEmail(object.getString("email"));

                            if (object.getString("id") != null && user.getHeaderImage() == null) {
                                final String url = "https://graph.facebook.com/" + object.getString("id") + "/picture?type=large&return_ssl_resources=1";
                                SharePersistent.savePerference(LoginSelectActivity.this, KEY_FBHEADER, user.getFacebookId() + "," + url);
                                user.setHeaderImage(url);
                            }

//                            try {
//                                if (object.get("location") != null) {
//                                    String city = object.getJSONObject("location").getString("name").split(",")[0];
//                                    user.setPostcode(city);
//                                }
//                            } catch (Exception e) {
//
//                            }


                            facebookUserLogin(user);

                        } catch (Exception e) {

                            showToastWithTime("Login with Facebook fail", 3);

                        }
                    }
                }
        ).executeAsync();
    }


    public void onLoginFbSuccess(String fbId) {
        getFacebookUserDetails();
    }

    private void gotoProfilePageFill(boolean isregistWithfb) {
        App.getApp().putTemPObject("tag", isregistWithfb);
        Tool.startActivity(this, ProfileFillActivity.class);
        hideProgressDialog();
    }

    private void gotoProfilePage() {

        Intent intent = new Intent();
        intent.setClass(LoginSelectActivity.this, LoginActivity.class);

        if (null != getIntent() && getIntent().hasExtra("msg")) {
            intent.putExtra("msg", getIntent().getStringExtra("msg"));
        }

        startActivity(intent);
        hideProgressDialog();
    }

    private void facebookUserLogin(final User user) {

        final JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("token", AccessToken.getCurrentAccessToken().getToken());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        reSetTask();

        baseTask = new BaseTask(LoginSelectActivity.this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (!isFinishing()) {
                    if (null != result) {
                        if (StringUtils.isEmpty(result.getMessage())) {
                            beforGo(user);
                        } else {
                            Tool.showMessageDialog("Net error", LoginSelectActivity.this);
                        }
                    } else {
                        Tool.showMessageDialog("Net error", LoginSelectActivity.this);
                    }
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.facebookUserLogin(LoginSelectActivity.this, jsonObject);

                } catch (Exception e) {
                    LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
                }
                return netResult;
            }
        });
        baseTask.execute(new HashMap<String, String>());

    }

    private void beforGo(final User fbUser) {


        getProfile(this, new OnRequestEndCallBack() {
            @Override
            public void onRequestEnd(NetResult netResult) {
                if (null != netResult) {


                    User databaseUser = (User) netResult.getData()[0];


//                    if (!StringUtils.isEmpty(databaseUser.getFacebookId())) {
//
//                        String fbName = fbUser.getProfileName();
//
//                        if (StringUtils.isEmpty(databaseUser.getResultName()) && !StringUtils.isEmpty(fbName)) {
//                            String[] fullName = fbName.split(" ");
//                            if (null != fullName && fbName.length() == 2) {
//                                databaseUser.setProfileName(fullName[0] + App.getApp().getSplit() + fullName[1]);
//                            } else if (null != fullName && fullName.length != 2) {
//                                databaseUser.setProfileEmail(fbName);
//                            }
//                        }
//                    }


                    App.getApp().setUser(databaseUser);

                    if (databaseUser.isUpdateProfile()) {
                        gotoProfilePage();
                    } else {
                        if (fbUser != null) {
                            fbUser.setUserId(databaseUser.getUserId());
                            App.getApp().setUser(fbUser);
                            gotoProfilePageFill(true);
                        } else {
                            gotoProfilePageFill(false);
                        }
                    }

                } else {
                    Tool.showMessageDialog("Net error!", LoginSelectActivity.this);
                }
            }
        });


//        if(null!=user && !StringUtils.isEmpty(user.getFacebookId())){
//
//
//
//        }else{
//            getProfile(this, new OnRequestEndCallBack() {
//                @Override
//                public void onRequestEnd(NetResult netResult) {
//                    if (null != netResult) {
//                        User databaseUser = (User) netResult.getData()[0];
//                        App.getApp().setUser(databaseUser);
//
//                        if (databaseUser.isUpdateProfile()) {
//                            gotoProfilePage();
//
//                        } else {
//                            if (user != null) {
//                                user.setUserId(databaseUser.getUserId());
//                                App.getApp().setUser(user);
//                                gotoProfilePageFill(true);
//                            } else {
//                                gotoProfilePageFill(false);
//                            }
//                        }
//
//                    } else {
//                        Tool.showMessageDialog("Net error!", LoginSelectActivity.this);
//                    }
//                }
//            });
//        }

    }

    @OnClick(R.id.buttonLogin)
    public void onLoginClick() {

        Intent intent = new Intent();
        intent.setClass(LoginSelectActivity.this, LoginActivity.class);

        if (null != getIntent() && getIntent().hasExtra("msg")) {
            intent.putExtra("msg", getIntent().getStringExtra("msg"));
        }

        Tool.startActivity(this, LoginActivity.class);

    }

    @OnClick(R.id.buttonSingUp)
    public void onSingUpClick() {
        Tool.startActivity(this, SignUpActivity.class);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitBy2Click();
        }
        return false;
    }

    /**
     * 双击退出函数
     */
    private static Boolean isExit = false;

    private void exitBy2Click() {
        Timer tExit = null;
        if (isExit == false) {
            isExit = true; // 准备退出
            Toast.makeText(this, "Click again to exit", Toast.LENGTH_SHORT).show();
            tExit = new Timer();
            tExit.schedule(new TimerTask() {
                @Override
                public void run() {
                    isExit = false;
                }
            }, 2000);

        } else {

            LoginManager.getInstance().logOut();
            Intent intent = new Intent(LoginSelectActivity.this, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("isExit", true);

            startActivity(intent);

//            App.getApp().setUser(null);
            finish();
            System.gc();

//            finish();
//            System.exit(0);

        }
    }

}

