package com.tipppr.android.activity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.net.NetInterface;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class ChangePasswordActivity extends BaseTippprActivity {


    @InjectView(R.id.editTextPwd)
    EditText editTextPwd;

    @InjectView(R.id.editTextPwdConfirm)
    EditText editTextPwdConfirm;

    @InjectView(R.id.buttonConfirm)
    Button buttonConfirm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.inject(this);
    }


    @OnClick(R.id.buttonConfirm)
    public void onConfirmClick() {
        if (checkValue()) {
            changePassword(getInput(editTextPwd), true);
        }
    }


    private void changePassword(final String pwd, final boolean showDialog) {

        reSetTask();

        baseTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    baseTask.showDialogForSelf(true);
                }

            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {

                    JSONObject jso = new JSONObject();
                    jso.put("password", pwd);
                    netResult = NetInterface.changePassword(ChangePasswordActivity.this, jso);

                } catch (Exception e) {
                    LogUtil.e(App.tag, "change password error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (showDialog) {
                    baseTask.hideDialogSelf();
                }

                if (null != result) {
                    if (StringUtils.isEmpty(result.getMessage())) {

//                        showToast("Change password successfully.", 2);
//                        finish();

                        Tool.showMessageDialog("Change password successfully.", ChangePasswordActivity.this, new DialogCallBackListener() {

                            @Override
                            public void onDone(boolean yesOrNo) {
                                finish();
                            }
                        });

                    } else {
                        Tool.showMessageDialog(result.getMessage(), ChangePasswordActivity.this);
                    }
                } else {
                    Tool.showMessageDialog("Net error!", ChangePasswordActivity.this);
                }


            }
        });

        baseTask.execute(new HashMap<String, String>());


    }


    private boolean checkValue() {

        String password = getInput(editTextPwd);
        String passwordConfirm = getInput(editTextPwdConfirm);

        if (StringUtils.computStrlen(password) < 6) {
            Tool.showMessageDialog("Please enter at least 6 characters for password", this);
            return false;
        }


        if (!passwordConfirm.equals(password)) {

            Tool.showMessageDialog("Confirm password wrong!", ChangePasswordActivity.this);
            return false;
        }

        return true;
    }


    @OnClick(R.id.viewBack)
    public void onBackClick() {
        finish();
    }

}
