package com.tipppr.android.activity;

import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.TextView;

import com.common.util.Tool;
import com.google.android.gms.vision.text.Text;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.view.TextJustification;


import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class TippprDebitCardActivity extends BaseTippprActivity {

    @InjectView(R.id.textViewTop)
    TextView textViewTop;

    @InjectView(R.id.textViewBottom)
    TextView textViewBottom;

    private String linkText = "";
    private int width;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipper_debit_card);
        ButterKnife.inject(this);

        setLinkStype();
    }

    private void setLinkStype() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        width = dm.widthPixels;

        linkText = "For further information, go to  <font color=\"#ff000000\">" +
                "<B><a href=\"pp\"> www.tipppr.com</a></B></font>";

        textViewBottom.setText(getClickableHtml(linkText));
        textViewBottom.setAutoLinkMask(Linkify.WEB_URLS);
        textViewBottom.setMovementMethod(LinkMovementMethod.getInstance());

//        textViewTop.setTextSize(8*(float)width/320f);
        //TextJustification.justify(textViewTop, textViewTop.getWidth());
    }

    private CharSequence getClickableHtml(String html) {
        Spanned spannedHtml = Html.fromHtml(html);
        SpannableStringBuilder clickableHtmlBuilder = new SpannableStringBuilder(spannedHtml);
        URLSpan[] urls = clickableHtmlBuilder.getSpans(0, spannedHtml.length(), URLSpan.class);
        for (final URLSpan span : urls) {
            setLinkClickable(clickableHtmlBuilder, span);
        }
        return clickableHtmlBuilder;
    }

    private void setLinkClickable(final SpannableStringBuilder clickableHtmlBuilder,
                                  final URLSpan urlSpan) {
        int start = clickableHtmlBuilder.getSpanStart(urlSpan);
        int end = clickableHtmlBuilder.getSpanEnd(urlSpan);
        int flags = clickableHtmlBuilder.getSpanFlags(urlSpan);
        ClickableSpan clickableSpan = new ClickableSpan() {
            public void onClick(View view) {

                setLinkStype();
                String url = urlSpan.getURL();
                Tool.startUrl(TippprDebitCardActivity.this, "http://www.tipppr.com");
            }
        };
        clickableHtmlBuilder.setSpan(clickableSpan, start, end, flags);
    }

    @OnClick(R.id.viewBack)
    public void onClickback() {
        finish();
    }
}
