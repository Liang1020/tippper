package com.tipppr.android.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.callback.OnRequestEndCallBack;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.ImageUtils;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.facebook.login.LoginManager;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.karics.libary.zxing.android.MyQRActivity;
import com.pushwoosh.BasePushMessageReceiver;
import com.pushwoosh.BaseRegistrationReceiver;
import com.pushwoosh.PushManager;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.adapter.UserAdapter;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.net.NetInterface;
import com.tipppr.tipppr.view.DialogVerifyNotify;
import com.tipppr.tipppr.view.DialogVerifyReceMethod;
import com.tipppr.trans.MyBlurTransform;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class ProfileUserActivity extends BaseTippprActivity {


    @InjectView(R.id.imageViewBigHeader)
    ImageView imageViewBigHeader;

    @InjectView(R.id.imageViewBottomGlass)
    ImageView imageViewBottomGlass;

    @InjectView(R.id.imaveViewSmallHeader)
    ImageView imaveViewSmallHeader;


    @InjectView(R.id.textViewName)
    TextView textViewName;

    @InjectView(R.id.textViewEmail)
    TextView textViewEmail;

    @InjectView(R.id.textViewMobileNumber)
    TextView textViewMobileNumber;

    @InjectView(R.id.textViewTypeLabel)
    TextView textViewTypeLabel;


    @InjectView(R.id.viewPostAddress)
    View viewPostAddress;


    @InjectView(R.id.textViewPostalAddress)
    TextView textViewPostalAddress;

    @InjectView(R.id.textViewAddress)
    TextView textViewAddress;

    @InjectView(R.id.listView)
    ListView listView;

    @InjectView(R.id.imageViewDotCount)
    ImageView imageViewDotCount;

    @InjectView(R.id.textViewAddWorkLabel)
    TextView textViewAddWorkLabel;


    @InjectView(R.id.scrollView)
    ScrollView scrollView;

    @InjectView(R.id.viewAdd)
    View viewAdd;

    @InjectView(R.id.viewContactName)
    View viewContactName;


    @InjectView(R.id.textViewContactName)
    TextView textViewContactName;

    @InjectView(R.id.rateBar)
    RatingBar rateBar;

    @InjectView(R.id.textViewPaypal)
    TextView textViewPaypal;

    @InjectView(R.id.textViewVerifyStatus)
    TextView textViewVerifyStatus;


    @InjectView(R.id.textViewCity)
    TextView textViewCity;

    @InjectView(R.id.textViewState)
    TextView textViewState;

    @InjectView(R.id.textViewPostCode)
    TextView textViewPostCode;

    @InjectView(R.id.textViewCountry)
    TextView textViewCountry;

    @InjectView(R.id.textViewCurrency)
    TextView textViewCurrency;


    @InjectView(R.id.viewMobileNumberLine)
    View viewMobileNumberLine;

    @InjectView(R.id.viewContactNumber)
    View viewContactNumber;

    @InjectView(R.id.viewMobileNumber)
    View viewMobileNumber;

    @InjectView(R.id.textViewContactNumber)
    TextView textViewContactNumber;

    @InjectView(R.id.textViewReceivingMethod)
    TextView textViewReceivingMethod;


    Point p = null;

    private Bitmap bitmapMaskHeader;
    private int maskWidth, maskHeight;
    private short SHORT_GO_REQUEST_LIST = 77;
    private short SHORT_GO_ADDWORK = 78;
    private short SHORT_GO_PROFILE = 79;
    private short SHORT_GO_CREDIT = 79;
    private short SHORT_GO_RECEIVEMETHOD = 80;

    private ArrayList<User> arrayListRequestListOutGoing;
    private ArrayList<User> arrayListRequestListIncoming;
    private boolean fromHome = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_user);
        ButterKnife.inject(this);
        p = Tool.getDisplayMetrics(this);
        initHeaderLayoutParam();
        initUserView();

        refreshInfo();


        if (App.getApp().hasTempKey("fromHomeScream") && true == (boolean) App.getApp().getTempObject("fromHomeScream")) {
            fromHome = true;
        }


        App.getApp().clearTempKey("tag");

//        registerReceivers();

        App.getApp().setWeakReferenceProfileUserActivity(new WeakReference<ProfileUserActivity>(this));
    }


    public void refreshInfo() {
        loadFriendList(false);
        loadPendingFriendList(false);
        LogUtil.e(App.tag, "refresh info");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        unregisterReceivers();
        App.getApp().setWeakReferenceProfileUserActivity(null);
    }


    @OnClick(R.id.linearlayoutPaymentMethod)
    public void onClickPaymentmethod() {

        String label = getInput(textViewReceivingMethod);
        if (StringUtils.isEmpty(label)) {
            Tool.showMessageDialog("Not Setup", "None", this);
        } else {

            if ("Not Setup".equals(label)) {
                DialogUtils.showConfirmDialog(this, "Oops! No receiving method", "You haven't yet choose a receiving method. Click OK below to add one.",
                        "OK", "Cancel", new DialogCallBackListener() {
                            @Override
                            public void onDone(boolean yesOrNo) {
                                if (yesOrNo) {
                                    Tool.startActivityForResult(ProfileUserActivity.this, ReceivingMethodActivity.class, SHORT_GO_RECEIVEMETHOD);
                                }
                            }
                        }
                );
            } else {

                try {
                    JSONObject methodObject = new JSONObject(App.getApp().getUser().getRecemethod());
                    DialogVerifyReceMethod dialogVerifyReceMethod = new DialogVerifyReceMethod(this, "Receiving Method", methodObject.keys().next());

                    dialogVerifyReceMethod.setOnDialogEventListener(new DialogVerifyReceMethod.OnDialogEventListener() {

                        @Override
                        public void onEditClick() {
                            Tool.startActivityForResult(ProfileUserActivity.this, ReceivingMethodActivity.class, SHORT_GO_RECEIVEMETHOD);
                        }
                    });

                    dialogVerifyReceMethod.show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }


        }

    }


    @OnClick(R.id.linearLayoutCreditCard)
    public void onClickCreditCard() {

        User user = App.getApp().getUser();
        if (null != user) {
            if (null != user.getCard() && !StringUtils.isEmpty(user.getCard().getCardNumber())) {

                String cardNumber = user.getCard().getCardNumber();

                if (StringUtils.computStrlen(cardNumber) > 4) {
                    cardNumber = cardNumber.substring(cardNumber.length() - 4);
                }

                String pre = "xxxx xxxx xxxx ";

                DialogVerifyReceMethod dialogVerifyReceMethod = new DialogVerifyReceMethod(this, "Credit Card Information", pre + cardNumber);
                dialogVerifyReceMethod.setOnDialogEventListener(new DialogVerifyReceMethod.OnDialogEventListener() {
                    @Override
                    public void onDelClick() {
                        deleteCardInfo();
                    }
                });

                dialogVerifyReceMethod.show();

            } else {

                DialogUtils.showConfirmDialog(ProfileUserActivity.this, "Oops! No credit card information", "You haven't yet entered your credit card information. Click OK below to update your credit card information on your profile page.", "OK", "Cancel", new DialogCallBackListener() {
                    @Override
                    public void onDone(boolean yesOrNo) {
                        if (yesOrNo) {
                            Tool.startActivityForResult(ProfileUserActivity.this, CreditCardActivity.class, SHORT_GO_CREDIT);
                        }
                    }
                });

            }
        }

    }


    @OnClick(R.id.textViewVerifyStatus)
    public void onClickVerify() {

        User user = App.getApp().getUser();
        if (!user.isEmailVerify()) {

            DialogVerifyNotify dialogVerifyNotify = new DialogVerifyNotify(ProfileUserActivity.this);
            dialogVerifyNotify.setOnClickOkListener(new DialogVerifyNotify.OnClickOkListener() {
                @Override
                public void onVerifyClick() {
                    Tool.startActivityForResult(ProfileUserActivity.this, VerifyActivity.class, SHORT_REQUESET_VERIFY);
                }
            });
            dialogVerifyNotify.show();
        }
    }


    //Registration receiver
    BroadcastReceiver mBroadcastReceiver = new BaseRegistrationReceiver() {
        @Override
        public void onRegisterActionReceive(Context context, Intent intent) {


        }
    };

    //Push message receiver
    private BroadcastReceiver mReceiver = new BasePushMessageReceiver() {
        @Override
        protected void onMessageReceive(Intent intent) {
            //JSON_DATA_KEY contains JSON payload of push notification.
//            doOnMessageReceive(intent.getExtras().getString(JSON_DATA_KEY));

            LogUtil.i(App.tag, "receiv push msg in profile user activity");
            refreshInfo();

        }
    };

    //Registration of the receivers
    public void registerReceivers() {
        IntentFilter intentFilter = new IntentFilter(getPackageName() + ".action.PUSH_MESSAGE_RECEIVE");
        registerReceiver(mReceiver, intentFilter, getPackageName() + ".permission.C2D_MESSAGE", null);
        registerReceiver(mBroadcastReceiver, new IntentFilter(getPackageName() + "." + PushManager.REGISTER_BROAD_CAST_ACTION));
    }

    public void unregisterReceivers() {
        //Unregister receivers on pause
        try {
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
            // pass.
        }

        try {
            unregisterReceiver(mBroadcastReceiver);
        } catch (Exception e) {
            //pass through
        }
    }


    @OnClick(R.id.viewQR)
    public void onQrClick() {
        App.getApp().putTemPObject("uid", App.getApp().getUser().getUserId());
        Tool.startActivity(this, MyQRActivity.class);
    }


    @OnClick(R.id.viewAddress)
    public void addViewAddress() {

        gotoMapActivity();
    }


    @OnClick(R.id.viewPostAddress)
    public void onPostalAddressClick() {
        gotoMapActivity();

    }


    private void gotoMapActivity() {


//        String lng = App.getApp().getUser().getLng();
//        String lat = App.getApp().getUser().getLat();
//
//        if (!StringUtils.isEmpty(lat + lng)) {
//
//            LatLng latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
//
//            App.getApp().putTemPObject("address", App.getApp().getUser().getPostcode());
//
//            App.getApp().putTemPObject("latlng", latLng);
//
//            Tool.startActivity(this, MapsActivity.class);
//        }


    }


    @OnClick(R.id.viewAdd)
    public void onViewAddClick() {

        User currentUser = App.getApp().getUser();

        if (!currentUser.isEmailVerify()) {

            DialogVerifyNotify dialogVerifyNotify = new DialogVerifyNotify(ProfileUserActivity.this);
            dialogVerifyNotify.setOnClickOkListener(new DialogVerifyNotify.OnClickOkListener() {
                @Override
                public void onVerifyClick() {
                    Tool.startActivityForResult(ProfileUserActivity.this, VerifyActivity.class, SHORT_REQUESET_VERIFY);
                }
            });
            dialogVerifyNotify.show();
            return;
        }


        if (!ListUtiles.isAllEmpty(arrayListRequestListOutGoing, arrayListRequestListIncoming)) {
            App.getApp().putTemPObject("requestlist", arrayListRequestListOutGoing);
            App.getApp().putTemPObject("requestListIn", arrayListRequestListIncoming);
            Tool.startActivityForResult(this, RequestListActivity.class, SHORT_GO_REQUEST_LIST);

        } else {
            showToast("No request now!", 3);
        }

    }

    private BaseTask friendTask1 = null;

    private void loadFriendList(final boolean showDialog) {

        if (null != friendTask1 && friendTask1.getStatus() == AsyncTask.Status.RUNNING) {
            friendTask1.cancel(true);
        }

        friendTask1 = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

                if (showDialog) {
                    baseTask.showDialogForSelf(true);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    netResult = NetInterface.getFriendList(ProfileUserActivity.this, new JSONObject());
                } catch (Exception e) {
                    LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
                }

                return netResult;

            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (null != result && StringUtils.isEmpty(result.getMessage())) {
                    try {
                        buildFriendListAdapter((ArrayList<User>) result.getData()[0]);
                    } catch (Exception e) {
                        LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
                    }
                }

                if (showDialog) {
                    baseTask.hideDialogSelf();
                }
            }
        }, this);

        friendTask1.execute(new HashMap<String, String>());

    }

    private BaseTask friendTask2 = null;

    private void loadPendingFriendList(final boolean showDialog) {

        if (null != friendTask2 && friendTask2.getStatus() == AsyncTask.Status.RUNNING) {
            friendTask2.cancel(true);
        }

        friendTask2 = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    baseTask.showDialogForSelf(true);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {
                    netResult = NetInterface.getpendingFriendList(ProfileUserActivity.this, new JSONObject());
                } catch (Exception e) {
                    LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
                }

                return netResult;

            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result && StringUtils.isEmpty(result.getMessage())) {
                    try {
                        arrayListRequestListOutGoing = (ArrayList<User>) result.getData()[0];
                        arrayListRequestListIncoming = (ArrayList<User>) result.getData()[1];
                        LogUtil.i(App.tag, "found request:" + arrayListRequestListOutGoing.size());
                    } catch (Exception e) {
                        LogUtil.e(App.tag, "exception:in get count");
                    }

                    if ((ListUtiles.getListSize(arrayListRequestListIncoming) /*+ ListUtiles.getListSize(arrayListRequestListIncoming)*/) > 0) {
                        imageViewDotCount.setVisibility(View.VISIBLE);
                    } else {
                        imageViewDotCount.setVisibility(View.GONE);

                    }
                }
            }
        }, this);

        friendTask2.execute(new HashMap<String, String>());

    }


    private void buildFriendListAdapter(ArrayList<User> list) {
        UserAdapter userAdapter = new UserAdapter(this, list, true);
        userAdapter.setOnClickItemListener(new UserAdapter.OnClickItemListener() {
            @Override
            public void onDelClick(final User user) {

                String del = user.isBuiness() ? "workplace" : "associate";
                DialogUtils.showConfirmDialog(ProfileUserActivity.this, "Delete", "Do you confirm to delete this " + del + " ?", "Confirm", "Cancel", new DialogCallBackListener() {
                    @Override
                    public void onDone(boolean yesOrNo) {
                        if (yesOrNo) {
                            deleteFriend(user);
                        }
                    }
                });
            }

            @Override
            public void onClickItem(User user, int position) {
                App.getApp().putTemPObject("user", user);
                Tool.startActivity(ProfileUserActivity.this, ProfileAnyUserActivity.class);

            }
        });
        listView.setAdapter(userAdapter);
        Tool.setListViewHeightBasedOnChildren(listView);
        LogUtil.e(App.tag, "found friend:" + list.size());

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                User user = (User) parent.getAdapter().getItem(position);
                App.getApp().putTemPObject("user", user);
                Tool.startActivity(ProfileUserActivity.this, ProfileAnyUserActivity.class);
            }
        });
    }


    @OnClick(R.id.viewBack)
    public void onBackClick() {

        if (fromHome) {
            finish();
        } else {
            Tool.startActivity(this, HomeScreenActivity.class);
        }


    }


    @Override
    protected void onResume() {
        super.onResume();
        setupReceivingMethod(App.getApp().getUser());


    }

    @Override
    public void onBackPressed() {
        if (fromHome) {
            finish();
        } else {
            Tool.startActivity(this, HomeScreenActivity.class);
        }
    }

    @OnClick(R.id.buttonAdd)
    public void addWorkClick() {
        Tool.startActivityForResult(this, SearchUserActivity.class, SHORT_GO_ADDWORK);
    }

    @OnClick(R.id.textViewAddWorkLabel)
    public void addWorkLabelClick() {

        Tool.startActivityForResult(this, SearchUserActivity.class, SHORT_GO_ADDWORK);
    }


    @OnClick(R.id.imageButtonPP)
    public void onClickPP() {

        Tool.startActivity(this, PPActivity.class);
    }


    @OnClick(R.id.viewEdit)
    public void onClickEdit() {
        App.getApp().putTemPObject("fromProfile", true);
        Tool.startActivityForResult(this, ProfileFillActivity.class, SHORT_GO_PROFILE);

    }

    @OnClick(R.id.relativePaypal)
    public void onPaypalClick() {
        User user = App.getApp().getUser();
        if (!StringUtils.isEmpty(user.getPayPalEmail())) {
            Tool.showMessageDialog("Paypal:" + user.getPayPalEmail(), ProfileUserActivity.this);
        }
    }

    @InjectView(R.id.viewAddress)
    View viewAddress;

    @InjectView(R.id.viewAddressLine)
    View viewAddressLine;

    private void initUserView() {


        User user = App.getApp().getUser();

        if (null != user) {

            setupReceivingMethod(user);

            textViewEmail.setText(user.getResultEmail());
            textViewMobileNumber.setText(user.getPhone());

            rateBar.setRating(user.getReview());

            textViewCountry.setText(user.getCountry());


            String address = user.getPostcode();
            if (!StringUtils.isEmpty(address)) {
                String[] fullAddress = null;
                if (address.contains(getResources().getString(R.string.split))) {
                    fullAddress = user.getPostcode().split(getResources().getString(R.string.split_r));

                    if (null != fullAddress && fullAddress.length >= 4) {

                        textViewCity.setText(fullAddress[1]);
                        textViewState.setText(fullAddress[2]);
                        textViewPostCode.setText(fullAddress[3]);
                        if (user.isBuiness()) {
                            textViewAddress.setText(fullAddress[0]);
                        } else {
                            textViewPostalAddress.setText(fullAddress[0]);
                        }
                    } else {
                        if (user.isBuiness()) {
                            textViewAddress.setText(address);
                        } else {
                            textViewPostalAddress.setText(address);
                        }
                    }
                } else {
                    if (user.isBuiness()) {
                        textViewAddress.setText(address);
                    } else {
                        textViewPostalAddress.setText(address);
                    }
                }

            }


            textViewCurrency.setText(user.getCurrency());

            if (user.isEmailVerify()) {
                textViewVerifyStatus.setText("Verified");
            } else {
                textViewVerifyStatus.setText("Unverified");
            }

            if (user.isBuiness()) {

                textViewAddWorkLabel.setText("Add Associate");
                textViewName.setText(user.getBussness());

                viewAddress.setVisibility(View.VISIBLE);
                viewPostAddress.setVisibility(View.GONE);


                viewContactName.setVisibility(View.VISIBLE);
                textViewContactName.setText(user.getProfileName());


                viewContactNumber.setVisibility(View.VISIBLE);
                textViewContactNumber.setText(user.getPhone());

                viewMobileNumber.setVisibility(View.GONE);

                findViewById(R.id.viewContactNameLine).setVisibility(View.VISIBLE);
                findViewById(R.id.viewContactNumberLine).setVisibility(View.VISIBLE);
                findViewById(R.id.viewAddressLine).setVisibility(View.VISIBLE);

                findViewById(R.id.viewPostAddressLine).setVisibility(View.GONE);
                findViewById(R.id.viewMobileNumberLine).setVisibility(View.GONE);

            } else {
                textViewAddWorkLabel.setText("Add Workplace");

                viewAddress.setVisibility(View.GONE);
                viewAddressLine.setVisibility(View.GONE);


                viewPostAddress.setVisibility(View.VISIBLE);

                viewContactName.setVisibility(View.GONE);
                viewContactNumber.setVisibility(View.GONE);

                textViewName.setText(user.getProfileName());

                viewMobileNumber.setVisibility(View.VISIBLE);

                findViewById(R.id.viewContactNameLine).setVisibility(View.GONE);
                findViewById(R.id.viewContactNumberLine).setVisibility(View.GONE);
                findViewById(R.id.viewAddressLine).setVisibility(View.GONE);

                findViewById(R.id.viewPostAddressLine).setVisibility(View.VISIBLE);
                findViewById(R.id.viewMobileNumberLine).setVisibility(View.VISIBLE);

            }

            textViewTypeLabel.setText(user.isBuiness() ? "Associate" : "Workplaces");


//            if (!StringUtils.isEmpty(user.getPayPalEmail())) {
//                textViewPaypal.setText(user.getPayPalEmail());
//            }


            // fill headerImage
            if (!StringUtils.isEmpty(user.getHeaderImage())) {
                loadHeaderImage(user.getHeaderImage());
            } else {
                try {
                    if (!StringUtils.isEmpty(user.getFacebookId())) {
                        String fbHeader = SharePersistent.getPerference(ProfileUserActivity.this, KEY_FBHEADER);
                        String[] idpic = fbHeader.split(",");
                        if (idpic[0].equals(user.getFacebookId())) {
                            loadHeaderImage(idpic[1]);
                        }
                    }
                } catch (Exception e) {
                    LogUtil.e(App.tag, "load  fb img error");
                }

            }
        }
    }


    private void setupReceivingMethod(User user) {
        if (null != user) {

            if (StringUtils.isEmpty(user.getRecemethod())) {
                textViewReceivingMethod.setText("Not Setup");
            } else {

                try {
                    JSONObject methodObject = new JSONObject(user.getRecemethod());

                    textViewReceivingMethod.setTag(methodObject);

                    if (methodObject.has("bank_transfer")) {
                        textViewReceivingMethod.setText("Bank Transfer");

                    }
                    if (methodObject.has("cheque")) {
                        textViewReceivingMethod.setText("Cheque");
                    }

                    if (methodObject.has("paypal")) {
                        textViewReceivingMethod.setText("Paypal");
                    }
                    if (methodObject.has("tipppr_debit")) {
                        textViewReceivingMethod.setText("Tipppr Debit");
                    }

                } catch (JSONException e) {
                    textViewReceivingMethod.setText("Not Setup");
                }


            }
        }

    }


    private void loadHeaderImage(String headerUrl) {


        Picasso.with(this).load(headerUrl)
                .resize(maskWidth, maskHeight)
                .transform(new MyBlurTransform(App.getApp()))
                .centerCrop()
                .config(Bitmap.Config.RGB_565)
                .into(imageViewBigHeader, new Callback() {
                    @Override
                    public void onSuccess() {
                        imageViewBottomGlass.setBackgroundResource(R.drawable.bg_cover_photo_mask_cover);
                    }

                    @Override
                    public void onError() {
                    }
                });


        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Bitmap bitmapResult = ImageUtils.clipPatchImageNorecyle(bitmap, bitmapMaskHeader);
                imaveViewSmallHeader.setImageBitmap(bitmapResult);

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };

        imaveViewSmallHeader.setTag(target);
        Picasso.with(App.getApp()).load(headerUrl)
                .resize(maskWidth, maskHeight).config(Bitmap.Config.RGB_565)
                .centerCrop()
                .into(target);

    }


    private void initHeaderLayoutParam() {

        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(p.x, p.y / 2);
        imageViewBigHeader.setLayoutParams(rlp);

        RelativeLayout.LayoutParams rlp2 = new RelativeLayout.LayoutParams(p.x, p.y / 2);
        imageViewBottomGlass.setLayoutParams(rlp2);

        bitmapMaskHeader = BitmapFactory.decodeResource(getResources(), R.drawable.bg_mask_header);

        LogUtil.e(App.tag, " mask dimens:" + bitmapMaskHeader.getWidth() + " " + bitmapMaskHeader.getHeight());

        this.maskWidth = bitmapMaskHeader.getWidth();
        this.maskHeight = bitmapMaskHeader.getHeight();

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == SHORT_REQUEST_READ_EXTEAL) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (type_request_picture == TYPE_REQEST_PICTURE_SELECT) {
                    startSelectPicture();
                } else {
                    startTakePicture();
                }
            } else {
                showNotifyTextIn5Seconds("Read external storage access denied!");
            }
        }

    }

    @OnClick(R.id.buttonLogout)
    public void logoutClick() {

        DialogUtils.showConfirmDialog(this, "", "Logout?", "Confirm", "Cancel", new DialogCallBackListener() {
            @Override
            public void onDone(boolean yesOrNo) {

                if (yesOrNo) {

                    LoginManager.getInstance().logOut();

                    Intent intent = new Intent(ProfileUserActivity.this, LoginSelectActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    startActivity(intent);

                    App.getApp().setUser(null);
                    setUpPushWoosh(null);

                    finish();
                    System.gc();
                }

            }
        });


    }

    @OnClick(R.id.imageButtonContacts)
    public void onClickContacts() {

//        Tool.startActivity(this, HomeScreenActivity.class);
        onFacebookInviteClick();

    }

    public void onFacebookInviteClick() {
        String appLinkUrl, previewImageUrl;
//        appLinkUrl = "https://fb.me/1689621244645679";
        appLinkUrl = "https://fb.me/1702600256681111";
        previewImageUrl = "http://54.183.9.48/tipppr_preview_img.png";

        if (AppInviteDialog.canShow()) {

            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(appLinkUrl)
                    .setPreviewImageUrl(previewImageUrl)
                    .build();


            AppInviteDialog.show(this, content);
        } else {
            LogUtil.i(App.tag, "can not show fb invite");
        }
    }


    private BaseTask deleteTask;

    public void deleteFriend(final User user) {
        if (null != deleteTask && deleteTask.getStatus() == AsyncTask.Status.RUNNING) {
            deleteTask.cancel(true);
        }

        deleteTask = new BaseTask(this, new NetCallBack() {
            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);

            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {

                    JSONObject jso = new JSONObject();
                    jso.put("userid", user.getUserId());
                    netResult = NetInterface.deleteFriend(ProfileUserActivity.this, jso);
                } catch (Exception e) {
                    LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
                }

                return netResult;

            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (StringUtils.isEmpty(result.getMessage())) {
                        loadFriendList(true);
                    } else {
                        Tool.showMessageDialog(result.getMessage(), ProfileUserActivity.this);
                    }
                } else {
                    Tool.showMessageDialog("Net error!", ProfileUserActivity.this);

                }
            }
        });
        deleteTask.execute(new HashMap<String, String>());

    }


    private void deleteCardInfo() {

        reSetTask();
        baseTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {


                NetResult netResult = null;

                try {

                    netResult = NetInterface.deleteBillInfo(ProfileUserActivity.this, new JSONObject());

                } catch (Exception e) {
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {

                    if (StringUtils.isEmpty(result.getMessage())) {

                        Tool.showMessageDialog("Unbind credit card successfully.", ProfileUserActivity.this);

                        getProfile(ProfileUserActivity.this, new OnRequestEndCallBack() {
                            @Override
                            public void onRequestEnd(NetResult netResult) {
                                if (null != netResult) {
                                    User user = (User) netResult.getData()[0];
                                    App.getApp().setUser(user);
                                    initUserView();
                                }
                            }
                        });

                    } else {
                        Tool.showMessageDialog(result.getMessage(), ProfileUserActivity.this);
                    }
                } else {

                    showNotifyTextIn5Seconds(R.string.error_net);
                }


            }
        });

        baseTask.execute(new HashMap<String, String>());

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if ((requestCode == SHORT_GO_CREDIT || requestCode == SHORT_GO_RECEIVEMETHOD) && resultCode == RESULT_OK) {

            getProfile(this, new OnRequestEndCallBack() {
                @Override
                public void onRequestEnd(NetResult netResult) {
                    if (null != netResult) {
                        User user = (User) netResult.getData()[0];
                        App.getApp().setUser(user);
                        initUserView();
                    } else {
                        Tool.showMessageDialog("Net error!", ProfileUserActivity.this);
                    }
                }
            });

            return;
        }

        if (requestCode == SHORT_GO_PROFILE && resultCode == RESULT_OK) {
            initUserView();
            return;
        }


        if (requestCode == SHORT_REQUESET_VERIFY) {
            if (resultCode == RESULT_OK) {
                User nowUser = App.getApp().getUser();
                nowUser.setIsEmailVerify("1");
                App.getApp().setUser(nowUser);
                initUserView();
            }
            return;
        }

        if (requestCode == SHORT_GO_ADDWORK && resultCode == Activity.RESULT_OK) {
            loadPendingFriendList(true);
            loadFriendList(false);
        }
        if (requestCode == SHORT_GO_REQUEST_LIST && resultCode == Activity.RESULT_OK) {
            loadPendingFriendList(true);
            loadFriendList(false);
        }

    }

}
