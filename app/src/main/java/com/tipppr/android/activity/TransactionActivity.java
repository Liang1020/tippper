package com.tipppr.android.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.Constant;
import com.common.util.DateTool;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.NumberHelper;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.tipppr.adapter.TransActionMoreAdatper;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.bean.TransActionMore;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.net.NetInterface;
import com.tipppr.tipppr.view.DateSelectDialog;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class TransactionActivity extends BaseTippprActivity {


    @InjectView(R.id.textViewDuring)
    TextView textViewDuring;

    @InjectView(R.id.listViews)
    ListView listViews;

    @InjectView(R.id.totalMoneySent)
    TextView totalMoneySent;

    @InjectView(R.id.totalMoneyReceiver)
    TextView totalMoneyReceiver;


    @InjectView(R.id.textViewNodataTip)
    TextView textViewNodataTip;


    SimpleDateFormat simpleDateFormatRequest;

    int page_current = 1;
    int total = 0;


    private ArrayList<TransActionMore> arrayListHistory;
    private TransActionMoreAdatper transActionMoreAdatper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);
        ButterKnife.inject(this);

        simpleDateFormatRequest = new SimpleDateFormat("yyyy-MM-dd");

        textViewDuring.setTag(new Date());
        textViewDuring.setText("TODAY");

        Date tody = new Date();

        String todyLabel = sdf.format(tody);

        //===============================
        arrayListHistory = new ArrayList<>();
        transActionMoreAdatper = new TransActionMoreAdatper(TransactionActivity.this, arrayListHistory);
        listViews.setAdapter(transActionMoreAdatper);


        listViews.setOnScrollListener(new AbsListView.OnScrollListener() {
            //AbsListView view 这个view对象就是listview
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {

                    if (view.getLastVisiblePosition() == view.getCount() - 1) {

                        if (ListUtiles.getListSize(arrayListHistory) < total) {
                            Date date = (Date) textViewDuring.getTag();
                            setDateLael(date);
                            String todyLabel = sdf.format(date);
                            requestHistoryList(todyLabel, todyLabel, false);
                        }

                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
            }
        });


        //============
        page_current = 1;
        requestHistoryList(todyLabel, todyLabel, true);

    }


    @OnClick(R.id.buttonPreDay)
    public void onPreClick() {
        Date date = DateTool.addDayOnDate((Date) textViewDuring.getTag(), -1);
        setDateLael(date);

        String todyLabel = sdf.format(date);

        page_current = 1;

        requestHistoryList(todyLabel, todyLabel, true);
    }

    @OnClick(R.id.buttonNextDay)
    public void onNextDayClick() {

        Date selectDay = (Date) textViewDuring.getTag();

        if (Constant.SDFD2.format(selectDay).equals(DateTool.getNowTime(Constant.STRING_DAY_FORMAT2))) {
            Tool.showMessageDialog("Can not choose the future date.", TransactionActivity.this);
            return;
        }


        Date date = DateTool.addDayOnDate((Date) textViewDuring.getTag(), 1);
        setDateLael(date);

        String todyLabel = sdf.format(date);

        page_current = 1;
        requestHistoryList(todyLabel, todyLabel, true);
    }

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private void setDateLael(Date date) {

        String tarGetDate = sdf.format(date);

        String todyFormat = sdf.format(new Date());

        if (todyFormat.equals(tarGetDate)) {
            textViewDuring.setText("TODAY");
        } else {
            textViewDuring.setText(tarGetDate);
        }

        textViewDuring.setTag(date);
    }


    private BaseTask historyTask;

    private void requestHistoryList(final String start_time, final String end_time, final boolean isRefresh) {
        if (null != historyTask && historyTask.getStatus() == AsyncTask.Status.RUNNING) {
            historyTask.cancel(true);
        }

        historyTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {

                    JSONObject jsonObject = new JSONObject();

                    if (!StringUtils.isEmpty(start_time) && !StringUtils.isEmpty(end_time)) {

                        Date dstart = sdf.parse(start_time);
                        Date dend = sdf.parse(end_time);

                        LogUtil.i(App.tag, "start:" + sdf.format(dstart) + " dend:" + sdf.format(dend));

                        long ds = dstart.getTime();


//                        jsonObject.put("start_time", ds);
//                        long de = ds + (24 * 60 * 60 * 1000) - 1000;
//                        jsonObject.put("end_time", de);


                        if (start_time.equals(end_time)) {

                            jsonObject.put("start_time", ds);
                            long de = ds + (24 * 60 * 60 * 1000) - 1000;
                            jsonObject.put("end_time", de);

                        } else {
                            jsonObject.put("start_time", ds);
                            long de = dend.getTime() + (24 * 60 * 60 * 1000) - 1000;
                            ;
                            jsonObject.put("end_time", de);
                        }

                    }

                    jsonObject.put("page", page_current++);
//                    netResult = NetInterface.getPayHistory(TransactionActivity.this, jsonObject);

                    netResult = NetInterface.getPayBillHistory(TransactionActivity.this, jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.e(App.tag, " get history erros:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    ArrayList<TransActionMore> transList = (ArrayList<TransActionMore>) result.getData()[0];
                    total = (Integer) result.getData()[1];
                    buildAdapter(isRefresh, transList, (Float) result.getData()[2], (Float) result.getData()[3]);
                } else {
                    showNotifyTextIn5Seconds(R.string.error_net);
                    page_current--;
                }

            }
        });

        historyTask.execute(new HashMap<String, String>());

    }


    float totalOut = 0.0f;
    float totalIn = 0.0f;

    private void buildAdapter(boolean isRefresh, ArrayList<TransActionMore> transList, float totalIn, float totalOut) {


        LogUtil.i(App.tag, "history size:" + transList.size());

        totalMoneySent.setText("-$" + NumberHelper.keepDecimal2(String.valueOf(totalOut)));
        totalMoneyReceiver.setText("$" + NumberHelper.keepDecimal2(String.valueOf(totalIn)));


        if (ListUtiles.isEmpty(transList)) {
            page_current--;
            if (isRefresh) {
                arrayListHistory.clear();
            }
        } else {
            if (isRefresh) {
                arrayListHistory.clear();
                arrayListHistory.addAll(transList);
            } else {
                arrayListHistory.addAll(transList);
            }
        }


        if (arrayListHistory.isEmpty()) {
            textViewNodataTip.setVisibility(View.VISIBLE);
        } else {
            textViewNodataTip.setVisibility(View.GONE);
        }


        transActionMoreAdatper.notifyDataSetChanged();


    }


    private Date dateStart, dateEnd;

    @OnClick(R.id.textViewDuring)
    public void onDurationClick() {

        showCalendar();

    }

    @OnClick(R.id.imageViewToday)
    public void onClickImageView() {
        showCalendar();

    }


    private void showCalendar() {
        DateSelectDialog dateSelectDialog = new DateSelectDialog(this);
        if (null != dateStart && null != dateEnd) {
            dateSelectDialog.setDate(dateStart, dateEnd);
        }
        dateSelectDialog.setOnDateSelectListener(new DateSelectDialog.OnDateSelectListener() {
            @Override
            public void onDateSelect(Date date1, Date date2) {
                dateStart = date1;
                dateEnd = date2;
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

                String startText = sdf.format(date1);
                String endText = sdf.format(date2);

                String startTime = simpleDateFormatRequest.format(dateStart);
                String endTime = simpleDateFormatRequest.format(dateEnd);

                String todyTime = simpleDateFormatRequest.format(new Date());

                if (startTime.equals(endTime) && startTime.equals(todyTime)) {
                    textViewDuring.setText("TODAY");
                } else {
                    textViewDuring.setText(startText + "\nTO\n" + endText);
                }


                page_current = 1;
                requestHistoryList(startTime, endTime, true);

            }
        });
        dateSelectDialog.show();

    }

    @OnClick(R.id.viewBack)
    public void onBackCLick() {
        finish();
    }

}
