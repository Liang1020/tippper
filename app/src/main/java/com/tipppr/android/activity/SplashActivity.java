package com.tipppr.android.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.view.WindowManager;

import com.baidu.location.BDLocation;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.common.util.Tool;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;

import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends BaseTippprActivity {

    private static final String SEND_TAGS_STATUS_FRAGMENT_TAG = "send_tags_status_fragment_tag";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

    }

    private void goLogin() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                startLocationService();
                delayGo();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                }, SHORT_ACCESS_LOCATION);
            }
        } else {
            startLocationService();
            delayGo();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        LogUtil.i(App.tag, "Splash onresume");

        goLogin();

    }

    @Override
    protected void onStart() {
        super.onStart();
        LogUtil.i(App.tag, "Splash onstart");
//        if (PlayServicesUtils.isGooglePlayStoreAvailable(this)) {
//            PlayServicesUtils.handleAnyPlayServicesError(this);
//        }


    }

    public void delayGo() {

        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                if (SharePersistent.getBoolean(SplashActivity.this, "WatchGuid")) {
                    gotoLoginActivitiy();
                    cancel();
                    timer.cancel();
                } else {

                    Tool.startActivity(SplashActivity.this, GuidActivity.class);
                }


            }
        }, 2 * 1000);

    }


    private void gotoLoginActivitiy() {

        Intent intent = new Intent();
        intent.setClass(SplashActivity.this, LoginSelectActivity.class);

        if (null != getIntent() && getIntent().hasExtra("msg")) {
            intent.putExtra("msg", getIntent().getStringExtra("msg"));
        }

        startActivity(intent);

        LogUtil.i(App.tag, "goto login call");

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == SHORT_ACCESS_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startLocationService();
            } else {
                showToast("Location access denied!", 2);
            }

            delayGo();
        }
    }

    public void startLocationService() {
        App.getApp().setWeakReferenceActivity(new WeakReference<BaseTippprActivity>(this));
        App.getApp().startLocation();
    }


    @Override
    public void onLocationReceive(BDLocation bdLocation, Location googleLocation) {


    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if ((Intent.FLAG_ACTIVITY_CLEAR_TOP & intent.getFlags()) != 0) {
            finish();
        }
    }

    @Override
    public void onStartActivityException(Exception e) {
        LogUtil.e(App.tag, "start activity error:" + e.getLocalizedMessage());
    }
}
