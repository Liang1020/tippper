package com.tipppr.android.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.facebook.login.LoginManager;
import com.pushwoosh.fragment.PushEventListener;
import com.squareup.picasso.Picasso;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.net.NetInterface;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class HomeScreenActivity extends BaseTippprActivity implements PushEventListener {


    @InjectView(R.id.textViewAmount)
    TextView textViewAmount;

    @InjectView(R.id.linearLayoutNumbers)
    LinearLayout linearLayoutNumbers;

    @InjectView(R.id.imageViewBg)
    ImageView imageViewBg;


    private ArrayList<User> arrayListRequestListOutGoing;
    private ArrayList<User> arrayListRequestListIncoming;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_scream);
        ButterKnife.inject(this);
        App.getApp().setMainActivityCreate(true);
        initView();

        if (null != getIntent()) {
            String msg = getIntent().getStringExtra("msg");
            if (!StringUtils.isEmpty(msg)) {
                if (msg.endsWith("a tip.")) {
                    LogUtil.i(App.tag, " go to tip");
                    onHistoryClick();
                }
                if (msg.endsWith("a request.")) {
                    LogUtil.i(App.tag, " go to request list");
                    loadPendingFriendList(false);
                }
                if (msg.endsWith("your request.")) {
                    LogUtil.i(App.tag, " go to request list");
                    onClickBackRigh();
                }
            }

//            xxx sent you a tip.
//            xxx sent you a request.
//            xxx has accepted your request.

        }
    }


    public void initView() {


        for (int i = 0; i < 10; i++) {
            Button button = (Button) linearLayoutNumbers.findViewWithTag(String.valueOf(i));
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String input = getInput(textViewAmount);

                    if (input.contains(".") && !input.endsWith(".")) {

                        int dindex = input.lastIndexOf(".");
                        String afterDot = input.substring(dindex + 1, input.length());
                        LogUtil.i(App.tag, "afterdot:" + afterDot);
                        if (afterDot.length() >= 2) {
                            return;
                        }

                    }

//                    if(input.contains("$0")){
//                        textViewAmount.setText("$");
//                    }

                    String tag = (String) v.getTag();
                    textViewAmount.append(tag);

                    String inputText = getInput(textViewAmount);
                    if (inputText.startsWith("$00")) {
                        textViewAmount.setText("$0");
                    }

                    if (inputText.length() >= 3 && input.startsWith("$0") && !input.startsWith("$0.")) {
                        textViewAmount.setText(inputText.replace("$0", "$"));
                    }

                }
            });
        }


    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitBy2Click();
        }
        return false;
    }

    /**
     * 双击退出函数
     */
    private static Boolean isExit = false;

    private void exitBy2Click() {
        Timer tExit = null;
        if (isExit == false) {
            isExit = true; // 准备退出
            Toast.makeText(this, "Click again to exit", Toast.LENGTH_SHORT).show();
            tExit = new Timer();
            tExit.schedule(new TimerTask() {
                @Override
                public void run() {
                    isExit = false;
                }
            }, 2000);

        } else {

            LoginManager.getInstance().logOut();
            Intent intent = new Intent(HomeScreenActivity.this, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("isExit", true);

            startActivity(intent);

//            App.getApp().setUser(null);
            finish();
            System.gc();

//            finish();
//            System.exit(0);

        }
    }


    @OnClick(R.id.viewProfile)
    public void onClickBackRigh() {
        App.getApp().putTemPObject("fromHomeScream", true);
        Tool.startActivity(this, ProfileUserActivity.class);
    }

    @OnClick(R.id.buttonDot)
    public void onClickDot() {
        String input = getInput(textViewAmount);
        if (!StringUtils.isEmpty(input) && StringUtils.computStrlen(input) > 1 && !input.contains(".")) {

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(input + ".");
            textViewAmount.setText(stringBuilder);
        }

    }

    @OnClick(R.id.buttonIdDel)
    public void onClickDel() {
        String inputText = getInput(textViewAmount);
        textViewAmount.setText("$0");
//        if (!StringUtils.isEmpty(inputText) && (StringUtils.computStrlen(inputText) > 1)) {
//            textViewAmount.setText(inputText.substring(0, inputText.length() - 1));
//        }
    }

    private short SHORT_GO_PAY = 55;

    @OnClick(R.id.buttonTip)
    public void onTipClick() {

        String input = getInput(textViewAmount);
        if (!"$".equals(input)) {

            String money = input.replace("$", "");
            try {

                if (StringUtils.isEmpty(money)) {
                    showNotifyTextIn5Seconds("Please input amount first.");
                    return;
                }


                float moneyNumber = Float.parseFloat(money);

                if (moneyNumber < 1) {
                    showNotifyTextIn5Seconds(R.string.notify_less_money);
                    return;
                }


                if (moneyNumber > maxMoney) {
//                    showNotifyTextIn5Seconds(" Amount should not be more than " + maxMoney + ".");
                    Tool.showMessageDialog("Oops!", "You really are generous! However Tipppr only accepts tips up to $200. Please adjust your tip before continuing.", this);


                    return;
                }
                App.getApp().putTemPObject("money", money);
                Tool.startActivityForResult(HomeScreenActivity.this, TippprOptionActivity.class, SHORT_GO_PAY);
            } catch (Exception e) {
                LogUtil.e(App.tag, "exception:" + e.getLocalizedMessage());
                showNotifyTextIn5Seconds("Money input error!");
            }
        } else {
            showNotifyTextIn5Seconds("Please input amount first.");
            return;
        }
    }

    @OnClick(R.id.imageButtonTransaction)
    public void onHistoryClick() {
        Tool.startActivity(HomeScreenActivity.this, TransactionActivity.class);
    }


    private BaseTask friendTask2 = null;

    private void loadPendingFriendList(final boolean showDialog) {

        if (null != friendTask2 && friendTask2.getStatus() == AsyncTask.Status.RUNNING) {
            friendTask2.cancel(true);
        }

        friendTask2 = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    baseTask.showDialogForSelf(true);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {
                    netResult = NetInterface.getpendingFriendList(HomeScreenActivity.this, new JSONObject());
                } catch (Exception e) {
                    LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
                }

                return netResult;

            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result && StringUtils.isEmpty(result.getMessage())) {
                    try {
                        arrayListRequestListOutGoing = (ArrayList<User>) result.getData()[0];
                        arrayListRequestListIncoming = (ArrayList<User>) result.getData()[1];
                        LogUtil.i(App.tag, "found request:" + arrayListRequestListOutGoing.size());

                        App.getApp().putTemPObject("requestlist", arrayListRequestListOutGoing);
                        App.getApp().putTemPObject("requestListIn", arrayListRequestListIncoming);
                        Tool.startActivity(HomeScreenActivity.this, RequestListActivity.class);


                    } catch (Exception e) {
                        LogUtil.e(App.tag, "exception:in get count");

                    }
                }
            }
        }, this);

        friendTask2.execute(new HashMap<String, String>());

    }


    @Override
    protected void onResume() {
        super.onResume();
        App.getApp().setMainActivityVisible(true);
        updateImg();

    }


    private void updateImg() {

        Point p = Tool.getDisplayMetrics(this);

        User user = App.getApp().getUser();

        if (null != user) {

            String headerImg = user.getHeaderImage();

            if (!StringUtils.isEmpty(headerImg)) {
//                Picasso.with(App.getApp()).invalidate(headerImg);
                Picasso.with(App.getApp()).load(App.getApp().getUser().getHeaderImage())
                        .config(Bitmap.Config.RGB_565)
                        .resize(p.x, p.y)
//                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .centerCrop()
                        .into(imageViewBg);
            }
        }

    }


    @Override
    protected void onStop() {
        super.onStop();
        App.getApp().setMainActivityVisible(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        App.getApp().setMainActivityCreate(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SHORT_GO_PAY) {

            if (resultCode == RESULT_OK) {
                textViewAmount.setText("$0");
            }

        }
    }

    @Override
    public void doOnUnregisteredError(String s) {

    }

    @Override
    public void doOnRegisteredError(String s) {

    }

    @Override
    public void doOnRegistered(String s) {

    }

    @Override
    public void doOnMessageReceive(String s) {

        LogUtil.i(App.tag, "receiver msg home screen");

    }

    @Override
    public void doOnUnregistered(String s) {

    }
}
