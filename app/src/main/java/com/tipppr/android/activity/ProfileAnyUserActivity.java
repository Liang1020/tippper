package com.tipppr.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.callback.OnRequestEndCallBack;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.ImageUtils;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.push.PushTool;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.adapter.UserAdapter;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.net.NetInterface;
import com.tipppr.tipppr.view.DialogFriends;
import com.tipppr.tipppr.view.DialogReport;
import com.tipppr.tipppr.view.DialogVerifyNotify;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import jp.wasabeef.picasso.transformations.BlurTransformation;

public class ProfileAnyUserActivity extends BaseTippprActivity {


    @InjectView(R.id.imageViewBigHeader)
    ImageView imageViewBigHeader;

    @InjectView(R.id.imageViewBottomGlass)
    ImageView imageViewBottomGlass;

    @InjectView(R.id.imaveViewSmallHeader)
    ImageView imaveViewSmallHeader;


    @InjectView(R.id.textViewName)
    TextView textViewName;

    @InjectView(R.id.textViewEmail)
    TextView textViewEmail;

    @InjectView(R.id.textViewMobileNumber)
    TextView textViewMobileNumber;

    @InjectView(R.id.textViewTypeLabel)
    TextView textViewTypeLabel;

    @InjectView(R.id.viewPostAddress)
    View viewPostAddress;

    @InjectView(R.id.viewPostAddressLine)
    View viewPostAddressLine;

    @InjectView(R.id.textViewPostalAddress)
    TextView textViewPostalAddress;

    @InjectView(R.id.textViewAddress)
    TextView textViewAddress;

    @InjectView(R.id.listView)
    ListView listView;

    @InjectView(R.id.rateBar)
    RatingBar rateBar;

    Point p = null;

    private Bitmap bitmapMaskHeader;
    private int maskWidth, maskHeight;
    private short SHORT_GO_REQUEST_LIST = 77;
    private short SHOURT_GO_ADDWORK = 78;

    private ArrayList<User> arrayListRequestList;
    private User user;

    @InjectView(R.id.scrollView)
    ScrollView scrollView;

    @InjectView(R.id.buttonSendRequest)
    Button buttonSendRequest;

    private boolean isFromTip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_any_user);
        ButterKnife.inject(this);
        p = Tool.getDisplayMetrics(this);

        user = (User) App.getApp().getTempObject("user");

        if (App.getApp().hasTempKey("fromTip") && (isFromTip = (Boolean) App.getApp().getTempObject("fromTip"))) {
            buttonSendRequest.setText("Confirm");
        } else {
            if (App.getApp().getUser().isBuiness()) {
                buttonSendRequest.setText("Send Employer Request");
            } else {
                buttonSendRequest.setText("Send Employee Request");
            }
        }

        initHeaderLayoutParam();
        loadAllUserProfile(true, user.getUserId());

//        initUserView();
//        loadFriendList(false);

//        scrollView.post(new Runnable() {
//            @Override
//            public void run() {
//                scrollView.fullScroll(ScrollView.FOCUS_UP);
//            }
//        });


    }


    @OnClick(R.id.viewReport)
    public void onActivityClick() {

        DialogReport dialogReport = new DialogReport(ProfileAnyUserActivity.this, user.getUserId());
        dialogReport.show();

    }

    @OnClick(R.id.viewAddress)
    public void onClickAddress() {
        gotoMapActivity();
    }

    @OnClick(R.id.viewPostAddress)
    public void onCLickPostAddress() {

        gotoMapActivity();

    }

    private void gotoMapActivity() {


//        String lng = App.getApp().getUser().getLng();
//        String lat = App.getApp().getUser().getLat();
//
//        if (!StringUtils.isEmpty(lat + lng)) {
//
//            LatLng latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
//
//            App.getApp().putTemPObject("address", App.getApp().getUser().getPostcode());
//
//            App.getApp().putTemPObject("latlng", latLng);
//
//            Tool.startActivity(this, MapsActivity.class);
//        }
    }

    private void buildFriendListAdapter(ArrayList<User> list) {
        UserAdapter userAdapter = new UserAdapter(this, list, user);
        listView.setAdapter(userAdapter);
        Tool.setListViewHeightBasedOnChildren(listView);
        LogUtil.e(App.tag, "found friend:" + list.size());

    }


    BaseTask userProfileTask = null;

    private void loadAllUserProfile(final boolean showDialog, final String userId) {
        if (null != userProfileTask && userProfileTask.getStatus() == AsyncTask.Status.RUNNING) {
            userProfileTask.cancel(true);
        }

        userProfileTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    baseTask.showDialogForSelf(true);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.getUserAllProfile(ProfileAnyUserActivity.this, userId, new JSONObject());
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.e(App.tag, " get user profile:" + e.getMessage());

                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (showDialog) {
                    baseTask.hideDialogSelf();
                }

                if (null != result) {

                    if (StringUtils.isEmpty(result.getMessage())) {
                        user = (User) result.getData()[0];
                        initUserView();
                        getAllProfile(ProfileAnyUserActivity.this, App.getApp().getUser().getUserId(), new OnRequestEndCallBack() {
                            @Override
                            public void onRequestEnd(NetResult netResult) {

                                if (null != netResult && StringUtils.isEmpty(netResult.getMessage())) {
                                    User me = (User) netResult.getData()[0];

                                    if (isFromTip) {
                                        buttonSendRequest.setVisibility(View.VISIBLE);
                                    } else {

                                        ArrayList<User> friens = me.getAllFriends();
                                        if (ListUtiles.isEmpty(friens)) {

                                            if (me.getUserId().equals(user.getUserId()) || me.getLevel().equals(user.getLevel())) {
                                                buttonSendRequest.setVisibility(View.INVISIBLE);
                                            }else{
                                                buttonSendRequest.setVisibility(View.VISIBLE);
                                            }

                                        } else {

                                            for (User tu : friens) {
                                                if (tu.getUserId().equals(user.getUserId())) {
                                                    buttonSendRequest.setVisibility(View.INVISIBLE);
                                                    LogUtil.i(App.tag, "隐藏request btn:");
                                                    break;
                                                } else {
                                                    buttonSendRequest.setVisibility(View.VISIBLE);
                                                    LogUtil.i(App.tag, "显示request btn:");
                                                }
                                            }


                                            if (me.getUserId().equals(user.getUserId()) || me.getLevel().equals(user.getLevel())) {
                                                buttonSendRequest.setVisibility(View.INVISIBLE);
                                            }

                                        }

//                                        if(App.getApp().getUser().isBuiness() && me.isBuiness()){
//                                            buttonSendRequest.setVisibility(View.INVISIBLE);
//                                        }
                                    }

//                                    if (App.getApp().getUser().getLevel().equals(me.getLevel())) {
//                                        buttonSendRequest.setVisibility(View.INVISIBLE);
//                                    }
//

                                }


                            }
                        });
                    } else {
                        Tool.showMessageDialog(result.getMessage(), ProfileAnyUserActivity.this);
                    }
                } else {
                    Tool.showMessageDialog("Net error!", ProfileAnyUserActivity.this);
                }


            }
        }, this);

        userProfileTask.execute(new HashMap<String, String>());

    }


    @OnClick(R.id.viewBack)
    public void onBackClick() {
        finish();
    }


    @OnClick(R.id.buttonSendRequest)
    public void onClickSendRequest() {
        if (isFromTip) {

            if (ListUtiles.isEmpty(user.getAllFriends())) {
                App.getApp().putTemPObject("selectUser2", ProfileAnyUserActivity.this.user);
                setResult(RESULT_OK);
                finish();

            } else {
                DialogFriends dialogFriends = new DialogFriends(this, user);

                dialogFriends.setOnContinuClickListener(new DialogFriends.OnContinuClickListener() {
                    @Override
                    public void onClickDone(User user) {
                        if (null != user) {
                            App.getApp().putTemPObject("selectUser1", user);
                        }
                        App.getApp().putTemPObject("selectUser2", ProfileAnyUserActivity.this.user);
                        setResult(RESULT_OK);
                        finish();
                    }
                });
                dialogFriends.show();
            }

        } else {


            User currentUser = App.getApp().getUser();
            if (!currentUser.isEmailVerify()) {

                DialogVerifyNotify dialogVerifyNotify = new DialogVerifyNotify(ProfileAnyUserActivity.this);
                dialogVerifyNotify.setOnClickOkListener(new DialogVerifyNotify.OnClickOkListener() {
                    @Override
                    public void onVerifyClick() {
                        Tool.startActivityForResult(ProfileAnyUserActivity.this, VerifyActivity.class, SHORT_REQUESET_VERIFY);
                    }
                });
                dialogVerifyNotify.show();
                return;
            }

            DialogUtils.showConfirmDialog(ProfileAnyUserActivity.this, "", " Do you want to send request?", "confirm", "cancel", new DialogCallBackListener() {
                @Override
                public void onDone(boolean yesOrNo) {
                    if (yesOrNo) {
                        addUserList(user);
                    }
                }
            });

        }

    }


    public void addUserList(final User user) {

        reSetTask();

        baseTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("userid", user.getUserId());

                    netResult = NetInterface.beFriend(baseTask.weakReferenceContext.get(), jsonObject);

//                  xxx sent you a request.

                    String content = "";

                    User currentUser = App.getApp().getUser();

                    if (currentUser.isBuiness()) {
                        content = currentUser.getBussness();
                    } else {
                        content = currentUser.getProfileName();
                    }


                    ArrayList<String> targetid = new ArrayList<>();
                    targetid.add(user.getUserId());

                    content += " sent you a request.";

                    JSONObject jsonObjetType = new JSONObject();
                    jsonObjetType.put("type", 1);

                    PushTool.sendPushWithPushwoosh(targetid, content, jsonObjetType);

                } catch (Exception e) {
                    LogUtil.e(App.tag, "exception error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (!StringUtils.isEmpty(result.getMessage())) {
                        Tool.showMessageDialog(result.getMessage(), ProfileAnyUserActivity.this);
                    } else {

                        Tool.showMessageDialog("Request has been sent successfully.", ProfileAnyUserActivity.this, new DialogCallBackListener() {

                            @Override
                            public void onDone(boolean yesOrNo) {
                                backResultOk();
                            }
                        });

                    }

                } else {
                    Tool.showMessageDialog("Net error!", ProfileAnyUserActivity.this);
                }
            }
        }, this);

        baseTask.execute(new HashMap<String, String>());

    }

    public void backResultOk() {
//        App.getApp().putTemPObject("user", user);
        setResult(Activity.RESULT_OK);
        finish();

    }


    @InjectView(R.id.viewAddress)
    View viewAddress;

    @InjectView(R.id.viewAddressLine)
    View viewAddressLine;

    @InjectView(R.id.linearlayoutEmailContent)
    LinearLayout linearlayoutEmailContent;

    @InjectView(R.id.viewLineContactLine)
    View viewLineContactLine;

    @InjectView(R.id.lienarLayoutContactNumber)
    View lienarLayoutContactNumber;

    private void initUserView() {

        if (null != user) {

            textViewEmail.setText(user.getEmail());

            textViewMobileNumber.setText(user.getPhone());

            rateBar.setRating(user.getReview());

            if (user.isBuiness()) {

                viewAddress.setVisibility(View.VISIBLE);
//                viewAddress.setVisibility(View.VISIBLE);
//                viewAddressLine.setVisibility(View.VISIBLE);

//                viewPostAddress.setVisibility(View.GONE);
                viewPostAddressLine.setVisibility(View.VISIBLE);

                //===================

                String postCode = user.getPostcode();
                if (!StringUtils.isEmpty(postCode)) {
                    textViewAddress.setText(user.getPostcode().replace(App.getApp().getSplit(), ", "));
                }
                //=================================

                textViewName.setText(user.getBussness());

            } else {
//                viewAddress.setVisibility(View.GONE);
//                viewAddressLine.setVisibility(View.GONE);
//                viewPostAddress.setVisibility(View.VISIBLE);

                viewLineContactLine.setVisibility(View.GONE);
                linearlayoutEmailContent.setVisibility(View.GONE);
                lienarLayoutContactNumber.setVisibility(View.GONE);

                viewPostAddressLine.setVisibility(View.GONE);
                viewAddress.setVisibility(View.GONE);

                textViewName.setText(user.getName());

            }

            textViewPostalAddress.setText(user.getPostcode());
            textViewTypeLabel.setText(User.ROLE_BUSINESS.equals(user.getLevel()) ? "Associate" : "Workplaces");


            // fill headerImage
            if (!StringUtils.isEmpty(user.getHeaderImage())) {

                String headerUrl = user.getHeaderImage();


                Picasso.with(this).load(headerUrl)
                        .resize(p.x, p.y / 2)
                        .transform(new BlurTransformation(this))
                        .centerCrop()
                        .config(Bitmap.Config.RGB_565)
                        .into(imageViewBigHeader, new Callback() {
                            @Override
                            public void onSuccess() {
                                imageViewBottomGlass.setBackgroundResource(R.drawable.bg_cover_photo_mask_cover);
                            }

                            @Override
                            public void onError() {

                            }
                        });

                Target target = new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        Bitmap bitmapResult = ImageUtils.clipPatchImageNorecyle(bitmap, bitmapMaskHeader);
                        imaveViewSmallHeader.setImageBitmap(bitmapResult);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                };

                imaveViewSmallHeader.setTag(target);
                Picasso.with(App.getApp()).load(headerUrl)
                        .resize(maskWidth, maskHeight).config(Bitmap.Config.RGB_565)
                        .centerCrop()
                        .noFade()
                        .into(target);
            }


            buildFriendListAdapter(user.getAllFriends());

        }
    }


    private void initHeaderLayoutParam() {

        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(p.x, p.y / 2);
        imageViewBigHeader.setLayoutParams(rlp);

        RelativeLayout.LayoutParams rlp2 = new RelativeLayout.LayoutParams(p.x, p.y / 2);
        imageViewBottomGlass.setLayoutParams(rlp2);

        bitmapMaskHeader = BitmapFactory.decodeResource(getResources(), R.drawable.bg_mask_header);

        LogUtil.e(App.tag, " mask dimens:" + bitmapMaskHeader.getWidth() + " " + bitmapMaskHeader.getHeight());

        this.maskWidth = bitmapMaskHeader.getWidth();
        this.maskHeight = bitmapMaskHeader.getHeight();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SHORT_REQUESET_VERIFY) {

            if (resultCode == RESULT_OK) {
                User nowUser = App.getApp().getUser();
                nowUser.setIsEmailVerify("1");
                App.getApp().setUser(nowUser);
            }
            return;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == SHORT_REQUEST_READ_EXTEAL) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (type_request_picture == TYPE_REQEST_PICTURE_SELECT) {
                    startSelectPicture();
                } else {
                    startTakePicture();

                }
            } else {
                showNotifyTextIn5Seconds("Read external storage access denied!");
            }
        }

    }


}
