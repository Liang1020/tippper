package com.tipppr.android.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.common.util.LogUtil;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class PaypalLoginActivity extends BaseTippprActivity {


    @InjectView(R.id.webView)
    WebView webView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paypal_login);
        ButterKnife.inject(this);

        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebViewClient(new WebViewClient() {
            /**
             * 在点击请求的是链接是才会调用，
             * 重写此方法返回true表明点击网页里面的链接还是在当前的webview里跳转，不跳到浏览器那边。
             * @return
             */
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);

                LogUtil.e(App.tag, "error:" + error.toString());
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                showProgressDialog();

                LogUtil.i(App.tag, "url:" + url);

                String successPrefix = "http://www.tipppr.com/?scope=email&code=";
                String failPrefix = "http://www.tipppr.com/?scope=email&error=";

                if (url.startsWith(successPrefix)) {

                    String code = url.replace(successPrefix, "");
                    LogUtil.i(App.tag, "code:" + code);

                    App.getApp().putTemPObject("code", code);
                    setResult(RESULT_OK);
                    finish();

                }
                if (url.startsWith(failPrefix)) {

                    setResult(RESULT_CANCELED);
                    finish();

                    return;
                }


                LogUtil.i(App.tag, "url:" + url);

//                String testUrl="https://stripe.com/connect/default/oauth/test";
//                String realaserUrl="https://stripe.com/connect/default/oauth/live";
//
//                if (url.startsWith(realaserUrl)) {
//
//                    String path = "https://connect.stripe.com/oauth/token";
//                    String code = url.substring(url.lastIndexOf('=') + 1);
//                    String skScret = sk;
//                    String grant_type = "authorization_code";
//                }
//                LogUtil.i(App.tag, url);

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                LogUtil.i(App.tag, "complete finish:" + url);
                hideProgressDialog();
            }
        });

        //login_paypal.html

        webView.loadUrl("file:///android_asset/login_sand_box.html");
//        webView.loadUrl("file:///android_asset/login_paypal.html");
    }


    @OnClick(R.id.viewBack)
    public void onClickBack() {
        finish();
    }


}
