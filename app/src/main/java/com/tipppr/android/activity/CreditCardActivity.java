package com.tipppr.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.callback.OnRequestEndCallBack;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DateTool;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.util.ValidateTool;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.net.NetInterface;
import com.tipppr.tipppr.view.DialogSingleOption;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class CreditCardActivity extends BaseTippprActivity {


    @InjectView(R.id.editTextAccountNumber)
    EditText editTextAccountNumber;

    @InjectView(R.id.editTextExpiredDate)
    EditText editTextExpiredDate;

    @InjectView(R.id.editTextCVV)
    EditText editTextCVV;

    @InjectView(R.id.editTextFirstName)
    EditText editTextFirstName;


    @InjectView(R.id.editTextLastName)
    EditText editTextLastName;

    @InjectView(R.id.editTextEmail)
    EditText editTextEmail;

    @InjectView(R.id.editTextMobileNumber)
    EditText editTextMobileNumber;

    @InjectView(R.id.editTextStreet)
    EditText editTextStreet;

    @InjectView(R.id.editTextCity)
    EditText editTextCity;

    @InjectView(R.id.editTextState)
    EditText editTextState;

    @InjectView(R.id.editTextPostCode)
    EditText editTextPostCode;

    @InjectView(R.id.linearLayoutCurrency)
    LinearLayout linearLayoutCurrency;

    @InjectView(R.id.textViewCountryCurrency)
    TextView textViewCountry;

    @InjectView(R.id.textViewCurrency)
    TextView textViewCurrency;

    @InjectView(R.id.textViewTitle)
    TextView textViewTitle;

    @InjectView(R.id.textViewSkip)
    TextView textViewSkip;

    @InjectView(R.id.textViewTitleStep)
    TextView textViewTitleStep;


    @InjectView(R.id.lienarLayoutFirstnameLastName)
    LinearLayout lienarLayoutFirstnameLastName;

    boolean isfromRegist = false;

    short SHORT_GO_PROFILE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_card);
        ButterKnife.inject(this);
        initView();

    }

    public void initView() {

        if (App.getApp().hasTempKey("tag") && (Boolean) App.getApp().getTempObject("tag") == true) {
            isfromRegist = true;

            textViewTitle.setVisibility(View.INVISIBLE);
            textViewSkip.setVisibility(View.VISIBLE);
            textViewTitleStep.setVisibility(View.VISIBLE);

        } else {
            isfromRegist = false;
            textViewTitle.setVisibility(View.VISIBLE);
            textViewSkip.setVisibility(View.INVISIBLE);
            textViewTitleStep.setVisibility(View.INVISIBLE);
        }


        User user = App.getApp().getUser();

        if (null != user) {

            if (!user.isBuiness() && user.getResultName().contains("|")) {
                String[] names = user.getResultName().split("\\|");
                if (names.length == 2) {
                    editTextFirstName.setText(names[0]);
                    editTextLastName.setText(names[1]);
                }
            }
            editTextEmail.setText(user.getResultEmail());
            editTextMobileNumber.setText(user.getPhone());
            textViewCurrency.setText(user.getCurrency());
            //======

            String postCodes = user.getPostcode();


            String[] address = postCodes.split("\\|");
            if (!StringUtils.isEmpty(postCodes) && address.length == 4) {
                //516Northboume, avenue, downer|ChChengdu|la|post123

                editTextStreet.setText(address[0]);
                editTextCity.setText(address[1]);
                editTextState.setText(address[2]);
                editTextPostCode.setText(address[3]);
            }

            LogUtil.i(App.tag, "postcode:" + postCodes);

            if (!StringUtils.isEmpty(user.getCountry())) {

                if ("USA".equals(user.getCountry())) {
                    //======
                    textViewCountry.setText("US");
                }
                if ("Cannada".equals(user.getCountry())) {
                    //======
                    textViewCountry.setText("CA");
                }
                if ("Australia".equals(user.getCountry())) {
                    //======
                    textViewCountry.setText("AU");
                }

            }
        }

    }


    @OnClick(R.id.viewCurrency)
    public void onClickViewCurency() {

        closeSoftWareKeyBoradInBase();


        LogUtil.i(App.tag, "click currecy xx");


        String[] array = {"USD", "AUD", "CAD"};

        DialogSingleOption dialogSingleOption = new DialogSingleOption(this, array, getInput(textViewCurrency));

        dialogSingleOption.setTitle("Currency");

        dialogSingleOption.setOnOptionSelectDoneListener(new DialogSingleOption.OnOptionSelectDoneListener() {
            @Override
            public void onSelectDonw(String seelct) {

                textViewCurrency.setText(seelct);

            }
        });


        dialogSingleOption.show();


    }


    private boolean haveUnSavedInfo() {


        String ret = getInput(editTextAccountNumber) +
                getInput(editTextCVV) +
                getInput(editTextExpiredDate) +
                getInput(editTextFirstName) +
                getInput(editTextLastName) +
                getInput(editTextEmail) +
                getInput(editTextMobileNumber) +
                getInput(textViewCurrency) +
                getInput(editTextStreet) +
                getInput(editTextCity) +
                getInput(editTextState) +
                getInput(editTextPostCode) +
                getInput(textViewCountry);
        if (StringUtils.isEmpty(ret)) {
            return false;
        } else {
            return true;
        }

    }


    @OnClick(R.id.linearLayoutCurrency)
    public void onCountryCurrencyCLick() {

        closeSoftWareKeyBoradInBase();


        String[] array = {"US", "AU", "CA"};

        DialogSingleOption dialogSingleOption = new DialogSingleOption(this, array, getInput(textViewCountry));

        dialogSingleOption.setTitle("Country");

        dialogSingleOption.setOnOptionSelectDoneListener(new DialogSingleOption.OnOptionSelectDoneListener() {
            @Override
            public void onSelectDonw(String seelct) {
                textViewCountry.setText(seelct);
            }
        });


        dialogSingleOption.show();


//        DialogCountryCurrency dialogCountryCurrency = new DialogCountryCurrency(this, getInput(textViewCountry));
//
//        dialogCountryCurrency.setOnOptionSelectDoneListener(new DialogCountryCurrency.OnOptionSelectDoneListener() {
//            @Override
//            public void onSelectDonw(String country, String currency) {
//
//                LogUtil.i(App.tag, "country:" + country + " currency:" + currency);
//                textViewCountry.setText(country + "/" + currency);
//            }
//        });
//        dialogCountryCurrency.show();

    }


    @OnClick(R.id.viewBack)
    public void onViewbackClick() {
        onExitPage();
    }


    @Override
    public void onBackPressed() {

        onExitPage();
    }

    private void onExitPage() {

        if (haveUnSavedInfo()) {
            DialogUtils.showConfirmDialog(this, "Oops!", "You are about to leave this page however you have entered credit card information which has not yet been saved. Are you sure you want to leave the page without saving your information?", "Confirm", "Cancel", new DialogCallBackListener() {
                @Override
                public void onDone(boolean yesOrNo) {
                    if (yesOrNo) {
                        finish();
                    } else {

                    }

                }
            });
        } else {
            finish();
        }
    }


    @OnTextChanged(R.id.editTextExpiredDate)
    public void onInputChanged() {

        if (getInput(editTextExpiredDate).length() == 2 && !getInput(editTextExpiredDate).contains("/")) {
            editTextExpiredDate.setText(getInput(editTextExpiredDate) + "/");
            editTextExpiredDate.setSelection(getInput(editTextExpiredDate).length());
        }

        if (getInput(editTextExpiredDate).length() == 3) {
            editTextExpiredDate.setSelection(getInput(editTextExpiredDate).length());
        }


    }


    public boolean checkValue() {


        String cardNumber = getInput(editTextAccountNumber).replace("-", "");


        String cardExp = getInput(editTextExpiredDate);
        String cvv = getInput(editTextCVV);

        if (StringUtils.isEmpty(cardNumber) || StringUtils.computStrlen(cardNumber.replace("-", "")) != 16) {
            return false;
        }

        if (StringUtils.isEmpty(cardExp)) {
            return false;
        }


        try {
            String[] input = getInput(editTextExpiredDate).split("/");

            SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
            Date d = sdf.parse(getInput(editTextExpiredDate));

            Date now = DateTool.getNow();


            if (now.getTime() > d.getTime()) {
                return false;

            }

            if (StringUtils.computStrlen(cvv) != 3 && StringUtils.computStrlen(cvv) != 4) {
                return false;

            }

        } catch (Exception e) {
            return false;
        }

        String cardFirstName = getInput(editTextFirstName);


        if (StringUtils.isEmpty(cardFirstName)) {
            return false;
        }

        String cardlastName = getInput(editTextLastName);
        if (StringUtils.isEmpty(cardlastName)) {
            return false;
        }

        String cardEmail = getInput(editTextEmail);
        if (!ValidateTool.checkEmail(cardEmail)) {
            return false;
        }

        String mobilNumber = getInput(editTextMobileNumber);
        if (StringUtils.isEmpty(mobilNumber)) {
            return false;
        }

        String curreency = getInput(textViewCurrency);
        if (StringUtils.isEmpty(mobilNumber)) {
            return false;
        }

        String street = getInput(editTextStreet);

        if (StringUtils.isEmpty(street)) {
            return false;
        }


        String cardCity = getInput(editTextCity);

        if (StringUtils.isEmpty(cardCity)) {
            return false;
        }

        String cardState = getInput(editTextState);
        if (StringUtils.isEmpty(cardState)) {
            return false;
        }

        String cardPostCode = getInput(editTextPostCode);
        if (StringUtils.isEmpty(cardPostCode)) {
            return false;
        }


        String countryCurrecy = getInput(textViewCountry);
        if (StringUtils.isEmpty(countryCurrecy)) {
            return false;
        }


        return true;
    }


    private void gotoVerifyActivity() {


        App.getApp().putTemPObject("tag", true);
        Tool.startActivity(this, VerifyActivity.class);


    }


    @OnClick(R.id.textViewSkip)
    public void onSkipClick() {
        gotoVerifyActivity();
    }


    @OnClick(R.id.buttonSave)
    public void onClickSave() {

      /*  {
                "cardNumber":"",
                "cardExp":"",
                "cvv":"",

                "cardFirstName":"",
                "cardLastName":"",
                "cardEmail":"",
                "cardCountry":"",
                "cardCurrency":"",
                "cardCity":"",
                "cardState":"",
                "cardPostCode":""
                "cardAddress":""

        }

            creditCardjson.put("cardNumber", "4111111111111111");
            creditCardjson.put("cardExp", "12/2016");
            creditCardjson.put("cvv", "123");
            creditCardjson.put("cardFirstName", "Sand");
            creditCardjson.put("cardLastName", "W");
            creditCardjson.put("cardEmail", "sand@domain.com");

            creditCardjson.put("cardCountry", "US");
            creditCardjson.put("cardCurrency", "USD");
            creditCardjson.put("cardCity", "Chengdu");
            creditCardjson.put("cardState", "LA");
            creditCardjson.put("cardPostCode", "4100");
            creditCardjson.put("cardAddress", "123 Boring Road");

        */


        String cardNumber = getInput(editTextAccountNumber).replace("-", "");
        String cardExp = getInput(editTextExpiredDate);
        String cvv = getInput(editTextCVV);

        if (!checkCard(cardNumber, cardExp, cvv)) {
            return;
        }

        User user = App.getApp().getUser();

        String cardFirstName = getInput(editTextFirstName);

        if (StringUtils.isEmpty(cardFirstName)) {
            Tool.showMessageDialog("Firstname is empty.", this);
            return;
        }

        String cardlastName = getInput(editTextLastName);
        if (StringUtils.isEmpty(cardlastName)) {
            Tool.showMessageDialog("Lastname is empty.", this);
            return;
        }


        String cardEmail = user.getResultEmail();
        String mobilNumber = user.getPhone();
        String cardCurrency = user.getCurrency();
        String cardCountry = "";
        String postCodes = user.getPostcode();

        String street = "";
        String cardCity = "";
        String cardState = "";
        String cardPostCode = "";

        String[] address = postCodes.split("\\|");
        if (!StringUtils.isEmpty(postCodes) && address.length == 4) {
            //516Northboume, avenue, downer|ChChengdu|la|post123
            street = (address[0]);
            cardCity = (address[1]);
            cardState = (address[2]);
            cardPostCode = (address[3]);
        }
        LogUtil.i(App.tag, "postcode:" + postCodes);

        if (!StringUtils.isEmpty(user.getCountry())) {

            if ("USA".equals(user.getCountry())) {
                //======
                cardCountry = "US";
            }
            if ("Cannada".equals(user.getCountry())) {
                //======
                cardCountry = "CA";
            }
            if ("Australia".equals(user.getCountry())) {
                //======
                cardCountry = "AU";
            }

        }

        //========================下面的内容不再从页面获取============================
//
//        String cardEmail = getInput(editTextEmail);
        if (!ValidateTool.checkEmail(cardEmail)) {
            Tool.showMessageDialog("Card email format error.", this);
            return;
        }
//
//        String mobilNumber = getInput(editTextMobileNumber);
        if (StringUtils.isEmpty(mobilNumber)) {

            showMeessage2ConfirmProfile();
//            Tool.showMessageDialog("Mobile number is empty.", this);
            return;
        }
//
//
//        String cureency = getInput(textViewCurrency);
//
        if (StringUtils.isEmpty(cardCurrency)) {
            showMeessage2ConfirmProfile();
//            Tool.showMessageDialog("Please select currency.", this);
            return;
        }
//
//
//        String street = getInput(editTextStreet);
//
        if (StringUtils.isEmpty(street)) {
            showMeessage2ConfirmProfile();
//            Tool.showMessageDialog("Street is empty.", this);
            return;
        }
//
//
//        String cardCity = getInput(editTextCity);
//
        if (StringUtils.isEmpty(cardCity)) {
            showMeessage2ConfirmProfile();
//            Tool.showMessageDialog("Card city is empty", this);
            return;
        }
//
//        String cardState = getInput(editTextState);
        if (StringUtils.isEmpty(cardState)) {
            showMeessage2ConfirmProfile();
//            Tool.showMessageDialog("Card state is empty", this);
            return;
        }
//
//        String cardPostCode = getInput(editTextPostCode);
        if (StringUtils.isEmpty(cardPostCode)) {
            showMeessage2ConfirmProfile();
//            Tool.showMessageDialog("Postcode is empty", this);
            return;
        }
//
//
//        String cardCountry = getInput(textViewCountry);
//        String cardCurrency = getInput(textViewCurrency);


        try {


            final JSONObject creditCardjson = new JSONObject();


            creditCardjson.put("cardNumber", cardNumber);
            creditCardjson.put("cardExp", cardExp);
            creditCardjson.put("cvv", cvv);


            creditCardjson.put("cardFirstname", cardFirstName);
            creditCardjson.put("cardLastname", cardlastName);
            creditCardjson.put("cardEmail", cardEmail);

            creditCardjson.put("cardCountry", cardCountry);
            creditCardjson.put("cardCurrency", cardCurrency);
            creditCardjson.put("cardCity", cardCity);
            creditCardjson.put("cardState", cardState);
            creditCardjson.put("cardPostcode", cardPostCode);
            creditCardjson.put("cardAddress", street);
            creditCardjson.put("cardPhone", mobilNumber);


            LogUtil.i(App.tag, "json:" + creditCardjson.toString());

            String msg = "To create an ID for your credit card, we will transfer $1.00 from your credit card to TIPPPR bank account. This will be refund to you in 3 working days.";

            DialogUtils.showConfirmDialog(CreditCardActivity.this, "Finalise Credit Card Setup", msg, "Confirm", "Cancel", new DialogCallBackListener() {
                @Override
                public void onDone(boolean yesOrNo) {

                    if (yesOrNo) {
                        saveCardNumber(creditCardjson);

                    }
                }
            });

        } catch (Exception e) {
            LogUtil.i(App.tag, "error msg in bind card:" + e.getLocalizedMessage());
        }


    }


    private void saveCardNumber(final JSONObject jsonObjectCard) {

        reSetTask();
        baseTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {
                    netResult = NetInterface.getBillpro(CreditCardActivity.this, jsonObjectCard);

                } catch (Exception e) {

                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null != result) {
                    if (StringUtils.isEmpty(result.getMessage())) {

                        Tool.showMessageDialog("Thanks!", "Your credit card details have been recorded successfully.", CreditCardActivity.this, new DialogCallBackListener() {
                            @Override
                            public void onDone(boolean yesOrNo) {
                                if (isfromRegist) {
                                    getProfile(CreditCardActivity.this, new OnRequestEndCallBack() {
                                        @Override
                                        public void onRequestEnd(NetResult netResult) {

                                            if (null != netResult) {
                                                User user = (User) netResult.getData()[0];
                                                App.getApp().setUser(user);
                                                gotoVerifyActivity();
                                            } else {
                                                Tool.showMessageDialog("Net error!", CreditCardActivity.this);
                                            }
                                        }
                                    });

                                } else {
                                    setResult(RESULT_OK);
                                    finish();
                                }
                            }
                        });


                    } else {
                        Tool.showMessageDialog(result.getMessage(), CreditCardActivity.this);
                    }
                } else {

                    showNotifyTextIn5Seconds(R.string.error_net);
                }


            }
        });

        baseTask.execute(new HashMap<String, String>());

    }


    private void showMeessage2ConfirmProfile() {

        Tool.showMessageDialog("Please complete your profile.", this, new DialogCallBackListener() {

            @Override
            public void onDone(boolean yesOrNo) {
                if (yesOrNo) {
                    App.getApp().putTemPObject("fromCard", true);
                    Tool.startActivityForResult(CreditCardActivity.this, ProfileFillActivity.class, SHORT_GO_PROFILE);
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SHORT_GO_PROFILE && resultCode == RESULT_OK) {

            initView();
        }
    }
}
