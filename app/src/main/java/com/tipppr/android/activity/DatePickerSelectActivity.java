package com.tipppr.android.activity;

import android.os.Bundle;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.MySimpleTimePickerView;
import com.bigkoo.pickerview.TimePickerView;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class DatePickerSelectActivity extends BaseTippprActivity {


    @InjectView(R.id.linearLayoutDateStart)
    LinearLayout linearLayoutDateStart;

    @InjectView(R.id.linearLayoutDateEnd)
    LinearLayout linearLayoutDateEnd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_picker_select);
        ButterKnife.inject(this);


        MySimpleTimePickerView timePickerViewStart = new MySimpleTimePickerView(this, TimePickerView.Type.YEAR_MONTH_DAY);

        LinearLayout.LayoutParams layoutParamsStart = new LinearLayout.LayoutParams(-1, -1);
        linearLayoutDateStart.addView(timePickerViewStart.getRootView(), layoutParamsStart);


        MySimpleTimePickerView timePickerViewEnd = new MySimpleTimePickerView(this, TimePickerView.Type.YEAR_MONTH_DAY);

        LinearLayout.LayoutParams layoutParamsEnd = new LinearLayout.LayoutParams(-1, -1);

        linearLayoutDateEnd.addView(timePickerViewEnd.getRootView(), layoutParamsEnd);


    }

    @OnClick(R.id.viewBack)
    public void onBackClick() {
        finish();
    }


}
