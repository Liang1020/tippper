package com.tipppr.android.activity;

import android.app.Activity;
import android.location.Address;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.bean.SimpleAddress;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.adapter.AddressAdapter;
import com.tipppr.tipppr.adapter.AddressSimpleAdapter;
import com.tipppr.tipppr.json.JSONHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class AddressListActivity extends BaseTippprActivity {


    @InjectView(R.id.listViewAddress)
    ListView listViewAddress;

    @InjectView(R.id.editTextInput)
    EditText editTextInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);

        ButterKnife.inject(this);
    }

    @OnTextChanged(R.id.editTextInput)
    public void onInputChanged() {

        String input = getInput(editTextInput);
        if (!StringUtils.isEmpty(input)) {
            getAddressTask(input);
//            getAutoCompleteList(input);
        } else {
            listViewAddress.setAdapter(null);
        }
    }

    @OnClick(R.id.viewBack)
    public void onBackClick() {
        finish();
    }



    private void getAddressTask(final String addres) {

        reSetTask();

        baseTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

//                baseTask.showDialogForSelf(true);

            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = new NetResult();
                    List<Address> add = Tool.getLocationFromAddress(baseTask.weakReferenceContext.get(), addres, 20);
                    netResult.setTag(add);

                } catch (Exception e) {
                    LogUtil.i(App.tag, "find address none");
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {

//                baseTask.hideDialogSelf();

                if (null != result) {
                    if (!ListUtiles.isEmpty((ArrayList<Address>) result.getTag())) {

                        ArrayList<Address> addresList = (ArrayList<Address>) result.getTag();

                        if (ListUtiles.isEmpty(addresList)) {
                            showToast("Location not found!", 3);
                        }


                        AddressAdapter addressAdapter = new AddressAdapter(baseTask.weakReferenceContext.get(), addresList);
                        listViewAddress.setAdapter(addressAdapter);
                        listViewAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                Address address = (Address) parent.getAdapter().getItem(position);

                                LogUtil.i(App.tag, "address:" + address.toString());

                                App.getApp().putTemPObject("address", parent.getAdapter().getItem(position));
                                setResult(Activity.RESULT_OK);
                                finish();

                            }
                        });

                    } else {
//                        Tool.showMessageDialog("There is no result found.\nPlease check the input.", (Activity) baseTask.weakReferenceContext.get());
                    }

                } else {
                    Tool.showMessageDialog("Net error or GMS not install!", (Activity) baseTask.weakReferenceContext.get());
                }

            }
        }, AddressListActivity.this);

        baseTask.execute(new HashMap<String, String>());
    }


    public ArrayList<SimpleAddress> getSipAdrFromGS(String jsonStr) throws JSONException {

        JSONObject jsonObject = new JSONObject(jsonStr);
        LogUtil.i(App.tag, "result:" + jsonStr);
        ArrayList<SimpleAddress> addressList = new ArrayList<>();

        if ("OK".equals(JSONHelper.getStringFroJso("status", jsonObject))) {

            JSONArray listArray = jsonObject.getJSONArray("predictions");

            if (null != listArray && listArray.length() > 0) {

                for (int i = 0, isize = listArray.length(); i < isize; i++) {

                    JSONObject objAdr = listArray.getJSONObject(i);

                    String desciption = JSONHelper.getStringFroJso("description", objAdr);
                    String placeId = JSONHelper.getStringFroJso("place_id", objAdr);

                    SimpleAddress simpleAddress = new SimpleAddress();
                    simpleAddress.setSimpleDesccrption(desciption);
                    simpleAddress.setSimplePlaceId(placeId);

                    addressList.add(simpleAddress);
                }

            }
        } else {
            LogUtil.e(App.tag, "google api request fail");
        }


        return addressList;
    }

    public final String gkey = "AIzaSyCMsE3GjIzhwjI38Rqcc9hnJDitFQKy0CU";

    private void getAutoCompleteList(final String input) {
        reSetTask();
        baseTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
//                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {

                    //https://maps.googleapis.com/maps/api/place/autocomplete/json?language=zh-CN&input=成&types=geocode&key=
                    String host = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + URLEncoder.encode(input, "utf-8") + "&types=geocode&key=" + gkey;

                    URL url = new URL(host);

                    InputStream ins = url.openStream();

                    String json = JSONHelper.getJsonFromInputStream(ins);

                    ArrayList<SimpleAddress> simpleList = getSipAdrFromGS(json);

                    netResult = new NetResult();

                    netResult.setData(new Object[]{simpleList});


                } catch (Exception e) {
                    e.printStackTrace();
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
//                baseTask.hideDialogSelf();
                if (null != result) {
                    AddressSimpleAdapter asadapter = new AddressSimpleAdapter(AddressListActivity.this, (ArrayList<SimpleAddress>) result.getData()[0]);
                    listViewAddress.setAdapter(asadapter);
                    listViewAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                            Address address = (Address) parent.getAdapter().getItem(position);
//
//                            LogUtil.i(App.tag, "address:" + address.toString());
//
//                            App.getApp().putTemPObject("address", parent.getAdapter().getItem(position));
//                            setResult(Activity.RESULT_OK);
//                            finish();

                        }
                    });

                }


            }
        });

        baseTask.execute(new HashMap<String, String>());
    }

}
