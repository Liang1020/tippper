package com.tipppr.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.common.util.LogUtil;
import com.pushwoosh.internal.PushManagerImpl;
import com.tipppr.android.activity.HomeScreenActivity;
import com.tipppr.android.activity.SplashActivity;

import org.json.JSONObject;

public class NotificationReceiver extends BroadcastReceiver {


    public void onReceive(Context context, Intent intent) {


        LogUtil.e(App.tag, "NotificationReceiver when open notify on status bar call this");


        Bundle pushBundle = PushManagerImpl.preHandlePush(context, intent);
        if (pushBundle == null)
            return;




        try {

            JSONObject dataObject = PushManagerImpl.bundleToJSON(pushBundle);
            LogUtil.i(App.tag, dataObject.toString());

            Intent i = null;

            if (App.getApp().isMainActivityCreate()) {
                i = new Intent();
                if (App.getApp().isMainActivityVisible()) {
//                    i = new Intent();

                    i = new Intent(context, HomeScreenActivity.class);
                    i.putExtra("msg", dataObject.getString("title"));
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);

                } else {
                    i = new Intent(context, HomeScreenActivity.class);
                    i.putExtra("msg", dataObject.getString("title"));
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                }
            } else {
                i = new Intent(context, SplashActivity.class);
                i.putExtra("msg", dataObject.getString("title"));
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }


        } catch (Exception e) {
            LogUtil.e(App.tag, "fuck errr on click notify");
        }


//        //Get default launcher intent for clarity
//        Intent launchIntent  = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
//        launchIntent.addCategory("android.intent.category.LAUNCHER");
//
//        launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
//
//        //Put push notifications payload in Intent
//        launchIntent.putExtras(pushBundle);
//        launchIntent.putExtra(PushManager.PUSH_RECEIVE_EVENT, dataObject.toString());
//
//        //Start activity!
//        context.startActivity(launchIntent);
//
//        //Let Pushwoosh SDK post-handle push (track stats, etc.)
//        PushManagerImpl.postHandlePush(context, intent);
    }
}