package com.tipppr.android;

import android.os.Environment;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.common.util.FontUtils;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.facebook.FacebookSdk;
import com.google.android.gms.maps.model.LatLng;
import com.paypal.android.MEP.PayPal;
import com.push.NotificationFactorySample;
import com.pushwoosh.PushManager;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.squareup.picasso.Picasso;
import com.tipppr.android.activity.ProfileUserActivity;
import com.tipppr.tipppr.BaseTippprActivity;
import com.tipppr.tipppr.bean.User;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/** integrate buddybuild
 * Created by wangzy on 16/1/20.
 */
public class App extends MultiDexApplication implements PushManager.GetTagsListener{


    private static App app;

    private String token;

//  private User user;


    public static final boolean isFree = false;
    public static File CACHE_DIR_IMAGE;
    public static int RESULT_CODE_CAMERA = 1;
    public static String CACHE_DIR;
    public static String CACHE_DIR_PRODUCTS;
    public static String CACHE_DIR_EXCEL;
    public static String CACHE_DIR_PDF;

    public static final String DirFileName = "cretve_cache";
    public static final String KEY_REMEBERPWD = "asdf";
    public static final String KEY_USER = "xxvaaf";
    public static final String KE_PWD = "adfaafadf";
    public static final String KEY_CREDIT="asdwerwerweraw";

    public LocationClient mLocationClient;
    public MyLocationListener mMyLocationListener;
    private WeakReference<BaseTippprActivity> weakReferenceActivity;
    private WeakReference<ProfileUserActivity> weakReferenceProfileUserActivity;

    private LatLng currentLatlng;

    private HashMap<String, Object> tempParamap;
    public static String tag = "tippper";
    public static String keyUser = "keyUser";


    private boolean isMainActivityCreate = false;
    private boolean isMainActivityVisible = false;

    public final static String requestSuffix="sent you a request.";
    public final static String reqeustTipSuffix="sent you a tip";
    public final static String requestAcceptSuffix="has accepted your request.";


    private PayPal pp;

//    public  Typeface typefaceBold;
//    public  Typeface typefaceBook;
//    public  Typeface typefaceMedium;

    private ProfileUserActivity profileUserActivity;

    private String split;
    private String splitR;


    @Override
    public void onCreate() {

//        MultiDex.install(this);

        super.onCreate();
        this.app = this;

        //===============================================================

        //add this to AppTheme <item name="android:typeface">monospace</item> to change app text style
        FontUtils.getInstance().replaceSystemDefaultFontFromAsset(this, "fonts/gotham_book.ttf");

//        CrashReport.initCrashReport(getApplicationContext(), "900018879", true);

        Picasso.with(this);
        this.tempParamap = new HashMap<String, Object>();

//      initLeakCannary();

        initPayPal();

        locationInit();

        FacebookSdk.sdkInitialize(this);

        LogUtil.i(App.tag, "hash:" + Tool.getSigedHash(this));


        final PushManager pushManager = PushManager.getInstance(this);

        //Start push manager, this will count app open for Pushwoosh stats as well
        try {
            pushManager.onStartup(this);
        } catch (Exception e) {
            Log.e("Pushwoosh", e.getLocalizedMessage());
        }

        //Register for push!
        pushManager.registerForPushNotifications();
        pushManager.setNotificationFactory(new NotificationFactorySample());

        String pushToken = PushManager.getPushToken(this);
        LogUtil.i(App.tag,"pushToken:"+pushToken);



    }

    public String getSplit() {

        if(StringUtils.isEmpty(split)){
            split=getResources().getString(R.string.split);
        }

        return split;
    }


    public String getSplitR() {
        if(StringUtils.isEmpty(splitR)){
            splitR=getResources().getString(R.string.split_r);
        }
        return splitR;
    }


    private void initPayPal() {


        String PAYPAL_SANDBOX_APP_ID = "APP-80W284485P519543T";//APP-80W284485P519543T

        PayPal.initWithAppID(this, PAYPAL_SANDBOX_APP_ID, PayPal.ENV_SANDBOX);


    }

    private void locationInit() {

        mLocationClient = new LocationClient(this);
        mMyLocationListener = new MyLocationListener();
        mLocationClient.registerLocationListener(mMyLocationListener);
    }


    public static String returnCacheDir() {
        firstRun();
        return CACHE_DIR;
    }

    public static void firstRun() {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            CACHE_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + DirFileName;
        } else {
            CACHE_DIR = Environment.getRootDirectory().getAbsolutePath() + "/" + DirFileName;
        }
        CACHE_DIR_PRODUCTS = CACHE_DIR + "/products";
        File cacheDir = new File(App.CACHE_DIR);
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        File cacheDirProducts = new File(App.CACHE_DIR_PRODUCTS);
        if (!cacheDirProducts.exists()) {
            cacheDirProducts.mkdirs();
        }
    }


    public static App getApp() {
        return app;
    }


    public WeakReference<BaseTippprActivity> getWeakReferenceActivity() {
        return weakReferenceActivity;
    }

    public void setWeakReferenceActivity(WeakReference<BaseTippprActivity> weakReferenceActivity) {
        this.weakReferenceActivity = weakReferenceActivity;
    }


    //====initLeakCannary==
    private RefWatcher refWatcher;

    public static RefWatcher getRefWatcher() {
        App application = App.getApp();
        return application.refWatcher;
    }

    private void initLeakCannary() {
        refWatcher = LeakCanary.install(this);
    }

    public String getToken() {
        return SharePersistent.getPerference(this, "token");
//        return token;
    }

    public void setToken(String token) {
        this.token = token;
        SharePersistent.savePerference(this, "token", token);
    }


    public User getUser() {
        try {
            return (User) SharePersistent.getObjectValue(this, keyUser);
        } catch (Exception e) {

            LogUtil.e(App.tag, "get user error:" + e.getLocalizedMessage());
        }
        return null;
//        return user;
    }

    public void setUser(User user) {
//        this.user = user;
        SharePersistent.setObjectValue(this, keyUser, user);
    }

    @Override
    public void onTagsReceived(Map<String, Object> map) {

        LogUtil.i(App.tag,"received tags:==============");

    }

    @Override
    public void onError(Exception e) {

    }


    /**
     * location call back
     */
    public class MyLocationListener implements BDLocationListener {

        @Override
        public void onReceiveLocation(BDLocation location) {
            //Receive Location
            StringBuffer sb = new StringBuffer(256);
            sb.append("time : ");
            sb.append(location.getTime());
            sb.append("\nerror code : ");
            sb.append(location.getLocType());
            sb.append("\nlatitude : ");
            sb.append(location.getLatitude());
            sb.append("\nlontitude : ");
            sb.append(location.getLongitude());
            sb.append("\nradius : ");
            sb.append(location.getRadius());
            if (location.getLocType() == BDLocation.TypeGpsLocation) {// GPS定位结果
                sb.append("\nspeed : ");
                sb.append(location.getSpeed());// 单位：公里每小时
                sb.append("\nsatellite : ");
                sb.append(location.getSatelliteNumber());
                sb.append("\nheight : ");
                sb.append(location.getAltitude());// 单位：米
                sb.append("\ndirection : ");
                sb.append(location.getDirection());
                sb.append("\naddr : ");
                sb.append(location.getAddrStr());
                sb.append("\ndescribe : ");
                sb.append("gps定位成功");


                onLocationReceive(location);

                if (null != weakReferenceActivity && null != weakReferenceActivity.get()) {
                    weakReferenceActivity.get().onLocationReceive(location, null);
                }


            } else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {// 网络定位结果
                sb.append("\naddr : ");
                sb.append(location.getAddrStr());
                //运营商信息
                sb.append("\noperationers : ");
                sb.append(location.getOperators());
                sb.append("\ndescribe : ");
                sb.append("网络定位成功");
                onLocationReceive(location);
                if (null != weakReferenceActivity && null != weakReferenceActivity.get()) {
                    weakReferenceActivity.get().onLocationReceive(location, null);
                }
            } else if (location.getLocType() == BDLocation.TypeOffLineLocation) {// 离线定位结果
                sb.append("\ndescribe : ");
                sb.append("离线定位成功，离线定位结果也是有效的");
                onLocationReceive(location);
                if (null != weakReferenceActivity && null != weakReferenceActivity.get()) {
                    weakReferenceActivity.get().onLocationReceive(location, null);
                }
            } else if (location.getLocType() == BDLocation.TypeServerError) {
                sb.append("\ndescribe : ");
                sb.append("服务端网络定位失败，可以反馈IMEI号和大体定位时间到loc-bugs@baidu.com，会有人追查原因");
            } else if (location.getLocType() == BDLocation.TypeNetWorkException) {
                sb.append("\ndescribe : ");
                sb.append("网络不同导致定位失败，请检查网络是否通畅");
            } else if (location.getLocType() == BDLocation.TypeCriteriaException) {
                sb.append("\ndescribe : ");
                sb.append("无法获取有效定位依据导致定位失败，一般是由于手机的原因，处于飞行模式下一般会造成这种结果，可以试着重启手机");
            }

//            LogUtil.i(App.tag, "result:" + sb.toString());
//            LogUtil.i(App.tag, sb.toString());
        }

    }


    public void onLocationReceive(BDLocation bdLocation) {
        if (null != bdLocation) {
            LogUtil.d(App.tag, "定位成功");
            LatLng latLng = new LatLng(bdLocation.getLatitude(), bdLocation.getLongitude());
            App.getApp().setCurrentLatlng(latLng);
            App.getApp().stopLocation();
        }
    }

    private LocationClientOption.LocationMode tempMode = LocationClientOption.LocationMode.Hight_Accuracy;
    private String tempcoor = "gcj02";//google tenxun,等全球通用标准


    public void initLocation() {
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(tempMode);//可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        option.setCoorType(tempcoor);//可选，默认gcj02，设置返回的定位结果坐标系，
        int span = 1000;
        option.setScanSpan(span);//可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        option.setIsNeedAddress(true);//可选，设置是否需要地址信息，默认不需要
        option.setOpenGps(true);//可选，默认false,设置是否使用gps
        option.setLocationNotify(true);//可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
        option.setIgnoreKillProcess(true);//可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
        mLocationClient.setLocOption(option);
    }

    public void startLocation() {
        initLocation();
        mLocationClient.start();//定位SDK start之后会默认发起一次定位请求，开发者无须判断isstart并主动调用request
        mLocationClient.requestLocation();
    }


    public void stopLocation() {
        if (null != mLocationClient) {
            mLocationClient.stop();
        }
    }

    public void setCurrentLatlng(LatLng currentLatlng) {
        this.currentLatlng = currentLatlng;
    }

    public LatLng getCurrentLatlng() {
        return currentLatlng;
    }


    public void putTemPObject(String key, Object object) {
        tempParamap.put(key, object);
    }

    public boolean hasTempKey(String key) {

        return tempParamap.containsKey(key);
    }

    public void clearTempKey(String key) {
        if (tempParamap.containsKey(key)) {
            tempParamap.remove(key);
        }

    }

    public Object getTempObject(String key) {

        return tempParamap.remove(key);
    }

    public PayPal getPp() {
        return pp;
    }

    public void setPp(PayPal pp) {
        this.pp = pp;
    }


    public boolean isMainActivityCreate() {
        return isMainActivityCreate;
    }

    public void setMainActivityCreate(boolean mainActivityCreate) {
        isMainActivityCreate = mainActivityCreate;
    }

    public boolean isMainActivityVisible() {
        return isMainActivityVisible;
    }

    public void setMainActivityVisible(boolean mainActivityVisible) {
        isMainActivityVisible = mainActivityVisible;
    }

    public ProfileUserActivity getProfileUserActivity() {
        return profileUserActivity;
    }

    public void setProfileUserActivity(ProfileUserActivity profileUserActivity) {
        this.profileUserActivity = profileUserActivity;
    }

    public WeakReference<ProfileUserActivity> getWeakReferenceProfileUserActivity() {
        return weakReferenceProfileUserActivity;
    }

    public void setWeakReferenceProfileUserActivity(WeakReference<ProfileUserActivity> weakReferenceProfileUserActivity) {
        this.weakReferenceProfileUserActivity = weakReferenceProfileUserActivity;
    }
}
