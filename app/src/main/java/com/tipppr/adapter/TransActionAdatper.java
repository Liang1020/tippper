package com.tipppr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.common.util.ListUtiles;
import com.tipppr.android.R;
import com.tipppr.bean.TransAction;
import com.tipppr.tipppr.bean.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 16/3/28.
 */
public class TransActionAdatper extends BaseAdapter {


    private ArrayList<TransAction> transActions;
    private Context context;
    private LayoutInflater layoutInflater;

    public TransActionAdatper(Context context, ArrayList<TransAction> transActions) {
        this.context = context;
        this.transActions = transActions;
        this.layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return transActions.size();
    }

    @Override
    public Object getItem(int position) {
        return transActions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    private SimpleDateFormat sdfparse = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat sdfformat = new SimpleDateFormat("MM-dd-yyyy");

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_trans_history, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        TransAction trans = transActions.get(position);

        viewHolder.textViewAmount.setText("$" + trans.getAmount());

        try {
            viewHolder.textViewTransDate.setText(sdfformat.format(sdfparse.parse(trans.getCreated_at())));
        } catch (Exception e) {

        }

//        String receiver = "";

        ArrayList<User> receivers = trans.getReceivers();

        if (ListUtiles.getListSize(receivers) == 1) {
            viewHolder.textViewTransReceiver.setText(receivers.get(0).getName());
        } else if (ListUtiles.getListSize(receivers) == 2) {
            viewHolder.textViewTransReceiver.setText(
                    receivers.get(0).getName() + "\n" +
                            receivers.get(1).getName()
            );
        }


        return convertView;
    }


    class ViewHolder {

        @InjectView(R.id.textViewTransDate)
        TextView textViewTransDate;

        @InjectView(R.id.textViewTransReceiver)
        TextView textViewTransReceiver;

        @InjectView(R.id.textViewAmount)
        TextView textViewAmount;

//        @InjectView(R.id.imageViewStatus)
//        ImageView imageViewStatus;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

    }
}
