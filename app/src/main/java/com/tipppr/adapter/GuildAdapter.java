package com.tipppr.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.indicator.view.IndicatorView;
import com.tipppr.android.R;

import java.util.ArrayList;

/**
 * Created by wangzy on 15/11/3.
 */
public class GuildAdapter extends PagerAdapter {


    private ArrayList<View> views;

    private Context context;

    private ViewPager viewPager;


    private IndicatorView indicatorView;


    private View guildView1;
    private View guildView2;
    private View guildView3;
    private View guildView4;
    private View guildView5;


    private Button buttonGo;

    public OnSkipListener onSkipListener;

    public GuildAdapter(final Context context, final ViewPager viewPager) {
        this.context = context;
        this.views = new ArrayList<View>();
        this.viewPager = viewPager;

        guildView1 = View.inflate(context, R.layout.page_guid_0, null);
        guildView2 = View.inflate(context, R.layout.page_guid_1, null);
        guildView3 = View.inflate(context, R.layout.page_guid_2, null);
        guildView4 = View.inflate(context, R.layout.page_guid_3, null);
        guildView5 = View.inflate(context, R.layout.page_guid_4, null);


        views.add(guildView1);
        views.add(guildView2);
        views.add(guildView3);
        views.add(guildView4);
        views.add(guildView5);


        for (View v : views) {


            v.findViewById(R.id.textViewSkype).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (null != onSkipListener) {
                        onSkipListener.onSkipClick();
                    }

                }
            });


        }
    }


    @Override
    public int getCount() {
        return views.size();
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(views.get(position));
        return views.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(views.get(position));
    }


    public static interface OnSkipListener {

        public void onSkipClick();
    }


    public OnSkipListener getOnSkipListener() {
        return onSkipListener;
    }

    public void setOnSkipListener(OnSkipListener onSkipListener) {
        this.onSkipListener = onSkipListener;
    }
}
