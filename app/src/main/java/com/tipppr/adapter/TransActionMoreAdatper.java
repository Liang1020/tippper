package com.tipppr.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.common.util.DateTool;
import com.common.util.LogUtil;
import com.common.util.NumberHelper;
import com.common.util.StringUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.bean.TransActionMore;
import com.tipppr.tipppr.bean.User;
import com.tipppr.tipppr.view.HexImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 16/3/28.
 */
public class TransActionMoreAdatper extends BaseAdapter {


    private ArrayList<TransActionMore> transActions;
    private Context context;
    private LayoutInflater layoutInflater;
    private Bitmap bitmapMaskHeader;
    private ArrayList<Target> targets;
    private User userCurrent;
    private SimpleDateFormat sdf;
    private SimpleDateFormat sdfuni;

    public TransActionMoreAdatper(Context context, ArrayList<TransActionMore> transActions) {
        this.context = context;
        this.transActions = transActions;
        this.layoutInflater = LayoutInflater.from(context);
        this.targets = new ArrayList<>();
        this.userCurrent = App.getApp().getUser();
        this.bitmapMaskHeader = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_mask_header);
        this.sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.sdfuni=new SimpleDateFormat("dd-MMM-yyyy HH:mm");
    }

    @Override
    public void notifyDataSetChanged() {


        Collections.sort(transActions, new Comparator<TransActionMore>() {
            @Override
            public int compare(TransActionMore lhs, TransActionMore rhs) {


                try {

                    Date l = sdf.parse(lhs.getCreateAt());
                    Date r = sdf.parse(rhs.getCreateAt());

                    return l.getTime() < r.getTime() ? 1 : -1;

                } catch (Exception e) {

                }

                return 0;
            }
        });


        super.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return transActions.size();
    }

    @Override
    public Object getItem(int position) {
        return transActions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    private SimpleDateFormat sdfparse = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat sdfformat = new SimpleDateFormat("MMM-dd-yyyy HH:mm");

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_trans_history_more, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        TransActionMore trans = transActions.get(position);


        if (trans.isSent(userCurrent)) {
            viewHolder.textViewAmount.setTextColor(Color.parseColor("#39D321"));
            viewHolder.textViewAmount.setText("-$" + NumberHelper.keepDecimal2(trans.getAmount()));
        } else {

            viewHolder.textViewAmount.setTextColor(Color.parseColor("#50BDE0"));

            if (null != trans.getReceiverUser1()) {
                if (userCurrent.getUserId().equals(trans.getReceiverUser1().getUserId())) {
                    viewHolder.textViewAmount.setText("$" + NumberHelper.keepDecimal2(trans.getReceiverUser1().getAmount2me()));
                }
            }

            if (null != trans.getReceiverUser2()) {
                if (userCurrent.getUserId().equals(trans.getReceiverUser2().getUserId())) {
                    viewHolder.textViewAmount.setText("$" + NumberHelper.keepDecimal2(trans.getReceiverUser2().getAmount2me()));
                }
            }
        }


//      String timeLabel = trans.getLocaledTime();//paypal

        String timeLabel = trans.getBirllTransDate();

        try {
            Date date = sdf.parse(timeLabel);

            String localDate = DateTool.getGenlinTime2Location(date);

            viewHolder.textViewTransDate.setText(localDate);

            Date nd=DateTool.formatter.parse(localDate);

            int ampm = Calendar.getInstance().get(Calendar.AM_PM);
//            viewHolder.textViewTransDate.setText(localDate);
            viewHolder.textViewTransDate.setText(this.sdfuni.format(nd)+""+(ampm == 1 ? "PM" : "AM"));

        } catch (Exception e) {
            viewHolder.textViewTransDate.setText(timeLabel);
            LogUtil.i(App.tag, "time format error:" + position + " " + timeLabel);
        }

//        viewHolder.textViewTransDate.setText(timeLabel);
        viewHolder.textViewStatus.setText(trans.getStatusText());


        String receiver = trans.getNameLabel();

        if (trans.isSent(userCurrent)) {
            viewHolder.textViewRecevers.setText(receiver);
        } else {
            viewHolder.textViewRecevers.setText(trans.getSenderName());
        }

        String headerUrl = "";
        if (trans.isSent(userCurrent)) {
            headerUrl = trans.getHeaderImageUrl();
        } else {
            headerUrl = trans.getSenderHeaderImageUrl();
        }
//        String headerUrl = trans.getHeaderImageUrl();

        LogUtil.i(App.tag,"header url:"+headerUrl);

        final HexImageView hexImageView = viewHolder.imageViewHeader;

        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                hexImageView.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
//                hexImageView.setImageResource(R.drawable.hary);
            }
        };
        targets.add(target);

        if (StringUtils.isEmpty(headerUrl)) {
            Picasso.with(context).load(R.drawable.hary).
                    placeholder(R.drawable.hary).
                    centerCrop().
                    resize(bitmapMaskHeader.getWidth(), bitmapMaskHeader.getHeight()).
                    into(target);
        } else {
            Picasso.with(context).load(headerUrl).
                    placeholder(R.drawable.hary).
                    centerCrop().
                    resize(bitmapMaskHeader.getWidth(), bitmapMaskHeader.getHeight()).
                    into(target);
        }

        return convertView;
    }

    public void desctory() {
        targets.clear();
    }


    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (null != bitmapMaskHeader && !bitmapMaskHeader.isRecycled()) {
            bitmapMaskHeader.recycle();
        }
        desctory();
    }

    class ViewHolder {

        @InjectView(R.id.textViewTimelabel)
        TextView textViewTransDate;

        @InjectView(R.id.imageViewHeader)
        HexImageView imageViewHeader;

        @InjectView(R.id.textViewRecevers)
        TextView textViewRecevers;

        @InjectView(R.id.textViewStatus)
        TextView textViewStatus;

        @InjectView(R.id.textViewAmount)
        TextView textViewAmount;

//        @InjectView(R.id.imageViewStatus)
//        ImageView imageViewStatus;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

    }
}
