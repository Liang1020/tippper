package com.tipppr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tipppr.android.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 16/5/24.
 */
public class SpninnerAdapter extends BaseAdapter {


    private String[] values;
    private String[] labels;
    private Context context;
    private LayoutInflater layoutInflater;


    public SpninnerAdapter(Context context, String[] values, String[] labels) {
        this.context = context;
        this.values = values;
        this.labels = labels;
        this.layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {

        return labels.length;
    }

    @Override
    public Object getItem(int position) {
        return values[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_spinner, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textViewOptions.setText(labels[position]);
        viewHolder.textViewOptions.setTag(values[position]);

        return convertView;
    }

    class ViewHolder {


        @InjectView(R.id.textViewSlection)
        TextView textViewOptions;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

    }
}
