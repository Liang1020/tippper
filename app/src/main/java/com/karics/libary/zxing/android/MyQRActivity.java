package com.karics.libary.zxing.android;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import com.common.util.LogUtil;
import com.common.util.Tool;
import com.karics.libary.zxing.encode.CodeCreator;
import com.tipppr.android.App;
import com.tipppr.android.R;
import com.tipppr.tipppr.BaseTippprActivity;

import java.io.File;
import java.io.FileOutputStream;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MyQRActivity extends BaseTippprActivity {


    @InjectView(R.id.imageViewQR)
    public ImageView imageViewQR;

    public Bitmap bitmap = null;

    public String savedir;

    public short SHORT_REQUEST_SD = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_qr);
        ButterKnife.inject(this);
        try {
            String uid = (String) App.getApp().getTempObject("uid");
            LogUtil.i(App.tag, "create qr img:" + uid);
            Point p = Tool.getDisplayMetrics(this);

            bitmap = CodeCreator.createQRCode(uid, (int) (Math.min(p.x, p.y)));
            imageViewQR.setImageBitmap(bitmap);


        } catch (Exception e) {
            LogUtil.e(App.tag, "exception:" + e.getLocalizedMessage());
            e.printStackTrace();
        } finally {

        }
    }


    public String saveDirQrImg() {
        try {
            savedir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator + System.currentTimeMillis() + ".png";

            File dirFile = new File(savedir);

            FileOutputStream fout = new FileOutputStream(dirFile);

            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fout);

            fout.flush();
            fout.close();

            return savedir;
        } catch (Exception e) {
            LogUtil.i(App.tag, "exceptino:" + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return "";

    }


    @OnClick(R.id.textViewSendQrImg)
    public void onQrSendClick() {

//        Intent intent = new Intent(Intent.ACTION_SEND);
//        String[] tos = { "way.ping.li@gmail.com" };
//        String[] ccs = { "way.ping.li@gmail.com" };
//        intent.putExtra(Intent.EXTRA_EMAIL, tos);
//        intent.putExtra(Intent.EXTRA_CC, ccs);
//        intent.putExtra(Intent.EXTRA_TEXT, "This is my Tipppr qr card");
//        intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
//
//        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(savedir));
//        intent.setType("image/*");
//        intent.setType("message/rfc882");
//        Intent.createChooser(intent, "Choose Email Client");
//        startActivity(intent);


//        Intent email = new Intent(
//                android.content.Intent.ACTION_SEND);
//        File file = new File(savedir);
//        if (file != null && file.exists()) {//flashImagepathString 为图片的本地路径
//            Uri outputFileUri = Uri.fromFile(file);
//            email.putExtra(Intent.EXTRA_STREAM, outputFileUri);
//        }
//        email.setType("image/png");
////        email.putExtra(Intent.EXTRA_SUBJECT, datas[2] + "-来自AA");//邮件主题
//        startActivity(email);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                saveDirQrImg();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,}, SHORT_REQUEST_SD);
                return;
            }
        }else{
            saveDirQrImg();
        }


        //================发送邮件 qr为附件方式=====

        Intent email = new Intent(android.content.Intent.ACTION_SEND);

        File file = new File(savedir);
        //邮件发送类型：带附件的邮件
        email.setType("application/octet-stream");
        //邮件接收者（数组，可以是多位接收者）
//        String[] emailReciver = new String[]{"123@qq.com", "456@163.com"};

        String[] emailReciver = new String[]{};

        String emailTitle = "";
        String emailContent = "";
        //设置邮件地址
        email.putExtra(android.content.Intent.EXTRA_EMAIL, emailReciver);
        //设置邮件标题
        email.putExtra(android.content.Intent.EXTRA_SUBJECT, emailTitle);
        //设置发送的内容
        email.putExtra(android.content.Intent.EXTRA_TEXT, emailContent);


        //附件
        email.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));

        //调用系统的邮件系统
        startActivity(Intent.createChooser(email, "Please choose app to send email"));


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == SHORT_REQUEST_SD && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
            onQrSendClick();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (null != bitmap && !bitmap.isRecycled()) {
            bitmap.recycle();
        }
    }

    @OnClick(R.id.viewBack)
    public void onClickBack() {
        finish();
    }

}
