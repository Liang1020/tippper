package com.karics.libary.zxing.view;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

/**
 * Created by wangzy on 16/3/10.
 */
public class PayPalWebViewImpl extends WebView {
    public PayPalWebViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
