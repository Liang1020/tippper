package com.push;

import com.common.util.LogUtil;
import com.tipppr.android.App;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by wangzy on 16/4/18.
 */
public class PushTool {


    public static boolean sendPushWithPushwoosh(ArrayList<String> listIds, String content, JSONObject customJson) {

        final String PUSHWOOSH_SERVICE_BASE_URL = "https://cp.pushwoosh.com/json/1.3/";
        final String AUTH_TOKEN = "s5sxS05l8w0lj3aO0n9lHhSpUZWAooBVZ21d26YafHFBJIVMgIjwt7GsFhHcOS4QD5NFzDKkoEfnZ6vP2Bwy";
        final String APPLICATION_CODE = "20E4A-239D3";

        try {
            String method = "createMessage";
            URL url = new URL(PUSHWOOSH_SERVICE_BASE_URL + method);

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("application", APPLICATION_CODE);
            jsonObject.put("auth", AUTH_TOKEN);

            JSONArray jsonArray = new JSONArray();

            JSONObject jno1 = new JSONObject();
            jno1.put("send_date", "now");
            jno1.put("content", content);

            JSONArray idsArray = new JSONArray();

            for (String id : listIds) {
                idsArray.put(id);
            }

            JSONArray conditionArray = new JSONArray().put("userid").put("IN");
            conditionArray.put(idsArray);
            JSONArray conditions = new JSONArray().put(conditionArray);

            jno1.put("conditions", conditions);

            if (null != customJson) {
                jno1.put("data", new JSONObject().put("custom", customJson));

            }


            jsonArray.put(jno1);
            jsonObject.put("notifications", jsonArray);

            JSONObject rootObject = new JSONObject();

            rootObject.put("request", jsonObject);

            System.out.println(rootObject.toString());

            JSONObject response = SendServerRequest.sendJSONRequest(url, rootObject.toString());

            LogUtil.i(App.tag, "Response is: " + response);
            LogUtil.i(App.tag, "send push success::" + response.toString());

            return true;
        } catch (Exception e) {
            LogUtil.e(App.tag, " send push fail:" + e.getLocalizedMessage());
        }

        return false;


    }


    @Deprecated
    public static String sendPushWithUrban(String userid, String jsonStr) {

        HttpURLConnection conn = null;

        try {

            String hoString = "https://go.urbanairship.com/api/push";
            String username = "m7JEg02iRja60TD7jQjgQw";
            String password = "PSO9mMXTRYO0T9B7adoPOA";

            // String input = username + ":" + password;
            // String encoding = new sun.misc.BASE64Encoder().encode(input.getBytes());
            // String auth = "Basic " + encoding;


            URL url = new URL(hoString);

            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setRequestProperty("Charset", "utf-8");
            conn.setRequestProperty("Accept", "application/vnd.urbanairship+json; version=3");
            conn.setRequestProperty("AppKey", "PSO9mMXTRYO0T9B7adoPOA");
            conn.setRequestProperty("Authorization", "Basic bTdKRWcwMmlSamE2MFREN2pRamdRdzpQU085bU1YVFJZTzBUOUI3YWRvUE9B");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.connect();

            //=====================
            JSONObject json = new JSONObject();

            // =====================
            // json.put("audience", "all");
            JSONObject audien = new JSONObject();
            audien.put("alias", userid);
            json.put("audience", audien);
            // =====================

            JSONObject notifcationObj = new JSONObject();
            notifcationObj.put("alert", jsonStr);
            json.put("notification", notifcationObj);

//        JSONArray devicesTypes = new JSONArray();
//        devicesTypes.add("android");

            json.put("device_types", "all");


            //============================
            OutputStream os = conn.getOutputStream();
            os.write(json.toString().getBytes());

            InputStream inputStream = conn.getInputStream();


            return getResultInputStream(inputStream);
        } catch (Exception e) {
            return getResultInputStream(conn.getErrorStream());
        }

    }


    public static String getResultInputStream(InputStream inputStream) {

        try {

            BufferedInputStream bios = new BufferedInputStream(inputStream);

            byte[] buffer = new byte[512];
            int ret = -1;
            ByteArrayOutputStream byo = new ByteArrayOutputStream();
            while ((ret = bios.read(buffer)) != -1) {
                byo.write(buffer, 0, ret);
            }
            bios.close();
            inputStream.close();

            String result = new String(byo.toByteArray(), "utf-8");

            return result;
        } catch (Exception e) {
            LogUtil.e(App.tag, "error for parse:" + e.getLocalizedMessage());
        }

        return "";
    }


}


class SendServerRequest {
    static JSONObject sendJSONRequest(URL url, String request) {
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
            writer.write(request.getBytes("UTF-8"));
            writer.flush();
            writer.close();

            return parseResponse(connection);
        } catch (Exception e) {
            LogUtil.e(App.tag, "An error occurred: " + e.getMessage());
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    static JSONObject parseResponse(HttpURLConnection connection) throws IOException, JSONException {
        String line;
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuilder response = new StringBuilder();

        while ((line = reader.readLine()) != null) {
            response.append(line).append('\r');
        }
        reader.close();

        return new JSONObject(response.toString());
    }
}
