package com.dialog;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tipppr.android.R;

/**
 * Created by wangzy on 16/4/28.
 */
public class DialogShowCardInfo extends LinearLayout {


    private TextView textViewNumberShow;
    private View contentView;
    private OnCardInfoUIListener onCardInfoUIListener;
    private String cardNumber = "";

    public DialogShowCardInfo(Context context, AttributeSet attrs) {
        super(context, attrs);
        contentView = addSubView();
        textViewNumberShow = (TextView) contentView.findViewById(R.id.textViewNumberShow);


        contentView.findViewById(R.id.textViewConfirm).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hide();
                if (null != onCardInfoUIListener) {
                    onCardInfoUIListener.onConfirm(cardNumber);
                }

            }
        });

        contentView.findViewById(R.id.viewCloseDialogCardInfoShow).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hide();


                if (null != onCardInfoUIListener) {
                    onCardInfoUIListener.onCancel();
                }
            }
        });

    }


    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
        textViewNumberShow.setText(cardNumber);
    }


    private View addSubView() {

        int width = LayoutParams.MATCH_PARENT;
        int height = LayoutParams.MATCH_PARENT;

        LinearLayout.LayoutParams lpp = new LinearLayout.LayoutParams(width, height);

        View view = View.inflate(getContext(), R.layout.dialog_cardinfo_show, null);
        addView(view, lpp);


        view.findViewById(R.id.viewCloseDialogCardInfoShow).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hide();
            }
        });

        return view;
    }


    public OnCardInfoUIListener getOnCardInfoUIListener() {
        return onCardInfoUIListener;
    }

    public void setOnCardInfoUIListener(OnCardInfoUIListener onCardInfoUIListener) {
        this.onCardInfoUIListener = onCardInfoUIListener;
    }

    public void hide() {
        setVisibility(View.GONE);
    }

    public void show() {
        setVisibility(View.VISIBLE);
    }

    public static abstract class OnCardInfoUIListener {

        public void onCancel() {
        }

        ;

        public void onConfirm(String cardNumber) {

        }
    }

}
