package com.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.BaseActivity;
import com.tipppr.android.R;

/**
 * Created by wangzy on 16/4/27.
 */
public class DialogCreditCardFill extends MyBasePicker {


    private OnCardInfoConfirmListener onCardInfoConfirmListener;


    public DialogCreditCardFill(final Context context, boolean hasEditCard) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_fill_credit_card, contentContainer);


        final EditText editTextAccountNumber = (EditText) findViewById(R.id.editTextAccountNumber);

        final EditText editTextAccountName = (EditText) findViewById(R.id.edittextAccountName);

        final EditText editTextCCv = (EditText) findViewById(R.id.editTextCCV);


        findViewById(R.id.viewCloseDialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();

                if (null != onCardInfoConfirmListener) {
                    onCardInfoConfirmListener.onCloseClick();
                }

            }
        });


        findViewById(R.id.textViewConfirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onCardInfoConfirmListener) {
                    BaseActivity activity = (BaseActivity) context;
                    String accountNumber = activity.getInput(editTextAccountNumber);
                    String accountName = activity.getInput(editTextAccountName);
                    String ccv = activity.getInput(editTextCCv);
                    onCardInfoConfirmListener.onConfirmClick(accountNumber, accountName, ccv);
                }

            }
        });


    }


    public OnCardInfoConfirmListener getOnCardInfoConfirmListener() {
        return onCardInfoConfirmListener;
    }

    public void setOnCardInfoConfirmListener(OnCardInfoConfirmListener onCardInfoConfirmListener) {
        this.onCardInfoConfirmListener = onCardInfoConfirmListener;
    }

    public static abstract class OnCardInfoConfirmListener {


        public void onConfirmClick(String card, String name, String ccv) {
        }

        public void onCloseClick() {
        }

        public void onDeleteClick(){

        }

    }
}
