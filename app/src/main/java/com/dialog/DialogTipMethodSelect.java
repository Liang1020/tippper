package com.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.BaseActivity;
import com.common.util.SharePersistent;
import com.common.util.StringUtils;
import com.tipppr.android.App;
import com.tipppr.android.R;

/**
 * Created by wangzy on 16/4/27.
 */
public class DialogTipMethodSelect extends MyBasePicker {


    private OnMethodListener onMethodListener;
    private boolean hasAcount;

    public DialogTipMethodSelect(final Context context, boolean hasAcount) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_tip_method, contentContainer);
        this.hasAcount = hasAcount;


        final LinearLayout linearLayoutCardInfo = (LinearLayout) findViewById(R.id.linearLayoutCardInfo);
        final View textViewConfirmCardInfo = findViewById(R.id.textViewConfirmCardInfo);


        final EditText editTextAccountNumber = (EditText) findViewById(R.id.editTextAccountNumber);

        final EditText editTextAccountName = (EditText) findViewById(R.id.edittextAccountName);

        final EditText editTextCCv = (EditText) findViewById(R.id.editTextCCV);


        textViewConfirmCardInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                linearLayoutCardInfo.setVisibility(View.GONE);
                BaseActivity activity = (BaseActivity) context;

                String accountNunber = activity.getInput(editTextAccountNumber);
                String accountname = activity.getInput(editTextAccountName);
                String cvc = activity.getInput(editTextCCv);

                if (!StringUtils.isEmpty(accountNunber) && !StringUtils.isEmpty(accountname) && !StringUtils.isEmpty(cvc)) {
                    SharePersistent.setObjectValue(activity, App.KEY_CREDIT, accountNunber + "," + accountname + "," + cvc);
                }

                if (null != onMethodListener) {
                    onMethodListener.onCreditCardInfoConfirm(accountNunber, accountname, cvc);

                }
            }
        });


        linearLayoutCardInfo.findViewById(R.id.viewCloseDialogCardInfo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutCardInfo.setVisibility(View.GONE);
            }
        });


        findViewById(R.id.viewCloseDialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onMethodListener) {
                    onMethodListener.onCancelClick();
                }
            }
        });

        findViewById(R.id.textViewPayPal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onMethodListener) {
                    onMethodListener.onPayPalClick();
                }

            }
        });
        findViewById(R.id.textViewCreditcard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                linearLayoutCardInfo.setVisibility(View.VISIBLE);

//                if (null != onMethodListener) {
//                    onMethodListener.onCreditCard();
//                }

                if (null != onMethodListener) {
                    onMethodListener.onBindClick();
                }

                dismiss();

            }
        });

    }


    public OnMethodListener getOnMethodListener() {
        return onMethodListener;
    }

    public void setOnMethodListener(OnMethodListener onMethodListener) {
        this.onMethodListener = onMethodListener;
    }

    public static abstract class OnMethodListener {


        public void onCancelClick() {
        }


        public void onPayPalClick() {
        }

        public void onCreditCard() {

        }

        public void onBindClick() {

        }

        public void onCreditCardInfoConfirm(String number, String name, String cvc) {
        }


    }


}
