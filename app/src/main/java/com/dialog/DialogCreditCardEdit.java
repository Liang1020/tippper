package com.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.tipppr.android.R;

/**
 * Created by wangzy on 16/4/27.
 */
public class DialogCreditCardEdit extends MyBasePicker {


    private OnCreditDialogListener onCreditDialogListener;


    public DialogCreditCardEdit(Context context, boolean hasEditCard) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_credit_card, contentContainer);


        View viewDelete = findViewById(R.id.linearLayoutDeleteCard);

        viewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onCreditDialogListener) {
                    onCreditDialogListener.onDeleteClick();
                }
            }
        });

        View viewAddcard = findViewById(R.id.linearlayoutAddCard);

        viewAddcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onCreditDialogListener) {
                    onCreditDialogListener.onAddClick();
                }
            }
        });

        View linearLayoutCancel = findViewById(R.id.linearLayoutCancel);
        linearLayoutCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onCreditDialogListener) {
                    onCreditDialogListener.onCancelClick();
                }

            }
        });

        if (hasEditCard) {
            viewDelete.setVisibility(View.VISIBLE);
            viewAddcard.setVisibility(View.GONE);
        } else {
            viewDelete.setVisibility(View.GONE);
            viewAddcard.setVisibility(View.VISIBLE);
        }


    }

    public OnCreditDialogListener getOnCreditDialogListener() {
        return onCreditDialogListener;
    }

    public void setOnCreditDialogListener(OnCreditDialogListener onCreditDialogListener) {
        this.onCreditDialogListener = onCreditDialogListener;
    }

    public static abstract class OnCreditDialogListener {


        public void onCancelClick() {
        }


        public void onAddClick() {
        }

        public void onDeleteClick() {
        }

    }


}
