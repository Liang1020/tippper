package tipppr.theappteam.com;

import android.test.AndroidTestCase;

import com.common.util.LogUtil;
import com.common.util.Tool;
import com.tipppr.android.App;

import java.util.TimeZone;

/**
 * Created by wangzy on 16/3/2.
 */
public class Test extends AndroidTestCase {



    public void testUnittest(){
        LogUtil.i(App.tag,"this is unit test");

    }


    public void testTimeZone() {

        LogUtil.i(App.tag, "current time-zone:" + TimeZone.getDefault().getID());

        String[] timeIds = TimeZone.getAvailableIDs();
        LogUtil.i(App.tag, "total id:" + timeIds.length);
        for (String id : timeIds) {
            LogUtil.i(App.tag, id);
        }

    }

    public void testCase() {

        double cd1Lat = 30.552677;
        double cd1lng = 104.065493;

        double tlat = 30.572816;
        double tlng = 104.066801;

        String distance = "" + Tool.getDistance(cd1lng, cd1Lat, tlat, tlng);

        LogUtil.i(App.tag, "custome:" + distance);

    }

}
